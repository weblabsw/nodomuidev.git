## API Report File for "nodomui"

> Do not edit this file. It is a report generated by [API Extractor](https://api-extractor.com/).

```ts

import { EventMethod } from 'nodom3';
import { Model } from 'nodom3';
import { Module } from 'nodom3';
import { VirtualDom } from 'nodom3';

// @public
export interface ITipCfg {
    closable?: boolean;
    closeTime?: number;
    exclusive?: boolean;
    icon?: string;
    id?: number;
    position?: string;
    showIcon?: boolean;
    text: string;
    theme?: string;
    timeout?: number;
    width?: number;
}

// @public
export class NodomUI {
    static getText(name: string, p1?: any, p2?: any): string;
    static setLang(lang: string): void;
}

// @public
export function nuialert(cfg: any): void;

// @public
export function nuiconfirm(cfg: any): void;

// Warning: (ae-forgotten-export) The symbol "UILoading" needs to be exported by the entry point index.d.ts
//
// @public
export function nuiloading(): UILoading;

// @public
export function nuiprompt(cfg: any): void;

// @public
export function nuitip(cfg: ITipCfg): void;

// @public
export type RouterTabItem = {
    title: string;
    closable?: boolean;
    active?: boolean;
    path: string;
};

// @public
export type TabItem = {
    title: string;
    closable?: boolean;
    active?: boolean;
    tab: UTabItem;
};

// @public
export class UIAccordion extends Module {
    template(props?: object): string;
}

// @public
export class UIAccordionItem extends Module {
    __clickItem(model: Model): void;
    template(props?: object): string;
}

// @public
export class UIAnimation extends Module {
    __close(): void;
    __open(): void;
    data(): {
        __open: boolean;
        __show: boolean;
    };
    onMount(): void;
    onUpdate(): void;
    template(props?: object): string;
}

// @beta
export class UIBadge extends Module {
    template(props?: object): string;
}

// @public
export class UIButton extends Module {
    template(props?: object): string;
}

// @public
export class UIButtonGroup extends Module {
    modules: (typeof UIButton)[];
    template(): string;
}

// @public
export class UICarousel extends Module {
    __addItem(item: UICarouselItem): void;
    __animating: boolean;
    __animation: string;
    __autoplay: boolean;
    __currentIndex: number;
    __interval: number;
    __timer: unknown;
    data(): {
        __count: number;
        __indicators: any[];
    };
    onBeforeRender(): void;
    // (undocumented)
    onMount(): void;
    template(props?: object): string;
}

// @public
export class UICarouselItem extends Module {
    __setLoc(flag: any, loc1: any, loc2: any): void;
    onMount(): void;
    onUpdate(): void;
    template(): string;
}

// Warning: (ae-forgotten-export) The symbol "BaseInput" needs to be exported by the entry point index.d.ts
//
// @public
export class UICheckbox extends BaseInput {
    __clickCheck(model: any, dom: any, evObj: any, e: any): void;
    template(props: any): string;
}

// @alpha
export class UIColorPicker extends BaseInput {
    __valueUpdate(): void;
    data(): {
        color: Color;
        value: string;
    };
    onBeforeFirstRender(): void;
    template(props: any): string;
}

// @public
export class UIDate extends BaseInput {
    data(): {
        __open: boolean;
        __dates: any[];
    };
    modules: (typeof UIAnimation)[];
    template(props: any): string;
}

// @public
export class UIDialog extends Module {
    __checkClose(): void;
    __close(): void;
    __open(): void;
    data(): {
        __show: boolean;
        __open: boolean;
    };
    template(props?: object): string;
}

// @beta
export class UIDrawer extends Module {
    __checkClose(): void;
    __close(): void;
    __inner(): void;
    __open(): void;
    data(): {
        __show: boolean;
        __open: boolean;
    };
    template(props?: object): string;
}

// @public
export class UIFile extends BaseInput {
    data(): {
        __uploading: boolean;
        __uploadingText: string;
    };
    template(props: object): string;
}

// @public
export class UIForm extends Module {
    __addItem(item: UIFormItem): void;
    __clearErrorTips(): void;
    __labelWidth: string;
    __unitWidth: string;
    __verify(): boolean;
    template(props?: object): string;
}

// @public
export class UIFormItem extends Module {
    __addValidator(props: any): void;
    __clearErrorTip(): void;
    __verify(value?: unknown): boolean;
    onBeforeFirstRender(): void;
    template(props?: object): string;
}

// @public
export class UIGrid extends Module {
    __addColumn(col: UIGridCol): void;
    __addRow(rows: any): void;
    __clearColumns(): void;
    __getSelectedRows(): any;
    __removeRow(param: any): void;
    __setExpandDom(expand: UIGridExpand): void;
    __setSortField(field: string, type: number): void;
    onCompile(): void;
    template(props: any): string;
}

// @public
export class UIGridCol extends Module {
    bodyDom: VirtualDom;
    field: string;
    fixed: string;
    headDom: VirtualDom;
    onCompile(): void;
    sortable: boolean;
    template(props?: object): string;
    title: string;
    width: number;
}

// @public
export class UIGridExpand extends Module {
    // (undocumented)
    node: VirtualDom;
    template(): any;
}

// @public
export class UIInput extends BaseInput {
    template(props: any): string;
}

// @public
export class UILayout extends Module {
    // (undocumented)
    template(props?: object): string;
}

// @public
export class UILayoutBottom extends Module {
    template(): string;
}

// @public
export class UILayoutCenter extends Module {
    template(): string;
}

// @public
export class UILayoutLeft extends Module {
    template(props?: object): string;
}

// @public
export class UILayoutRight extends Module {
    template(props?: object): string;
}

// @public
export class UILayoutTop extends Module {
    // (undocumented)
    template(): string;
}

// @public
export class UIList extends BaseInput {
    __clickItem(model: any, dom: any, evObj: any, e: any): void;
    protected __initValue(): void;
    template(props: any): string;
}

// @public
export class UIListTransfer extends BaseInput {
    data(): {
        __selectedRows: any[];
    };
    onBeforeRender(): void;
    template(props: any): string;
}

// @public
export class UIMenu extends Module {
    onBeforeRender(): void;
    template(props?: object): string;
}

// @public
export class UIMessageBox extends Module {
    // Warning: (ae-forgotten-export) The symbol "UIMessageButton" needs to be exported by the entry point index.d.ts
    __buttons: UIMessageButton[];
    __checkClose(): void;
    __close(): void;
    data(): {
        __show: boolean;
        __open: boolean;
        answer: string;
    };
    template(): string;
}

// @public
export class UIMessageBoxManager {
    static alert(cfg: any): void;
    static confirm(cfg: any): void;
    static prompt(cfg: any): void;
}

// @public
export class UIPagination extends Module {
    data(): {
        pageNo: number;
        pageSize: number;
        total: number;
        pages: any[];
    };
    template(props?: object): string;
}

// @public
export class UIPanel extends Module {
    clickButton(model: any, dom: any): void;
    template(props?: object): string;
}

// @public
export class UIPanelBody extends Module {
    template(): string;
}

// @public
export class UIProgress extends Module {
    template(props?: object): string;
}

// @public
export class UIRadio extends Module {
    __click(): void;
    onBeforeFirstRender(): void;
    template(props: any): string;
    value: any;
}

// @public
export class UIRadioGroup extends BaseInput {
    __add(radio: any): void;
    protected __initValue(): void;
    __setCurrent(radio: any): void;
    template(props: any): string;
}

// @beta
export class UIRating extends BaseInput {
    data(): {
        __starsArray: any[];
    };
    template(props: object): string;
}

// @public
export class UIRelationMap extends BaseInput {
    __displayField: string;
    protected __initValue(): void;
    __valueField: string;
    onBeforeRender(): void;
    template(props?: object): string;
}

// @alpha
export class UIRichEditor extends Module {
    __handleContentUp(): void;
    // (undocumented)
    data(): {
        configArray: any;
    };
    template(props?: object): string;
}

// @public
export class UIRouterTab extends Module {
    __activeTab(data: string | number | RouterTabItem): void;
    __closeTab(model: RouterTabItem): void;
    data(): {
        tabs: any[];
    };
    onBeforeRender(): void;
    template(props: any): string;
}

// @public
export class UISelect extends BaseInput {
    protected __initValue(): void;
    data(): {
        __open: boolean;
        showText: string;
        __x: number;
        __y: number;
        __width: number;
        __height: number;
        __content: string;
    };
    onBeforeFirstRender(): void;
    template(props: any): string;
}

// @alpha
export class UISkeleton extends Module {
    template(props?: object): string;
}

// @alpha
export class UISkeletonItem extends Module {
    template(props?: object): string;
}

// @public
export class UISlider extends BaseInput {
    protected __initValue(): void;
    onBeforeRender(): void;
    onMount(): void;
    template(props?: object): string;
}

// @alpha
export type UIStepNode = {
    __status: number;
    __checked: string;
};

// @beta
export class UISteps extends Module {
    onBeforeRender(): void;
    template(props?: object): string;
}

// @public
export class UISwitch extends BaseInput {
    template(props: any): string;
}

// @public
export class UITab extends Module {
    __activeTab(data: TabItem | string | number): void;
    __addTab(cfg: TabItem): void;
    __closeTab(data: TabItem | string | number): void;
    data(): {
        tabs: any[];
    };
    template(props: any): string;
}

// @beta
export class UITimeLine extends BaseInput {
    data(): {
        __evalue: number;
        __flag: boolean;
    };
    template(props: any): string;
}

// @public
export class UITip extends Module {
    __close(model: any): void;
    __removeHide(): void;
    data(): {
        topData: any[];
        rightData: any[];
        bottomData: any[];
        leftData: any[];
    };
    template(props: any): string;
}

// @public
export class UITipManager {
    static addTip(cfg: ITipCfg): void;
    static removeTip(): void;
}

// @public
export class UITool {
    static cacPosition(event: MouseEvent, relPos: number, width: number, height: number): number[];
    static clearSpace(src: string): string;
    // @beta
    static getDisplacement(event: MouseEvent, el: HTMLElement, vertical: boolean): number;
    static getRealPos(el: HTMLElement): number[];
    static getRelPos(el: HTMLElement): number[];
    static toAttrString(obj: object): string;
}

// @public
export class UIToolbar extends Module {
    modules: (typeof UIButton)[];
    template(): string;
}

// @public
export class UITree extends BaseInput {
    protected __initValue(): void;
    template(props?: object): string;
}

// @public
export class UTabItem extends Module {
    __hide(): void;
    __show(): void;
    data(): {
        show: boolean;
    };
    onBeforeFirstRender(): void;
    template(props: any): string;
}


export * from "nodom3";

// Warnings were encountered during analysis:
//
// dist/plugins/colorpicker.d.ts:26:9 - (ae-forgotten-export) The symbol "Color" needs to be exported by the entry point index.d.ts

// (No @packageDocumentation comment for this package)

```
