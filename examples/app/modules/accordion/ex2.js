import { Module } from '/dist/nodomui.min.js'
export class accordionex2 extends Module {
    template(props) {
        return `
            <ui-accordion single>
                <ui-accordion-item title='第1层'>
                    由html element全面改成虚拟dom，即由json数据对象方式管理模块dom树，同时强化虚拟dom操作
                </ui-accordion-item>
                <ui-accordion-item title='第2层'>
                    增加了IoC模式，模块支持即插即用，支持模块单例和非单例模式，灵活掌控资源消耗；
                </ui-accordion-item>
                <ui-accordion-item title='第3层' opened>
                    改变插件接入方式，支持自定义element tag接入和对象化接入；
                </ui-accordion-item>
                <ui-accordion-item title='第4层'>
                    提供大量插件，便于快速搭建应用，插件持续增加。模型为数据模型，是数据的辅助对象，NoDom中，每个数据对象都对应一个model，模块基于model进行渲染，model的变化会导致模块的增量渲染，通常是多个model变化，进行一次渲染）
                </ui-accordion-item>
            </ui-accordion>
        `;
    }
}