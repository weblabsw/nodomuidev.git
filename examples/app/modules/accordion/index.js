import { Module } from '/dist/nodomui.min.js'
import { ExModule } from '../exmodule.js'
import { AttrGrid } from '../attrgrid.js'
import { EventGrid } from '../eventgrid.js'
import { MethodGrid } from '../methodgrid.js'
import { EXCONFIG } from './config.js'
import { accordionex1 } from './ex1.js'
import { accordionex2 } from './ex2.js'
import { accordionex3 } from './ex3.js'



export class MAccordion extends Module {
    modules = [ExModule, AttrGrid, EventGrid, MethodGrid, accordionex1, accordionex2, accordionex3];
    template(props) {
        return `
            <div class='--component'>
                <h1>According 折叠面板</h1>
                <div class='--component-desc'>
                    {{desc}}
                </div>
                <h2>例子</h2>
                <div x-repeat={{examples}} index='idx'>
                    <exmodule $desc={{desc}} $title={{(idx+1) + ' '  + title}} $imp={{imp}} $exname={{name}} expath='accordion'/>
                </div>
                <h2>属性</h2>
                <h3>UI{{name}}/ui-{{name.toLowerCase()}}</h3>
                <AttrGrid $rows={{accordionAttrs}} />
                <h3>UI{{name}}/ui-{{name.toLowerCase()}}</h3>
                <AttrGrid $rows={{accordionColAttrs}} />
            </div>
        `
    }
    data() {
        return EXCONFIG;
    }
}