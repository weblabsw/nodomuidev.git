import { Module } from '/dist/nodomui.min.js'
export class accordionex1 extends Module {
    template(props) {
        return `
            <ui-accordion>
                <ui-accordion-item title='第1层'>
                    <div>
                        由html element全面改成虚拟dom，即由json数据对象方式管理模块dom树，同时强化虚拟dom操作
                    </div>
                </ui-accordion-item>
                <ui-accordion-item title='第2层' opened>
                    <div>hello</div>
                    <div>
                    增加了IoC模式，模块支持即插即用，支持模块单例和非单例模式，灵活掌控资源消耗；
                    </div>
                </ui-accordion-item>
                <ui-accordion-item title='第3层'>
                    <div>
                        改变插件接入方式，支持自定义element tag接入和对象化接入；
                    </div>
                </ui-accordion-item>
            </ui-accordion>
        `;
    }
}