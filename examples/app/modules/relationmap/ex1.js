import{Module} from '/dist/nodomui.min.js'
export class relationmapex1 extends Module{
    template(props){
        return `
            <div>
                <button e-click='change'>change</button>
                <div>map value is:{{JSON.stringify(mapValue)}}</div>
                <ui-relationmap $data={{mapData}} field='mapValue' display-field='title' value-field='id' left-title='组' top-title='用户'/>
            </div>
        `
    }
    data(){
        return {
            mapValue:[
                //第一个值为rows的id，第二个值为cols的id
                [1,2],
                [1,12],
                [2,6],
                [3,8]
            ],
            mapData:{
                rows: [
                    { id: 1, title: "系统管理员" },
                    { id: 2, title: "店铺管理员" },
                    { id: 3, title: "用户管理员" },
                    { id: 4, title: "用户" }
                ],
                cols: [
                    { id: 1, title: "yang" },
                    { id: 2, title: "张三" },
                    { id: 12, title: "李四" },
                    { id: 6, title: "王武" },
                    { id: 8, title: "field" }
                ]
            }
        }			
    }
    
    change(){
        this.model['mapValue'].splice(1,2,[4,8]);
    }
}