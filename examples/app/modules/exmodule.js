import {Module} from '/dist/nodomui.min.js'
import { UICode } from './uicode.js'
export class ExModule extends Module{
    modules=[UICode];
    template(props){
        this.model['file'] = this.model.exname.substring(props.expath.length) + '.js';
        const mdlName = props.expath + this.model.exname;
        return `
            <div>
                <h3>{{title}}</h3>
                <div x-show={{desc}} class='--component-desc'>
                    {{desc}}
                </div>
                <div x-show={{imp}} class='--fore-warn-color'>
                    {{imp}}
                </div>
                <div class='--component-ex'>
                    <div class='--component-ex-show'>
                        <module name='${mdlName}'/>
                    </div>
                    <uicode url={{exname+'.js'}} exname='${props.expath}'/>
                </div>
            </div>
        `
    }
}