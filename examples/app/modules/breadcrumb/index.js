import { Module } from '/dist/nodomui.min.js'
import { ExModule } from '../exmodule.js'
import { AttrGrid } from '../attrgrid.js'
import { EventGrid } from '../eventgrid.js'
import { MethodGrid } from '../methodgrid.js'

import { EXCONFIG } from './config.js'
import { breadcrumbex1 } from './ex1.js'
import { breadcrumbex2 } from './ex2.js'
import { breadcrumbex3 } from './ex3.js'

export class MBreadcrumb extends Module {
    modules = [ExModule, AttrGrid, EventGrid, MethodGrid, breadcrumbex1, breadcrumbex2, breadcrumbex3];

    template() {
        return `
            <div class='--component'>
                <h1>Breadcrumb 面包屑</h1>
                <div class='--component-desc'>
                    {{desc}}
                </div>
                <h2>例子</h2>
                <div x-repeat={{examples}} index='idx'>
                    <exmodule $desc={{desc}} $title={{(idx+1) + ' '  + title}} $imp={{imp}} $exname={{name}} expath='breadcrumb'/>
                </div>
        
                <h2>属性</h2>
                <h3>UI{{name}}/ui-{{lowerCaseName}}</h3>
                <AttrGrid $rows={{breadcrumbAttrs}} />

                <h3>UI{{name}}Item/ui-{{lowerCaseName}}-item</h3>
                <AttrGrid $rows={{breadcrumbItemAttrs}} />
            </div>`
    }

    data() {
        return EXCONFIG;
    }
}