import { Module } from '/dist/nodomui.min.js'

export class breadcrumbex1 extends Module {
    template(props) {
        return `
                <div>
                    <div style="display: flex;flex-direction: column; height: 80px;justify-content: space-around;">
                        <ui-breadcrumb>
                            <ui-breadcrumb-item title="首页" to="/"/>
                            <ui-breadcrumb-item title="路径1" to="/path1"/>
                            <ui-breadcrumb-item title="路径2" to="/path2"/>
                            <ui-breadcrumb-item title="路径3"/>
                        </ui-breadcrumb>
                        <ui-breadcrumb separator=">">
                            <ui-breadcrumb-item title="首页"/>
                            <ui-breadcrumb-item title="路径1"/>
                            <ui-breadcrumb-item title="路径2"/>
                            <ui-breadcrumb-item title="路径3"/>
                        </ui-breadcrumb>
                    </div>
                </div>
            `;
    }
}