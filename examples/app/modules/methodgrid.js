import {Module} from '/dist/nodomui.min.js'

export class MethodGrid extends Module{
    template(){
        return `
            <ui-grid $data={{rows}} grid-line='rows'>
                <ui-grid-col title='方法名' width='180'>{{name}}</ui-grid-col>
                <ui-grid-col title='说明'>{{desc}}</ui-grid-col>
                <ui-grid-col title='参数'>{{params}}</ui-grid-col>
                <ui-grid-col title='返回值' width='100'>{{returns}}</ui-grid-col>
            </ui-grid>
        `
    }
}