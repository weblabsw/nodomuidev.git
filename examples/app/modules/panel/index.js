import { Module } from '/dist/nodomui.min.js'
import { ExModule } from '../exmodule.js'
import { AttrGrid } from '../attrgrid.js'
import { EventGrid } from '../eventgrid.js'
import { MethodGrid } from '../methodgrid.js'
import { EXCONFIG } from './config.js'
import { panelex1 } from './ex1.js'
import { panelex2 } from './ex2.js'
import { panelex3 } from './ex3.js'

export class MPanel extends Module {
    modules = [ExModule, AttrGrid, EventGrid, MethodGrid, panelex1, panelex2, panelex3];
    template(props) {
        return `
            <div class='--component'>
                <h1>Panel 面板</h1>
                <div class='--component-desc'>
                    {{desc}}
                </div>
                <h2>例子</h2>
                <div x-repeat={{examples}} index='idx'>
                    <exmodule $desc={{desc}} $title={{(idx+1) + ' '  + title}} $imp={{imp}} $exname={{name}} expath='panel'/>
                </div>
        
                <h2>属性</h2>
                <h3>UIPanel/ui-panel</h3>
                <AttrGrid $rows={{panelAttrs}} />

                <h2>方法</h2>
                <h3>UIPanel/ui-panel</h3>
                <MethodGrid $rows={{panelMethods}} />
            </div>
        `
    }
    data() {
        return EXCONFIG;
    }
}