import { Module } from "/dist/nodomui.min.js";
export class panelex1 extends Module {
    template(props) {
        return `
            <ui-panel title='panel测试' style='width:500px;height:400px'>
				<ui-toolbar>
					<ui-button nobg='true' icon="menu" />
					<ui-button nobg='true' title='颜色' />
					<ui-button nobg='true' icon="search" />
					<ui-button nobg='true' icon="ashbin" />
				</ui-toolbar>
				<ui-panel-body $hobbies={{hobbies}}>
					<ui-list
						$data={{hobbies}}
						field="hobby"
						value-field="hid"
						onItemClick="clickItem"
					>{{htitle}}</ui-list>
				</ui-panel-body>
				<ui-button-group align='right'>
					<ui-button theme="active" icon="add" title='添加' />
					<ui-button theme="error" icon="ashbin" title='删除' />
					<ui-button theme="warn" icon="edit" title='编辑' e-click='edit'/>
					<ui-button icon="search" title='查询' e-click='query'/>
				</ui-button-group>
			</ui-panel>
        `;
    }
    data() {
        return {
            style: 'width:500px;height:400px;',
            hobbies: [
                { hid: 1, htitle: "体育", icon: "message", desc: "体育运动很不错" },
                { hid: 2, htitle: "阅读", icon: "search", desc: "阅读让人睿智" },
                { hid: 3, htitle: "健身", icon: "home", disable: true, desc: "健身让人上瘾" },
                { hid: 4, htitle: "电脑游戏", icon: "user", desc: "好玩可别贪玩" },
                { hid: 5, htitle: "户外运动", icon: "password", desc: "感受大自然，轻呼吸" },
                { hid: 6, htitle: "旅游", icon: "prompt", disable: true, desc: "读万卷书，行万里路" }
            ]
        }
    }

    edit(model, dom) {
        console.log('edit', model, this);
        alert("编辑完成？")
    }
    query(model, dom) {
        console.log('query', model, this);
        alert("查询完成？")
    }
    clickItem(model, dom, evobj, e) {
        console.log(model, dom);
        alert("你点击了"+model.htitle)
    }
}