export const EXCONFIG = {
    desc: '使用ui-panel(面板)，ui-panel-body(面板内容区域)，ui-toolbar，ui-button-group四个组件进行设置',
    panelAttrs: [{
        name: 'title',
        desc: '面板标题',
        type: 'string',
        values: '-',
        defaults: '-'
    }, {
        name: 'bgcolor',
        desc: '设置面板上部标题栏背景色',
        type: 'string',
        value: '-',
        default: '-'
    }, {
        name: 'color',
        desc: '设置面板上部标题栏字体颜色',
        type: 'string',
        value: '-',
        default: '-'
    }, {
        name: 'buttons',
        desc: "按钮，以','分割，按钮事件以'|'分割,如：minus|clickMinus,close|clickClose",
        type: 'string',
        values: '-',
        default: '-'
    }],
    panelMethods: [{
        name: '__clickButton',
        desc: '为标题栏按钮设置样式，以及触发点击时事件',
        params: 'model(改变状态的model)，dom(改变状态的dom节点)',
        returns: '-'
    }],
    examples: [{
        title: '简单应用',
        name: 'ex1'
    }, {
        title: '设置标题背景色与字体颜色',
        desc: '通过bgcolor，color属性来设置其标题背景色与字体颜色',
        name: 'ex2'
    }, {
        title: '为面板右上角添加按钮与按钮对应事件',
        desc: '通过在ui-panel中添加button属性来实现',
        name: 'ex3'
    }]
}