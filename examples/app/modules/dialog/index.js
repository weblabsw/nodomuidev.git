import { Module } from '/dist/nodomui.min.js'
import { ExModule } from '../exmodule.js'
import { AttrGrid } from '../attrgrid.js'
import { EventGrid } from '../eventgrid.js'
import { MethodGrid } from '../methodgrid.js'
import { EXCONFIG } from './config.js'
import { dialogex1 } from './ex1.js'
import { dialogex2 } from './ex2.js'

export class MDialog extends Module {
    modules = [ExModule, AttrGrid, EventGrid, MethodGrid, dialogex1, dialogex2];
    template(props) {
        return `
            <div class='--component'>
                <h1>Dialog 对话框</h1>
                <div class='--component-desc'>
                    {{desc}}
                </div>
                <h2>例子</h2>
                <div x-repeat={{examples}} index='idx'>
                    <exmodule $desc={{desc}} $title={{(idx+1) + ' '  + title}} $imp={{imp}} $exname={{name}} expath='dialog'/>
                </div>
                <h2>属性</h2>
                <h3>UI{{name}}/ui-{{name.toLowerCase()}}</h3>
                <AttrGrid $rows={{dialogAttrs}} />

                <h2>事件</h2>
                <h3>UI{{name}}/ui-{{name.toLowerCase()}}</h3>
                <EventGrid $rows={{dialogEvents}} />

                <h2>方法</h2>
                <h3>UI{{name}}/ui-{{name.toLowerCase()}}</h3>
                <MethodGrid $rows={{dialogMethods}} />
            </div>
        `
    }
    data() {
        return EXCONFIG;
    }
}