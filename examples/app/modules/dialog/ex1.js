import{Module} from '/dist/nodomui.min.js'
export class dialogex1 extends Module{
    template(props){
        return `
            <div>
                <ui-button e-click='open' title="打开"></ui-button>
                <ui-dialog title='测试dialog' width='500px' height='400px' closable>
                    <ui-dialog-body>
                        测试dialog内容    
                    </ui-dialog-body>
                    <ui-button-group align='right'>
                        <ui-button icon="cross" title='关闭' e-click='close'/>
                        <ui-button theme="active" icon="save" title='保存' e-click='save'/>
                    </ui-button-group>
                </ui-dialog>
            </div>
        `;
    }
    data(){
        return {
            openDialog:false
        }
    }

    open(){
        this.getModule('uidialog').__open();
    }

    close(){
        this.getModule('uidialog').__close();
    }

    save(model){
        this.getModule('uidialog').__close();
    }
}