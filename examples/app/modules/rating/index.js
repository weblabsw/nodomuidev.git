import { Module } from '/dist/nodomui.min.js'
import { ExModule } from '../exmodule.js'
import { AttrGrid } from '../attrgrid.js'
import { EventGrid } from '../eventgrid.js'
import { MethodGrid } from '../methodgrid.js'
import { EXCONFIG } from './config.js'
import { ratingex1 } from './ex1.js'
import { ratingex2 } from './ex2.js'
import { ratingex3 } from './ex3.js'
import { ratingex4 } from './ex4.js'
import { ratingex5 } from './ex5.js'
import { ratingex6 } from './ex6.js'
import { ratingex7 } from './ex7.js'

export class MRating extends Module {
    modules = [ExModule, AttrGrid, EventGrid, MethodGrid, ratingex1, ratingex2, ratingex3, ratingex4, ratingex5, ratingex6, ratingex7];
    template(props) {
        return `
            <div class='--component'>
                <h1>Rating 评分</h1>
                <div class='--component-desc'>
                    {{desc}}
                </div>
                <h2>例子</h2>
                <div x-repeat={{examples}} index='idx'>
                    <exmodule $desc={{desc}} $title={{(idx+1) + ' '  + title}} $imp={{imp}} $exname={{name}} expath='rating'/>
                </div>
        
                <h2>属性</h2>
                <h3>UI{{name}}/ui-{{name.toLowerCase()}}</h3>
                <AttrGrid $rows={{ratingAttrs}} />
            </div>
        `
    }
    data() {
        return EXCONFIG;
    }
}