export const EXCONFIG = {
    name:'Rating',
    desc:'使用ui-rating组件进行设置。',
    ratingAttrs:[{
        name:'field',
        desc:'绑定值',
        type:'number',
        values:'-',
        defaults:'-'
    },{
        name:'size',
        desc:'尺寸',
        type:'string',
        values:'small,normal,large',
        defaults:'normal'
    },{
        name:'max',
        desc:'最大评分值',
        type:'number',
        values:'-',
        defaults:'5'
    },{
        name:'readonly',
        desc:'只读',
        type:'无',
        values:'-',
        defaults:'-'
    },{
        name:'allow-half',
        desc:'允许半选',
        type:'无',
        values:'-',
        defaults:'-'
    },{
        name:'touchable',
        desc:'支持滑动手势',
        type:'无',
        values:'-',
        defaults:'-'
    },{
        name:'color',
        desc:'自定义颜色',
        type:'string',
        values:'-',
        defaults:'-'
    }],
    examples:[{
        title:'简单应用',
        name:'ex1'
    },{
        title:'设置尺寸大小',
        desc: '通过size进行设置，可选值为small/normal/large，默认为normal。',
        name:'ex2'
    },{
        title:'设置只读',
        desc: '通过readonly进行设置。',
        name:'ex3'
    },{
        title:'设置最大评分数',
        desc: '通过max进行设置，默认最大评分数为5。',
        name:'ex4'
    },{
        title:'设置半选',
        desc: '通过allow-half进行设置。',
        name:'ex5'
    },{
        title:'支持滑动手势',
        desc: '通过touchable进行设置。',
        name:'ex6'
    },{
        title:'设置颜色',
        desc: '通过color进行设置。',
        name:'ex7'
    }]
}