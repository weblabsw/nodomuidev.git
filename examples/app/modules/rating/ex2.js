import{Module} from '/dist/nodomui.min.js'
export class ratingex2 extends Module{
    template() {
        return `
            <div>
                <ui-rating field="score" size="small">{{score}}</ui-rating>
                <ui-rating field="score">{{score}}</ui-rating>
                <ui-rating field="score" size="large" >{{score}}</ui-rating>
            </div>
        `
    }
    data() {
        return {
            score: 3,
        }
    }
}