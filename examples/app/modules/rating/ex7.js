import{Module} from '/dist/nodomui.min.js'
export class ratingex7 extends Module{
    template() {
        return `
            <div>
                <ui-rating field="score5" color="red">{{score5}}</ui-rating>
                <ui-rating field="score5" color="#72B2F4">{{score5}}</ui-rating>
            </div>
        `
    }
    data() {
        return {
            score5: 3
        }
    }
}