import{Module} from '/dist/nodomui.min.js'
export class ratingex3 extends Module{
    template() {
        return `
            <div>
                <ui-rating field="score1" readonly>{{score1}}</ui-rating>
            </div>
        `
    }
    data() {
        return {
            score1: 2
        }
    }
}