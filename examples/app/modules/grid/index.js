import{Module} from '/dist/nodomui.min.js'
import { ExModule } from '../exmodule.js'
import { AttrGrid } from '../attrgrid.js'
import { EventGrid } from '../eventgrid.js'
import { MethodGrid } from '../methodgrid.js'
import{EXCONFIG} from './config.js'
import{gridex1} from './ex1.js'
import{gridex2} from './ex2.js'
import{gridex3} from './ex3.js'
import{gridex4} from './ex4.js'
import{gridex5} from './ex5.js'
import{gridex6} from './ex6.js'
import{gridex7} from './ex7.js'
import{gridex8} from './ex8.js'
import{gridex9} from './ex9.js'
import{gridex10} from './ex10.js'
import{gridex11} from './ex11.js'
import{gridex12} from './ex12.js'
import{gridex13} from './ex13.js'
import{gridex14} from './ex14.js'
import{gridex15} from './ex15.js'

export class MGrid extends Module{
    modules=[ExModule,AttrGrid,EventGrid,MethodGrid,gridex1,gridex2,gridex3,gridex4,gridex5,gridex6,gridex7,gridex8,gridex9,gridex10,gridex11,gridex12,gridex13,gridex14,gridex15];
    template(props){
        return `
            <div class='--component'>
                <h1>Grid 表格</h1>
                <div class='--component-desc'>
                    {{desc}}
                </div>
                <h2>例子</h2>
                <div x-repeat={{examples}} index='idx'>
                    <exmodule $desc={{desc}} $title={{(idx+1) + ' '  + title}} $imp={{imp}} $exname={{name}} expath='grid'/>
                </div>
                <h2>属性</h2>
                <h3>UIGrid/ui-grid</h3>
                <AttrGrid $rows={{gridAttrs}} />

                <h3>UIGridCol/ui-grid-col</h3>
                <AttrGrid $rows={{gridColAttrs}} />

                <h2>事件</h2>
                <h3>UIGrid/ui-grid</h3>
                <EventGrid $rows={{gridEvents}} />

                <h2>方法</h2>
                <h3>UIGrid/ui-grid</h3>
                <MethodGrid $rows={{gridMethods}} />
            </div>
        `
    }
    data(){
        return EXCONFIG;
    }
}