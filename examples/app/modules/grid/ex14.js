
import{Module} from '/dist/nodomui.min.js'
export class gridex14 extends Module{
    template(){
        return `
            <div>
                <button e-click='changeCols' >修改列</button>
                <ui-grid $data={{changeData}} grid-line='both' row-alt>
                    <ui-grid-col x-repeat={{cols}} width={{width}} title={{title}} field={{field}} />
                    <ui-grid-col title='操作'>
                        <ui-button title='删除' theme='error' e-click='delClick:nopopo'/> &nbsp;&nbsp;
                        <ui-button title='编辑' theme='success' /> 
                    </ui-grid-col>
                </ui-grid>
            </div>
        `;
    }
    data(){
        return{
            cols:[
                {title:'用户id',field:'userId',width:80},
                {title:'用户名',field:'userName',width:120},
                {title:'性别',field:'sexy',width:80},
                {title:'电话号码',field:'mobile',width:150}
            ],
            changeData:[
                {userId:1,userName:'yang',sexy:'男',mobile:'1380000000'},
                {userId:2,userName:'nodom',sexy:'女',mobile:'13811111111'},
                {userId:3,userName:'noomi',sexy:'女',mobile:'13822222222'},
                {userId:4,userName:'relaen',sexy:'女',mobile:'13833333333'}
            ]
        }
    }

    changeCols(model){
        let grid = this.getModule('UIGrid');
        //修改列之前需要清空现有列
        grid.__clearColumns(0,model.changeData.length);
        const cols = [
            [
                {title:'用户id',field:'userId',width:80},
                {title:'用户名',field:'userName',width:120},
                {title:'性别',field:'sexy',width:80},
                {title:'电话号码',field:'mobile',width:150}
            ],
            [
                {title:'商家id',field:'companyId',width:80},
                {title:'商家名',field:'companyName',width:120},
                {title:'商户号',field:'companyNo',width:80},
                {title:'地址',field:'addr',width:150},
                {title:'联系人',field:'linkMan',width:100},
                {title:'经营范围',field:'scope',width:200}
            ]
        ]
        const changeData = [
            [
                {userId:1,userName:'yang',sexy:'男',mobile:'1380000000'},
                {userId:2,userName:'nodom',sexy:'女',mobile:'13811111111'},
                {userId:3,userName:'noomi',sexy:'女',mobile:'13822222222'},
                {userId:4,userName:'relaen',sexy:'女',mobile:'13833333333'}
            ],
            [
                {companyId:1,companyName:'爱咖啡',companyNo:'em001',addr:'四川绵阳',linkMan:'yang',scope:'咖啡、茶'},
                {companyId:2,companyName:'奶茶小店',companyNo:'em002',addr:'北京东城',linkMan:'nodom',scope:'奶茶、冰激凌'},
                {companyId:3,companyName:'mm快餐店',companyNo:'em003',addr:'成都武侯',linkMan:'noomi',scope:'汉堡、薯条'}
            ]
        ]
        if(!this.colDataIndex){
            this.colDataIndex = 0;
        }
        const index = (++this.colDataIndex)%2;
        model.cols = cols[index];
        model.changeData = changeData[index];
    }
}