export const EXCONFIG = {
    desc:'使用ui-grid(表格),ui-grid-col(列),ui-grid-expand(列展开)三个组件进行设置。',
    gridAttrs:[{
        name:'$data',
        desc:'表格数据',
        type:'json array',
        values:'-',
        defaults:'-'
    },{
        name:'row-alt',
        desc:'设置奇偶行背景色不同，该属性为无值属性',
        type:'无',
        values:'-',
        defaults:'-'
    },{
        name:'grid-line',
        desc:'设置网格线类型',
        type:'string',
        values:'cols(列),rows(行),both(行列)',
        defaults:'无网格线'
    },{
        name:'fix-head',
        desc:'设置表头固定，该属性为无值属性',
        type:'无',
        values:'-',
        defaults:'-'
    },{
        name:'checkable',
        desc:'设置表格带复选框功能，该属性为无值属性',
        type:'无',
        values:'-',
        defaults:'-'
    },{
        name:'single',
        desc:'设置单选功能，同时设置checkable属性时有效，该属性为无值属性',
        type:'无',
        values:'-',
        defaults:'-'
    }],
    gridColAttrs:[{
        name:'title',
        desc:'列标题',
        type:'string',
        values:'-',
        defaults:'-'
    },{
        name:'width',
        desc:'宽度，不带单位，使用时直接按照px计算，如果不设置，则默认flex:1，如果自动铺满，最后一列不设置宽度',
        type:'number',
        values:'-',
        defaults:'-'
    },{
        name:'sortable',
        desc:'此列是否排序，需同时设置field属性',
        type:'boolean',
        values:'true/false',
        defaults:'false'
    },{
        name:'field',
        desc:'如果sortable，则必须设置，如果没设置列显示内容(模板中无插槽)，则默认显示此项对应的值',
        type:'string',
        values:'-',
        defaults:'-'
    },{
        name:'title-style',
        desc:'表头样式，文本对齐方式采用flex',
        type:'string',
        values:'-',
        defaults:'-'
    },{
        name:'body-style',
        desc:'表格样式，文本对齐方式采用flex',
        type:'string',
        values:'-',
        defaults:'-'
    }],
    gridEvents:[{
        name:'onSelectChange',
        desc:'行选择状态修改时触发',
        params:'model(改变状态的行model)',
    },{
        name:'onRowClick',
        desc:'单击行时触发',
        params:'model(同上),dom(改变状态的行dom节点),evObj(NEvent事件对象),e(HTML Event对象)',
    },{
        name:'onRowDblClick',
        desc:'双击行时触发',
        params:'同上',
    },{
        name:'onRowExpand',
        desc:'行展开时触发，配置ui-grid-expand组件时有效',
        params:'model(同上)',
    },{
        name:'onRowCollapse',
        desc:'行从展开变为折叠状态时触发，配置ui-grid-expand组件时有效',
        params:'model(同上)',
    }],
    gridMethods:[{
        name:'__addRow',
        desc:'添加数据',
        params:'rows(一行或多行数据)',
        returns:'-'
    },{
        name:'__removeRow',
        desc:'删除数据',
        params:'条件对象，用于查找符合该参数条件的所有数据',
        returns:'-'
    },{
        name:'__getSelectedRows',
        desc:'获取所有选中的行',
        params:'-',
        returns:'选中的行数据数组'
    },{
        name:'__clearColumns',
        desc:'清空列',
        params:'-',
        returns:'-'
    }],
    examples:[{
        title:'简单应用',
        name:'ex1'
    },{
        title:'网格线',
        desc:'通过grid-line进行设置，可选值为rows,cols,both，不设置则不显示网格线。',
        name:'ex2'
    },{
        title:'行背景色交替',
        desc:'通过row-alt设置。',
        name:'ex3'
    },{
        title:'固定表头',
        desc:'通过fix-head进行设置。',
        name:'ex4'
    },{
        title:'复选框',
        desc:'通过checkable设置显示复选框。',
        name:'ex5'
    },{
        title:'复选框-单选模式',
        desc:'通过single进行设置。',
        name:'ex6'
    },{
        title:'列排序',
        desc:'通过ui-grid-col设置sortable配置项',
        imp:'注意：需在ui-grid-col中配置field项，指向数据中需要排序的数据项名。',
        name:'ex7'
    },{
        title:'展开行',
        desc:'通过配置ui-grid-expand组件。',
        name:'ex8'
    },{
        title:'行展开-动态加载数据',
        desc:'通过onRowExpand事件实现数据动态加载。',
        name:'ex9'
    },{
        title:'自定义列模版',
        desc:'使用插槽设置显示内容。',
        name:'ex10'
    },{
        title:'横向滚动',
        desc:'如果所有ui-grid-col都设置了width，则当横向宽度不足时，会出现横向滚动条。',
        name:'ex11'
    },{
        title:'分页',
        desc:'结合ui-pagination组件使用。',
        name:'ex12'
    }/*,{
        title:'固定列',
        desc:'通过ui-grid-col的fixed属性“fixed”设置，左右只能各设置0或1列作为固定列，同时需要设置所有列宽。',
        imp:'配置了onRowClick，操作按钮的click事件需要设置为禁止冒泡(nopopo)。',
        name:'ex13'
    },{
        title:'列动态变化',
        desc:'动态变换列，将无法设置自定义显示，默认为field对应值，所以ui-grid-col必须配置field。',
        name:'ex14'
    },{
        title:'自定义表头颜色和标题位置',
        desc:'设置自定义样式，title-style用于设置表头样式，bodyStyle用于设置表格body样式，对齐方式参考flex。',            
        name:'ex15'
    }*/]
}