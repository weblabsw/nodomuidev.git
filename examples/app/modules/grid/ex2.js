
import{Module} from '/dist/nodomui.min.js'
export class gridex2 extends Module{
    template(){
        return `
            <ui-grid $data={{rows}} style='height:300px;' grid-line='rows'>
                <ui-grid-col width='80' title='编号'>{{id}}</ui-grid-col>
                <ui-grid-col width='200' title='框架'>{{name}}</ui-grid-col>
                <ui-grid-col width='150' title='发布时间'>{{pubTime}}</ui-grid-col>
                <ui-grid-col title='描述' >{{desc}}</ui-grid-col>
            </ui-grid>
        `;
    }
    data(){
        return{
            rows:[
                {id:1,name:'nodom',pubTime:'2017年11月',desc:'Web技术实验室发布的web前端框架'},
                {id:2,name:'noomi',pubTime:'2019年10月',desc:'Web技术实验室发布的node.js服务端框架'},
                {id:3,name:'relaen',pubTime:'2020年10月',desc:'Web技术实验室发布的node.js ORM框架'},
                {id:4,name:'react',pubTime:'2013年5月',desc:'Facebook开源发布的web前端框架'},
                {id:5,name:'vue',pubTime:'2013年2月',desc:'国内优秀web前端框架'},
                {id:6,name:'angular',pubTime:'2010年2月',desc:'Goole发布的web前端框架'}
            ]
        }
    }
}