export const EXCONFIG = {
    name:'Tree',
    desc: '使用ui-tree组件进行设置。',
    treeAttrs: [{
        name: '$data',
        desc: '树形结构依赖的数据，结构为:{children:[{***:***,...,children:[]},...]}',
        type: 'string',
        values: '-',
        defaults: '-'
    }, {
        name: 'field',
        desc: '当tree作为输入项时有效，设置次向后，将显示checkbox',
        type: 'string',
        values: '-',
        defaults: '-'
    }, {
        name: 'display-field',
        desc: '显示数据项名，对应上传后返回的displayField',
        type: 'string',
        values: '-',
        defaults: '-'
    }, {
        name: 'value-field',
        desc: '值字段名，当设置field时，需要设置该项，否则无法获取值',
        type: 'string',
        values: '-',
        defaults: '-'
    }, {
        name: 'icons',
        desc: '节点图标，依次为为非叶子节点关闭状态，打开状态，叶子节点，如果只有两个，则表示非叶子节点和叶子节点，如果1个，则表示非叶子节点。',
        type: 'string[]',
        values: '-',
        defaults: '-'
    }],
    treeEvents: [{
        name: 'onItemClick',
        desc: '节点点击事件',
        params: 'model(被点击数据项model),dom(被点击数据项的dom节点),evObj(NEvent事件对象),e(HTML Event对象)',
    }, {
        name: 'onExpand',
        desc: '节点展开事件',
        params: 'model,dom,evObj,e',
    }, {
        name: 'onCollapse',
        desc: '节点闭合事件',
        params: 'model,dom,evObj,e',
    }],
    examples: [{
        title: '简单应用',
        desc: '展开关闭节点',
        name: 'ex1'
    }, {
        title: '自定义树节点icons',
        desc: '通过icons属性进行设置',
        name: 'ex2'
    }, {
        title: 'tree作为输入',
        desc: '通过field属性进行设置',
        name: 'ex3'
    }, {
        title: '节点点击事件',
        desc: '通过onItemClick设置',
        name: 'ex4'
    }, {
        title: '节点展开事件',
        desc: '通过onExpand设置',
        name: 'ex5'
    }, {
        title: '节点关闭事件',
        desc: '通过onCollapse设置',
        name: 'ex6'
    }]

}
