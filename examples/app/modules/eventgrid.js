import {Module} from '/dist/nodomui.min.js'

export class EventGrid extends Module{
    template(){
        return `
            <ui-grid $data={{rows}} grid-line='rows'  class='--eventgrid'>
                <ui-grid-col title='事件名' width='180'>{{name}}</ui-grid-col>
                <ui-grid-col title='说明'>{{desc}}</ui-grid-col>
                <ui-grid-col title='参数'>
                    <div x-repeat={{params}}>{{name}}:&nbsp;{{desc}}</div>
                </ui-grid-col>
            </ui-grid>
        `
    }
}