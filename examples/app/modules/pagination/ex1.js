import { Module } from '/dist/nodomui.min.js'
export class paginationex1 extends Module {
    template(props) {
        return ` 
        <ui-pagination class='page'
            total={{total}}
            page-size={{30}}
            page-no={{2}}
            ps-array={{[10,30,50,100]}}
            show-total
            show-jump
            show-num={{10}}
            big-step={{10}}
        />
        `;
    }
    data() {
        return {
            total: 100
        }
    }
}