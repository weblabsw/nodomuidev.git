import { Module } from '/dist/nodomui.min.js'
import { ExModule } from '../exmodule.js'
import { AttrGrid } from '../attrgrid.js'
import { EventGrid } from '../eventgrid.js'
import { MethodGrid } from '../methodgrid.js'
import { EXCONFIG } from './config.js'
import { paginationex1 } from './ex1.js'
import { paginationex2 } from './ex2.js'
export class MPagination extends Module {
    modules = [ExModule, AttrGrid, EventGrid, MethodGrid, paginationex1,paginationex2];
    template(props) {
        return `
            <div class='--component'>
                <h1>Pagination 分页</h1>
                <div class='--component-desc'>
                    {{desc}}
                </div>
                <h2>例子</h2>
                <div x-repeat={{examples}} index='idx'>
                    <exmodule $desc={{desc}} $title={{(idx+1) + ' '  + title}} $imp={{imp}} $exname={{name}} expath='pagination'/>
                </div>
               
                <h2>属性</h2>
                <h3>UI{{name}}/ui-{{name.toLowerCase()}}</h3>
                <AttrGrid $rows={{gridAttrs}} />

                <h2>事件</h2>
                <h3>UI{{name}}/ui-{{name.toLowerCase()}}</h3>
                <EventGrid $rows={{gridEvents}} />
                
            </div>
        `;
    }
    data() {
        return EXCONFIG;
    }
}