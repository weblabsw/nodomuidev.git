import { Module } from '/dist/nodomui.min.js'
export class paginationex2 extends Module {
    template(props) {
        return ` 
        <ui-pagination class='page'
        total={{total}}
        page-size={{50}}
        page-no={{1}}
        ps-array={{[10,30,50,100]}}
        show-total
        show-jump
        show-num={{10}}
        big-step={{10}}
        onChange="changePage"
    />
        `;
    }
    data() {
        return {
            total: 150,
        }
    }
    changePage(pageNo,pageSize){
        console.log('page:',pageNo,'pageSize:',pageSize)
    }
}