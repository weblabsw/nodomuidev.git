export const EXCONFIG = {
    name:'Pagination',
    desc: '使用ui-pagination组件进行设置。',
    gridAttrs: [{
        name: '$total',
        desc: '数据项总数',
        type: 'number',
        values: '-',
        defaults: '-'
    }, {
        name: 'ps-array',
        desc: '页面大小数组，数组字符串，默认[10,20,30,50]',
        type: 'array',
        values: '-',
        defaults: '-'
    }, {
        name: 'show-total',
        desc: '显示总记录数',
        type: 'number',
        values: '-',
        defaults: '-'
    }, {
        name: 'show-jump',
        desc: '显示跳转',
        type: '无',
        values: '-',
        defaults: '-'
    }, {
        name: 'show-num',
        desc: '显示页面号的数量，默认10',
        type: 'number',
        values: '-',
        defaults: '-'
    }, {
        name: 'big-step',
        desc: '一大步移动页数，默认5',
        type: 'number',
        values: '-',
        defaults: '-'
    }],
    gridEvents: [{
        name: 'onChange',
        desc: '页号或页面大小改变时执行方法名',
        params: 'nowPage(当前页码),pageSize(页面大小)',
    }],
    examples: [{
        title: '简单应用',
        name: 'ex1'
    },{
        title: '更换页码事件',
        desc:'使用onChange事件',
        name: 'ex2'
    }]

}
