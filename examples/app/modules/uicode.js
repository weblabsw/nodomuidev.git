import{Module,Nodom} from '/dist/nodomui.min.js'
export class UICode extends Module{
    basePath = '/examples/app/modules/';
    template(props){
        this.setExcludeProps(['url','exname'])
        return `
            <div class='ui-code'>
                <div class='ui-code-switcher' e-click='switch'>
                    {{open?'隐藏代码':'显示代码'}}
                </div>
                <div x-show={{open}} class='ui-code-code'>
                    <pre>{{code}}</pre>
                </div>
            </div>
        `
    }

    data(){
        return{
            code:'',
            open:false
        }
    }

    onBeforeFirstRender(){
        Nodom.request({
            url:this.basePath + this.props.exname  + '/' + this.props.url,
        }).then(r=>{
            this.model.code = r
        })
    }

    switch(){
        this.model.open = !this.model.open;
    }
}