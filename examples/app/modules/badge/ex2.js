import{Module} from '/dist/nodomui.min.js'
export class badgeex2 extends Module{
    template(props){
        return `
            <div>
                <ui-badge value="1000" max="10">
                    <ui-button size="small" title='评论' />
                </ui-badge>
                <ui-badge value="11" max="3">
                    <ui-button size="small" title='捐赠' />
                </ui-badge>
                <ui-badge value="1000" max="99">
                    购物车
                </ui-badge>
                
                <style>
                    .ui-badge{
                        margin-right:40px;
                        margin-bottom:10px;
                    }
                </style>
            </div>
        `;
    }
}