import { Module } from '/dist/nodomui.min.js'
export class formex1 extends Module {
	template(props) {
		return ` 
			<div>
				<ui-form label-width='100px' unit-width='50px'>
					<div class='ui-row'>
						<ui-form-item label="require" field="field1" required unit='s' style='width:150px'>
							<ui-input field="field1" />
						</ui-form-item>
						<ui-form-item label='number' field="field2" validator='number' required  unit='m/s'>
							<ui-input field="field2"/>
						</ui-form-item>
					</div>
					<div class='ui-row'>
						<ui-form-item label='min(10)' field="field3" validator='min:10'  unit='ms'>
							<ui-input field="field3"/>
						</ui-form-item>
						<ui-form-item label='max(10000)' field="field4" validator='max:10000'  unit='km/s'>
							<ui-input field="field4"/>
						</ui-form-item>
					</div>
					<div class='ui-row'>
						<ui-form-item label="between(2,150)" field="field5" validator='between:2:150'>
							<ui-input field="field5"/>
						</ui-form-item>
						<ui-form-item label="minLength(2)" field="field6" validator='minLength:2'>
							<ui-input field="field6"/>
						</ui-form-item>
					</div>
					<div class='ui-row'>
						<ui-form-item label="date" field="date" validator='date'>
							<ui-date field="date"/>
						</ui-form-item>
						<ui-form-item label="爱好" field="hobby" required>
							<ui-select
								$data={{hobbies}}
								field="hobby"
								value-field="id"
								display-field="title"
								allow-empty
								multiple
							>{{title}}</ui-select>
						</ui-form-item>
					</div>
					<div class='ui-row'>
						<ui-form-item label="url" field="url" validator='url'>
							<ui-input field="url"/>
						</ui-form-item>
						<ui-form-item label="maxLength(5)" field="field7" validator='maxLength:5'>
							<ui-input field="field7"/>
						</ui-form-item>
						<ui-form-item label="betweenLength(5,10)" field="field8" validator='betweenLength:5:10'>
							<ui-input field="field8"/>
						</ui-form-item>
					</div>
					<div class='ui-row'>
						<ui-form-item label="email" field="email" validator='email'>
							<ui-input field="email"/>
						</ui-form-item>
						<ui-form-item label="mobile" field="telNum" validator='mobile'>
							<ui-input field="telNum"/>
						</ui-form-item>
						<ui-form-item label="正则表达式(nodom开头)" field="nodom" validator='regexp:^(nodom).*'>
							<ui-input field='nodom'/>
						</ui-form-item>
					</div>
					<div class='ui-row'>
						<ui-form-item label="idno" field="idno" validator='idno'>
							<ui-input field="idno"/>
						</ui-form-item>
					</div>
					<div class='ui-row'>
						<ui-form-item label="time" field="time" validator='time'>
							<ui-input field="time"/>
						</ui-form-item>
						<ui-form-item label="datetime" field="datetime" validator='datetime'>
							<ui-input field="datetime"/>
						</ui-form-item>
					</div>
					
					<div class='ui-row'>
						<ui-form-item label="密码" field="pwd" required validator='method:checkPwd'>
							<ui-input type='password' field="pwd"/>
						</ui-form-item>
						<ui-form-item label="确认密码" field="pwd1" required validator='method:checkPwd'>
							<ui-input type='password' field="pwd1"/>
						</ui-form-item>
					</div>

					<div class='ui-row'>
						<ui-form-item label="头像" field="headImg" required>
							<ui-file height='38px'
								field='headImg'
								value-field="id"
								display-field="fileName"
								url-field="url"
								upload-name="file"
								delete-url="http://localhost:3000/__file/delete"
								upload-url="http://localhost:3000/__file/upload" 
								/>
						</ui-form-item>
					</div>

					<div class='ui-row'>
						<ui-form-item label="描述" field="description">
							<ui-input type='textarea' field="description"/>
						</ui-form-item>
					</div>
					
					<div class='ui-row'>
						<ui-form-item label="年龄段" field="ages" required>
							<ui-range type='number' field='ages'/>
						</ui-form-item>
						<ui-form-item label="日期段" field="dates">
							<ui-range type='date' field='dates'/>
						</ui-form-item>
					</div>

					<div class='ui-row'>
						<ui-form-item label="时间段" field="times" required>
							<ui-range type='time' field='times'/>
						</ui-form-item>
					</div>
				</ui-form>
				<ui-button theme='active' title='提交' e-click='save'/>
			</div>
        `;
	}
	data() {
		return {
			field1: 300,
			name: "zhangsan",
			telNum: 18681262848,
			date: '2022-10-11',
			datetime: '2022-10-11 12:21:12',
			time: '12:21:12',
			idno: 511621199805113111,
			times: ['11:20', '10:20'],
			headImg: [],
			hobbies: [
				{ id: 1, title: "体育", icon: "message", desc: "体育运动很不错" },
				{ id: 2, title: "阅读", icon: "search", desc: "阅读让人睿智" },
				{ id: 3, title: "健身", icon: "home", disable: true, desc: "健身让人上瘾" },
				{ id: 4, title: "电脑游戏", icon: "user", desc: "好玩可别贪玩" },
				{ id: 5, title: "户外运动", icon: "password", desc: "感受大自然，轻呼吸" },
				{ id: 6, title: "旅游", icon: "prompt", disable: true, desc: "读万卷书，行万里路" },
			]
		}
	}

	save(){
		this.getModule('uiform').__verify();
	}
}
