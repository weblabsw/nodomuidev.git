import { Module } from '/dist/nodomui.min.js'
export class timelineex2 extends Module {
    template(props) {
        return `
        <ui-timeline 
        $data={{timelist2}} 
        color-field='tcolor' 
        size-field='tsize' 
        icon-field='ticon'
        >
        </ui-timeline>
        `;
    }
    data() {
        return {
            timelist2: this.genData2(),
        }
    }
    genData2() {
        return [
            { ttitle: "支持自定义颜色", tdate: "2023-06-11 20:46", tcolor: "rgb(11, 189, 135)" },
            { ttitle: "支持自定义尺寸", tdate: "2023-06-13 20:46", tsize: "large" },
            { ttitle: "支持空心圆显示", tdate: "2023-06-15 20:46", tsize: "hollow" },
            { ttitle: "支持自定义图标", tdate: "2023-06-15 20:46", ticon: "search" },
            { ttitle: "默认样式的节点", tdate: "2023-06-18 20:46" }
        ]
    }
}