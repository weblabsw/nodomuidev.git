export const EXCONFIG = {
    name:'Timeline',
    desc: '使用ui-timeline组件进行设置。',
    gridAttrs: [{
        name: '$data',
        desc: '数据',
        type: 'json array',
        values: '-',
        defaults: '-'
    }, {
        name: 'reverse',
        desc: '是否需要反转',
        type: 'boolean',
        values: 'true/false',
        defaults: 'false'
    }, {
        name: 'color-field',
        desc: '自定义颜色',
        type: 'string',
        values: '-',
        defaults: '-'
    },
    {
        name: 'size-field',
        desc: '自定义尺寸',
        type: 'string',
        values: 'small、normal、big、hollow(空心圆)',
        defaults: 'normal'
    }, {
        name: 'icon-field',
        desc: '自定义图标',
        type: 'string',
        values: '-',
        defaults: '-'
    }, {
        name: 'place-ment',
        desc: '时间戳位置 true:"上面"/false:"下面" 默认为false',
        type: 'boolean',
        values: '-',
        defaults: '-'
    }],

    examples: [{
        title: '简单应用',
        desc: 'Timeline 可按照时间戳正序或倒序排列,时间戳是其区分于其他控件的重要特征，使⽤时注意与 Steps 步骤条等区分',
        name: 'ex1'
    }, {
        title: '自定义颜色、大小、图标',
        desc: '可根据实际场景⾃定义节点尺⼨、颜⾊，或直接使⽤图标。',
        name: 'ex2'
    }, {
        title: '自定义时间戳',
        desc: '当内容在垂直⽅向上过⾼时，可将时间戳置于内容之上',
        name: 'ex3'
    }]

}
