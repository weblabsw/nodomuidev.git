import { Module } from '/dist/nodomui.min.js'
export class buttonex6 extends Module {
    template(props) {
        return `
            <div>
                <ui-button icon='prompt' theme='warn' disabled title='小图标' e-click='click' class="item"/>

                <style>
                    .item{
                        margin-right:30px;
                        margin-bottom:10px;
                    }
                </style>
            </div>
        `;
    }
    click(){
        alert('ex6');
    }
}