import { Module } from '/dist/nodomui.min.js'
export class buttonex2 extends Module {
    template(props) {
        return `
            <div>
                <ui-button icon='close'  title='close图标' class="item"/>
                <ui-button icon='edit' vertical-icon title='edit图标' class="item"/>
                <ui-button icon='search' title='search图标' class="item"/>
                <ui-button icon='prompt' vertical-icon title='prompt图标' class="item"/>

                <style>
                    .item{
                        margin-right:30px;
                        margin-bottom:10px;
                    }
                </style>
            </div>
        `;
    }
}