import { Module } from '/dist/nodomui.min.js'
export class buttonex3 extends Module {
    template(props) {
        return `
            <div>
                <ui-button icon='close' theme='active'  title='active主题' class="item"/>
                <ui-button icon='edit' theme='success'  title='success主题' class="item"/>
                <ui-button icon='ashbin' theme='error'  title='error主题' class="item"/>
                <ui-button icon='warning' theme='warn' title='warn主题' class="item"/>

                <style>
                    .item{
                        margin-right:30px;
                        margin-bottom:10px;
                    }
                </style>
            </div>
        `;
    }
}