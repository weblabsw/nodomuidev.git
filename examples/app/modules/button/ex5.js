import { Module } from '/dist/nodomui.min.js'
export class buttonex5 extends Module {
    template(props) {
        return `
            <div>
                <ui-button icon='prompt' theme='warn' nobg  title='小图标' class="item"/>
                <ui-button icon='prompt' theme='warn' nobg  e-click='click' class="item"/>

                <style>
                    .item{
                        margin-right:30px;
                        margin-bottom:10px;
                    }
                </style>
            </div>
        `;
    }

    click(){
        alert('ex5');
    }
}