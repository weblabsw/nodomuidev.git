import{Module,nuiloading} from '/dist/nodomui.min.js'
export class loadingex4 extends Module{
    template(){
        return `
            <div>
                <ui-button e-click='show3' title='打开2次，4s后强制关闭' />
                <ui-loading color='red'/>
            </div>
        `
    }

    show3(){
        //开启1次后3s关闭
        nuiloading().__show();
        //延迟3s后手动关闭
        setTimeout(()=>{
            nuiloading().__show();
        },3000);

        setTimeout(()=>{
            nuiloading().__close(true);
        },4000)
    }
}