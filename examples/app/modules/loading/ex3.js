import{Module,nuiloading} from '/dist/nodomui.min.js'
export class loadingex3 extends Module{
    template(){
        return `
            <div>
                <ui-button e-click='show2' title='打开1次，3s后手动关闭' />
                <ui-loading color='red'/>
            </div>
        `
    }

    show2(){
        //开启1次后3s关闭
        nuiloading().__show();
        //延迟3s后手动关闭
        setTimeout(()=>{
            nuiloading().__close();
        },3000);
    }
}