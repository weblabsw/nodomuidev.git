import { Module } from '/dist/nodomui.min.js'
export class carouselex5 extends Module {
    template(props) {
        return `
            <ui-carousel style="height: 300px; width: 600px;" show-indicator onChange='changeIndex'>
              <ui-carousel-item  x-repeat={{items}} style={{back}}>{{content}}</ui-carousel-item>
            </ui-carousel>
        `;
    }
    data() {
        return {
            items: [
                { content: "这是一个测试1", back: 'background-color: red;' },
                { content: "这是一个测试2", back: 'background-color: pink;' },
                { content: "这是一个测试3", back: 'background-color: blue;' },
                { content: "这是一个测试4", back: 'background-color: yellow;' },
                { content: "这是一个测试5", back: 'background-color: green;' },
            ]
        }
    }
    changeIndex(oldIndex, newIndex) {
        console.log('当前页索引是'+oldIndex,'下一页索引是'+newIndex)
    }
}