import{Module} from '/dist/nodomui.min.js'
import { ExModule } from '../exmodule.js'
import { AttrGrid } from '../attrgrid.js'
import { EventGrid } from '../eventgrid.js'
import { MethodGrid } from '../methodgrid.js'
import{EXCONFIG} from './config.js'
import{carouselex1} from './ex1.js'
import{carouselex2} from './ex2.js'
import{carouselex3} from './ex3.js'
import{carouselex4} from './ex4.js'
import{carouselex5} from './ex5.js'


export class MCarousel extends Module{
    modules=[ExModule,AttrGrid,EventGrid,MethodGrid,carouselex1,carouselex2,carouselex3,carouselex4,carouselex5];
    template(props){
        return `
            <div class='--component'>
                <h1>Carousel 走马灯</h1>
                <div class='--component-desc'>
                    {{desc}}
                </div>
                <h2>例子</h2>
                <div x-repeat={{examples}} index='idx'>
                    <exmodule $desc={{desc}} $title={{(idx+1) + ' '  + title}} $imp={{imp}} $exname={{name}} expath='carousel'/>
                </div>
        
                <h2>属性</h2>
                <h3>UI{{name}}/ui-{{name.toLowerCase()}}</h3>
                <AttrGrid $rows={{carouselAttrs}} />

                <h2>事件</h2>
                <h3>UI{{name}}/ui-{{name.toLowerCase()}}</h3>
                <EventGrid $rows={{carouselEvents}} />

            </div>
        `
    }
    data(){
        return EXCONFIG;
    }
}