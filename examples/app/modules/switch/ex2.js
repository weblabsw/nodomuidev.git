import { Module } from '/dist/nodomui.min.js'
export class switchex2 extends Module {
    template(props) {
        return `
        <ui-switch field="status1" active-color='green' inactive-color='red'/>
        `;
    }
    data() {
        return {
            status1: true
        }
    }
}