export const EXCONFIG = {
    name:'Switch',
    desc: '使用ui-switch组件进行设置。',
    gridAttrs: [{
        name: 'active-color',
        desc: '激活状态颜色',
        type: 'string',
        values: '-',
        defaults: '-'
    }, {
        name: 'inactive-color',
        desc: '非激活状态颜色',
        type: 'string',
        values: '-',
        defaults: '-'
    }, {
        name: 'values',
        desc: '值数组',
        type: 'string',
        values: '-',
        defaults: '[false,true]'
    }],
    examples: [{
        title: '简单应用',
        name: 'ex1'
    }, {
        title: '自定义颜色',
        desc: '可通过active-color和inactive-color自定义颜色',
        name: 'ex2'
    }, {
        title: '自定义开关值',
        desc: '可通过values设置自定义开关值，示例中设置为[0,1]',
        name: 'ex3'
    }]

}
