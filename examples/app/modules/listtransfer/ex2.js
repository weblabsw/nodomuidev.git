import {Module} from "/dist/nodomui.min.js";
export class listtransferex2 extends Module{
    template(props) {
        return`
            <div>
            <button e-click="change">修改</button>
            <ui-listtransfer
                style="height: 200px; width: 500px"
                $data={{users2}}
                field="selectedUser2"
                value-field="uid"
                disable-field='disable'
            >
                <span style="width: 100px; display: inline-block">{{userName}}</span>
                <span>{{company}}</span>
            </ui-listtransfer>
            </div>
        `;
    }
    data(){
        return{
            selectedUser2: [3,4],
            users2: [
                { uid: 1, userName: "yang lei", company: "swust" },
                { uid: 2, userName: "张三", company: "阿里" ,disable:true},
                { uid: 3, userName: "李四", company: "华为" },
                { uid: 4, userName: "王武", company: "腾讯" },
                { uid: 5, userName: "fieldyang", company: "京东" },
                { uid: 6, userName: "路人甲", company: "google" }
            ]
        }
    }
    change (model,dom) {
        this.model.selectedUser2 = [1,3];
    }
}