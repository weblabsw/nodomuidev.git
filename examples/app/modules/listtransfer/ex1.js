import {Module} from "/dist/nodomui.min.js";
export class listtransferex1 extends Module{
    template(props) {
        return`
           <div style="min-height: 200px; width: 500px">
                <div>值：{{selectedUser1}}</div>
                <ui-listtransfer
                    $data={{users1}}
                    field="selectedUser1"
                    value-field="uid"
                >{{userName}}</ui-listtransfer>
           </div>
        `;
    }
    data(){
        return{
            selectedUser1: [2,3],
            users1: [
                { uid: 1, userName: "yang lei", company: "swust" },
                { uid: 2, userName: "张三", company: "阿里" },
                { uid: 3, userName: "李四", company: "华为" },
                { uid: 4, userName: "王武", company: "腾讯" },
                { uid: 5, userName: "fieldyang", company: "京东" },
                { uid: 6, userName: "路人甲", company: "google" }
            ],
        }
    }
}