export const EXCONFIG = {
    name:'Listtransfer',
    desc:'使用ui-listtransfer组件进行设置',
    listtransferAttrs:[{
        name:'$data',
        desc:'列表数据数组',
        type:'json array',
        values:'-',
        defaults:'-'
    },{
        name:'field',
        desc:'绑定父模块字段数组',
        type:'array',
        values:'-',
        defaults:'-'
    },{
        name:'value-field',
        desc:'值字段名，为$data属性',
        type:'string',
        values:'-',
        defaults:'-'
    },{
        name:'disable-field',
        desc:'禁用字段名，若此$data中此字段名值为true，则禁止穿梭',
        type:'string',
        values:'-',
        defaults:'-'
    }],

    examples:[{
        title:'基本应用，设置field,$data,value-field',
        name:'ex1'
    },{
        title:'禁止项',
        desc:'通过说明禁用字段名以及在$data中设置disable禁止穿梭',
        name:'ex2'
    }]
}