import{Module} from '/dist/nodomui.min.js'
import { ExModule } from '../exmodule.js'
import { AttrGrid } from '../attrgrid.js'
import { EventGrid } from '../eventgrid.js'
import { MethodGrid } from '../methodgrid.js'
import{EXCONFIG} from './config.js'
import{checkboxex1} from './ex1.js'
import{checkboxex2} from './ex2.js'
import {checkboxex3} from "./ex3.js";


export class MCheckbox extends Module{
    modules=[ExModule,AttrGrid,EventGrid,MethodGrid,checkboxex1,checkboxex2,checkboxex3];
    template(props){
        return `
            <div class='--component'>
                <h1>Checkbox 复选框</h1>
                <div class='--component-desc'>
                    {{desc}}
                </div>
                <h2>例子</h2>
                <div x-repeat={{examples}} index='idx'>
                    <exmodule $desc={{desc}} $title={{(idx+1) + ' '  + title}} $imp={{imp}} $exname={{name}} expath='checkbox'/>
                </div>
        
                <h2>属性</h2>
                <h3>UI{{name}}/ui-{{name.toLowerCase()}}</h3>
                <AttrGrid $rows={{checkboxAttrs}} />
               
                <h2>事件</h2>
                <h3>UI{{name}}/ui-{{name.toLowerCase()}}</h3>
                <EventGrid $rows={{checkboxEvents}} />
            </div>
        `
    }
    data(){
        return EXCONFIG;
    }
}