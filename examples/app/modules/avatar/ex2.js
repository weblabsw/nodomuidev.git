import { Module } from '/dist/nodomui.min.js'

export class avatarex2 extends Module {
    template(props) {
        return `
                <div>
                    <div style="display:flex;justify-content:space-between;align-items:center;">
                        <ui-avatar icon="user"/>
                        <ui-avatar src="/examples/app/modules/avatar/avatar.png"/>
                        <ui-avatar >USER</ui-avatar>
                    </div>
                </div>
            `;
    }
}