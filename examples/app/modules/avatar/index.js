import { Module } from '/dist/nodomui.min.js'
import { ExModule } from '../exmodule.js'
import { AttrGrid } from '../attrgrid.js'
import { EventGrid } from '../eventgrid.js'
import { MethodGrid } from '../methodgrid.js'

import { EXCONFIG } from './config.js'
import { avatarex1 } from './ex1.js'
import { avatarex2 } from './ex2.js'

export class MAvatar extends Module {
    modules = [ExModule, AttrGrid, EventGrid, MethodGrid, avatarex1, avatarex2];

    template() {
        return `
            <div class='--component'>
                <h1>Avatar 头像展示</h1>
                <div class='--component-desc'>
                    {{desc}}
                </div>
                <h2>例子</h2>
                <div x-repeat={{examples}} index='idx'>
                    <exmodule $desc={{desc}} $title={{(idx+1) + ' '  + title}} $imp={{imp}} $exname={{name}} expath='avatar'/>
                </div>
        
                <h2>属性</h2>
                <h3>UI{{name}}/ui-{{lowerCaseName}}</h3>
                <AttrGrid $rows={{popoverAttrs}} />
                <h3>注意：对于icon属性，写属性但不传值才有对应的默认值</h3>
            </div>`
    }

    data() {
        return EXCONFIG;
    }
}