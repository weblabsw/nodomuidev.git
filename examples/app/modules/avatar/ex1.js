import { Module } from '/dist/nodomui.min.js'

export class avatarex1 extends Module {
    template(props) {
        return `
                <div>
                    <div style="display:flex;justify-content:space-between;align-items:center;">
                        <ui-avatar icon="user" shape="circle" />
                        <ui-avatar icon="user" size="small" shape="circle" />
                        <ui-avatar icon="user" size="normal" shape="circle" />
                        <ui-avatar icon="user" size="big" shape="circle" />
                        
                        <ui-avatar icon="user" shape="square"/>
                        <ui-avatar icon="user" size="small" shape="square"/>
                        <ui-avatar icon="user" size="normal" shape="square"/>
                        <ui-avatar icon="user" size="big" shape="square"/>
                    </div>
                </div>

            `;
    }
}