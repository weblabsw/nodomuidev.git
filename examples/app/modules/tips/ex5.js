import { Module, nuitip } from '/dist/nodomui.min.js'
export class tipsex5 extends Module {
    template(props) {
        return `
            <div>
                <ui-button e-click='showtip5' title="不同theme主题显示"></ui-button>
                <ui-tip />
            </div>
        `;
    }
    data() {
        return {
            index: 1
        }
    }

    showtip5(model) {
        let themes = ['active', 'success', 'warn', 'error'];
        nuitip({
            text: 'hello left' + model.index++,
            position: 'left',
            closable: true,
            icon: 'insurance',
            theme: themes[model.index % 4],
        });

    }
}