import { Module, nuitip } from '/dist/nodomui.min.js'
export class tipsex3 extends Module {
    template(props) {
        return `
            <div>
                <ui-button e-click='showtip3' title="关闭按钮显示"></ui-button>
                <ui-tip />
            </div>
        `;
    }
    data() {
        return {
            index: 1
        }
    }

    showtip3(model) {
        nuitip({
            text: 'hello' + model.index++,
            closable: true
        });
    }
}