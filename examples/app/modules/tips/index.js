import { Module } from '/dist/nodomui.min.js'
import { ExModule } from '../exmodule.js'
import { AttrGrid } from '../attrgrid.js'
import { EventGrid } from '../eventgrid.js'
import { MethodGrid } from '../methodgrid.js'
import { EXCONFIG } from './config.js'
import { tipsex1 } from './ex1.js'
import { tipsex2 } from './ex2.js'
import { tipsex3 } from './ex3.js'
import { tipsex4 } from './ex4.js'
import { tipsex5 } from './ex5.js'
import { tipsex6 } from './ex6.js'


export class MTip extends Module {
    modules = [ExModule, AttrGrid, EventGrid, MethodGrid, tipsex1, tipsex2, tipsex3, tipsex4, tipsex5, tipsex6];
    template(props) {
        return `
            <div class='--component'>
                <h1>Tip 消息提示</h1>
                <div class='--component-desc'>
                    {{desc}}
                </div>
                <h2>例子</h2>
                <div x-repeat={{examples}} index='idx'>
                    <exmodule $desc={{desc}} $title={{(idx+1) + ' '  + title}} $imp={{imp}} $exname={{name}} expath='tips'/>
                </div>
        

                <h2>方法</h2>
                <h3>设置uitip方法使用</h3>
                <MethodGrid $rows={{tipMethods}} />    
            </div>
            
            <ui-tip />
        `;
    }
    data() {
        return EXCONFIG;
    }
}