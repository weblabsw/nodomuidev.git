import { Module, nuitip } from '/dist/nodomui.min.js'
export class tipsex2 extends Module {
    template(props) {
        return `
            <div>
                <ui-button e-click='showtip2' title="不同位置随机显示"></ui-button>
                <ui-tip />
            </div>
        `;
    }
    data() {
        return {
            index: 1
        }
    }

    showtip2(model) {
        let position = ['top', 'bottom', 'left', 'right'];
        nuitip({
            text: 'hello' + model.index++,
            position: position[model.index % 4],
        });
    }
}