import { Module } from '/dist/nodomui.min.js'
export class ex1 extends Module {
    template(props) {
        return ` 
        <div>
            <div class='--icon-item' x-repeat={{icons}}>
                <div class='--icon-item-icon'>
                    <b class={{'ui-icon-' + name}} />
                </div>
                <div class='--icon-item-title'>{{'ui-icon-' + name}}</div>
            </div>

            <style scope='this'>
                .--icon-item{
                    width:150px;
                    height:120px;
                    padding:10px;
                    display:inline-block;
                    background-color:#f5f5f5;
                    color:#000;
                    margin:10px;
                    box-sizing:border-box;
                    border-radius:5px;
                    text-align:center;
                    overflow:hidden;
                }

                .--icon-item:hover{
                    background-color:#666;
                    color:#fff;
                }

                .--icon-item .--icon-item-icon{
                    font-size:32px;
                    line-height:50px;
                    height:50px;
                    box-sizing:border-box;
                    transition:font-size ease 0.3s;
                }

                .--icon-item .--icon-item-icon:hover{
                    font-size:64px;
                }
                
                .--icon-item .--icon-item-title{
                    margin-top:10px;
                }
            </style>
        </div>

        `;
    }
    data() {
        return {
            icons: [
                /**箭头相似 */
                { name: 'arrow-right' },
                { name: 'arrow-left' },
                { name: 'double-arrow-right' },
                { name: 'double-arrow-left' },
                { name: 'left-oneway' },
                { name: 'right-oneway' },
                { name: 'right-arrow' },
                { name: 'next' },
                { name: 'flex' },
                { name: 'magnify' },
                { name: 'full-screen' },
                { name: 'exit-fullscreen' },
                { name: 'exchange' },
                /**提示 */
                { name: 'warning' },
                { name: 'warning1' },
                { name: 'help' },
                { name: 'prompt' },
                { name: 'abandon' },
                /**排版 */
                { name: 'leftalign' },
                { name: 'rightalign' },
                { name: 'centeralign' },
                { name: 'topalign' },
                { name: 'bottomalign' },
                { name: 'text' },
                /**文档 */
                { name: 'copy' },
                { name: 'cut' },
                { name: 'edit' },
                { name: 'save' },
                { name: 'backup' },
                { name: 'ppt' },
                { name: 'pdf' },
                { name: 'list' },
                { name: 'excel' },
                { name: 'unknown-file' },
                { name: 'empty-file' },
                { name: 'folder' },
                /**用户、密码 */
                { name: 'user' },
                { name: 'user-manage' },
                { name: 'group' },
                { name: 'unlock' },
                { name: 'password' },
                { name: 'exit' },
                { name: 'login' },

                { name: 'close' },
                { name: 'select' },
                { name: 'cross' },

                { name: 'calendar' },
                { name: 'time' },

                { name: 'scan' },
                { name: 'insurance' },

                { name: 'search' },
                { name: 'eye' },
                { name: 'eyeclose' },

                /**声音 */
                { name: 'music' },
                { name: 'play' },
                { name: 'stop' },
                { name: 'record' },
                { name: 'voice' },
                { name: 'voiceout' },
                { name: 'headset' },


                { name: 'category' },
                { name: 'menu' },
                { name: 'history' },
                { name: 'setting ' },
                { name: 'ashbin' },
                { name: 'qrcode' },
                { name: 'download' },
                { name: 'upload' },
                { name: 'cloud-upload' },
                { name: 'cloud-download' },
                { name: 'home' },
                { name: 'favorite' },
                { name: 'favorite-fill' },
                { name: 'collect' },
                { name: 'collect-fill' },
                { name: 'map' },
                { name: 'printer' },
                { name: 'more' },
                { name: 'more-v' },
                { name: 'link' },
                { name: 'refresh' },
                { name: 'withdraw' },
                { name: 'reset' },
                { name: 'share' },
                { name: 'sizeout' },
                { name: 'sizein' },
                { name: 'attach' },
                { name: 'male' },
                { name: 'female' },
                { name: 'tile' },
                { name: 'target' },
                { name: 'clean' },
                { name: 'check' },

                { name: 'update' },
                { name: 'application' },
                { name: 'usb' },

                { name: 'clear-catch' },
                { name: 'broken-line' },
                { name: 'overview' },

                { name: 'panel' },
                { name: 'report' },

                { name: 'tag' },
                { name: 'remark' },

                { name: 'login' },
                { name: 'return' },

                { name: 'internet' },
                { name: 'server' },

                { name: 'message' },
                { name: 'message1' },
                { name: 'email' },
                { name: 'repair' },
                { name: 'good' },
                { name: 'notgood' },
                { name: 'picture' },
            ]

        }
    }
}