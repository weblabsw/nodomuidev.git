import { Module } from '/dist/nodomui.min.js'
import { ExModule } from '../exmodule.js'
import { AttrGrid } from '../attrgrid.js'
import { EventGrid } from '../eventgrid.js'
import { MethodGrid } from '../methodgrid.js'
import { EXCONFIG } from './config.js'
import { listex1 } from './ex1.js'
import { listex2 } from './ex2.js'
import { listex3 } from './ex3.js'
import { listex4 } from './ex4.js'
import {listex5} from "./ex5.js";

export class MList extends Module {
    modules = [ExModule, AttrGrid, EventGrid, MethodGrid, listex1, listex2, listex3, listex4,listex5];
    template(props) {
        return `
            <div class='--component'>
                <h1>List 列表</h1>
                <div class='--component-desc'>
                    {{desc}}
                </div>
                <h2>例子</h2>
                <div x-repeat={{examples}} index='idx'>
                    <exmodule $desc={{desc}} $title={{(idx+1) + ' '  + title}} $imp={{imp}} $exname={{name}} expath='list'/>
                </div>
        
                <h2>属性</h2>
                <h3>UI{{name}}/ui-{{name.toLowerCase()}}</h3>
                <AttrGrid $rows={{listAttrs}} />

                <h2>事件</h2>
                <h3>UI{{name}}/ui-{{name.toLowerCase()}}</h3>
                <EventGrid $rows={{listEvents}} />

            </div>
        `
    }
    data() {
        return EXCONFIG;
    }
}