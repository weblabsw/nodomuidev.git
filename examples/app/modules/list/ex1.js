import { Module } from '/dist/nodomui.min.js'
export class listex1 extends Module {
    template(props) {
        return `
            <div>
				<div>爱好为：{{hobby}}</div>
				<ui-list
					$data={{hobbies}}
					field="hobby"
					value-field="hid"
					class='list'>
					{{htitle}}
				</ui-list>

                <style>
					.list{
						width:400px;
						height:200px;
					}
				</style>
            </div>
        `;
    }
    data() {
        return {
            hobby: 2,
            hobbies: this.genData(),
        }
    }
    genData() {
        return [
            { hid: 1, htitle: "体育", icon: "message", desc: "体育运动很不错" },
            { hid: 2, htitle: "阅读", icon: "search", desc: "阅读让人睿智" },
            { hid: 3, htitle: "健身", icon: "home", disable: true, desc: "健身让人上瘾" },
            { hid: 4, htitle: "电脑游戏", icon: "user", desc: "好玩可别贪玩" },
            { hid: 5, htitle: "户外运动", icon: "password", desc: "感受大自然，轻呼吸" },
            { hid: 6, htitle: "旅游", icon: "prompt", disable: true, desc: "读万卷书，行万里路" },
        ]
    }
}