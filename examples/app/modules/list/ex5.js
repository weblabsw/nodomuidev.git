import { Module } from '/dist/nodomui.min.js'
export class listex5 extends Module {
    template(props) {
        return `
            <div>
				<ui-list
					field="hobby3"
					$data={{hobbies3}}
					class="list1"
					value-field="hid"
					disable-field='disable'
					onItemClick="clickItem"
					class='list'
					>
					<b class={{"ui-icon-" + icon}}></b>
					<span class='title'>{{htitle}}</span>
					<span class='desc'>{{desc}}</span>
				</ui-list>

                <style>
					.list{
						width:400px;
						height:200px;
					}
                    .title{
						margin:0 5px;
						width:80px;
						display:inline-block;
					}
					.desc{
						width:130px;
						display:inline-block;
					}
				</style>
            </div>
        `;
    }
    data() {
        return {
            hobby3: 2,
            hobbies3: this.genData(),
        }
    }
    clickItem(model, dom, evobj, e) {
        alert('你选择了'+model.desc);
    }

    genData() {
        return [
            { hid: 1, htitle: "体育", icon: "message", desc: "体育运动很不错" },
            { hid: 2, htitle: "阅读", icon: "search", desc: "阅读让人睿智" },
            { hid: 3, htitle: "健身", icon: "home", disable: true, desc: "健身让人上瘾" },
            { hid: 4, htitle: "电脑游戏", icon: "user", desc: "好玩可别贪玩" },
            { hid: 5, htitle: "户外运动", icon: "password", desc: "感受大自然，轻呼吸" },
            { hid: 6, htitle: "旅游", icon: "prompt", disable: true, desc: "读万卷书，行万里路" },
        ]
    }
}