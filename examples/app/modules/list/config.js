export const EXCONFIG = {
    name:'List',
    desc: '使用ui-list进行设置。',
    listAttrs: [{
        name: '$data',
        desc: '列表数据',
        type: 'json array',
        values: '-',
        defaults: '-'
    }, {
        name: 'field',
        desc: '绑定父模块的字段',
        type: 'string',
        values: '-',
        defaults: '-'
    }, {
        name: 'value-field',
        desc: '值字段名',
        type: 'string',
        values: '-',
        defaults: '-'
    }, {
        name: 'disable-field',
        desc: '禁用字段名',
        type: 'string',
        values: '-',
        defaults: '-'
    }],
    listEvents: [{
        name: 'onItemClick',
        desc: '对话框打开时事件',
        params: [
            {name:'model',desc:'dom model'},
            {name:'dom',desc:'改变状态的dom节点'},
            {name:'evObj',desc:'NEvent事件对象'},
            {name:'e',desc:'HTML Event对象'}
        ]
    }],
    examples: [{
        title: '简单应用',
        desc: '单项选择',
        name: 'ex1'
    }, {
        title: '多选',
        desc: '通过multiple设置',
        name: 'ex2'
    }, {
        title: '带禁用项list',
        desc: '通过disable-field设置data数组元素对应字段名，当对应项为true，表示禁用',
        name: 'ex3'
    }, {
        title: '自定义列表项',
        name: 'ex4'
    },{
        title: '点击选项事件',
        imp:'设置了disable无法触发',
        desc:'通过OnItemClick方法设置',
        name:'ex5',
    }]
}