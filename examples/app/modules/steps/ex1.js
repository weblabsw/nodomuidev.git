import { Module } from '/dist/nodomui.min.js'
export class stepsex1 extends Module {
    template(props) {
        return `
            <div>
                <ui-steps 
                    current={{current}} 
                    $data={{steps}}
                    >
                </ui-steps>    

                <ui-button e-click='next' title="下一步"></ui-button>
                <ui-button e-click='reset'title='复原'></ui-button>
            </div>
        `;
    }
    data() {
        return {
            current: 0,
            steps: [
                {
                    title: "Step 1",
                }, {
                    title: "Step 2",
                }, {
                    title: "Step 3"
                },{
                    title: "Step 4"
                }
            ],
        }
    }
    next() {
        this.model['current'] += 1;
    }
    reset(){
        this.model['current'] = 0;
    }
}