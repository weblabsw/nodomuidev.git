import { Module } from '/dist/nodomui.min.js'
export class stepsex4 extends Module {
    template(props) {
        return `
            <div >
                <ui-steps 
                    current={{current}} 
                    $data={{steps}}
                    >
                </ui-steps>    
            </div>
        `;
    }
    data() {
        return {
            current: 1,
            steps: [
                {
                    title: "Step 1",
                    icon: "ui-icon-edit",
                    desc: "This is a description."
                }, {
                    title: "Step 2",
                    icon: "ui-icon-upload",
                    desc: "This is a description."
                }, {
                    title: "Step 3",
                    icon: "ui-icon-download",
                    desc: "This is a description."
                }
            ],
        }
    }
    next() {
        this.model['current'] += 1;
    }
}