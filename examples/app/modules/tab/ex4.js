import{Module} from '/dist/nodomui.min.js'
export class tabex4 extends Module{
    template(props){
        return `
            <ui-tab>
                <ui-tab-item x-repeat={{tabs}} title={{title}} active={{active}} closable={{closable}} >
                    <p>{{content}}</p>
                </ui-tab-item>
                <ui-tab-item title='tab5' closable='true' active='true'>
                    <p style="color: red">this is tab5</p>
                </ui-tab-item>
            </ui-tab>
				`
    }

    data(){
        return {
            index:5,
            tabs:[
                {title:'tab1',content:'这是tab1'},
                {title:'tab2',content:'这是tab2'},
                {title:'tab3',content:'这是tab3',closable:true,active:true},
                {title:'tab4',content:'这是tab4',closable:true}
            ]
        }
    }
}