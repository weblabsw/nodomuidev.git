import{Module} from '/dist/nodomui.min.js'
export class tabex10 extends Module{
    template(props){
        return `
            <ui-tab onTabChange='onTabChange' bgColor='#333' color='#fff' active-bgcolor='#900'>
                <ui-tab-item x-repeat={{tabs}} title={{title}} active={{active}} closable={{closable}}>
                    <p>{{content}}</p>
                </ui-tab-item>
            </ui-tab>
				`
    }

    data(){
        return {
            index:5,
            tabs:[
                {title:'tab1',content:'this is tab1',closable:true},
                {title:'tab2',content:'this is tab2',closable:true},
                {title:'tab3',content:'this is tab3',closable:true},
                {title:'tab4',content:'this is tab4',closable:true}
            ]
        }
    }
    onTabChange(oldTab,newTab){
        if(newTab && oldTab){
            console.log(oldTab,newTab);
            // alert("你更改了tab，查看控制台");
        }
    }
}