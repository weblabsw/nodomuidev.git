import{Module} from '/dist/nodomui.min.js'
class M1 extends Module{
    template(){
        return `<div>hello module1</div>`;
    }
}

class M2 extends Module{
    template(){
        return `<div>hello module2</div>`;
    }
}

class M3 extends Module{
    template(){
        return `<div>{{content}}</div>`;
    }
    data(){
        return{
            content:'MB'
        }
    }

    onInit(model){
        foo();
        function foo(){
            model.content = 'M3' + Math.random();
            setTimeout(foo,5000);
        }
    }
}

export class tabex9 extends Module{
    modules=[M1,M2,M3];
    template(props){
        return `
            <ui-tab bgcolor='#333' color='#fff' active-bgcolor='#888' active-color='#444' style='width:400px;height:300px;'>
                <ui-tab-item x-repeat={{tabs}} title={{title}} active={{active}} closable={{closable}}>
                    <div x-module={{mdl}} />
                </ui-tab-item>
            </ui-tab>
				`
    }

    data(){
        return {
            index:1,
            tabs:[
                {title:'tab1',mdl:'m1'},
                {title:'tab2',mdl:'m2'},
                {title:'tab3',mdl:'m3'}
            ]
        }
    }
}