import{Module} from '/dist/nodomui.min.js'
export class tabex6 extends Module{
    template(props){
        return `
            <div>
                <ui-button title='添加tab' theme='active' e-click='add'/> &nbsp;
                <ui-button title='关闭第二个tab' theme='error' e-click='close2'/>&nbsp;
                <ui-button title='关闭title为tab3的tab' theme='error' e-click='close3'/>&nbsp;
                <ui-button e-click='closeAll' title='关闭所有tab' />
                <br/>
                <ui-tab>
                    <ui-tab-item x-repeat={{tabs}} title={{title}} active={{active}} closable={{closable}}>
                        <p>{{content}}</p>
                    </ui-tab-item>
                    <ui-tab-item title='tab5' closable='true'>
                        <p style="color: red">this is tab5</p>
                    </ui-tab-item>
                </ui-tab>
            </div>
			`;
    }

    data(){
        return {
            index:5,
            tabs:[
                {title:'tab1',content:'这是tab1'},
                {title:'tab2',content:'这是tab2',closable:true},
                {title:'tab3',content:'这是tab3',closable:true},
                {title:'tab4',content:'这是tab4',closable:true}
            ]
        }
    }
    add(){
        this.model.tabs.push({title:'新增tab' + this.model.index,content:'新增tab内容' + this.model.index++,active:true,closable:true});
    }

    close2(){
        this.getModule('UITab').__closeTab(1)
    }

    close3(){
        this.getModule('UITab').__closeTab('tab3')
    }

    closeAll(){
        this.getModule('UITab').__closeAll();
    }
}