
import { Module } from '/dist/nodomui.min.js'
export class selectex5 extends Module {
    template() {
        return `
            <div>
                <label>爱好：</label>
                <ui-select
                    field='hobby4'
                    $data={{hobbies4}}
                    value-field='hid'
                    display-field='htitle'
                    disable-field='disable'
                    list-width='300'
                    list-height='200'
                    allow-empty>
                    <b class={{'ui-icon-' + icon}} />
                    <span class='title'>{{htitle}}</span>
                    <span class='desc'>{{desc}}</span>
                </ui-select>

                <style>
                    .title{
                        margin:0 5px;
                        width:80px;
                        display:inline-block;
                    }
                    .desc{
                        width:130px;
                        display:inline-block;
                    }
                </style>
            </div>
        `
    }

    data() {
        return {
            hobby4: 4,
            hobbies4: this.genData()
        }
    }

    genData() {
        return [
            { hid: 1, htitle: "体育", icon: "message", desc: "体育运动很不错" },
            { hid: 2, htitle: "阅读", icon: "search", desc: "阅读让人睿智" },
            { hid: 3, htitle: "健身", icon: "home", disable: true, desc: "健身让人上瘾", disable: true },
            { hid: 4, htitle: "电脑游戏", icon: "user", desc: "好玩可别贪玩" },
            { hid: 5, htitle: "户外运动", icon: "password", desc: "感受大自然，轻呼吸", disable: false },
            { hid: 6, htitle: "美食", icon: "message", desc: "做一个努力的吃货" },
            { hid: 7, htitle: "旅游", icon: "search", desc: "美景使人流连忘返" }
        ]
    }
}