import {Module,nuiconfirm} from '/dist/nodomui.min.js'

export class messageboxex2 extends Module{
    template(props) {
        return`
            <div>
                <ui-button title='默认confirm对话框' e-click='showConfirm'/>&nbsp;
                <ui-button title='自定义confirm对话框' e-click='showConfirm1'/>
                <ui-messagebox />        
            </div>
        `;
    }
    showConfirm(model){
        nuiconfirm({
            text:'确定要删除吗，删除后无法恢复？',
            icon:'prompt',
            buttons:[
                {
                    text:'确定',
                    theme:'active',
                    callback:()=>{
                        console.log('确定了！');
                    }
                }
            ]
        });
    }
    showConfirm1(model){
        nuiconfirm({
            text:'出现异常，选择操作！',
            icon:'prompt',
            cancelTitle:'退出',
            buttons:[
                {
                    text:'删除',
                    theme:'error',
                    callback:()=>{
                        console.log('已删除');
                    }
                },
                {
                    text:'忽略',
                    theme:'success',
                    callback:()=>{
                        console.log('已忽略');
                    }
                }
            ]
        });
    }
}