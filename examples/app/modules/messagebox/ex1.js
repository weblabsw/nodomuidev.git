import {Module, nuiconfirm, nuialert, nuiprompt, ModuleFactory} from '/dist/nodomui.min.js'
export class messageboxex1 extends Module{
    template(){
        return`
                <div>
                    <ui-button title='alert对话框' e-click='showAlert'/>&nbsp;
                    <ui-button title='默认confirm对话框' e-click='showConfirm'/>&nbsp;
                    <ui-button title='默认prompt对话框' e-click='showPrompt'/>
                    <ui-messagebox />
                </div>
            `;
    }
    data(){
        return {

        }
    }

    showAlert(model){
        nuialert({
            text:'数据已成功保存！'
        });
    }

    showConfirm(model){
        nuiconfirm({
            text:'确定要删除吗，删除后无法恢复？',
            icon:'prompt',
            buttons:[
                {
                    text:'确定',
                    theme:'active',
                    callback:()=>{
                        console.log('已确定');
                    }
                }
            ]
        });
    }
    showPrompt(){
        nuiprompt({
            text:'你觉得nodom好用吗？',
            icon:'prompt',
            cancelTitle:'关闭',
            buttons:[
                {
                    text:'确定',
                    theme:'active',
                    callback:(model)=>{
                        console.log('answer is:',model.answer);
                    }
                }
            ]
        });
    }

}
