import { Module } from '/dist/nodomui.min.js'
import { ExModule } from '../exmodule.js'
import { AttrGrid } from '../attrgrid.js'
import { EventGrid } from '../eventgrid.js'
import { MethodGrid } from '../methodgrid.js'
import { EXCONFIG } from './config.js'
import { fileex1 } from './ex1.js'
import { fileex2 } from './ex2.js'
export class MFile extends Module {
    modules = [ExModule, AttrGrid, EventGrid, MethodGrid, fileex1, fileex2];
    template(props) {
        return `
            <div class='--component'>
                <h1>File 文件上传</h1>
                <div class='--component-desc'>
                    {{desc}}
                </div>
                <h2>例子</h2>
                <div x-repeat={{examples}} index='idx'>
                    <exmodule $desc={{desc}} $title={{(idx+1) + ' '  + title}} $imp={{imp}} $exname={{name}} expath='file'/>
                </div>
        
                <h2>属性</h2>
                <h3>UI{{name}}/ui-{{name.toLowerCase()}}</h3>
                <AttrGrid $rows={{fileAttrs}} />
            </div>
        `;
    }
    data() {
        return EXCONFIG;
    }
}