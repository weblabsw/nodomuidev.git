export const EXCONFIG = {
    name:'file',
    desc: '使用ui-file组件进行设置,值通过$data传入，$data对应数据项必须为数组，这样就可以在file插件添加文件后，直接在model中获得。',
    fileAttrs: [{
        name: 'field',
        desc: '对应数据项名',
        type: 'string',
        values: '-',
        defaults: '-'
    }, {
        name: 'value-field',
        desc: '值数据项名，对应上传后返回的valueField',
        type: 'string',
        values: '-',
        defaults: '-'
    }, {
        name: 'display-field',
        desc: '显示数据项名，对应上传后返回的displayField',
        type: 'string',
        values: '-',
        defaults: '-'
    }, {
        name: 'multiple',
        desc: '是否支持多个文件，设置则表示上传多个文件',
        type: 'boolean',
        values: '-',
        defaults: '-'
    }, {
        name: 'url-field',
        desc: '上传后返回的文件url对应的字段名',
        type: 'string',
        values: '-',
        defaults: '-'
    }, {
        name: 'upload-name',
        desc: '上传名，与服务器接收保持一致',
        type: 'string',
        values: '-',
        defaults: '-'
    }, {
        name: 'upload-url',
        desc: '上传url',
        type: 'string',
        values: '-',
        defaults: '-'
    }, {
        name: 'delete-url',
        desc: '删除url',
        type: 'string',
        values: '-',
        defaults: '-'
    }, {
        name: 'max-count',
        desc: '最大上传数量，multiple设置时有效',
        type: 'number',
        values: '-',
        defaults: '-'
    }, {
        name: 'file-type',
        desc: '上传资源类型，如果为image，上传成功后显示缩略图，displayField为url对应项，否则显示文件名，对应数据项为display-field。',
        type: 'string',
        values: '-',
        defaults: '-'
    }, {
        name: 'width',
        desc: '每个显示项宽度',
        type: 'string',
        values: '-',
        defaults: '-'
    }, {
        name: 'height',
        desc: '每个显示项高度',
        type: 'string',
        values: '-',
        defaults: '-'
    }],

    examples: [{
        title: '简单应用',
        name: 'ex1'
    }, {
        title: '图片 允许多个文件',
        desc: '当多个时，可以设置每个元素的宽和高',
        name: 'ex2'
    }]

}
