import { Module } from '/dist/nodomui.min.js'
export class fileex1 extends Module {
    template(props) {
        return ` 
        <ui-file width='150px' height='50px'
        field='files1'
        value-field="id"
        display-field="fileName"
        url-field="url"
        upload-name="file"
        delete-url="http://localhost:3000/delete"
        upload-url="http://localhost:3000/upload" />
        `;
    }
    data() {
        return {
            // files1:[]
            // files1: [
            //     { id: "2", fileName: "file1", url: "http://localhost:3000/upload/tmp/b18eadf0-b41a-11ec-9c94-4169a31adf6d.jpg" }
            // ]
        }
    }
}