export const EXCONFIG = {
    name: 'Card',
    lowerCaseName: 'card',
    desc: '使用ui-card组件进行设置,将信息聚合在卡片容器中展示。',
    cardAttrs: [{
        name: 'width',
        desc: 'Card 宽度',
        type: 'string',
        values: '-',
        defaults: '-'
    }, {
        name: 'height',
        desc: 'Card 高度',
        type: 'string',
        values: '-',
        defaults: '-'
    }, {
        name: 'shadow',
        desc: 'Card	阴影显示时机',
        type: 'string',
        values: 'always | hover | never',
        defaults: 'always'
    }, {
        name: 'body-style',
        desc: 'body 的 CSS 样式，默认布局方式为flex',
        type: 'string',
        values: '-',
        defaults: '-'
    }],
    examples: [{
        title: '基础用法',
        desc: "卡片包含标题，内容以及操作区域。Card 组件由 header body 和 footer组成。 header 和 footer是可选的，其内容取决于一个命名 slot。",
        name: 'ex1'
    }, {
        title: '简单卡片',
        desc: '卡片可以只有内容区域。',
        name: 'ex2'
    }, {
        title: '自定义body-style的卡片',
        desc: '可配置定义更丰富的内容展示。配置 body-style 属性来自定义 body 部分的样式',
        name: 'ex3'
    }, {
        title: '带有阴影效果的卡片',
        desc: '通过 shadow 属性设置卡片阴影出现的时机。 该属性的值可以是：always、hover 或 never',
        name: 'ex4'
    },
    ]
}