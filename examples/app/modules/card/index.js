import { Module } from '/dist/nodomui.min.js'
import { ExModule } from '../exmodule.js'
import { AttrGrid } from '../attrgrid.js'
import { EventGrid } from '../eventgrid.js'
import { MethodGrid } from '../methodgrid.js'

import { EXCONFIG } from './config.js'
import { cardex1 } from './ex1.js'
import { cardex2 } from './ex2.js'
import { cardex3 } from './ex3.js'
import { cardex4 } from './ex4.js'

export class MCard extends Module {
    modules = [ExModule, AttrGrid, EventGrid, MethodGrid, cardex1, cardex2, cardex3, cardex4];

    template() {
        return `
            <div class='--component'>
                <h1>Card 卡片</h1>
                <div class='--component-desc'>
                    {{desc}}
                </div>
                <h2>例子</h2>
                <div x-repeat={{examples}} index='idx'>
                    <exmodule $desc={{desc}} $title={{(idx+1) + ' '  + title}} $imp={{imp}} $exname={{name}} expath='card'/>
                </div>
        
                <h2>属性</h2>
                <h3>UI{{name}}/ui-{{lowerCaseName}}</h3>
                <AttrGrid $rows={{cardAttrs}} />
            </div>`
    }

    data() {
        return EXCONFIG;
    }
}