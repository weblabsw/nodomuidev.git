import{Module,nuiloading} from '/dist/nodomui.min.js'
export class layoutex1 extends Module{
    template(){
        return `
            <div>
                <ui-layout class='layout' vertical>
                    <ui-layout-top class='header' >顶部</ui-layout-top>
                    <ui-layout-center class='center' >
                        <div>
                         <span>中心</span>
                         <ui-button title='ok'/>
                        </div>    
                    </ui-layout-center>
                </ui-layout>
                
                <style>
                    .layout{
                        height:300px;
                    }
            
                    .layout >*{
                        color:#fff;
                        font-size:16px;
                    }
                    .header{
                        line-height:50px;
                        text-align: center;
                        background-color:#4fc4f7;
                        justify-content: center;
                        align-items: center;
                    }
            
                    .footer{
                        padding-top:5px;
                        justify-content: center;
                        align-items: center;
                        background-color:#4fc4f7;
                        text-align: center;
                    }
            
                    .left{
                        background-color:#81d4f8;
                        width:150px;
                    }
            
                    .right{
                        background-color:#81d4f8;
                        width:200px;
                    }
            
                    .center{
                        background-color:#e1f5fe;
                        color:#222;
                    }
                </style>
            </div>
        `;
    }
}