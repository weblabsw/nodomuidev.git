import { Module } from '/dist/nodomui.min.js'
import { ExModule } from '../exmodule.js'
import { AttrGrid } from '../attrgrid.js'
import { EventGrid } from '../eventgrid.js'
import { MethodGrid } from '../methodgrid.js'
import { EXCONFIG } from './config.js'
import { layoutex1 } from './ex1.js'
import { layoutex2 } from './ex2.js'
import { layoutex3 } from './ex3.js'
import { layoutex4 } from './ex4.js'
import { layoutex5 } from './ex5.js'
import { layoutex6 } from './ex6.js'

export class MLayout extends Module {
    modules = [ExModule, AttrGrid, EventGrid, MethodGrid, layoutex1, layoutex2, layoutex3, layoutex4, layoutex5, layoutex6];
    template(props) {
        return `
            <div class='--component'>
                <h1>Layout 布局</h1>
                <div class='--component-desc'>
                    {{desc}}
                </div>
                <h2>例子</h2>
                <div x-repeat={{examples}} index='idx'>
                    <exmodule $desc={{desc}} $title={{(idx+1) + ' '  + title}} $imp={{imp}} $exname={{name}} expath='layout'/>
                </div>
        
                <h2>属性</h2>
                <h3>UILayout/ui-layout</h3>
                <AttrGrid $rows={{layoutAttrs}} />

                <h3>UILayoutLeft/ui-layout-left</h3>
                <AttrGrid $rows={{layoutLeftAttrs}} />
                
                <h3>UILayoutRight/ui-layout-right</h3>
                <AttrGrid $rows={{layoutRightAttrs}} />

            </div>
        `
    }
    data() {
        return EXCONFIG;
    }
}