import{Module,nuiloading} from '/dist/nodomui.min.js'
export class layoutex5 extends Module{
    template(){
        return `
            <div>
                <ui-layout class='layout'>
                    <ui-layout-left class='left'>左侧</ui-layout-left>
                    <ui-layout vertical>
                        <ui-layout-top class='header'>顶部</ui-layout-top>
                        <ui-layout-center class='center'>中心</ui-layout-center>
                        <ui-layout-bottom class='footer'>底部</ui-layout-bottom>
                    </ui-layout>
                    <ui-layout-right class='right' >右侧</ui-layout-right>
                </ui-layout>
            </div>
        `;
    }
}