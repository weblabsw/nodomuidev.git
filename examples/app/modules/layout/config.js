export const EXCONFIG = {
    desc:'使用ui-layout(布局)，ui-layout-top(顶部布局)，ui-layout-left(左布局)，ui-layout-center(中间布局)，ui-layout-right(右布局)，ui-layout-bottom(底部布局)6个组件进行设置。',
    layoutAttrs:[{
        name:'vertical',
        desc:'是否为纵向分布',
        type:'boolean',
        values:'true/false',
        defaults:'false'
    }],
    layoutLeftAttrs:[{
        name:'sizable',
        desc:'设置可拖动',
        type:'boolean',
        values:'true/false',
        defaults:'false'
    }],
    layoutRightAttrs:[{
        name:'sizable',
        desc:'设置可拖动',
        type:'boolean',
        values:'true/false',
        defaults:'false'
    }],
    examples:[{
        title:'上中布局',
        desc: '通过ui-layout-top和ui-layout-center嵌套在ui-layout进行设置。',
        name:'ex1'
    },{
        title:'上中下布局',
        desc: '通过ui-layout-top，ui-layout-center和ui-layout-bottom嵌套在ui-layout进行设置。',
        name:'ex2'
    },{
        title:'左上中布局',
        desc: '通过ui-layout-left以及ui-layout-top，ui-layout-center嵌套在ui-layout进行设置。',
        name:'ex3'
    },{
        title:'左上中下布局',
        desc: '通过ui-layout-left以及ui-layout-top，ui-layout-center，ui-layout-bottom嵌套在ui-layout进行设置。',
        name:'ex4'
    },{
        title:'左上中下右布局',
        desc: '通过ui-layout-left和ui-layout-top，ui-layout-center，ui-layout-bottom嵌套在ui-layout以及ui-layout-right进行设置。',
        name:'ex5'
    },{
        title:'上左中右下布局',
        desc: '通过ui-layout-top和ui-layout-left，ui-layout-center，ui-layout-right嵌套在ui-layout以及ui-layout-bottom进行设置。',
        name:'ex6'
    }]
}