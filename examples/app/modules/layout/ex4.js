import{Module,nuiloading} from '/dist/nodomui.min.js'
export class layoutex4 extends Module{
    template(){
        return `
            <div>
                <ui-layout class='layout'>
                    <ui-layout-left class='left' sizable>左侧</ui-layout-left>
                    <ui-layout vertical>
                        <ui-layout-top class='header'>顶部</ui-layout-top>
                        <ui-layout-center class='center'>中心</ui-layout-center>
                        <ui-layout-bottom class='footer'>底部</ui-layout-bottom>
                    </ui-layout>
                </ui-layout>
            </div>
        `;
    }
}