
import { Module } from '/dist/nodomui.min.js'
export class sliderex4 extends Module {
    template(props) {
        return `
            <div style="width:200px;height:300px">
                <span>拖动进度：{{outValue}}</span>
                 <ui-slider  field='outValue'  height={{height}}  
                 vertical min={{min}} max={{max}} style="left:20px;top:20px"  show-step={{true}} step={{10}}/>
            </div>
        `;
    }
    data() {
        return {
            outValue: 40,
            height: 200,
            min: 0,
            max: 100
        }
    }

}