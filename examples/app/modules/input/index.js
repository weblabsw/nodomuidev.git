import{Module} from '/dist/nodomui.min.js'
import { ExModule } from '../exmodule.js'
import { AttrGrid } from '../attrgrid.js'
import { EventGrid } from '../eventgrid.js'
import { MethodGrid } from '../methodgrid.js'
import{EXCONFIG} from './config.js'
import{inputex1} from './ex1.js'
import{inputex2} from './ex2.js'
import{inputex3} from './ex3.js'
import{inputex4} from './ex4.js'
import{inputex5} from './ex5.js'
import{inputex6} from './ex6.js'
import{inputex7} from './ex7.js'


export class MInput extends Module{
    modules=[ExModule,AttrGrid,EventGrid,MethodGrid,inputex1,inputex2,inputex3,inputex4,inputex5,inputex6,inputex7];
    template(props){
        return `
            <div class='--component'>
                <h1>Input 输入框</h1>
                <div class='--component-desc'>
                    {{desc}}
                </div>
                <h2>例子</h2>
                <div x-repeat={{examples}} index='idx'>
                    <exmodule $desc={{desc}} $title={{(idx+1) + ' '  + title}} $imp={{imp}} $exname={{name}} expath='input'/>
                </div>
        
                <h2>属性</h2>
                <h3>UI{{name}}/ui-{{name.toLowerCase()}}</h3>
                <AttrGrid $rows={{inputAttrs}} />

            </div>
        `
    }
    data(){
        return EXCONFIG;
    }
}