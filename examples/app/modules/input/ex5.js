import {Module} from "/dist/nodomui.min.js";
export class inputex5 extends Module{
    template(props) {
        return`
            <div>
                <ui-input type='text' field='f5' >
                        <slot name='before'><div class='slot before'><b class='ui-icon-search' />http://</div></slot>
                        <slot name='after'><div class='slot after'>.com</div></slot>
                </ui-input>
                <style>
                    .slot{
                        width:80px;
                        height:100%;
                        display:flex;
                        flex:1;
                        justify-content: center;
                        align-items: center;
                    }
                
                    .before{
                        width:100px;
                        background-color:#ccc;
                        color:white;
                        border-top-left-radius:5px;
                        border-bottom-left-radius:5px;
                    }
                
                    .after{
                        border-top-right-radius:5px;
                        border-bottom-right-radius:5px;
                        background-color:blue;
                        color:white;
                    }
                </style>
            </div>
        `;
    }
    data(){
        return{
            f5:'www.nodom.cn/webroute/home'
        }
    }
}