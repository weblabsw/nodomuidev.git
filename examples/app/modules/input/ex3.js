import {Module} from "/dist/nodomui.min.js";
export class inputex3 extends Module{
    template(props) {
        return`
              <ui-input field='f3' allow-clear />
        `;
    }
    data(){
        return{
            f3:'noomi'
        }
    }
}