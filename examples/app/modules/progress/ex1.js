import { Module } from "/dist/nodomui.min.js";
export class progressex1 extends Module {
    template() {
        return `
          <div style='padding:15px'>
            <div style='width:300px'>
                <ui-progress percent={{percent}}/>
            </div>
            <ui-button e-click="subPercent" title='-' />
              <ui-button e-click="addPercent" title='+' />
          </div>
        `
    }
    data() {
        return {
            percent: 10,
        }
    }

    addPercent(model, dom) {
        console.log(this.model.percent);
        this.model.percent += 3;
    }

    subPercent(model, dom) {
        console.log(this.model.percent);
        this.model.percent -= 3;
    }
}
