import { Module } from '/dist/nodomui.min.js'
import { ExModule } from '../exmodule.js'
import { AttrGrid } from '../attrgrid.js'
import { EventGrid } from '../eventgrid.js'
import { MethodGrid } from '../methodgrid.js'
import { EXCONFIG } from './config.js'
import { progressex1 } from './ex1.js'
import { progressex2 } from './ex2.js'
import { progressex3 } from './ex3.js'
import { progressex4 } from './ex4.js'
import { progressex5 } from './ex5.js'
import { progressex6 } from './ex6.js'

export class MProgress extends Module {
    modules = [ExModule, AttrGrid, EventGrid, MethodGrid, progressex1, progressex2, progressex3, progressex4, progressex5, progressex6];
    template(props) {
        return `
            <div class='--component'>
                <h1>Progress 进度条</h1>
                <div class='--component-desc'>
                    {{desc}}
                </div>
                <h2>例子</h2>
                <div x-repeat={{examples}} index='idx'>
                    <exmodule $desc={{desc}} $title={{(idx+1) + ' '  + title}} $imp={{imp}} $exname={{name}} expath='progress'/>
                </div>
        
                <h2>属性</h2>
                <h3>UI{{name}}/ui-{{name.toLowerCase()}}</h3>
                <AttrGrid $rows={{progressAttrs}} />
            </div>
        `
    }
    data() {
        return EXCONFIG;
    }
}