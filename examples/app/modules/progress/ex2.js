import { Module } from "/dist/nodomui.min.js";
export class progressex2 extends Module {
    template() {
        return `
          <div style='padding:15px'>
            <div style='width:300px'>
                <ui-progress size='small'  percent={{percent}} style='margin-bottom: 10px'/>
                <ui-progress size='normal' percent={{percent}} style='margin-bottom:10px'/>
                <ui-progress size='large'  percent={{percent}} style='margin-bottom: 10px'/>
            </div>
            <ui-button e-click="subPercent" title='-' />
              <ui-button e-click="addPercent" title='+' />
          </div>
        `
    }
    data() {
        return {
            percent: 10,
        }
    }

    addPercent(model, dom) {
        console.log(this.model.percent);
        this.model.percent += 3;
    }

    subPercent(model, dom) {
        console.log(this.model.percent);
        this.model.percent -= 3;
    }
}
