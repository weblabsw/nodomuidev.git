import {Module} from '/dist/nodomui.min.js'
import { MUINavgator } from './uinavigator.js';

export class MUIIndex extends Module {
    modules=[MUINavgator]
    template(){
        return `
            <div>
              <ui-layout class='--layout'>
                <ui-layout-left style='width:300px'>
                    <MUINavgator/>
                </ui-layout-left>
                <ui-layout-center>
                    <div x-router role='router'></div>
                </ui-layout-center>
             </ui-layout>
             <ui-messagebox />
             <ui-tip />
             <ui-loading />
            </div>
        `;
    }
    
    onInit(){
        
    }

    
}

// window.addEventListener("beforeunload", (event) => {
//     const path = location.pathname;
//     if(path.startsWith('/uirouter')){
//         // Cancel the event as stated by the standard.
//         event.preventDefault();
//         // Chrome requires returnValue to be set.
//         event.returnValue = "";
//         const url = '/examples/index.html#' + path;
//         location.href = url;
//     }
// });

window.addEventListener('load',(e)=>{
    let hash = location.hash;
        if(hash){
            Router.go(hash.substring(1));
        }
})