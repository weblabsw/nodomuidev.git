nodomui是基于nodom3.0的一套为开发者、设计者和产品经理准备的前端组件库。

# 源码

gitee:https://gitee.com/weblabsw/nodomuidev.git

### npm 安装及编译

1. 安装依赖包后，安装nodom3，在命令行执行：

```shell
npm i
npm install nodom3
```

2. 开始编译，生成nodomui.js及相关文件，在命令行执行：

生成nodomui.*.js文件

```
npm run build
```

生成nodom.css文件

```
npm run sass
```

# 文件结构

1. 组件源码目录：`./plugins`。

2. 组件样式远嘛目录：`./sass`。

编译js文件目录：

- 开发环境：`./dist/nodomui.js`
- 生产环境：`./dist/nodomui.min.js`

编译css文件目录：

- 开发环境`./example/css/nodomui.css`。
- 生产环境`./example/css/nodomui.min.css`。

实例所在目录：`./examples`，其中html文件展示组件效果及属性方法介绍，具体实现方式参考其子目录`./app`的组件名文件夹，example入口文件在 `./examples/index.html`。


### 版本
版本信息参考[版本信息](./update.md)。

# 引入

引入完整的`nodomui.js`，使用ES Module实现模块化，此外还需引入组件相关css文件`nodomui.css`，使用方式如下：

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <title>nodomui example - grid usage</title>
    <link rel="stylesheet" type="text/css" href="css/nodomui.css" />
</head>
<body>
</body>
<script type='module'>
    import{Nodom,Module} from '/dist/nodomui.js'
    class gridModule extends Module{
      //your code
    }	
    Nodom.app(gridModule);
</script>
</html>
```

# 简单介绍

**注意**：建议拥有html，css及js基础，此外需要了解一定的[nodom](https://gitee.com/weblabsw/nodom3)相关知识。

接下来，你可以通过标签的形式使用nodomui的组件，如：`<ui-button />`来使用按钮组件。nodomui的其他组件的使用，也都是通过使用`ui`开头的标签。

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <title>nodomui example - button usage</title>
    <link rel="stylesheet" type="text/css" href="css/nodomui.css" />
</head>
<body>
    <div></div>
</body>
<script type='module'>
    import{Nodom,Module} from '/dist/nodomui.js'
    class buttonModule extends Module {
        template(props) {
            return `
                <div>
                    <ui-button size="small"  title='small' /> 
                    <ui-button size="normal"  title='normal' />
                    <ui-button size="large"  title='large' />
                </div>
            `;
        }
    }
    Nodom.app(buttonModule,'div');
</script>
</html>
```

## 开始使用

至此，一个基于nodom和 nodomui 的开发环境已经搭建完毕，现在就可以编写代码了。各个组件的使用方法及相关属性，请运行并参阅说明文档--`./examples/index.html`。