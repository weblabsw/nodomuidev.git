# 插件

## 命名规范

### 类名
以“UI”开头，插件名单词首字母大写，如UIGrid，UIGridCol；

### 配置项
单词全部小写，如果为单词组合，以“-”进行分隔；

### 事件
事件不分隔，如：onitemclick，onclick，onrowclick等

### 插件内成员方法和属性
以“__”开头，单词采用驼峰结构，需标明 private和public

## 注意问题
1. 模板串不要由数据项来构建，避免数据改变导致的模板串改变，从而导致编译
2. 由用户提供的非常规事件（非html标准事件），通过“on***”的方式传递到插件，在插件中进行调用，尽量保证nodom事件参数一致性，某些事件确实无法保持，可自行设定

# 重新生成api文档所需步骤：
1. npm run build (生成d.ts文件)
2. api-extractor run  (生成api相关及json文件)
3. api-documenter markdown -i "./etc" -o "./api"  (根据json文档生成md文件，其中 ./etc为json存放地 ./api为md文件存放地)

## 注意
1. 新写组件如：slider,rating等，现在注释为@beta 代表：最终打算由第三方开发者使用，但尚未发布。该工具可能会从公开发布中删除声明。如确认发布，可改为@public
2. 尚未完成组件及工具类 如：colorPicker及color，现在注释为:@alpha 代表：API不应在生产中使用，该工具可能会从公开版本中删除声明，完成后可改为@public或@beta