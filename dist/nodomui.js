/**
 * 自定义元素管理器
 *
 * @remarks
 * 所有自定义元素需要添加到管理器才能使用
 */
class DefineElementManager {
    /**
     * 添加自定义元素
     * @param clazz -   自定义元素类或类数组
     */
    static add(clazz) {
        if (Array.isArray(clazz)) {
            for (const c of clazz) {
                this.elements.set(c.name.toUpperCase(), c);
            }
        }
        else {
            this.elements.set(clazz.name.toUpperCase(), clazz);
        }
    }
    /**
     * 获取自定义元素类
     * @param tagName - 元素名
     * @returns         自定义元素类
     */
    static get(tagName) {
        return this.elements.get(tagName.toUpperCase());
    }
    /**
     * 是否存在自定义元素
     * @param tagName - 元素名
     * @returns         存在或不存在
     */
    static has(tagName) {
        return this.elements.has(tagName.toUpperCase());
    }
}
/**
 * 自定义元素集合
 */
DefineElementManager.elements = new Map();

/**
 * 指令类型
 */
class DirectiveType {
    /**
     * 构造方法
     * @param name -    指令类型名
     * @param handle -  渲染时执行方法
     * @param prio -    类型优先级
     */
    constructor(name, handler, prio) {
        this.name = name;
        this.prio = prio >= 0 ? prio : 10;
        this.handler = handler;
    }
}

/**
 * 指令管理器
 */
class DirectiveManager {
    /**
     * 增加指令映射
     * @param name -    指令类型名
     * @param handle -  渲染处理函数
     * @param prio -    类型优先级
     */
    static addType(name, handler, prio) {
        this.directiveTypes.set(name, new DirectiveType(name, handler, prio));
    }
    /**
     * 移除指令映射
     * @param name -    指令类型名
     */
    static removeType(name) {
        this.directiveTypes.delete(name);
    }
    /**
     * 获取指令
     * @param name -    指令类型名
     * @returns         指令类型或undefined
     */
    static getType(name) {
        return this.directiveTypes.get(name);
    }
    /**
     * 是否含有某个指令
     * @param name -    指令类型名
     * @returns         true/false
     */
    static hasType(name) {
        return this.directiveTypes.has(name);
    }
}
/**
 * 指令映射
 */
DirectiveManager.directiveTypes = new Map();

/******************************************************************************
Copyright (c) Microsoft Corporation.

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
PERFORMANCE OF THIS SOFTWARE.
***************************************************************************** */

function __awaiter(thisArg, _arguments, P, generator) {
  function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
  return new (P || (P = Promise))(function (resolve, reject) {
      function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
      function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
      function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
      step((generator = generator.apply(thisArg, _arguments || [])).next());
  });
}

typeof SuppressedError === "function" ? SuppressedError : function (error, suppressed, message) {
  var e = new Error(message);
  return e.name = "SuppressedError", e.error = error, e.suppressed = suppressed, e;
};

/*
 * 英文消息文件
 */
const NodomMessage_en = {
    /**
     * tip words
     */
    TipWords: {
        application: "Application",
        system: "System",
        module: "Module",
        clazz: "类",
        moduleClass: 'ModuleClass',
        model: "Model",
        directive: "Directive",
        directiveType: "Directive-type",
        expression: "Expression",
        event: "Event",
        method: "Method",
        filter: "Filter",
        filterType: "Filter-type",
        data: "Data",
        dataItem: 'Data-item',
        route: 'Route',
        routeView: 'Route-container',
        plugin: 'Plugin',
        resource: 'Resource',
        root: 'Root',
        element: 'VirtualDom'
    },
    /**
     * error info
     */
    ErrorMsgs: {
        unknown: "unknown error",
        uninit: "{0}未初始化",
        paramException: "{0} '{1}' parameter error，see api",
        invoke: "method {0} parameter {1} must be {2}",
        invoke1: "method {0} parameter {1} must be {2} or {3}",
        invoke2: "method {0} parameter {1} or {2} must be {3}",
        invoke3: "method {0} parameter {1} not allowed empty",
        exist: "{0} is already exist",
        exist1: "{0} '{1}' is already exist",
        notexist: "{0} is not exist",
        notexist1: "{0} '{1}' is not exist",
        notupd: "{0} not allow to change",
        notremove: "{0} not allow to delete",
        notremove1: "{0} {1} not allow to delete",
        namedinvalid: "{0} {1} name error，see name rules",
        initial: "{0} init parameter error",
        jsonparse: "JSON parse error",
        timeout: "request overtime",
        config: "{0} config parameter error",
        config1: "{0} config parameter '{1}' error",
        itemnotempty: "{0} '{1}' config item '{2}' not allow empty",
        itemincorrect: "{0} '{1}' config item '{2}' error",
        needEndTag: "element {0} is not closed",
        needStartTag: "without start tag matchs {0}",
        tagError: "element {0} error",
        wrongTemplate: "wrong template",
        wrongExpression: "expression error: {0} "
    },
    WeekDays: ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"]
};

/*
 * 中文消息文件
 */
const NodomMessage_zh = {
    /**
     * 提示单词
     */
    TipWords: {
        application: "应用",
        system: "系统",
        module: "模块",
        clazz: "类",
        moduleClass: '模块类',
        model: "模型",
        directive: "指令",
        directiveType: "指令类型",
        expression: "表达式",
        event: "事件",
        method: "方法",
        filter: "过滤器",
        filterType: "过滤器类型",
        data: "数据",
        dataItem: '数据项',
        route: '路由',
        routeView: '路由容器',
        plugin: '插件',
        resource: '资源',
        root: '根',
        element: '元素'
    },
    /**
     * 异常信息
     */
    ErrorMsgs: {
        unknown: "未知错误",
        uninit: "{0}未初始化",
        paramException: "{0}'{1}'方法参数错误，请参考api",
        invoke: "{0} 方法参数 {1} 必须为 {2}",
        invoke1: "{0} 方法参数 {1} 必须为 {2} 或 {3}",
        invoke2: "{0} 方法参数 {1} 或 {2} 必须为 {3}",
        invoke3: "{0} 方法参数 {1} 不能为空",
        exist: "{0} 已存在",
        exist1: "{0} '{1}' 已存在",
        notexist: "{0} 不存在",
        notexist1: "{0} '{1}' 不存在",
        notupd: "{0} 不可修改",
        notremove: "{0} 不可删除",
        notremove1: "{0} {1} 不可删除",
        namedinvalid: "{0} {1} 命名错误，请参考用户手册对应命名规范",
        initial: "{0} 初始化参数错误",
        jsonparse: "JSON解析错误",
        timeout: "请求超时",
        config: "{0} 配置参数错误",
        config1: "{0} 配置参数 '{1}' 错误",
        itemnotempty: "{0} '{1}' 配置项 '{2}' 不能为空",
        itemincorrect: "{0} '{1}' 配置项 '{2}' 错误",
        needEndTag: "{0} 标签未闭合",
        needStartTag: "未找到与 {0} 匹配的开始标签",
        tagError: "标签 {0} 错误",
        wrongTemplate: "模版格式错误",
        wrongExpression: "表达式 {0} 错误"
    },
    WeekDays: ["星期日", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六"]
};

/**
 * 模块工厂
 * @remarks
 * 管理所有模块类、模块实例
 */
class ModuleFactory {
    /**
     * 添加模块实例到工厂
     * @param item -  模块对象
     */
    static add(item) {
        // 第一个为主模块
        if (this.modules.size === 0) {
            this.mainModule = item;
        }
        this.modules.set(item.id, item);
        //添加模块类
        this.addClass(item.constructor);
    }
    /**
     * 获得模块
     * @remarks
     * 当name为id时，则获取对应id的模块
     *
     * 当name为字符串时，表示模块类名
     *
     * 当name为class时，表示模块类
     *
     * @param name -  类或实例id
     */
    static get(name) {
        const tp = typeof name;
        let mdl;
        if (tp === 'number') { //数字，模块id
            return this.modules.get(name);
        }
        else {
            if (tp === 'string') { //字符串，模块类名
                name = name.toLowerCase();
                if (!this.classes.has(name)) { //为别名
                    name = this.aliasMap.get(name);
                }
                if (this.classes.has(name)) {
                    mdl = Reflect.construct(this.classes.get(name), [++this.moduleId]);
                }
            }
            else { //模块类
                mdl = Reflect.construct(name, [++this.moduleId]);
            }
            if (mdl) {
                mdl.init();
                return mdl;
            }
        }
    }
    /**
     * 是否存在模块类
     * @param clazzName -   模块类名
     * @returns     true/false
     */
    static hasClass(clazzName) {
        const name = clazzName.toLowerCase();
        return this.classes.has(name) || this.aliasMap.has(name);
    }
    /**
     * 添加模块类
     * @param clazz -   模块类
     * @param alias -   别名
     */
    static addClass(clazz, alias) {
        //转换成小写
        const name = clazz.name.toLowerCase();
        if (this.classes.has(name)) {
            return;
        }
        this.classes.set(name, clazz);
        //添加别名
        if (alias) {
            this.aliasMap.set(alias.toLowerCase(), name);
        }
    }
    /**
     * 获取模块类
     * @param name -    类名或别名
     * @returns         模块类
     */
    static getClass(name) {
        name = name.toLowerCase();
        return this.classes.has(name) ? this.classes.get(name) : this.classes.get(this.aliasMap.get(name));
    }
    /**
     * 加载模块
     * @remarks
     * 用于实现模块懒加载
     * @param modulePath -   模块类路径
     * @returns              模块类
     */
    static load(modulePath) {
        return __awaiter(this, void 0, void 0, function* () {
            const m = yield import(modulePath);
            if (m) {
                //通过import的模块，查找模块类
                for (const k of Object.keys(m)) {
                    if (m[k].name) {
                        this.addClass(m[k]);
                        return m[k];
                    }
                }
            }
        });
    }
    /**
     * 从工厂移除模块
     * @param id -    模块id
     */
    static remove(id) {
        this.modules.delete(id);
    }
    /**
     * 设置应用主模块
     * @param m - 	模块
     */
    static setMain(m) {
        this.mainModule = m;
    }
    /**
     * 获取应用主模块
     * @returns 	应用的主模块
     */
    static getMain() {
        return this.mainModule;
    }
}
/**
 * 模块对象集合
 * @remarks
 * 格式为map，其中：
 *
 * key: 模块id
 *
 * value: 模块对象
 */
ModuleFactory.modules = new Map();
/**
 * 模块类集合
 * @remarks
 * 格式为map，其中：
 *
 *  key:    模块类名或别名
 *
 *  value:  模块类
 */
ModuleFactory.classes = new Map();
/**
 * 别名map
 * @remarks
 * 格式为map，其中：
 *
 * key:     别名
 *
 * value:   类名
 */
ModuleFactory.aliasMap = new Map();
/**
 * 模块id自增量
 */
ModuleFactory.moduleId = 0;

/**
 * 表达式类
 * @remarks
 * 表达式中的特殊符号
 *
 *  this:指向渲染的module
 *
 *  $model:指向当前dom的model
 */
class Expression {
    /**
     * @param exprStr -	表达式串
     */
    constructor(exprStr) {
        this.id = Util.genId();
        if (!exprStr || (exprStr = exprStr.trim()) === '') {
            return;
        }
        if (Nodom.isDebug) {
            this.exprStr = exprStr;
        }
        const funStr = this.compile(exprStr);
        this.execFunc = new Function('$model', 'return ' + funStr);
    }
    /**
     * 编译表达式串，替换字段和方法
     * @param exprStr -   表达式串
     * @returns         编译后的表达式串
     */
    compile(exprStr) {
        //字符串，object key，有效命名(函数或字段)
        const reg = /('[\s\S]*?')|("[\s\S]*?")|(`[\s\S]*?`)|([a-zA-Z$_][\w$]*\s*?:)|((\.{3}|\.)?[a-zA-Z$_][\w$]*(\.[a-zA-Z$_][\w$]*)*(\s*[\[\(](\s*\))?)?)/g;
        let r;
        let retS = '';
        let index = 0; //当前位置
        while ((r = reg.exec(exprStr)) !== null) {
            let s = r[0];
            if (index < r.index) {
                retS += exprStr.substring(index, r.index);
            }
            if (s[0] === "'" || s[0] === '"' || s[0] === '`') { //字符串
                retS += s;
            }
            else {
                const lch = s[s.length - 1];
                if (lch === ':') { //object key
                    retS += s;
                }
                else if (lch === '(' || lch === ')') { //函数，非内部函数
                    retS += handleFunc(s);
                }
                else { //字段 this $model .field等不做处理
                    if (s.startsWith('this.')
                        || Util.isKeyWord(s)
                        || (s[0] === '.' && s[1] !== '.')
                        || s === '$model') { //非model属性
                        retS += s;
                    }
                    else { //model属性
                        let s1 = '';
                        if (s.startsWith('...')) { // ...属性名
                            s1 = '...';
                            s = s.substring(3);
                        }
                        retS += s1 + '$model.' + s;
                    }
                }
            }
            index = reg.lastIndex;
        }
        if (index < exprStr.length) {
            retS += exprStr.substring(index);
        }
        return retS;
        /**
         * 处理函数串
         * @param str -   源串
         * @returns     处理后的串
         */
        function handleFunc(str) {
            //去除空格
            str = str.replace(/\s+/g, '');
            const ind1 = str.lastIndexOf('(');
            const ind2 = str.indexOf('.');
            //第一段
            const fn1 = (ind2 !== -1 ? str.substring(0, ind2) : str.substring(0, ind1));
            //函数名带 .
            if (ind2 !== -1) {
                return str;
            }
            if (ind2 === -1) {
                let s = "this.invokeMethod('" + fn1 + "'";
                s += str[str.length - 1] !== ')' ? ',' : ')';
                return s;
            }
            return '$model.' + str;
        }
    }
    /**
     * 表达式计算
     * @param module -  模块
     * @param model - 	模型
     * @returns 		计算结果
     */
    val(module, model) {
        if (!this.execFunc) {
            return;
        }
        let v;
        try {
            v = this.execFunc.call(module, model);
        }
        catch (e) {
            if (Nodom.isDebug) {
                console.error(new NError("wrongExpression", this.exprStr).message);
                console.error(e);
            }
        }
        return v;
    }
}

/**
 * css 管理器
 * @privateRemarks
 * 针对不同的rule，处理方式不同
 *
 * CssStyleRule 进行保存和替换，同时模块作用域scope有效
 *
 * CssImportRule 路径不重复添加，因为必须加在stylerule前面，所以需要记录最后的import索引号
 */
class CssManager {
    /**
     * 处理style 元素
     * @param module -  模块
     * @param dom -     虚拟dom
     * @returns         如果是styledom，则返回true，否则返回false
     */
    static handleStyleDom(module, dom) {
        if (dom.props['scope'] === 'this') {
            let root;
            //找到根节点
            for (root = dom.parent; root === null || root === void 0 ? void 0 : root.parent; root = root.parent)
                ;
            const cls = this.cssPreName + module.id;
            if (root.props['class']) {
                root.props['class'] = root.props['class'] + ' ' + cls;
            }
            else {
                root.props['class'] = cls;
            }
        }
    }
    /**
     * 处理 style 下的文本元素
     * @param module -  模块
     * @param dom -     style text element
     * @returns         如果是styleTextdom返回true，否则返回false
     */
    static handleStyleTextDom(module, dom) {
        if (!dom.parent || dom.parent.tagName !== 'style') {
            return false;
        }
        //scope=this，在模块根节点添加 限定 class
        CssManager.addRules(module, dom.textContent, dom.parent && dom.parent.props['scope'] === 'this' ? '.' + this.cssPreName + module.id : undefined);
        return true;
    }
    /**
     * 添加多个css rule
     * @param cssText -     rule集合
     * @param module -      模块
     * @param scopeName -   作用域名(前置选择器)
     */
    static addRules(module, cssText, scopeName) {
        //sheet 初始化
        if (!this.sheet) {
            //safari不支持 cssstylesheet constructor，用 style代替
            const sheet = document.createElement('style');
            document.head.appendChild(sheet);
            this.sheet = document.styleSheets[0];
        }
        //如果有作用域，则清除作用域下的rule
        if (scopeName) {
            this.clearModuleRules(module);
        }
        //是否限定在模块内
        //cssRule 获取正则式  @import
        const reg = /(@[a-zA-Z]+\s+url\(.+?\))|([.#@a-zA-Z]\S*(\s*\S*\s*?)?{)|\}/g;
        //import support url正则式
        const regImp = /@[a-zA-Z]+\s+url/;
        // keyframe font page support... 开始 位置
        let startIndex = -1;
        // { 个数，遇到 } -1 
        let beginNum = 0;
        let re;
        while ((re = reg.exec(cssText)) !== null) {
            if (regImp.test(re[0])) { //@import
                handleImport(re[0]);
            }
            else if (re[0] === '}') { //回收括号，单个样式结束判断
                if (startIndex >= 0 && --beginNum <= 0) { //style @ end
                    const txt = cssText.substring(startIndex, re.index + 1);
                    if (txt[0] === '@') { //@开头
                        this.sheet.insertRule(txt, CssManager.sheet.cssRules ? CssManager.sheet.cssRules.length : 0);
                    }
                    else { //style
                        handleStyle(module, txt, scopeName);
                    }
                    startIndex = -1;
                    beginNum = 0;
                }
            }
            else { //style 或 @内部
                if (startIndex === -1) {
                    startIndex = re.index;
                }
                beginNum++;
            }
        }
        /**
         * 处理style rule
         * @param module -      模块
         * @param cssText -     css 文本
         * @param scopeName -   作用域名(前置选择器)
         */
        function handleStyle(module, cssText, scopeName) {
            const reg = /.+(?=\{)/; //匹配字符"{"前出现的所有字符
            const r = reg.exec(cssText);
            if (!r) {
                return;
            }
            // 保存样式名，在模块 object manager 中以数组存储
            if (scopeName) {
                let arr = module.cssRules;
                if (!arr) {
                    arr = [];
                    module.cssRules = arr;
                }
                arr.push((scopeName + ' ' + r[0]));
                //为样式添加 scope name
                cssText = scopeName + ' ' + cssText;
            }
            //加入到样式表
            CssManager.sheet.insertRule(cssText, CssManager.sheet.cssRules ? CssManager.sheet.cssRules.length : 0);
        }
        /**
         * 处理import rule
         * @param cssText - css文本
         * @returns         如果cssText中"()"内有字符串且importMap中存在键值为"()"内字符串的第一个字符，则返回void
         */
        function handleImport(cssText) {
            const ind = cssText.indexOf('(');
            const ind1 = cssText.lastIndexOf(')');
            if (ind === -1 || ind1 === -1 || ind >= ind1) {
                return;
            }
            const css = cssText.substring(ind + 1, ind1);
            if (CssManager.importMap.has(css)) {
                return;
            }
            //插入import rule
            CssManager.sheet.insertRule(cssText, CssManager.importIndex++);
            CssManager.importMap.set(css, true);
        }
    }
    /**
     * 清除模块css rules
     * @param module -  模块
     */
    static clearModuleRules(module) {
        const rules = module.cssRules;
        if (!rules || rules.length === 0) {
            return;
        }
        //从sheet清除
        for (let i = 0; i < this.sheet.cssRules.length; i++) {
            const r = this.sheet.cssRules[i];
            if (r.selectorText && rules.indexOf(r.selectorText) !== -1) {
                this.sheet.deleteRule(i--);
            }
        }
        //置空cache
        module.cssRules = [];
    }
}
/**
 * import url map，用于存储import的url路径
 */
CssManager.importMap = new Map();
/**
 * importrule 位置
 */
CssManager.importIndex = 0;
/**
 * css class 前置名
 */
CssManager.cssPreName = '___nodom_module_css_';

/**
 * 渲染器
 * @remarks
 * nodom渲染操作在渲染器中实现
 */
class Renderer {
    /**
     * 设置根容器
     * @param rootEl - 根html element
     */
    static setRootEl(rootEl) {
        this.rootEl = rootEl;
    }
    /**
     * 获取根容器
     * @returns 根 html element
     */
    static getRootEl() {
        return this.rootEl;
    }
    /**
     * 添加到渲染列表
     * @param module - 模块
     */
    static add(module) {
        if (!module) {
            return;
        }
        //如果已经在列表中，不再添加
        if (!this.waitList.includes(module.id)) {
            //计算优先级
            this.waitList.push(module.id);
        }
    }
    /**
     * 从渲染队列移除
     * @param module -  模块
     */
    static remove(module) {
        let index;
        if ((index = this.waitList.indexOf(module.id)) !== -1) {
            //不能破坏watiList顺序，用null替换
            this.waitList.splice(index, 1, null);
        }
    }
    /**
     * 渲染
     * @remarks
     * 如果存在渲染队列，则从队列中取出并依次渲染
     */
    static render() {
        for (; this.waitList.length > 0;) {
            const id = this.waitList[0];
            if (id) { //存在id为null情况，remove方法造成
                const m = ModuleFactory.get(id);
                m === null || m === void 0 ? void 0 : m.render();
            }
            //渲染后移除
            this.waitList.shift();
        }
    }
    /**
     * 渲染dom
     * @remarks
     * 此过程将VirtualDom转换为RenderedDom。
     * 当对单个节点进行更新渲染时，可避免整个模块的渲染，这种方式适用于组件、指令等编码过程使用。
     * @param module -          模块
     * @param src -             源dom
     * @param model -           模型
     * @param parent -          父dom
     * @param key -             附加key，放在domkey的后面
     * @param notRenderChild -  不渲染子节点
     * @returns                 渲染后节点
     */
    static renderDom(module, src, model, parent, key, notRenderChild) {
        //初始化渲染节点
        const dst = {
            key: key ? src.key + '_' + key : src.key,
            model: model,
            vdom: src,
            parent: parent,
            moduleId: src.moduleId,
            slotModuleId: src.slotModuleId,
            staticNum: src.staticNum
        };
        //静态节点只渲染1次
        if (src.staticNum > 0) {
            src.staticNum--;
        }
        //源模块（编译模板时产生的模块）
        //表达式不能用module，因为涉及到方法调用时，必须用srcModule
        const srcModule = ModuleFactory.get(dst.moduleId);
        //标签
        if (src.tagName) {
            dst.tagName = src.tagName;
            //添加key属性
            dst.props = {};
            //设置svg标志
            if (src.isSvg) {
                dst.isSvg = src.isSvg;
            }
        }
        //处理model指令
        const mdlDir = src.getDirective('model');
        if (mdlDir) {
            mdlDir.exec(module, dst);
        }
        if (dst.tagName) { //标签节点
            this.handleProps(module, src, dst, srcModule);
            //处理style标签，如果为style，则不处理assets
            if (src.tagName === 'style') {
                CssManager.handleStyleDom(module, dst);
            }
            else if (src.assets && src.assets.size > 0) {
                dst.assets || (dst.assets = {});
                for (const p of src.assets) {
                    dst.assets[p[0]] = p[1];
                }
            }
            //处理directive时，导致禁止后续渲染，则不再渲染，如show指令
            if (!this.handleDirectives(module, src, dst)) {
                return null;
            }
            //非子模块节点，处理事件
            if (src.events) {
                dst.events || (dst.events = []);
                for (const ev of src.events) {
                    dst.events.push(ev);
                }
            }
            //子节点渲染
            if (!notRenderChild) {
                if (src.children && src.children.length > 0) {
                    dst.children = [];
                    for (const c of src.children) {
                        this.renderDom(module, c, dst.model, dst, key);
                    }
                }
            }
        }
        else { //文本节点
            if (src.expressions) { //文本节点
                let value = '';
                for (const expr of src.expressions) {
                    if (expr instanceof Expression) { //处理表达式
                        const v1 = expr.val(srcModule, dst.model);
                        value += v1 !== undefined && v1 !== null ? v1 : '';
                    }
                    else {
                        value += expr;
                    }
                }
                dst.textContent = value;
            }
            else {
                dst.textContent = src.textContent;
            }
        }
        //添加到dom tree，必须放在handleDirectives后，因为有可能directive执行后返回false
        if (parent) {
            if (!parent.children) {
                parent.children = [];
            }
            parent.children.push(dst);
        }
        return dst;
    }
    /**
     * 处理指令
     * @param module -  模块
     * @param src -     编译节点
     * @param dst -     渲染节点
     * @returns         true继续执行，false不执行后续渲染代码，也不加入渲染树
    */
    static handleDirectives(module, src, dst) {
        if (!src.directives || src.directives.length === 0) {
            return true;
        }
        for (const d of src.directives) {
            //model指令不执行
            if (d.type.name === 'model') {
                continue;
            }
            if (!d.exec(module, dst)) {
                return false;
            }
        }
        return true;
    }
    /**
     * 处理属性
     * @param module -      模块
     * @param src -         编译节点
     * @param dst -         渲染节点
     * @param srcModule -   源模块
     */
    static handleProps(module, src, dst, srcModule) {
        var _a;
        if (((_a = src.props) === null || _a === void 0 ? void 0 : _a.size) > 0) {
            for (const k of src.props) {
                const v = k[1] instanceof Expression ? k[1].val(srcModule, dst.model) : k[1];
                dst.props[k[0]] = handleValue(v);
            }
        }
        //如果为子模块，则合并srcDom属性
        if (src.key === 1) {
            mergeProps(module, dst);
        }
        /**
         * 对空value进行处理
         * @param v -   待处理value
         * @returns     空字符串或原value
         */
        function handleValue(v) {
            // 属性值为空，则设置为''
            return (v === undefined || v === null || v === '' || typeof v === 'string' && v.trim() === '') ? '' : v;
        }
        /**
         * 处理根节点属性
         * @param module -  所属模块
         * @param dom -     dom节点
         */
        function mergeProps(module, dom) {
            var _a;
            if (module.props) {
                for (const k of Object.keys(module.props)) {
                    let value = dom.props[k];
                    if ((_a = module.excludedProps) === null || _a === void 0 ? void 0 : _a.includes(k)) {
                        continue;
                    }
                    let v = module.props[k];
                    if (v) {
                        if (typeof v === 'string') {
                            v = v.trim();
                        }
                        //合并style  props.style + dst.style
                        if ('style' === k) {
                            if (!value) {
                                value = v;
                            }
                            else {
                                value = (v + ';' + value).replace(/;{2,}/g, ';');
                            }
                        }
                        else if ('class' === k) { //合并class,dst.class + props.class 
                            if (!value) {
                                value = v;
                            }
                            else {
                                value += ' ' + v;
                            }
                        }
                        else if (!value) { //其他情况，如果不存在dst.props[k]，则直接用module.props[k]
                            value = v;
                        }
                    }
                    dom.props[k] = handleValue(value);
                }
            }
        }
    }
    /**
     * 更新到html树
     * @param module -  模块
     * @param dom -     渲染节点
     * @param oldDom -  旧节点
     * @returns         渲染后的节点
     */
    static updateToHtml(module, dom, oldDom) {
        var _a;
        const el = oldDom.node;
        if (!el) {
            dom.node = this.renderToHtml(module, dom, (_a = oldDom.parent) === null || _a === void 0 ? void 0 : _a.node);
            return dom.node;
        }
        else if (dom.tagName) { //html dom节点已存在
            //设置element key属性
            for (const prop of ['props', 'assets']) {
                const old = oldDom[prop];
                //设置属性
                if (dom[prop]) {
                    for (const k of Object.keys(dom[prop])) {
                        const pv = dom[prop][k];
                        if (prop === 'props') { //attribute
                            el.setAttribute(k, pv);
                        }
                        else { //asset
                            el[k] = pv;
                        }
                        //删除旧节点对应k
                        if (old) {
                            delete old[k];
                        }
                    }
                }
                //清理多余属性
                if (old) {
                    const keys = Object.keys(old);
                    if (keys.length > 0) {
                        for (const k of keys) {
                            if (prop === 'props') { //attribute
                                el.removeAttribute(k);
                            }
                            else { //asset
                                el[k] = null;
                            }
                        }
                    }
                }
            }
            //删除旧事件
            // module.eventFactory.removeEvent(dom);
            //绑定事件
            module.eventFactory.handleDomEvent(dom, oldDom);
        }
        else { //文本节点
            el['textContent'] = dom.textContent;
        }
        return el;
    }
    /**
     * 渲染到html树
     * @param module - 	        模块
     * @param src -             渲染节点
     * @param parentEl - 	    父html
     * @returns                 渲染后的html节点
     */
    static renderToHtml(module, src, parentEl) {
        let el;
        if (src.tagName) {
            el = newEl(src);
        }
        else {
            el = newText(src);
        }
        // element、渲染子节点且不为子模块，处理子节点
        if (el && src.tagName && !src.childModuleId) {
            genSub(el, src);
        }
        if (el && parentEl) {
            parentEl.appendChild(el);
        }
        return el;
        /**
         * 新建element节点
         * @param dom - 	虚拟dom
         * @returns 		新的html element
         */
        function newEl(dom) {
            let el;
            // 子模块自行渲染
            if (dom.childModuleId) {
                const m = ModuleFactory.get(dom.childModuleId);
                //创建替代节点
                if (m) {
                    const comment = document.createComment("module " + m.constructor.name + ':' + m.id);
                    Renderer.add(m);
                    dom.node = comment;
                    return comment;
                }
                return;
            }
            //style，只处理文本节点
            if (dom.tagName === 'style') {
                genSub(el, dom);
                return;
            }
            //处理svg节点
            if (dom.isSvg) {
                el = document.createElementNS("http://www.w3.org/2000/svg", dom.tagName);
                if (dom.tagName === 'svg') {
                    el.setAttribute('xmlns', 'http://www.w3.org/2000/svg');
                }
            }
            else { //普通节点
                el = document.createElement(dom.tagName);
            }
            //保存element
            dom.node = el;
            //设置属性
            for (const p of Object.keys(dom.props)) {
                el.setAttribute(p, dom.props[p]);
            }
            //asset
            if (dom.assets) {
                for (const p of Object.keys(dom.assets)) {
                    el[p] = dom.assets[p];
                }
            }
            module.eventFactory.handleDomEvent(dom);
            return el;
        }
        /**
         * 新建文本节点
         */
        function newText(dom) {
            //样式表处理，如果是样式表文本，则不添加到dom树
            if (CssManager.handleStyleTextDom(module, dom)) {
                return;
            }
            dom.node = document.createTextNode(dom.textContent || '');
            return dom.node;
        }
        /**
         * 生成子节点
         * @param pEl -     父节点
         * @param dom -     dom节点
         */
        function genSub(pEl, dom) {
            if (dom.children && dom.children.length > 0) {
                dom.children.forEach(item => {
                    let el1;
                    if (item.tagName) {
                        el1 = newEl(item);
                        //element节点，产生子节点
                        if (el1 instanceof Element) {
                            genSub(el1, item);
                        }
                    }
                    else {
                        el1 = newText(item);
                    }
                    if (el1) {
                        pEl.appendChild(el1);
                    }
                });
            }
        }
    }
    /**
     * 处理更改的dom节点
     * @param module -        待处理模块
     * @param changeDoms -    修改后的dom节点数组
     */
    static handleChangedDoms(module, changeDoms) {
        const slotDoms = {};
        //替换数组
        const repArr = [];
        //添加或移动节点
        const addOrMove = [];
        //保留原有html节点
        for (const item of changeDoms) {
            //如果为slot节点，则记录，单独处理
            if (item[1].slotModuleId && item[1].slotModuleId !== module.id) {
                if (slotDoms[item[1].slotModuleId]) {
                    slotDoms[item[1].slotModuleId].push(item);
                }
                else {
                    slotDoms[item[1].slotModuleId] = [item];
                }
                continue;
            }
            switch (item[0]) {
                case 1: //添加
                    // addArr.push(item);
                    addOrMove.push(item);
                    break;
                case 2: //修改
                    //子模块不处理，由setProps处理
                    if (item[1].childModuleId) {
                        Renderer.add(ModuleFactory.get(item[1].childModuleId));
                    }
                    else {
                        this.updateToHtml(module, item[1], item[2]);
                    }
                    break;
                case 3: //删除
                    module.domManager.freeNode(item[1], true);
                    break;
                case 4: //移动
                    // moveArr.push(item);
                    addOrMove.push(item);
                    break;
                default: //替换
                    repArr.push(item);
            }
        }
        //替换
        if (repArr.length > 0) {
            for (const item of repArr) {
                this.replace(module, item[1], item[2]);
            }
        }
        if (addOrMove.length > 1) {
            addOrMove.sort((a, b) => a[4] > b[4] ? 1 : -1);
        }
        for (; addOrMove.length > 0;) {
            const item = addOrMove[0];
            const pEl = item[3].node;
            if (!pEl) {
                continue;
            }
            //如果为add，则新建节点，否则直接取旧节点
            const n1 = item[0] === 1 ? Renderer.renderToHtml(module, item[1], null) : item[1].node;
            if (n1) {
                let index = item[4];
                //检查 目标位置 > item[4]且 原位置 < item[4] 的兄弟节点，此类节点后续要移走，会影响节点位置，需要在当前
                const arr = addOrMove.filter(ii => ii[0] === 4 && ii[3].node === pEl && ii[4] >= item[4] && ii[5] < item[4]);
                index += arr.length;
                moveNode2Loc(n1, pEl, index);
            }
            //首节点处理后移除
            addOrMove.shift();
        }
        //处理slot节点改变后影响的子模块
        const keys = Object.keys(slotDoms);
        if (keys && keys.length > 0) {
            for (const k of keys) {
                const m = ModuleFactory.get(parseInt(k));
                if (m) {
                    Renderer.add(m);
                }
            }
        }
        /**
         * 移动节点到指定位置
         * @param node -    待插入节点
         * @param pEl -     父节点
         * @param loc -     位置
         */
        function moveNode2Loc(node, pEl, loc) {
            const mdl = findMdlNode(node);
            let findLoc = false;
            for (let i = 0, index = 0; i < pEl.childNodes.length; i++, index++) {
                const c = pEl.childNodes[i];
                //子模块占位符和模块算一个计数位置
                if (findMdlNode(c) !== null) {
                    i++;
                }
                //找到插入位置
                if (index === loc) {
                    if (mdl === null) {
                        pEl.insertBefore(node, c);
                    }
                    else { //占位符和模块一并移动
                        pEl.insertBefore(mdl, c);
                        pEl.insertBefore(node, mdl);
                    }
                    findLoc = true;
                    break;
                }
            }
            //只能作为pEl的最后节点
            if (!findLoc) {
                if (mdl === null) {
                    pEl.appendChild(node);
                }
                else {
                    pEl.appendChild(node);
                    pEl.appendChild(mdl);
                }
            }
        }
        /**
         * 找到注释节点对应的子模块节点
         * @param node      注释节点
         * @returns         注释节点对应的子模块节点或null
         */
        function findMdlNode(node) {
            return node && node instanceof Comment && node.nextSibling && node.nextSibling instanceof Element
                && node.textContent.endsWith(node.nextSibling.getAttribute('role')) ? node.nextSibling : null;
        }
    }
    /**
     * 替换节点
     * @param module -  模块
     * @param src -     待替换节点
     * @param dst -     被替换节点
     */
    static replace(module, src, dst) {
        var _a, _b;
        const el = this.renderToHtml(module, src, null);
        if (dst.childModuleId) { //被替换节点为子模块
            const m1 = ModuleFactory.get(dst.childModuleId);
            const pEl = (_a = m1.srcDom.node) === null || _a === void 0 ? void 0 : _a.parentElement;
            if (!pEl) {
                return;
            }
            const el1 = m1.srcDom.node.previousSibling;
            m1.destroy();
            if (el1) {
                Util.insertAfter(el, el1);
            }
            else if (pEl.childNodes.length === 0) {
                pEl.appendChild(el);
            }
            else {
                pEl.insertBefore(el, pEl.childNodes[0]);
            }
        }
        else {
            const pEl = (_b = dst.node) === null || _b === void 0 ? void 0 : _b.parentElement;
            if (!pEl) {
                return;
            }
            pEl.replaceChild(el, dst.node);
            module.domManager.freeNode(dst, true);
        }
    }
}
/**
 * 等待渲染列表
 */
Renderer.waitList = [];

class RequestManager {
    /**
     * 设置相同请求拒绝时间间隔
     * @param time -  时间间隔（ms）
     */
    static setRejectTime(time) {
        this.rejectReqTick = time;
    }
    /**
     * ajax 请求
     *
     * @param config -  object 或 string，如果为string，则表示url，直接以get方式获取资源，如果为 object，配置项如下:
     * ```
     *  参数名|类型|默认值|必填|可选值|描述
     *  -|-|-|-|-|-
     *  url|string|无|是|无|请求url
     *	method|string|GET|否|GET,POST,HEAD|请求类型
     *	params|object/FormData|空object|否|无|参数，json格式
     *	async|bool|true|否|true,false|是否异步
     *  timeout|number|0|否|无|请求超时时间
     *  type|string|text|否|json,text|
     *	withCredentials|bool|false|否|true,false|同源策略，跨域时cookie保存
     *  header|Object|无|否|无|request header 对象
     *  user|string|无|否|无|需要认证的请求对应的用户名
     *  pwd|string|无|否|无|需要认证的请求对应的密码
     *  rand|bool|无|否|无|请求随机数，设置则浏览器缓存失效
     * ```
     */
    static request(config) {
        return __awaiter(this, void 0, void 0, function* () {
            const time = Date.now();
            //如果设置了rejectReqTick，则需要进行判断
            if (this.rejectReqTick > 0) {
                if (this.requestMap.has(config.url)) {
                    const obj = this.requestMap.get(config.url);
                    if (time - obj['time'] < this.rejectReqTick && Util.compare(obj['params'], config.params)) {
                        return new Promise((resolve) => {
                            resolve(null);
                        });
                    }
                }
                //加入请求集合
                this.requestMap.set(config.url, {
                    time: time,
                    params: config.params
                });
            }
            return new Promise((resolve, reject) => {
                if (typeof config === 'string') {
                    config = {
                        url: config
                    };
                }
                config.params = config.params || {};
                //随机数
                if (config.rand) { //针对数据部分，仅在app中使用
                    config.params.$rand = Math.random();
                }
                let url = config.url;
                const async = config.async === false ? false : true;
                const req = new XMLHttpRequest();
                //设置同源策略
                req.withCredentials = config.withCredentials;
                //类型默认为get
                const method = (config.method || 'GET').toUpperCase();
                //超时，同步时不能设置
                req.timeout = async ? config.timeout : 0;
                req.onload = () => {
                    //正常返回处理
                    if (req.status === 200) {
                        let r = req.responseText;
                        if (config.type === 'json') {
                            try {
                                r = JSON.parse(r);
                            }
                            catch (e) {
                                reject({ type: "jsonparse" });
                            }
                        }
                        resolve(r);
                    }
                    else { //异常返回处理
                        reject({ type: 'error', url: url });
                    }
                };
                //设置timeout和error
                req.ontimeout = () => reject({ type: 'timeout' });
                req.onerror = () => reject({ type: 'error', url: url });
                //上传数据
                let data = null;
                switch (method) {
                    case 'GET':
                        //参数
                        let pa;
                        if (Util.isObject(config.params)) {
                            const ar = [];
                            for (const k of Object.keys(config.params)) {
                                let v = config.params[k];
                                if (v === undefined || v === null) {
                                    continue;
                                }
                                //对象转串
                                if (typeof v === 'object') {
                                    v = JSON.stringify(v);
                                }
                                ar.push(k + '=' + v);
                            }
                            pa = ar.join('&');
                        }
                        if (pa !== undefined) {
                            if (url.indexOf('?') !== -1) {
                                url += '&' + pa;
                            }
                            else {
                                url += '?' + pa;
                            }
                        }
                        break;
                    case 'POST':
                        if (config.params instanceof FormData) {
                            data = config.params;
                        }
                        else {
                            const fd = new FormData();
                            for (const k of Object.keys(config.params)) {
                                let v = config.params[k];
                                if (v === undefined || v === null) {
                                    continue;
                                }
                                if (typeof v === 'object') {
                                    v = JSON.stringify(v);
                                }
                                //对象转串
                                fd.append(k, v);
                            }
                            data = fd;
                        }
                        break;
                }
                //打开请求
                req.open(method, url, async, config.user, config.pwd);
                //设置request header
                if (config.header) {
                    Util.getOwnProps(config.header).forEach((item) => {
                        req.setRequestHeader(item, config.header[item]);
                    });
                }
                //发送请求
                req.send(data);
            }).catch((re) => {
                switch (re.type) {
                    case "error":
                        throw new NError("notexist1", NodomMessage.TipWords['resource'], re.url);
                    case "timeout":
                        throw new NError("timeout");
                    case "jsonparse":
                        throw new NError("jsonparse");
                }
            });
        });
    }
    /**
     * 清除超时的缓存请求
     */
    static clearCache() {
        const time = Date.now();
        if (this.rejectReqTick > 0) {
            if (this.requestMap) {
                for (const kv of this.requestMap) {
                    if (time - kv[1]['time'] > this.rejectReqTick) {
                        this.requestMap.delete(kv[0]);
                    }
                }
            }
        }
    }
}
/**
 * 拒绝相同请求（url，参数）时间间隔
 */
RequestManager.rejectReqTick = 0;
/**
 * 请求map，用于缓存之前的请求url和参数
 * key:     url
 * value:   请求参数
 */
RequestManager.requestMap = new Map();

/**
 * 路由类
 */
class Route {
    /**
     * 构造器
     * @param config - 路由配置项
     */
    constructor(config, parent) {
        /**
         * 路由参数名数组
         */
        this.params = [];
        /**
         * 路由参数数据
         */
        this.data = {};
        /**
         * 子路由
         */
        this.children = [];
        if (!config || Util.isEmpty(config.path)) {
            return;
        }
        this.id = Util.genId();
        //参数赋值
        for (const o of Object.keys(config)) {
            this[o] = config[o];
        }
        this.parent = parent;
        //解析路径
        if (this.path) {
            this.parse();
        }
        if (parent) {
            parent.addChild(this);
        }
        //子路由
        if (config.routes && Array.isArray(config.routes)) {
            config.routes.forEach((item) => {
                new Route(item, this);
            });
        }
    }
    /**
     * 添加子路由
     * @param child - 字路由
     */
    addChild(child) {
        this.children.push(child);
        child.parent = this;
    }
    /**
     * 通过路径解析路由对象
     */
    parse() {
        const pathArr = this.path.split('/');
        let node = this.parent;
        let param = [];
        let paramIndex = -1; //最后一个参数开始
        let prePath = ''; //前置路径
        for (let i = 0; i < pathArr.length; i++) {
            const v = pathArr[i].trim();
            if (v === '') {
                pathArr.splice(i--, 1);
                continue;
            }
            if (v.startsWith(':')) { //参数
                if (param.length === 0) {
                    paramIndex = i;
                }
                param.push(v.substring(1));
            }
            else {
                paramIndex = -1;
                param = []; //上级路由的参数清空
                this.path = v; //暂存path
                let j = 0;
                for (; j < node.children.length; j++) {
                    const r = node.children[j];
                    if (r.path === v) {
                        node = r;
                        break;
                    }
                }
                //没找到，创建新节点
                if (j === node.children.length) {
                    if (prePath !== '') {
                        new Route({ path: prePath }, node);
                        node = node.children[node.children.length - 1];
                    }
                    prePath = v;
                }
            }
            //不存在参数
            this.params = paramIndex === -1 ? [] : param;
        }
    }
    /**
     * 克隆
     * @returns 克隆对象
     */
    clone() {
        const r = new Route();
        Object.getOwnPropertyNames(this).forEach(item => {
            if (item === 'data') {
                return;
            }
            r[item] = this[item];
        });
        if (this.data) {
            r.data = Util.clone(this.data);
        }
        return r;
    }
}

/**
 * 调度器
 * @remarks
 * 管理所有需调度的任务并进行循环调度，默认采用requestAnimationFrame方式进行循环
 */
class Scheduler {
    /**
     * 执行任务
     */
    static dispatch() {
        Scheduler.tasks.forEach((item) => {
            if (Util.isFunction(item['func'])) {
                if (item['thiser']) {
                    item['func'].call(item['thiser']);
                }
                else {
                    item['func']();
                }
            }
        });
    }
    /**
     * 启动调度器
     * @param scheduleTick - 	渲染间隔（ms），默认50ms
     */
    static start(scheduleTick) {
        Scheduler.dispatch();
        if (window.requestAnimationFrame) {
            window.requestAnimationFrame(Scheduler.start);
        }
        else {
            window.setTimeout(Scheduler.start, scheduleTick || 50);
        }
    }
    /**
     * 添加任务
     * @param foo - 	待执行任务函数
     * @param thiser - 	this指向
     */
    static addTask(foo, thiser) {
        if (!Util.isFunction(foo)) {
            throw new NError("invoke", "Scheduler.addTask", "0", "function");
        }
        Scheduler.tasks.push({ func: foo, thiser: thiser });
    }
    /**
     * 移除任务
     * @param foo - 	任务函数
     */
    static removeTask(foo) {
        if (!Util.isFunction(foo)) {
            throw new NError("invoke", "Scheduler.removeTask", "0", "function");
        }
        let ind = -1;
        if ((ind = Scheduler.tasks.indexOf(foo)) !== -1) {
            Scheduler.tasks.splice(ind, 1);
        }
    }
}
/**
 * 待执行任务列表
 */
Scheduler.tasks = [];

/**
 * nodom提示消息
 */
let NodomMessage = NodomMessage_zh;
/**
 * Nodom接口暴露类
 */
class Nodom {
    /**
     * 应用初始化
     * @param clazz -     模块类
     * @param selector -  根模块容器选择器，默认使用document.body
     */
    static app(clazz, selector) {
        //设置渲染器的根 element
        Renderer.setRootEl(document.querySelector(selector) || document.body);
        //渲染器启动渲染任务
        Scheduler.addTask(Renderer.render, Renderer);
        //添加请求清理任务
        Scheduler.addTask(RequestManager.clearCache);
        //启动调度器
        Scheduler.start();
        ModuleFactory.get(clazz).active();
    }
    /**
     * 启用debug模式
     */
    static debug() {
        this.isDebug = true;
    }
    /**
     * 设置语言
     * @param lang -  语言（zh,en），默认zh
     */
    static setLang(lang) {
        //设置nodom语言
        switch (lang || 'zh') {
            case 'zh':
                NodomMessage = NodomMessage_zh;
                break;
            case 'en':
                NodomMessage = NodomMessage_en;
        }
    }
    /**
     * use插件（实例化）
     * @remarks
     * 插件实例化后以单例方式存在，第二次use同一个插件，将不进行任何操作，实例化后可通过Nodom['$类名']方式获取
     * @param clazz -   插件类
     * @param params -  参数
     * @returns         实例化后的插件对象
     */
    static use(clazz, params) {
        if (!clazz['name']) {
            new NError('notexist', NodomMessage.TipWords.plugin);
        }
        if (!this['$' + clazz['name']]) {
            this['$' + clazz['name']] = Reflect.construct(clazz, params || []);
        }
        return this['$' + clazz['name']];
    }
    /**
     * 创建路由
     * @remarks
     * 配置项可以用嵌套方式
     * @example
     * ```js
     * Nodom.createRoute([{
     *   path: '/router',
     *   //直接用模块类，需import
     *   module: MdlRouteDir,
     *   routes: [
     *       {
     *           path: '/route1',
     *           module: MdlPMod1,
     *           routes: [{
     *               path: '/home',
     *               //直接用路径，实现懒加载
     *               module:'/examples/modules/route/mdlmod1.js'
     *           }, ...]
     *       }, {
     *           path: '/route2',
     *           module: MdlPMod2,
     *           //设置进入事件
     *           onEnter: function (module,path) {},
     *           //设置离开事件
     *           onLeave: function (module,path) {},
     *           ...
     *       }
     *   ]
     * }])
     * ```
     * @param config -  路由配置
     * @param parent -  父路由
     */
    static createRoute(config, parent) {
        if (!Nodom['$Router']) {
            throw new NError('uninit', NodomMessage.TipWords.route);
        }
        let route;
        parent = parent || Nodom['$Router'].getRoot();
        if (Util.isArray(config)) {
            for (const item of config) {
                route = new Route(item, parent);
            }
        }
        else {
            route = new Route(config, parent);
        }
        return route;
    }
    /**
     * 创建指令
     * @param name -      指令名
     * @param priority -  优先级（1最小，1-10为框架保留优先级）
     * @param handler -   渲染时方法
     */
    static createDirective(name, handler, priority) {
        return DirectiveManager.addType(name, handler, priority);
    }
    /**
     * 注册模块
     * @param clazz -   模块类
     * @param name -    注册名，如果没有，则为类名
     */
    static registModule(clazz, name) {
        ModuleFactory.addClass(clazz, name);
    }
    /**
     * ajax 请求，如果需要用第三方ajax插件替代，重载该方法
     * @param config -  object 或 string，如果为string，则表示url，直接以get方式获取资源，如果为 object，配置项如下:
     * ```
     *  参数名|类型|默认值|必填|可选值|描述
     *  -|-|-|-|-|-
     *  url|string|无|是|无|请求url
     *	method|string|GET|否|GET,POST,HEAD|请求类型
     *	params|object/FormData|空object|否|无|参数，json格式
     *	async|bool|true|否|true,false|是否异步
     *  timeout|number|0|否|无|请求超时时间
     *  type|string|text|否|json,text|
     *	withCredentials|bool|false|否|true,false|同源策略，跨域时cookie保存
     *  header|Object|无|否|无|request header 对象
     *  user|string|无|否|无|需要认证的请求对应的用户名
     *  pwd|string|无|否|无|需要认证的请求对应的密码
     *  rand|bool|无|否|无|请求随机数，设置则浏览器缓存失效
     * ```
     */
    static request(config) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield RequestManager.request(config);
        });
    }
    /**
     * 重复请求拒绝时间间隔
     * @remarks
     * 如果设置此项，当url一致时且间隔时间小于time，则拒绝请求
     * @param time -  时间间隔（ms）
     */
    static setRejectTime(time) {
        RequestManager.setRejectTime(time);
    }
}

/**
 * 异常处理类
 */
class NError extends Error {
    constructor(errorName, ...params) {
        super(errorName);
        const msg = NodomMessage.ErrorMsgs[errorName];
        if (msg === undefined) {
            this.message = "未知错误";
            return;
        }
        //编译提示信息
        this.message = Util.compileStr(msg, params);
    }
}

/**
 * 基础服务库
 */
class Util {
    /**
     * 唯一主键
     */
    static genId() {
        return this.generatedId++;
    }
    /**
     * 初始化保留字map
     */
    static initKeyMap() {
        [
            'arguments', 'boolean', 'break', 'byte', 'catch',
            'char', 'const', 'default', 'delete', 'do',
            'double', 'else', 'enum', 'eval', 'false',
            'float', 'for', 'function', 'goto', 'if',
            'in', 'instanceof', 'int', 'let', 'long',
            'null', 'return', 'short', 'switch', 'this',
            'throw', 'true', 'try', 'this', 'throw',
            'typeof', 'var', 'while', 'with', 'Array',
            'Date', 'JSON', 'Set', 'Map', 'eval',
            'Infinity', 'isFinite', 'isNaN', 'isPrototypeOf', 'Math',
            'new', 'NaN', 'Number', 'Object', 'prototype', 'String',
            'isPrototypeOf', 'undefined', 'valueOf'
        ].forEach(item => {
            this.keyWordMap.set(item, true);
        });
    }
    /**
     * 是否为 js 保留关键字
     * @param name -    名字
     * @returns         如果为保留字，则返回true，否则返回false
     */
    static isKeyWord(name) {
        return this.keyWordMap.has(name);
    }
    /******对象相关******/
    /**
     * 对象复制
     * @param srcObj -  源对象
     * @param expKey -  不复制的键正则表达式或属性名
     * @param extra -   附加参数
     * @returns         复制的对象
     */
    static clone(srcObj, expKey, extra) {
        const map = new WeakMap();
        return clone(srcObj, expKey, extra);
        /**
         * clone对象
         * @param src -      待clone对象
         * @param expKey -   不克隆的键
         * @param extra -    clone附加参数
         * @returns        克隆后的对象
         */
        function clone(src, expKey, extra) {
            //非对象或函数，直接返回            
            if (!src || typeof src !== 'object' || Util.isFunction(src)) {
                return src;
            }
            let dst;
            //带有clone方法，则直接返回clone值
            if (src.clone && Util.isFunction(src.clone)) {
                return src.clone(extra);
            }
            else if (Util.isObject(src)) {
                dst = new Object();
                //把对象加入map，如果后面有新克隆对象，则用新克隆对象进行覆盖
                map.set(src, dst);
                Object.getOwnPropertyNames(src).forEach((prop) => {
                    //不克隆的键
                    if (expKey) {
                        if (expKey.constructor === RegExp && expKey.test(prop) //正则表达式匹配的键不复制
                            || Util.isArray(expKey) && expKey.includes(prop) //被排除的键不复制
                        ) {
                            return;
                        }
                    }
                    dst[prop] = getCloneObj(src[prop], expKey, extra);
                });
            }
            else if (Util.isMap(src)) {
                dst = new Map();
                //把对象加入map，如果后面有新克隆对象，则用新克隆对象进行覆盖
                src.forEach((value, key) => {
                    //不克隆的键
                    if (expKey) {
                        if (expKey.constructor === RegExp && expKey.test(key) //正则表达式匹配的键不复制
                            || expKey.includes(key)) { //被排除的键不复制
                            return;
                        }
                    }
                    dst.set(key, getCloneObj(value, expKey, extra));
                });
            }
            else if (Util.isArray(src)) {
                dst = [];
                //把对象加入map，如果后面有新克隆对象，则用新克隆对象进行覆盖
                src.forEach(function (item, i) {
                    dst[i] = getCloneObj(item, expKey, extra);
                });
            }
            return dst;
        }
        /**
         * 获取clone对象
         * @param value -     待clone值
         * @param expKey -    排除键
         * @param extra -     附加参数
         */
        function getCloneObj(value, expKey, extra) {
            if (typeof value === 'object' && !Util.isFunction(value)) {
                let co = null;
                if (!map.has(value)) { //clone新对象
                    co = clone(value, expKey, extra);
                }
                else { //从map中获取对象
                    co = map.get(value);
                }
                return co;
            }
            return value;
        }
    }
    /**
     * 比较两个对象值是否相同(只比较object和array)
     * @param src - 源对象
     * @param dst - 目标对象
     * @returns     值相同则返回true，否则返回false
     */
    static compare(src, dst) {
        return cmp(src, dst);
        function cmp(o1, o2) {
            if (o1 === o2) {
                return true;
            }
            const keys1 = Object.keys(o1);
            const keys2 = Object.keys(o2);
            if (keys1.length !== keys2.length) {
                return false;
            }
            for (const k of keys1) {
                if (typeof o1[k] === 'object' && typeof o2[k] === 'object') {
                    if (!cmp(o1[k], o2[k])) {
                        return false;
                    }
                }
                else if (o1[k] !== o2[k]) {
                    return false;
                }
            }
            return true;
        }
    }
    /**
     * 获取对象自有属性
     * @param obj - 需要获取属性的对象
     * @returns     返回属性数组
     */
    static getOwnProps(obj) {
        if (!obj) {
            return [];
        }
        return Object.getOwnPropertyNames(obj);
    }
    /**************对象判断相关************/
    /**
     * 判断是否为函数
     * @param foo - 检查的对象
     * @returns     true/false
     */
    static isFunction(foo) {
        return foo !== undefined && foo !== null && foo.constructor === Function;
    }
    /**
     * 判断是否为数组
     * @param obj -   检查的对象
     * @returns     true/false
     */
    static isArray(obj) {
        return Array.isArray(obj);
    }
    /**
     * 判断是否为map
     * @param obj -   检查的对象
     */
    static isMap(obj) {
        return obj !== null && obj !== undefined && obj.constructor === Map;
    }
    /**
     * 判断是否为对象
     * @param obj -   检查的对象
     * @returns     true/false
     */
    static isObject(obj) {
        return obj !== null && obj !== undefined && obj.constructor === Object;
    }
    /**
     * 判断对象/字符串是否为空
     * @param obj - 检查的对象
     * @returns     true/false
     */
    static isEmpty(obj) {
        if (obj === null || obj === undefined)
            return true;
        if (this.isObject(obj)) {
            const keys = Object.keys(obj);
            return keys.length === 0;
        }
        else if (typeof obj === 'string') {
            return obj === '';
        }
        return false;
    }
    /******日期相关******/
    /**
     * 日期格式化
     * @param timestamp -   时间戳
     * @param format -      日期格式
     * @returns             日期串
     */
    static formatDate(timeStamp, format) {
        if (typeof timeStamp === 'string') {
            //排除日期格式串,只处理时间戳
            if (/^\d+$/.test(timeStamp)) {
                timeStamp = Number(timeStamp);
            }
            else {
                throw new NError('invoke', 'Util.formatDate', '0', 'date string', 'date');
            }
        }
        //得到日期
        const date = new Date(timeStamp);
        // invalid date
        if (isNaN(date.getDay())) {
            throw new NError('invoke', 'Util.formatDate', '0', 'date string', 'date');
        }
        const o = {
            "M+": date.getMonth() + 1,
            "d+": date.getDate(),
            "h+": date.getHours(),
            "H+": date.getHours(),
            "m+": date.getMinutes(),
            "s+": date.getSeconds(),
            "S": date.getMilliseconds() //毫秒
        };
        let re;
        //年
        if (re = /(y+)/.exec(format)) {
            format = format.replace(re[0], (date.getFullYear() + "").substring(4 - re[0].length));
        }
        //月日
        this.getOwnProps(o).forEach(function (k) {
            if (re = new RegExp("(" + k + ")").exec(format)) {
                format = format.replace(re[0], re[0].length === 1 ? o[k] : ("00" + o[k]).substring((o[k] + '').length));
            }
        });
        //星期
        format = format.replace(/(E+)/, NodomMessage.WeekDays[date.getDay() + ""]);
        return format;
    }
    /******字符串相关*****/
    /**
     * 编译字符串，把 \{n\}替换成带入值
     * @param src -     待编译的字符串
     * @param params -  参数数组
     * @returns     转换后的消息
     */
    static compileStr(src, ...params) {
        if (!params || params.length === 0) {
            return src;
        }
        let reg;
        for (let i = 0; i < params.length; i++) {
            if (src.indexOf('\{' + i + '\}') !== -1) {
                reg = new RegExp('\\{' + i + '\\}', 'g');
                src = src.replace(reg, params[i]);
            }
            else {
                break;
            }
        }
        return src;
    }
    /**
     * 在节点后插入节点
     * @param src -     待插入节点
     * @param dst -     目标位置节点
     */
    static insertAfter(src, dst) {
        if (!dst.parentElement) {
            return;
        }
        const pEl = dst.parentElement;
        if (dst === pEl.lastChild) {
            pEl.appendChild(src);
        }
        else {
            pEl.insertBefore(src, dst.nextSibling);
        }
    }
}
/**
 * 全局id
 */
Util.generatedId = 1;
/**
 * js 保留字 map
 */
Util.keyWordMap = new Map();
//初始化keymap
Util.initKeyMap();

/**
 * 指令类
 */
class Directive {
    /**
     * 构造方法
     * @param type -  	    类型名
     * @param value - 	    指令值
     */
    constructor(type, value) {
        this.id = Util.genId();
        if (type) {
            this.type = DirectiveManager.getType(type);
            if (!this.type) {
                throw new NError('notexist1', NodomMessage.TipWords['directive'], type);
            }
        }
        if (typeof value === 'string') {
            this.value = value.trim();
        }
        else if (value instanceof Expression) {
            this.expression = value;
        }
        else {
            this.value = value;
        }
    }
    /**
     * 执行指令
     * @param module -  模块
     * @param dom -     渲染目标节点对象
     * @returns         是否继续渲染
     */
    exec(module, dom) {
        //禁用，不执行
        if (this.disabled) {
            return true;
        }
        if (this.expression) {
            this.value = this.expression.val(module, dom.model);
        }
        return this.type.handler.apply(this, [module, dom]);
    }
    /**
     * 克隆
     * @returns     新克隆的指令
     */
    clone() {
        const d = new Directive();
        d.type = this.type;
        d.expression = this.expression;
        d.value = this.value;
        return d;
    }
}

/**
 * 事件类
 * @remarks
 * 事件分为自有事件和代理事件，事件默认传递参数为：
 *
 * 0: model(事件对应数据模型)
 *
 * 1: dom(事件target对应的虚拟dom节点)
 *
 * 2: evObj(Nodom Event对象)
 *
 * 3: e(Html Event对象)
 */
class NEvent {
    /**
     * @param module -      模块
     * @param eventName -   事件名
     * @param eventCfg -    事件串或事件处理函数,以“:”分割,中间不能有空格,结构为: `方法名:delg:nopopo:once:capture`，`":"`后面的内容选择使用；
     *                      如果eventCfg为函数，则表示为事件处理函数
     * @param handler -     事件处理函数，此时eventCfg可以配置为 :delg:nopopo:once:capture等
     */
    constructor(module, eventName, eventCfg, handler) {
        this.id = Util.genId();
        this.module = module;
        this.name = eventName;
        //如果事件串不为空，则不需要处理
        if (!eventCfg && !handler) {
            return;
        }
        if (typeof eventCfg === 'string') {
            this.parseEvent(eventCfg.trim());
        }
        else if (typeof eventCfg === 'function') {
            this.handler = eventCfg;
        }
        if (typeof handler === 'function') {
            this.handler = handler;
        }
        this.touchOrNot();
    }
    /**
     * 解析事件字符串
     * @param eventStr -  待解析的字符串
     */
    parseEvent(eventStr) {
        eventStr.split(':').forEach((item, i) => {
            item = item.trim();
            if (i === 0) { //事件方法
                if (item !== '') {
                    this.handler = item;
                }
            }
            else { //事件附加参数
                switch (item) {
                    case 'delg':
                        this.delg = true;
                        break;
                    case 'nopopo':
                        this.nopopo = true;
                        break;
                    case 'once':
                        this.once = true;
                        break;
                    case 'capture':
                        this.capture = true;
                        break;
                }
            }
        });
    }
    /**
     * 触屏转换
     */
    touchOrNot() {
        if (document.ontouchend) { //触屏设备
            switch (this.name) {
                case 'click':
                    this.name = 'tap';
                    break;
                case 'mousedown':
                    this.name = 'touchstart';
                    break;
                case 'mouseup':
                    this.name = 'touchend';
                    break;
                case 'mousemove':
                    this.name = 'touchmove';
                    break;
            }
        }
        else { //转非触屏
            switch (this.name) {
                case 'tap':
                    this.name = 'click';
                    break;
                case 'touchstart':
                    this.name = 'mousedown';
                    break;
                case 'touchend':
                    this.name = 'mouseup';
                    break;
                case 'touchmove':
                    this.name = 'mousemove';
                    break;
            }
        }
    }
    /**
     * 设置附加参数值
     * @param module -    模块
     * @param dom -       虚拟dom
     * @param name -      参数名
     * @param value -     参数值
     */
    setParam(dom, name, value) {
        this.module.objectManager.setEventParam(this.id, dom.key, name, value);
    }
    /**
     * 获取附加参数值
     * @param dom -       虚拟dom
     * @param name -      参数名
     * @returns         附加参数值
     */
    getParam(dom, name) {
        return this.module.objectManager.getEventParam(this.id, dom.key, name);
    }
    /**
     * 移除参数
     * @param dom -       虚拟dom
     * @param name -      参数名
     */
    removeParam(dom, name) {
        return this.module.objectManager.removeEventParam(this.id, dom.key, name);
    }
    /**
     * 清参数cache
     * @param dom -       虚拟dom
     */
    clearParam(dom) {
        this.module.objectManager.clearEventParams(this.id, dom.key);
    }
}

/**
 * 虚拟dom
 * @remarks
 * 编译后的dom节点，与渲染后的dom节点(RenderedDom)不同
 */
class VirtualDom {
    /**
     * @param tag -     标签名
     * @param key -     key
     * @param module - 	模块
     */
    constructor(tag, key, module) {
        this.moduleId = module === null || module === void 0 ? void 0 : module.id;
        this.key = key;
        this.staticNum = 1;
        if (tag) {
            this.tagName = tag;
        }
    }
    /**
     * 移除多个指令
     * @param directives - 	待删除的指令类型数组或指令类型
     * @returns             如果虚拟dom上的指令集为空，则返回void
     */
    removeDirectives(directives) {
        if (!this.directives) {
            return;
        }
        //数组
        directives.forEach((d) => {
            this.removeDirective(d);
        });
    }
    /**
     * 移除指令
     * @param directive - 	待删除的指令类型名
     * @returns             如果虚拟dom上的指令集为空，则返回void
     */
    removeDirective(directive) {
        if (!this.directives) {
            return;
        }
        let ind;
        if ((ind = this.directives.findIndex((item) => item.type.name === directive)) !== -1) {
            this.directives.splice(ind, 1);
        }
        if (this.directives.length === 0) {
            delete this.directives;
        }
    }
    /**
     * 添加指令
     * @param directive -     指令对象
     * @param sort -          是否排序
     * @returns             如果虚拟dom上的指令集不为空，且指令集中已经存在传入的指令对象，则返回void
     */
    addDirective(directive, sort) {
        if (!this.directives) {
            this.directives = [directive];
            return;
        }
        else if (this.directives.find((item) => item.type.name === directive.type.name)) {
            return;
        }
        this.directives.push(directive);
        //指令按优先级排序
        if (sort) {
            this.sortDirective();
        }
    }
    /**
     * 指令排序
     * @returns           如果虚拟dom上指令集为空，则返回void
     */
    sortDirective() {
        if (!this.directives) {
            return;
        }
        if (this.directives.length > 1) {
            this.directives.sort((a, b) => {
                return DirectiveManager.getType(a.type.name).prio <
                    DirectiveManager.getType(b.type.name).prio
                    ? -1
                    : 1;
            });
        }
    }
    /**
     * 是否有某个类型的指令
     * @param typeName - 	    指令类型名
     * @returns             如果指令集不为空，且含有传入的指令类型名则返回true，否则返回false
     */
    hasDirective(typeName) {
        return this.directives && this.directives.find(item => item.type.name === typeName) !== undefined;
    }
    /**
     * 获取某个类型的指令
     * @param module -            模块
     * @param directiveType - 	指令类型名
     * @returns                 如果指令集为空，则返回void；否则返回指令类型名等于传入参数的指令对象
     */
    getDirective(directiveType) {
        if (!this.directives) {
            return;
        }
        return this.directives.find((item) => item.type.name === directiveType);
    }
    /**
     * 添加子节点
     * @param dom -       子节点
     * @param index -     指定位置，如果不传此参数，则添加到最后
     */
    add(dom, index) {
        if (!this.children) {
            this.children = [];
        }
        if (index) {
            this.children.splice(index, 0, dom);
        }
        else {
            this.children.push(dom);
        }
        dom.parent = this;
    }
    /**
     * 移除子节点
     * @param dom -   子节点
     */
    remove(dom) {
        const index = this.children.indexOf(dom);
        if (index !== -1) {
            this.children.splice(index, 1);
        }
    }
    /**
     * 是否拥有属性
     * @param propName -  属性名
     * @param isExpr -    是否只检查表达式属性
     * @returns         如果属性集含有传入的属性名返回true，否则返回false
     */
    hasProp(propName) {
        if (this.props) {
            return this.props.has(propName);
        }
    }
    /**
     * 获取属性值
     * @param propName -  属性名
     * @returns         传入属性名的value
     */
    getProp(propName) {
        if (this.props) {
            return this.props.get(propName);
        }
    }
    /**
     * 设置属性值
     * @param propName -  属性名
     * @param v -         属性值
     */
    setProp(propName, v) {
        if (!this.props) {
            this.props = new Map();
        }
        this.props.set(propName, v);
    }
    /**
     * 删除属性
     * @param props -     属性名或属性名数组
     * @returns         如果虚拟dom上的属性集为空，则返回void
     */
    delProp(props) {
        if (!this.props) {
            return;
        }
        this.props.delete(props);
    }
    /**
     * 设置asset
     * @param assetName -     asset name
     * @param value -         asset value
     */
    setAsset(assetName, value) {
        if (!this.assets) {
            this.assets = new Map();
        }
        this.assets.set(assetName, value);
        this.setStaticOnce();
    }
    /**
     * 删除asset
     * @param assetName -     asset name
     * @returns             如果虚拟dom上的直接属性集为空，则返回void
     */
    delAsset(assetName) {
        if (!this.assets) {
            return;
        }
        this.assets.delete(assetName);
        this.setStaticOnce();
    }
    /**
     * 设置cache参数
     * @param module -    模块
     * @param name -      参数名
     * @param value -     参数值
     */
    setParam(module, name, value) {
        module.objectManager.setDomParam(this.key, name, value);
    }
    /**
     * 获取参数值
     * @param module -    模块
     * @param name -      参数名
     * @returns         参数值
     */
    getParam(module, name) {
        return module.objectManager.getDomParam(this.key, name);
    }
    /**
     * 移除参数
     * @param module -    模块
     * @param name -      参数名
     */
    removeParam(module, name) {
        module.objectManager.removeDomParam(this.key, name);
    }
    /**
     * 设置单次静态标志
     */
    setStaticOnce() {
        if (this.staticNum !== -1) {
            this.staticNum = 1;
        }
    }
    /**
     * 克隆
     */
    clone() {
        const dst = new VirtualDom(this.tagName, this.key);
        dst.moduleId = this.moduleId;
        if (this.tagName) {
            //属性
            if (this.props && this.props.size > 0) {
                for (const p of this.props) {
                    dst.setProp(p[0], p[1]);
                }
            }
            if (this.assets && this.assets.size > 0) {
                for (const p of this.assets) {
                    dst.setAsset(p[0], p[1]);
                }
            }
            if (this.directives && this.directives.length > 0) {
                dst.directives = [];
                for (const d of this.directives) {
                    dst.directives.push(d.clone());
                }
            }
            //复制事件
            dst.events = this.events;
            //子节点clone
            if (this.children) {
                for (const c of this.children) {
                    dst.add(c.clone());
                }
            }
        }
        else {
            dst.expressions = this.expressions;
            dst.textContent = this.textContent;
        }
        dst.staticNum = this.staticNum;
        return dst;
    }
    /**
     * 保存事件
     * @param event - 	事件对象
     * @param index - 	位置
     */
    addEvent(event, index) {
        if (!this.events) {
            this.events = [event];
        }
        else if (!this.events.includes(event)) {
            if (index >= 0) {
                this.events.splice(index, 0, event);
            }
            else {
                this.events.push(event);
            }
        }
    }
}

const voidTagMap = new Set('area,base,br,col,embed,hr,img,input,link,meta,param,source,track,wbr'.split(','));
/**
 * 编译器
 *
 * @remarks
 * 用于编译模板串为虚拟dom(VirtualDom)节点，存放于模块的 domManager.vdomTree
 */
class Compiler {
    /**
     * 构造器
     * @param module - 模块
     */
    constructor(module) {
        /**
         * 自增型id
         */
        this.keyId = 0;
        /**
         * 虚拟dom数组
         */
        this.domArr = [];
        /**
         * 文本节点
         */
        this.textArr = [];
        /**
         * 是否是表达式文本节点
         */
        this.isExprText = false;
        /**
         * 当前编译的模板，用于报错的时候定位
         */
        this.template = '';
        this.module = module;
    }
    /**
     * 编译
     * @param elementStr - 	待编译html串
     * @returns             虚拟dom树根节点
     */
    compile(elementStr) {
        this.keyId = 0;
        if (!elementStr) {
            return;
        }
        // 清除注释
        this.template = elementStr.replace(/\<\!\-\-[\s\S]*?\-\-\>/g, '').trim();
        elementStr = this.template;
        // 编译
        this.compileTemplate(elementStr);
        // 处理未关闭节点
        if (this.domArr.length > 0) {
            this.forceClose(0);
        }
        return this.root;
    }
    /**
     * 产生dom key
     * @returns   dom key
     */
    genKey() {
        return ++this.keyId;
    }
    /**
     * 编译模板
     * @param srcStr - 	源串
     */
    compileTemplate(srcStr) {
        while (srcStr.length !== 0) {
            if (srcStr.startsWith('<')) {
                // 标签
                if (srcStr[1] == '/') {
                    // 结束标签
                    srcStr = this.compileEndTag(srcStr);
                }
                else {
                    // 开始标签
                    srcStr = this.compileStartTag(srcStr);
                }
            }
            else {
                // 文本节点
                srcStr = this.compileText(srcStr);
            }
        }
    }
    /**
     * 处理开始标签
     * @param srcStr - 待编译字符串
     * @returns 编译处理后的字符串
     */
    compileStartTag(srcStr) {
        // 抓取<div
        const match = /^<\s*([a-z][^\s\/\>]*)/i.exec(srcStr);
        // 抓取成功
        if (match) {
            // 设置当前正在编译的节点
            const dom = new VirtualDom(match[1].toLowerCase(), this.genKey(), this.module);
            if (dom.tagName === 'svg') {
                this.isSvg = true;
            }
            //设置svg标志
            if (this.isSvg) {
                dom.isSvg = this.isSvg;
            }
            if (!this.root) {
                this.root = dom;
            }
            if (this.current) {
                this.current.add(dom);
            }
            //设置当前节点
            this.current = dom;
            // 当前节点入栈
            this.domArr.push(dom);
            // 截断字符串 准备处理属性
            srcStr = srcStr.substring(match.index + match[0].length).trimStart();
        }
        else {
            // <!-- 或者<后跟符号不是字符
            // 当作text节点
            this.textArr.push(srcStr[0]);
            return srcStr.substring(1);
        }
        // 处理属性
        srcStr = this.compileAttributes(srcStr);
        // 属性处理完成之后 判断是否结束
        if (srcStr.startsWith('>')) {
            if (this.isVoidTab(this.current)) { //属于自闭合，则处理闭合
                this.handleCloseTag(this.current, true);
            }
            return srcStr.substring(1).trimStart();
        }
        return srcStr;
    }
    /**
     * 处理标签属性
     * @param srcStr - 待编译字符串
     * @returns 编译后字符串
     */
    compileAttributes(srcStr) {
        while (srcStr.length !== 0 && srcStr[0] !== '>') {
            // 抓取形如： /> a='b' a={{b}} a="b" a=`b` a $data={{***}} a={{***}}的属性串;
            const match = /^((\/\>)|\$?[a-z_][\w-]*)(?:\s*=\s*((?:'[^']*')|(?:"[^"]*")|(?:`[^`]*`)|(?:{{[^}}]*}})))?/i.exec(srcStr);
            // 抓取成功 处理属性
            if (match) {
                if (match[0] === '/>') { //自闭合标签结束则退出
                    // 是自闭合标签
                    this.handleCloseTag(this.current, true);
                    srcStr = srcStr.substring(match.index + match[0].length).trimStart();
                    break;
                }
                else { //属性
                    const name = match[1][0] !== '$' ? match[1].toLowerCase() : match[1];
                    // 是普通属性
                    let value = match[3];
                    if (value) {
                        if (value.startsWith('{{')) { //表达式
                            value = new Expression(value.substring(2, value.length - 2));
                            //表达式 staticNum为-1
                            this.current.staticNum = -1;
                        }
                        else if (value.startsWith('"') || value.startsWith("'")) { //字符串
                            value = value.substring(1, value.length - 1);
                        }
                    }
                    if (name.startsWith('x-')) {
                        // 指令
                        this.current.addDirective(new Directive(name.substring(2), value));
                    }
                    else if (name.startsWith('e-')) {
                        this.current.addEvent(new NEvent(this.module, name.substring(2), value));
                    }
                    else {
                        //普通属性
                        this.current.setProp(name, value);
                    }
                }
                srcStr = srcStr.substring(match.index + match[0].length).trimStart();
            }
            else {
                if (this.current) {
                    throw new NError('tagError', this.current.tagName);
                }
                throw new NError('wrongTemplate');
            }
        }
        return srcStr;
    }
    /**
     * 编译结束标签
     * @param srcStr - 	源串
     * @returns 		剩余的串
     */
    compileEndTag(srcStr) {
        // 抓取结束标签
        const match = /^<\/\s*([a-z][^\>]*)/i.exec(srcStr);
        if (match) {
            const name = match[1].toLowerCase().trim();
            //如果找不到匹配的标签头则丢弃
            let index;
            for (let i = this.domArr.length - 1; i >= 0; i--) {
                if (this.domArr[i].tagName === name) {
                    index = i;
                    break;
                }
            }
            //关闭
            if (index) {
                this.forceClose(index);
            }
            return srcStr.substring(match.index + match[0].length + 1);
        }
        return srcStr;
    }
    /**
     * 强制闭合
     * @param index - 在domArr中的索引号
     * @returns
     */
    forceClose(index) {
        if (index === -1 || index > this.domArr.length - 1) {
            return;
        }
        for (let i = this.domArr.length - 1; i >= index; i--) {
            this.handleCloseTag(this.domArr[i]);
        }
    }
    /**
     * 编译text
     * @param srcStr - 	源串
     * @returns
     */
    compileText(srcStr) {
        // 字符串最开始变为< 或者字符串消耗完 则退出循环
        while (!srcStr.startsWith('<') && srcStr.length !== 0) {
            if (srcStr.startsWith('{')) {
                // 可能是表达式
                const matchExp = /^{{([\s\S]*?)}}/i.exec(srcStr);
                if (matchExp) {
                    // 抓取成功
                    this.textArr.push(new Expression(matchExp[1]));
                    this.isExprText = true;
                    srcStr = srcStr.substring(matchExp.index + matchExp[0].length);
                }
                else {
                    // 跳过单独的{
                    typeof this.textArr[this.textArr.length] === 'string'
                        ? (this.textArr[this.textArr.length] += '{')
                        : this.textArr.push('{');
                    srcStr = srcStr.substring(1);
                }
            }
            else {
                // 非表达式，处理成普通字符节点
                const match = /([^\<\{]*)/.exec(srcStr);
                if (match) {
                    let txt;
                    if (this.current && this.current.tagName === 'pre') {
                        // 在pre标签里
                        txt = this.preHandleText(srcStr.substring(0, match.index + match[0].length));
                    }
                    else {
                        txt = this.preHandleText(srcStr.substring(0, match.index + match[0].length).trim());
                    }
                    if (txt !== '') {
                        this.textArr.push(txt);
                    }
                }
                srcStr = srcStr.substring(match.index + match[0].length);
            }
        }
        // 最开始是< 或者字符消耗完毕 退出循环
        const text = new VirtualDom(undefined, this.genKey(), this.module);
        if (this.isExprText) {
            text.expressions = [...this.textArr];
            //动态文本节点，staticNum=-1
            text.staticNum = -1;
        }
        else {
            text.textContent = this.textArr.join('');
        }
        if (this.current && (this.isExprText || text.textContent.length !== 0)) {
            this.current.add(text);
        }
        // 重置状态
        this.isExprText = false;
        this.textArr = [];
        // 返回字符串
        return srcStr;
    }
    /**
     * 预处理html保留字符 如 &nbsp;,&lt;等
     * @param str -   待处理的字符串
     * @returns     解析之后的串
     */
    preHandleText(str) {
        const reg = /&[a-z]+;/;
        if (reg.test(str)) {
            const div = document.createElement('div');
            div.innerHTML = str;
            return div.textContent;
        }
        return str;
    }
    /**
     * 处理当前节点是模块或者自定义节点
     * @param dom - 	虚拟dom节点
     */
    postHandleNode(dom) {
        const clazz = DefineElementManager.get(dom.tagName);
        if (clazz) {
            Reflect.construct(clazz, [dom, this.module]);
        }
        // 是否是模块类
        if (ModuleFactory.hasClass(dom.tagName)) {
            dom.addDirective(new Directive('module', dom.tagName));
            dom.tagName = 'div';
        }
    }
    /**
     * 处理插槽
     * @param dom - 	虚拟dom节点
     */
    handleSlot(dom) {
        var _a;
        if (!dom.children || dom.children.length === 0 || !dom.hasDirective('module')) {
            return;
        }
        //default slot节点
        let slotCt;
        for (let j = 0; j < dom.children.length; j++) {
            const c = dom.children[j];
            //已经是slot节点且有子节点
            if (c.hasDirective('slot') && ((_a = c.children) === null || _a === void 0 ? void 0 : _a.length) > 0) {
                const d = c.getDirective('slot');
                // 默认slot
                if (d.value === undefined || d.value === 'default') {
                    if (!slotCt) { // slotCt还不存在，直接替代
                        slotCt = c;
                    }
                    else {
                        //子节点合并到slotCt
                        slotCt.children = slotCt.children.concat(c.children);
                        //节点移除
                        dom.children.splice(j--, 1);
                    }
                }
                else {
                    continue;
                }
            }
            else { //非slot节点，添加到default slot节点
                if (!slotCt) { //初始化default slot container
                    //第一个直接被slotCt替换
                    slotCt = new VirtualDom('div', this.genKey(), this.module);
                    slotCt.addDirective(new Directive('slot', 'default'));
                    //当前位置，用slot替代
                    dom.children.splice(j, 1, slotCt);
                }
                else { //直接删除
                    dom.children.splice(j--, 1);
                }
                //添加到slotCt
                slotCt.add(c);
            }
        }
    }
    /**
     * 标签闭合
     */
    handleCloseTag(dom, isSelfClose) {
        this.postHandleNode(dom);
        dom.sortDirective();
        if (!isSelfClose) {
            this.handleSlot(dom);
        }
        //闭合节点出栈
        this.domArr.pop();
        //设置current为最后一个节点
        if (this.domArr.length > 0) {
            this.current = this.domArr[this.domArr.length - 1];
        }
        // 取消isSvg标识
        if (dom.tagName === 'svg') {
            this.isSvg = false;
        }
    }
    /**
     * 判断节点是否为空节点
     * @param dom -	带检测节点
     * @returns
     */
    isVoidTab(dom) {
        return voidTagMap.has(dom.tagName);
    }
}

/**
 * dom比较器
 */
class DiffTool {
    /**
     * 比较节点
     *
     * @param src -         待比较节点（新树节点）
     * @param dst - 	    被比较节点 (旧树节点)
     * @param changeArr -   增删改的节点数组
     * @returns	            改变的节点数组
     */
    static compare(src, dst) {
        const changeArr = [];
        compare(src, dst);
        return changeArr;
        /**
         * 比较节点
         * @param src -     待比较节点（新节点）
         * @param dst - 	被比较节点 (旧节点)
         */
        function compare(src, dst) {
            if (!src.tagName) { //文本节点
                if (!dst.tagName) {
                    if ((src.staticNum || dst.staticNum) && src.textContent !== dst.textContent) {
                        addChange(2, src, dst, dst.parent);
                    }
                    else if (src.childModuleId !== dst.childModuleId) { //子模块不同
                        addChange(5, src, dst, dst.parent);
                    }
                }
                else { //节点类型不同，替换
                    addChange(5, src, dst, dst.parent);
                }
            }
            else {
                //节点类型不同或对应的子模块不同，替换
                if ((src.childModuleId || dst.childModuleId) && src.childModuleId !== dst.childModuleId || src.tagName !== dst.tagName) {
                    addChange(5, src, dst, dst.parent);
                }
                else { //节点类型相同，但有一个不是静态节点或为根节点，进行属性比较
                    if ((src.staticNum || dst.staticNum || dst.key === 1) && isChanged(src, dst)) {
                        addChange(2, src, dst, dst.parent);
                    }
                    // 非子模块不比较子节点或者作为slot的子模块
                    compareChildren(src, dst);
                }
            }
            //如果src没有node，则使用dst的node
            if (!src.node) {
                src.node = dst.node;
            }
        }
        /**
         * 比较子节点
         * @param src -   新节点
         * @param dst -   旧节点
         */
        function compareChildren(src, dst) {
            //子节点处理
            if (!src.children || src.children.length === 0) {
                // 旧节点的子节点全部删除
                if (dst.children && dst.children.length > 0) {
                    dst.children.forEach(item => addChange(3, item, null, dst));
                }
            }
            else {
                //全部新加节点
                if (!dst.children || dst.children.length === 0) {
                    src.children.forEach((item, index) => addChange(1, item, null, dst, index));
                }
                else { //都有子节点
                    //存储比较后需要add的key
                    // const addObj={};
                    for (let ii = 0; ii < src.children.length; ii++) {
                        const node = src.children[ii];
                        const index = dst.children.findIndex(item => item.key === node.key);
                        if (index === -1) {
                            addChange(1, node, null, dst, ii);
                        }
                        else {
                            compare(node, dst.children[index]);
                            if (ii !== index) {
                                addChange(4, node, null, dst, ii, index);
                            }
                            //设置保留标志
                            dst.children[index].saved = true;
                        }
                    }
                    //删除多余节点
                    for (let o of dst.children) {
                        if (!o.saved) {
                            addChange(3, o, dst);
                        }
                    }
                    //子节点对比策略
                    // let [newStartIdx,newEndIdx,oldStartIdx,oldEndIdx] = [0,src.children.length-1,0,dst.children.length-1];
                    // let [newStartNode,newEndNode,oldStartNode,oldEndNode] = [
                    //     src.children[newStartIdx],
                    //     src.children[newEndIdx],
                    //     dst.children[oldStartIdx],
                    //     dst.children[oldEndIdx]
                    // ]
                    // while(newStartIdx <= newEndIdx && oldStartIdx <= oldEndIdx) {
                    //     if (oldStartNode.key === newStartNode.key) {  //新前旧前
                    //         compare(newStartNode,oldStartNode);
                    //         if(newStartIdx !== oldStartIdx){
                    //             addChange(4,newStartNode,null,dst,newStartIdx,oldStartIdx);
                    //         }
                    //         newStartNode = src.children[++newStartIdx];
                    //         oldStartNode = dst.children[++oldStartIdx];
                    //     } else if (oldEndNode.key === newEndNode.key) { //新后旧后
                    //         compare(newEndNode,oldEndNode);
                    //         if(oldEndIdx !== newEndIdx){
                    //             addChange(4,newEndNode,null,dst,newEndIdx,oldEndIdx);
                    //         }
                    //         newEndNode = src.children[--newEndIdx];
                    //         oldEndNode = dst.children[--oldEndIdx];
                    //     } else if (newStartNode.key === oldEndNode.key) { //新前旧后
                    //         //新前旧后
                    //         compare(newStartNode,oldEndNode);
                    //         //放在指定位置
                    //         if(newStartIdx !== oldEndIdx){
                    //             addChange(4,newStartNode,null,dst,newStartIdx,oldEndIdx);
                    //         }
                    //         newStartNode = src.children[++newStartIdx];
                    //         oldEndNode = dst.children[--oldEndIdx];
                    //     } else if (newEndNode.key === oldStartNode.key) {  //新后旧前
                    //         compare(newEndNode,oldStartNode);
                    //         if(newEndIdx !== oldStartIdx){
                    //             addChange(4, newEndNode, null,dst, newEndIdx,oldStartIdx);
                    //         }
                    //         newEndNode = src.children[--newEndIdx];
                    //         oldStartNode = dst.children[++oldStartIdx];
                    //     } else { // 都不相同
                    //         //加入到addObj
                    //         addObj[newStartNode.key]= addChange(1, newStartNode, null,dst,newStartIdx);
                    //         //新前指针后移
                    //         newStartNode = src.children[++newStartIdx];
                    //     }
                    // }
                    //多余新节点，需要添加
                    // if(newStartIdx<=newEndIdx) {
                    //     for (let i = newStartIdx; i <= newEndIdx; i++) {
                    //         // 添加到dst.children[i]前面
                    //         addChange(1,src.children[i], null ,dst,i);
                    //     }
                    // }
                    //有多余老节点，需要删除或变成移动
                    // if(oldStartIdx<=oldEndIdx){
                    //     for (let i = oldStartIdx,index=i; i <= oldEndIdx; i++,index++) {
                    //         const ch=dst.children[i];
                    //         //如果要删除的节点在addArr中，则表示move，否则表示删除
                    //         if(addObj.hasOwnProperty(ch.key)){ 
                    //             const o = addObj[ch.key];
                    //             if(index !== o[4]){ //修改add为move
                    //                 o[0] = 4;
                    //                 //设置move前位置
                    //                 o[5] = i;
                    //                 //从add转为move，需要比较新旧节点
                    //                 compare(o[1],ch);
                    //             }else{  //删除不需要移动的元素
                    //                 let ii;
                    //                 if((ii=changeArr.findIndex(item=>item[1].key === o[1].key)) !== -1){
                    //                     changeArr.splice(ii,1);
                    //                 }
                    //             }
                    //         }else{
                    //             addChange(3,ch,null,dst);
                    //             //删除的元素索引号-1，用于判断是否需要移动节点
                    //             index--;
                    //         }
                    //     }
                    // }
                }
            }
        }
        /**
         * 判断节点是否修改
         * @param src - 新树节点
         * @param dst - 旧树节点
         * @returns     true/false
         */
        function isChanged(src, dst) {
            for (const p of ['props', 'assets', 'events']) {
                //属性比较
                if (!src[p] && dst[p] || src[p] && !dst[p]) {
                    return true;
                }
                else if (src[p] && dst[p]) {
                    const keys = Object.keys(src[p]);
                    const keys1 = Object.keys(dst[p]);
                    if (keys.length !== keys1.length) {
                        return true;
                    }
                    else {
                        for (const k of keys) {
                            if (src[p][k] !== dst[p][k]) {
                                return true;
                            }
                        }
                    }
                }
            }
            return false;
        }
        /**
         * 添加到修改数组
         * @param type -    类型 add 1, upd 2,del 3,move 4 ,rep 5
         * @param dom -     目标节点
         * @param dom1 -    相对节点（被替换时有效）
         * @param parent -  父节点
         * @param loc -     添加或移动的目标index
         * @param loc1 -    被移动前位置
         * @returns         changed dom
        */
        function addChange(type, dom, dom1, parent, loc, loc1) {
            const o = [type, dom, dom1, parent, loc, loc1];
            // 被替换的不需要保留node
            if (type === 5) {
                delete dom.node;
            }
            changeArr.push(o);
            return o;
        }
    }
}

/**
 * 自定义元素
 *
 * @remarks
 * 用于扩充标签，主要用于指令简写，参考 ./extend/elementinit.ts。
 *
 * 如果未指定标签名，默认为`div`，也可以用`tag`属性指定
 *
 * @example
 * ```html
 *   <!-- 渲染后标签名为div -->
 *   <if cond={{any}}>hello</if>
 *   <!-- 渲染后标签名为p -->
 *   <if cond={{any}} tag='p'>hello</if>
 * ```
 */
class DefineElement {
    /**
     * 构造器，在dom编译后执行
     * @param node -    虚拟dom节点
     * @param module -  模块
     */
    constructor(node, module) {
        if (node.hasProp('tag')) {
            node.tagName = node.getProp('tag');
            node.delProp('tag');
        }
        else {
            node.tagName = 'div';
        }
    }
}

/**
 * dom管理器
 * @remarks
 * 用于管理module的虚拟dom树，渲染树，html节点
 */
class DomManager {
    /**
     * 构造方法
     * @param module -  所属模块
     */
    constructor(module) {
        this.module = module;
    }
    /**
     * 从virtual dom 树获取虚拟dom节点
     * @param key - dom key 或 props键值对
     * @returns     编译后虚拟节点
     */
    getVirtualDom(key) {
        if (!this.vdomTree) {
            return null;
        }
        return find(this.vdomTree);
        function find(dom) {
            //对象表示未props查找
            if (typeof key === 'object') {
                if (!Object.keys(key).find(k => key[k] !== dom.props.get(k))) {
                    return dom;
                }
            }
            else if (dom.key === key) { //key查找
                return dom;
            }
            if (dom.children) {
                for (const d of dom.children) {
                    const d1 = find(d);
                    if (d1) {
                        return d1;
                    }
                }
            }
        }
    }
    /**
     * 从渲染树获取key对应的渲染节点
     * @param key - dom key 或 props键值对
     * @returns     渲染后虚拟节点
     */
    getRenderedDom(key) {
        if (!this.renderedTree) {
            return;
        }
        return find(this.renderedTree, key);
        /**
         * 递归查找
         * @param dom - 渲染dom
         * @param key -   待查找key
         * @returns     key对应renderdom 或 undefined
         */
        function find(dom, key) {
            //对象表示未props查找
            if (typeof key === 'object') {
                if (dom.props && !Object.keys(key).find(k => key[k] !== dom.props[k])) {
                    return dom;
                }
            }
            else if (dom.key === key) { //key查找
                return dom;
            }
            if (dom.children) {
                for (const d of dom.children) {
                    if (!d) {
                        continue;
                    }
                    const d1 = find(d, key);
                    if (d1) {
                        return d1;
                    }
                }
            }
        }
    }
    /**
     * 释放节点
     * @remarks
     * 释放操作包括：如果被释放节点包含子模块，则子模块需要unmount；释放对应节点资源
     * @param dom -         虚拟dom
     * @param destroy -     是否销毁，当dom带有子模块时，如果设置为true，则子模块执行destroy，否则执行unmount
     */
    freeNode(dom, destroy) {
        if (dom.childModuleId) { //子模块
            const m = ModuleFactory.get(dom.childModuleId);
            if (m) {
                destroy ? m.destroy() : m.unmount();
            }
        }
        else { //普通节点
            const el = dom.node;
            //解绑所有事件
            this.module.eventFactory.removeEvent(dom);
            //子节点递归操作
            if (dom.children) {
                for (const d of dom.children) {
                    this.freeNode(d, destroy);
                }
            }
            // 从html移除
            if (el && el.parentElement) {
                el.parentElement.removeChild(el);
            }
        }
        //清除缓存
        const m1 = ModuleFactory.get(dom.moduleId);
        if (m1) {
            m1.objectManager.clearDomParams(dom.key);
        }
    }
}

/**
 * 事件工厂
 *
 * @remarks
 * 每个模块一个事件工厂，用于管理模块内虚拟dom对应的事件对象
 * 事件配置不支持表达式
 * 代理事件不支持capture和nopopo
 *
 * 当父模块传递事件给子模块时，子模块根节点的参数model为子模块srcDom的model（来源于父模块），根节点自带事件model为子模块model
 * 如果存在传递事件和自带事件，则执行顺序为 1.自带事件 2.传递事件
 * 示例如下：
 * ```ts
 * class M1 extends Module{
 *      template(props){
 *           return `
 *              <button e-click='click1'>${props.title}</button>
 *           `
 *       }
 *       click1(model,dom){
 *           console.log('m1自带事件触发',model,dom);
 *       }
 *   }
 * }
 * class MMain extends Module{
 *      modules=[M1];
 *      template(){
 *          return `
 *              <div>
 *                  <h3>子模块事件测试</h3>
 *                  <p>我是模块main</p>
 *                  <m1 title='aaa' e-click='clickBtn:delg'/>
 *              </div>
 *          `
 *      }
 *      clickBtn(model,dom){
 *          console.log('传递事件',model,dom);
 *      }
 * }
 * ```
 * 当点击按钮时，先执行M1的click1方法，再执行MMain的clickBtn方法，打印的model不相同，dom也不相同
 */
class EventFactory {
    /**
     * 构造器
     * @param module - 模块
     */
    constructor(module) {
        /**
         * 自有事件map
         * key: dom key
         * value: 对象
         * ```json
         * {
         *      eventName:事件名,
         *      controller:用于撤销事件绑定的 abortcontroller
         * }
         * ```
         * 相同eventName可能有多个事件
         */
        this.eventMap = new Map();
        /**
         * 代理事件map
         * key: domkey
         * value: 对象
         * ```json
         * {
         *      eventName:{
         *          controller:abort controller,
         *          events:{
         *              event:事件对象,
         *              dom:渲染节点,
         *              el:dom对应element
         *      }
         * }
         * ```
         * eventName：事件名，如click、mousemove等
         */
        this.delgMap = new Map();
        this.module = module;
    }
    /**
     * 删除事件
     * @param event -     事件对象
     * @param key -       对应dom keys
     */
    addEvent(dom, event) {
        dom.events.push(event);
        if (dom.node) {
            this.bind(dom, [event]);
        }
    }
    /**
     * 删除事件
     * @param dom -     事件对象
     * @param event -   如果没有指定event，则表示移除该节点所有事件
     */
    removeEvent(dom, event) {
        var _a, _b, _c;
        const events = event ? [event] : dom.events;
        if (!events || !Array.isArray(events)) {
            return;
        }
        for (const ev of events) {
            if (ev.delg) { //代理事件
                //为避免key冲突，外部key后面添加s
                const pkey = dom.key === 1 ? ((_b = (_a = this.module.srcDom) === null || _a === void 0 ? void 0 : _a.parent) === null || _b === void 0 ? void 0 : _b.key) + 's' : (_c = dom.parent) === null || _c === void 0 ? void 0 : _c.key;
                //找到父对象
                if (!parent || !this.delgMap.has(pkey)) {
                    return;
                }
                const cfg = this.delgMap.get(dom.parent.key);
                if (!cfg[event.name]) {
                    return;
                }
                const obj = cfg[event.name];
                const index = obj.events.findIndex(item => item.event === event);
                if (index !== -1) {
                    obj.events.splice(index, 1);
                }
                //解绑事件
                if (obj.events.length === 0) {
                    this.unbind(dom.parent.key, event);
                }
            }
            else {
                this.unbind(dom.key, event);
            }
        }
    }
    /**
     * 绑定dom节点所有事件
     * @remarks
     * 执行addEventListener操作
     * @param dom -   渲染dom
     * @param event - 事件对象数组
     */
    bind(dom, events) {
        const el = dom.node;
        for (const ev of events) {
            if (ev.delg) { //代理事件
                //如果为子模块，则取srcDom.parent进行代理
                const parent = dom.key === 1 ? this.module.srcDom.parent : dom.parent;
                if (parent) {
                    this.bindDelg(parent, dom, ev);
                }
            }
            else {
                const controller = new AbortController();
                //绑定事件
                el.addEventListener(ev.name, (e) => {
                    //禁止冒泡
                    if (ev.nopopo) {
                        e.stopPropagation();
                    }
                    this.invoke(ev, dom, e);
                }, {
                    capture: ev.capture,
                    once: ev.once,
                    signal: controller.signal
                });
                //once为true不保存
                if (!ev.once) {
                    const o = { eventName: ev.name, controller: controller };
                    //保存signal用于撤销事件
                    if (!this.eventMap.has(dom.key)) {
                        this.eventMap.set(dom.key, [o]);
                    }
                    else {
                        this.eventMap.get(dom.key).push(o);
                    }
                }
            }
        }
    }
    /**
     * 绑定到代理对象
     * @param dom -     代理dom
     * @param dom1 -    被代理dom
     * @param event -   事件对象
     */
    bindDelg(dom, dom1, event) {
        let map;
        //代理dom和被代理dom节点不一致，则需要在key后面添加s
        const pkey = dom.moduleId !== dom1.moduleId ? dom.key + 's' : dom.key;
        if (!this.delgMap.has(dom.key)) {
            map = new Map();
            this.delgMap.set(pkey, map);
        }
        else {
            map = this.delgMap.get(pkey);
        }
        let cfg;
        if (map.has(event.name)) {
            cfg = map.get(event.name);
            cfg.events.push({ event: event, dom: dom1 });
        }
        else {
            cfg = { controller: new AbortController, events: [{ event: event, dom: dom1 }] };
            map.set(event.name, cfg);
            dom.node.addEventListener(event.name, (e) => {
                this.doDelgEvent(dom, event.name, e);
            }, {
                signal: cfg.controller.signal
            });
        }
    }
    /**
     * 解绑dom节点事件
     * @param dom -     渲染dom或dom key
     * @param event -   事件对象或事件名，如果为空，则解绑该dom的所有事件，如果为事件名，则表示代理事件
     * @param delg -    是否为代理事件，默认false
     */
    unbind(key, event, delg) {
        var _a, _b, _c;
        //获取代理标志
        if (event && event instanceof NEvent) {
            delg || (delg = event.delg);
        }
        if (delg) { //代理事件
            if (!this.delgMap.has(key)) {
                return;
            }
            const obj = this.delgMap.get(key);
            if (event) { //清除指定事件
                const eventName = event instanceof NEvent ? event.name : event;
                if (obj.has(eventName)) {
                    (_b = (_a = obj.get(eventName)) === null || _a === void 0 ? void 0 : _a.controller) === null || _b === void 0 ? void 0 : _b.abort();
                }
            }
            else {
                for (const k of obj.keys()) {
                    (_c = obj.get(k).controller) === null || _c === void 0 ? void 0 : _c.abort();
                }
            }
        }
        else {
            if (!this.eventMap.has(key)) {
                return;
            }
            const arr = this.eventMap.get(key);
            if (event) {
                const o = arr.find(item => item.eventName === event);
                if (o) {
                    o.controller.abort();
                }
            }
            else {
                for (const o of arr) {
                    o.controller.abort();
                }
            }
            if (arr.length === 0) {
                this.eventMap.delete(key);
            }
        }
    }
    /**
     * 执行代理事件
     * @param dom -         代理节点
     * @param eventName -   事件名
     * @param e -           html event对象
     */
    doDelgEvent(dom, eventName, e) {
        //代理dom和被代理dom节点不一致，则需要在key后面添加s
        const key = dom.moduleId !== this.module.id ? dom.key + 's' : dom.key;
        if (!this.delgMap.has(key)) {
            return;
        }
        const map = this.delgMap.get(key);
        if (!map.has(eventName)) {
            return;
        }
        const cfg = map.get(eventName);
        const elArr = e.path || (e.composedPath ? e.composedPath() : undefined);
        if (!elArr) {
            return;
        }
        for (let ii = 0; ii < cfg.events.length; ii++) {
            const obj = cfg.events[ii];
            const ev = obj.event;
            //被代理的dom
            const dom1 = obj.dom;
            const el = dom1.node;
            for (let i = 0; i < elArr.length && elArr[i] !== dom.node; i++) {
                if (elArr[i] === el) {
                    this.invoke(ev, dom1, e);
                    // 只执行1次,移除事件
                    if (ev.once) {
                        //从当前dom删除
                        cfg.events.splice(ii--, 1);
                        //如果事件为空，则移除绑定的事件
                        if (cfg.events.length === 0) {
                            //解绑代理事件
                            this.unbind(key, eventName, true);
                        }
                    }
                    break;
                }
            }
        }
    }
    /**
     * 调用方法
     * @param event -   事件对象
     * @param dom -     渲染节点
     * @param e -       html 事件对象
     */
    invoke(event, dom, e) {
        // 如果事件所属模块和当前模块一致，则用当前dom model，否则表示为从父模块传递的事件，用子模块对应srcDom的model
        let model;
        if (event.module && event.module.id !== this.module.id) {
            model = this.module.srcDom.model;
            dom = this.module.srcDom;
        }
        else {
            //如果事件未更新，则dom还是之前的dom，需要找到最新的dom
            dom = event.module.getRenderedDom(dom.key);
            model = dom.model;
        }
        if (typeof event.handler === 'string') {
            event.module.invokeMethod(event.handler, model, dom, event, e);
        }
        else if (typeof event.handler === 'function') {
            event.handler.apply(event.module || this.module, [model, dom, event, e]);
        }
    }
    /**
     * 清除所有事件
     */
    clear() {
        //清除普通事件
        for (const key of this.eventMap.keys()) {
            this.unbind(key);
        }
        this.eventMap.clear();
        //清除代理事件
        for (const key of this.delgMap.keys()) {
            this.unbind(key, null, true);
        }
        this.delgMap.clear();
    }
    /**
     * 处理dom event
     * @param dom -     新dom
     * @param oldDom -  旧dom，dom进行修改时有效
     */
    handleDomEvent(dom, oldDom) {
        const events = dom.events;
        let arr = [];
        //存在旧节点时，需要对比旧节点事件
        if (oldDom && Array.isArray(oldDom.events)) {
            const oldEvents = oldDom.events;
            if (Array.isArray(events)) {
                events.forEach((ev) => {
                    let index;
                    //如果在旧节点已存在该事件，则从旧事件中移除
                    if ((index = oldEvents.indexOf(ev)) !== -1) {
                        oldEvents.splice(index, 1);
                    }
                    else { //记录未添加事件
                        arr.push(ev);
                    }
                });
            }
            //删除多余事件
            if (oldEvents.length > 0) {
                for (const ev of oldEvents) {
                    this.removeEvent(oldDom, ev);
                }
            }
        }
        else {
            arr = events;
        }
        //处理新节点剩余事件
        if ((arr === null || arr === void 0 ? void 0 : arr.length) > 0) {
            this.bind(dom, arr);
        }
    }
}

/**
 * 缓存模块
 */
class NCache {
    constructor() {
        /**
         * 缓存数据容器
         */
        this.cacheData = {};
        /**
         * 订阅map，格式为
         * ```js
         * {
         *  key:[{
         *      module:订阅模块,
         *      handler:回调钩子
         * },...]}
         * ```
         */
        this.subscribeMap = new Map();
    }
    /**
     * 通过提供的键名从内存中拿到对应的值
     * @param key - 键，支持"."（多级数据分割）
     * @returns     值或undefined
     */
    get(key) {
        let p = this.cacheData;
        if (key.indexOf('.') !== -1) {
            const arr = key.split('.');
            if (arr.length > 1) {
                for (let i = 0; i < arr.length - 1 && p; i++) {
                    p = p[arr[i]];
                }
                if (p) {
                    key = arr[arr.length - 1];
                }
            }
        }
        if (p) {
            return p[key];
        }
    }
    /**
     * 通过提供的键名和值将其存储在内存中
     * @param key -     键
     * @param value -   值
     */
    set(key, value) {
        let p = this.cacheData;
        const key1 = key;
        if (key.indexOf('.') !== -1) {
            const arr = key.split('.');
            if (arr.length > 1) {
                for (let i = 0; i < arr.length - 1; i++) {
                    if (!p[arr[i]] || typeof p[arr[i]] !== 'object') {
                        p[arr[i]] = {};
                    }
                    p = p[arr[i]];
                }
                key = arr[arr.length - 1];
            }
        }
        if (p) {
            p[key] = value;
        }
        //处理订阅
        if (this.subscribeMap.has(key1)) {
            const arr = this.subscribeMap.get(key1);
            for (const a of arr) {
                this.invokeSubscribe(a.module, a.handler, value);
            }
        }
    }
    /**
     * 通过提供的键名将其移除
     * @param key -   键
     */
    remove(key) {
        let p = this.cacheData;
        if (key.indexOf('.') !== -1) {
            const arr = key.split('.');
            if (arr.length > 1) {
                for (let i = 0; i < arr.length - 1 && p; i++) {
                    p = p[arr[i]];
                }
                if (p) {
                    key = arr[arr.length - 1];
                }
            }
        }
        if (p) {
            delete p[key];
        }
    }
    /**
     * 订阅
     * @param module -    订阅的模块
     * @param key -       订阅的属性名
     * @param handler -   回调函数或方法名（方法属于module），方法传递参数为订阅属性名对应的值
     */
    subscribe(module, key, handler) {
        if (!this.subscribeMap.has(key)) {
            this.subscribeMap.set(key, [{ module: module, handler: handler }]);
        }
        else {
            const arr = this.subscribeMap.get(key);
            if (!arr.find(item => item.module === module && item.handler === handler)) {
                arr.push({ module: module, handler: handler });
            }
        }
        //如果存在值，则执行订阅回调
        const v = this.get(key);
        if (v) {
            this.invokeSubscribe(module, handler, v);
        }
    }
    /**
     * 调用订阅方法
     * @param module -  模块
     * @param foo -     方法或方法名
     * @param v -       值
     */
    invokeSubscribe(module, foo, v) {
        if (typeof foo === 'string') {
            module.invokeMethod(foo, v);
        }
        else {
            foo.call(module, v);
        }
    }
}

/**
 * 全局缓存
 *
 * @remarks
 * 用于所有模块共享数据，实现模块通信
 */
class GlobalCache {
    /**
     * 保存到cache
     * @param key -     键，支持"."（多级数据分割）
     * @param value -   值
     */
    static set(key, value) {
        this.cache.set(key, value);
    }
    /**
     * 从cache读取
     * @param key - 键，支持"."（多级数据分割）
     * @returns     缓存的值或undefined
     */
    static get(key) {
        return this.cache.get(key);
    }
    /**
     * 订阅
     *
     * @remarks
     * 如果订阅的数据发生改变，则会触发handler
     *
     * @param module -    订阅的模块
     * @param key -       订阅的属性名
     * @param handler -   回调函数或方法名（方法属于module），方法传递参数为订阅属性名对应的值
     */
    static subscribe(module, key, handler) {
        this.cache.subscribe(module, key, handler);
    }
    /**
     * 从cache移除
     * @param key -   键，支持"."（多级数据分割）
     */
    static remove(key) {
        this.cache.remove(key);
    }
}
/**
 * NCache实例，用于存放缓存对象
 */
GlobalCache.cache = new NCache();

/**
 * watch 管理器
 */
class Watcher {
    /**
     * 添加监听
     * @remarks 相同model、key、module只能添加一次
     * @param module -  所属模块
     * @param model -   监听model
     * @param key -     监听属性或属性数组，如果为深度watch，则为func
     * @param func -    触发函数，参数依次为 model,key,oldValue,newValue，如果为深度watch，则为deep
     * @returns
     */
    static watch(module, model, key, func) {
        //深度监听
        if (typeof key === 'function') {
            return this.watchDeep(module, model, key);
        }
        if (!Array.isArray(key)) {
            key = [key];
        }
        for (let k of key) {
            if (!this.map.has(model)) {
                const o = {};
                o[k] = [{ module: module, func: func }];
                this.map.set(model, o);
            }
            else {
                const o = this.map.get(model);
                if (!o.hasOwnProperty(k)) {
                    o[k] = [{ module: module, func: func }];
                    this.map.set(model, o);
                }
                else {
                    const a = o[k];
                    //相同module只能监听一次
                    if (a.find(item => item.module === module)) {
                        continue;
                    }
                    a.push({ module: module, func: func });
                }
            }
        }
        //返回取消watch函数
        return () => {
            const o = this.map.get(model);
            for (let k of key) {
                let ii;
                //找到对应module的watch
                if ((ii = o[k].findIndex(item => item.module === module)) !== -1) {
                    o[k].splice(ii, 1);
                }
                if (o[k].length === 0) {
                    delete o[k];
                }
            }
        };
    }
    /**
     * 深度监听
     * @param module
     * @param model
     * @param func
     * @returns
     */
    static watchDeep(module, model, func) {
        if (this.deepMap.has(model)) {
            const arr = this.deepMap.get(model);
            if (arr.find(item => item.module === module)) {
                return;
            }
            arr.push({ module: module, func: func });
        }
        else {
            this.deepMap.set(model, [{ module: module, func: func }]);
        }
        return () => {
            this.deepMap.delete(model);
        };
    }
    /**
     * 处理监听
     * @param model -       model
     * @param key -         监听的属性名
     * @param oldValue -    旧值
     * @param newValue -    新值
     */
    static handle(model, key, oldValue, newValue) {
        if (this.map.size === 0 && this.deepMap.size === 0) {
            return;
        }
        let arr = [];
        if (this.map.has(model)) {
            const a = this.map.get(model)[key];
            if (a) {
                arr = a;
            }
        }
        //查找父model watch为true的对象
        if (this.deepMap.size > 0) {
            for (let m = model['__parent']; m; m = m['__parent']) {
                if (this.deepMap.has(m)) {
                    arr = arr.concat(this.deepMap.get(m));
                }
            }
        }
        if (arr.length > 0) {
            for (let o of arr) {
                o.func.call(o.module, model, key, oldValue, newValue);
            }
        }
    }
}
/**
 * model map
 * key: model
 * value: {key:{module:来源module,func:触发函数,deep:深度监听}}，其中key为监听属性
 */
Watcher.map = new Map();
/**
 * 深度watch map
 * key: model
 * value: {module:来源module,func:触发函数,deep:深度监听}
 */
Watcher.deepMap = new Map();

/**
 * 模型工厂
 * @remarks
 * 管理模块的model
 */
class ModelManager {
    /**
     * 添加共享model
     * @param model
     * @param modules
     */
    static addShareModel(model, module) {
        if (!this.shareModelMap.has(model)) {
            this.shareModelMap.set(model, [model['__module'], module]);
            return;
        }
        const arr = this.shareModelMap.get(model);
        if (arr.indexOf(module) === -1) {
            arr.push(module);
        }
    }
    /**
     * 获取module
     * @param model -   model
     * @returns         module或module数组（共享时）
     */
    static getModule(model) {
        if (this.shareModelMap.has(model)) {
            return this.shareModelMap.get(model);
        }
        return model['__module'];
    }
    /**
     * 更新model
     * @param model
     * @param key
     * @param oldValue
     * @param newValue
     * @returns
     */
    static update(model, key, oldValue, newValue) {
        if (this.shareModelMap.size > 0) {
            for (let m = model; m; m = m['__parent']) {
                if (this.shareModelMap.has(m)) {
                    for (let mdl of this.shareModelMap.get(m)) {
                        Renderer.add(mdl);
                    }
                    Watcher.handle(model, key, oldValue, newValue);
                    return;
                }
            }
        }
        Renderer.add(model['__module']);
        Watcher.handle(model, key, oldValue, newValue);
    }
    /**
     * 获取model属性值
     * @param key -     属性名，可以分级，如 name.firstName
     * @param model -   模型
     * @returns         属性值
     */
    static get(model, key) {
        if (key) {
            if (key.indexOf('.') !== -1) { //层级字段
                const arr = key.split('.');
                for (let i = 0; i < arr.length - 1; i++) {
                    model = model[arr[i]];
                    if (!model) {
                        break;
                    }
                }
                if (!model) {
                    return;
                }
                key = arr[arr.length - 1];
            }
            model = model[key];
        }
        return model;
    }
    /**
     * 设置model属性值
     * @param model -   模型
     * @param key -     属性名，可以分级，如 name.firstName
     * @param value -   属性值
     */
    static set(model, key, value) {
        if (key.includes('.')) { //层级字段
            const arr = key.split('.');
            for (let i = 0; i < arr.length - 1; i++) {
                //不存在，则创建新的model
                if (!model[arr[i]]) {
                    model[arr[i]] = {};
                }
                model = model[arr[i]];
            }
            key = arr[arr.length - 1];
        }
        model[key] = value;
    }
}
/**
 * 共享model，被多个module使用
 *
 */
ModelManager.shareModelMap = new Map();

/**
 * 模型类
 *
 * @remarks
 * 模型就是对数据做代理
 *
 * 注意：数据对象中，以下5个属性名（保留字）不能用，可以通过如：`model.__module`的方式获取保留属性
 *
 *      __key:模型的key
 *
 *      __module:所属模块
 *
 *      __parent:父模型
 *
 */
class Model {
    /**
     * @param data -    数据
     * @param module - 	模块对象
     * @param parent -  父模型
     * @param name -    模型在父对象中的prop name
     * @returns         模型
     */
    constructor(data, module, parent, name) {
        //数据不存在或已经代理，无需再创建
        if (!data || typeof data !== 'object' || data['__module']) {
            return data;
        }
        //设置key
        data['__key'] = Util.genId();
        // 创建模型
        const proxy = new Proxy(data, {
            set(src, key, value, receiver) {
                //值未变,proxy 不处理
                if (src[key] === value) {
                    return true;
                }
                //已是model且src module和value module不一致，表示被多个module共享，通常在传值过程中
                //加入共享model管理
                if (value && value['__module'] && src['__module'] !== value['__module']) {
                    ModelManager.addShareModel(value, src['__module'] || module);
                }
                const ov = receiver[key];
                src[key] = value;
                //model更新
                ModelManager.update(receiver, key, ov, value);
                return true;
            },
            get(src, key, receiver) {
                //如果为代理，则返回module
                if (key === '__module') {
                    return receiver ? module : undefined;
                }
                //父模型
                if (key === '__parent') {
                    return parent;
                }
                let m = src[key];
                //对象尚未初始化为model
                if (m && typeof m === 'object' && !m['__module']) {
                    m = new Model(m, module, receiver, key);
                    src[key] = m;
                }
                return m;
            },
            deleteProperty(src, key) {
                const oldValue = src[key];
                delete src[key];
                ModelManager.update(proxy, key, oldValue, undefined);
                return true;
            }
        });
        return proxy;
    }
}

/**
 * 对象管理器
 * @remarks
 * 用于存储模块的内存变量，`$`开始的数据项可能被nodom占用，使用时禁止使用。
 *
 * 默认属性集
 *
 *  $events     事件集
 *
 *  $domparam   dom参数
 */
class ObjectManager {
    /**
     * module   模块
     * @param module - 模块
     */
    constructor(module) {
        this.module = module;
        this.cache = new NCache();
    }
    /**
     * 保存到cache
     * @param key -     键，支持"."（多级数据分割）
     * @param value -   值
     */
    set(key, value) {
        this.cache.set(key + '', value);
    }
    /**
     * 从cache读取
     * @param key - 键，支持多级数据，如"x.y.z"
     * @returns     缓存的值或undefined
     */
    get(key) {
        return this.cache.get(key);
    }
    /**
     * 从cache移除
     * @param key -   键，支持"."（多级数据分割）
     */
    remove(key) {
        this.cache.remove(key);
    }
    /**
     * 设置事件参数
     * @param id -      事件id
     * @param key -     dom key
     * @param name -    参数名
     * @param value -   参数值
     */
    setEventParam(id, key, name, value) {
        this.cache.set('$events.' + id + '.$params.' + key + '.' + name, value);
    }
    /**
     * 获取事件参数值
     * @param id -      事件id
     * @param key -     dom key
     * @param name -    参数名
     * @returns         参数值
     */
    getEventParam(id, key, name) {
        return this.get('$events.' + id + '.$params.' + key + '.' + name);
    }
    /**
     * 移除事件参数
     * @param id -      事件id
     * @param key -     dom key
     * @param name -    参数名
     */
    removeEventParam(id, key, name) {
        this.remove('$events.' + id + '.$params.' + key + '.' + name);
    }
    /**
     * 清空事件参数
     * @param id -      事件id
     * @param key -     dom key
     */
    clearEventParams(id, key) {
        if (key) { //删除对应dom的事件参数
            this.remove('$events.' + id + '.$params.' + key);
        }
        else { //删除所有事件参数
            this.remove('$events.' + id + '.$params');
        }
    }
    /**
     * 设置dom参数值
     * @param key -     dom key
     * @param name -    参数名
     * @param value -   参数值
     */
    setDomParam(key, name, value) {
        this.set('$domparam.' + key + '.' + name, value);
    }
    /**
     * 获取dom参数值
     * @param key -     dom key
     * @param name -    参数名
     * @returns         参数值
     */
    getDomParam(key, name) {
        return this.get('$domparam.' + key + '.' + name);
    }
    /**
     * 移除dom参数值
     * @param key -     dom key
     * @param name -    参数名
     */
    removeDomParam(key, name) {
        this.remove('$domparam.' + key + '.' + name);
    }
    /**
     * 清除element 参数集
     * @param key -     dom key
     */
    clearDomParams(key) {
        this.remove('$domparam.' + key);
    }
    /**
     * 清除缓存dom对象集
     */
    clearAllDomParams() {
        this.remove('$domparam');
    }
}

/**
 * 模块状态类型
 */
var EModuleState;
(function (EModuleState) {
    /**
     * 已初始化
     */
    EModuleState[EModuleState["INIT"] = 1] = "INIT";
    /**
     * 取消挂载
     */
    EModuleState[EModuleState["UNMOUNTED"] = 2] = "UNMOUNTED";
    /**
     * 已挂载到dom树
     */
    EModuleState[EModuleState["MOUNTED"] = 3] = "MOUNTED";
})(EModuleState || (EModuleState = {}));

/**
 * 模块类
 *
 * @remarks
 * 模块方法说明：模板内使用的方法，包括事件方法，都在模块内定义
 *
 *  方法this：指向module实例
 *
 *  事件参数: model(当前按钮对应model),dom(事件对应虚拟dom),eventObj(事件对象),e(实际触发的html event)
 *
 *  表达式方法：参数按照表达式方式给定即可，如：
 * ```html
 *  <div>
 *      <div class={{getCls(st)}} e-click='click'>Hello Nodom</div>
 *  </div>
 * ```
 * ```js
 *  //事件方法
 *  click(model,dom,eventObj,e){
 *      //do something
 *  }
 *  //表达式方法
 *  //state 由表达式中给定，state由表达式传递，为当前dom model的一个属性
 *  getCls(state){
 *      //do something
 *  }
 * ```
 *
 * 模块事件，在模块不同阶段执行
 *
 * onInit              初始化后（constructor后，已经有model对象，但是尚未编译，只执行1次）
 *
 * onBeforeFirstRender 首次渲染前（只执行1次）
 *
 * onFirstRender       首次渲染后（只执行1次）
 *
 * onBeforeRender      渲染前
 *
 * onRender            渲染后
 *
 * onCompile           编译后
 *
 * onBeforeMount       挂载到document前
 *
 * onMount             挂载到document后
 *
 * onBeforeUnMount     从document脱离前
 *
 * onUnmount           从document脱离后
 *
 * onBeforeUpdate      更新到document前
 *
 * onUpdate            更新到document后
 */
class Module {
    /**
     * 构造器
     * @param id -  模块id
     */
    constructor(id) {
        /**
         * 子模块数组，模板中引用的所有子模块
         */
        this.children = [];
        /**
         * slot map
         *
         * key: slot name
         *
         * value: 渲染节点
         *
         */
        this.slots = new Map();
        this.id = id || Util.genId();
        // this.modelManager = new ModelManager(this);
        this.domManager = new DomManager(this);
        this.objectManager = new ObjectManager(this);
        this.eventFactory = new EventFactory(this);
        //加入模块工厂
        ModuleFactory.add(this);
    }
    /**
     * 初始化操作
     */
    init() {
        this.state = EModuleState.INIT;
        //注册子模块
        if (Array.isArray(this.modules)) {
            for (const cls of this.modules) {
                ModuleFactory.addClass(cls);
            }
            delete this.modules;
        }
        //初始化model
        this.model = new Model(this.data() || {}, this);
        this.doModuleEvent('onInit');
    }
    /**
     * 模板串方法，使用时需重载
     * @param props -   props对象，在模板中进行配置，从父模块传入
     * @returns         模板串
     * @virtual
     */
    template(props) {
        return null;
    }
    /**
     * 数据方法，使用时需重载
     * @returns  数据对象
     * @virtual
     */
    data() {
        return {};
    }
    /**
     * 模型渲染
     * @remarks
     * 渲染流程：
     *
     * 1. 获取首次渲染标志
     *
     * 2. 执行template方法获得模板串
     *
     * 3. 与旧模板串比较，如果不同，则进行编译
     *
     * 4. 判断是否存在虚拟dom树（编译时可能导致模板串为空），没有则结束
     *
     * 5. 如果为首次渲染，执行onBeforeFirstRender事件
     *
     * 6. 执行onBeforeRender事件
     *
     * 7. 保留旧渲染树，进行新渲染
     *
     * 8. 执行onRender事件
     *
     * 9. 如果为首次渲染，执行onFirstRender事件
     *
     * 10. 渲染树为空，从document解除挂载
     *
     * 11. 如果未挂载，执行12，否则执行13
     *
     * 12. 执行挂载，结束
     *
     * 13. 新旧渲染树比较，比较结果为空，结束，否则执行14
     *
     * 14. 执行onBeforeUpdate事件
     *
     * 15. 更新到document
     *
     * 16. 执行onUpdate事件，结束
     */
    render() {
        //不是主模块，也没有srcDom，则不渲染
        if (this !== ModuleFactory.getMain() && (!this.srcDom || this.state === EModuleState.UNMOUNTED)) {
            return;
        }
        //获取首次渲染标志
        const firstRender = this.oldTemplate === undefined;
        //检测模板并编译
        let templateStr = this.template(this.props);
        if (!templateStr) {
            return;
        }
        templateStr = templateStr.trim();
        if (templateStr === '') {
            return;
        }
        //与旧模板不一样，需要重新编译
        if (templateStr !== this.oldTemplate) {
            this.oldTemplate = templateStr;
            this.compile(templateStr);
        }
        //不存在domManager.vdomTree，不渲染
        if (!this.domManager.vdomTree) {
            return;
        }
        //首次渲染
        if (firstRender) {
            this.doModuleEvent('onBeforeFirstRender');
        }
        //渲染前事件
        this.doModuleEvent('onBeforeRender');
        //保留旧树
        const oldTree = this.domManager.renderedTree;
        //渲染
        const root = Renderer.renderDom(this, this.domManager.vdomTree, this.model);
        this.domManager.renderedTree = root;
        //每次渲染后事件
        this.doModuleEvent('onRender');
        //首次渲染
        if (firstRender) {
            this.doModuleEvent('onFirstRender');
        }
        //渲染树为空，从html卸载
        if (!this.domManager.renderedTree) {
            this.unmount();
            return;
        }
        //已经挂载
        if (this.state === EModuleState.MOUNTED) {
            if (oldTree && this.model) {
                //新旧渲染树节点diff
                const changeDoms = DiffTool.compare(this.domManager.renderedTree, oldTree);
                //执行更改
                if (changeDoms.length > 0) {
                    //html节点更新前事件
                    this.doModuleEvent('onBeforeUpdate');
                    Renderer.handleChangedDoms(this, changeDoms);
                    //html节点更新后事件
                    this.doModuleEvent('onUpdate');
                }
            }
        }
        else { //未挂载
            this.mount();
        }
    }
    /**
     * 添加子模块
     * @param module -    模块id或模块
     */
    addChild(module) {
        if (!this.children.includes(module)) {
            this.children.push(module);
            module.parentId = this.id;
        }
    }
    /**
     * 移除子模块
     * @param module -    子模块
     */
    removeChild(module) {
        const ind = this.children.indexOf(module);
        if (ind !== -1) {
            module.unmount();
            this.children.splice(ind, 1);
        }
    }
    /**
     * 激活模块(准备渲染)
     */
    active() {
        if (this.state === EModuleState.UNMOUNTED) {
            this.state = EModuleState.INIT;
        }
        Renderer.add(this);
    }
    /**
     * 挂载到document
     */
    mount() {
        var _a, _b, _c, _d;
        //不是主模块或srcDom.node没有父element，则不执行挂载
        if (this !== ModuleFactory.getMain() && !((_b = (_a = this.srcDom) === null || _a === void 0 ? void 0 : _a.node) === null || _b === void 0 ? void 0 : _b.parentElement)) {
            return;
        }
        //执行挂载前事件
        this.doModuleEvent('onBeforeMount');
        //渲染到fragment
        const rootEl = new DocumentFragment();
        const el = Renderer.renderToHtml(this, this.domManager.renderedTree, rootEl);
        //主模块，直接添加到根模块
        if (this === ModuleFactory.getMain()) {
            Renderer.getRootEl().appendChild(el);
        }
        else if ((_d = (_c = this.srcDom) === null || _c === void 0 ? void 0 : _c.node) === null || _d === void 0 ? void 0 : _d.parentElement) { //挂载到父模块中
            Util.insertAfter(el, this.srcDom.node);
        }
        //执行挂载后事件
        this.doModuleEvent('onMount');
        this.state = EModuleState.MOUNTED;
    }
    /**
     * 从document移除
     * @param passive -     被动卸载，父模块释放或模块被删除时导致的卸载，此时不再保留srcDom.node，状态修改INIT，否则修改为UNMOUNTED
     */
    unmount(passive) {
        var _a;
        // 主模块或状态为unmounted的模块不用处理
        if (this.state !== EModuleState.MOUNTED || ModuleFactory.getMain() === this) {
            return;
        }
        //从render列表移除
        Renderer.remove(this);
        //执行卸载前事件
        this.doModuleEvent('onBeforeUnMount');
        //清空event factory
        this.eventFactory.clear();
        //删除渲染树
        this.domManager.renderedTree = null;
        //设置状态，如果为被动卸载，则设置为init，否则设置为unmounted
        if (passive) {
            this.state = EModuleState.INIT;
        }
        else {
            this.state = EModuleState.UNMOUNTED;
        }
        //子模块被动卸载
        for (const m of this.children) {
            m.unmount(true);
        }
        //从html dom树摘除
        if ((_a = this.srcDom.node) === null || _a === void 0 ? void 0 : _a.parentElement) {
            //后节点不为comment，则为模块节点
            if (this.srcDom.node.nextSibling && !(this.srcDom.node.nextSibling instanceof Comment)) {
                this.srcDom.node.parentElement.removeChild(this.srcDom.node.nextSibling);
            }
            //如果是被动卸载，表示为父模块发起，则删除占位符
            if (passive) {
                this.srcDom.node.parentElement.removeChild(this.srcDom.node);
            }
        }
        //执行卸载后事件
        this.doModuleEvent('onUnMount');
    }
    /**
     * 销毁
     */
    destroy() {
        this.unmount(true);
        for (const m of this.children) {
            m.destroy();
        }
        //清理css url
        CssManager.clearModuleRules(this);
        //清除dom参数
        this.objectManager.clearAllDomParams();
        //从modulefactory移除
        ModuleFactory.remove(this.id);
    }
    /**
     * 获取父模块
     * @returns     父模块
     */
    getParent() {
        if (this.parentId) {
            return ModuleFactory.get(this.parentId);
        }
    }
    /**
     * 执行模块事件
     * @param eventName -   事件名
     * @returns             执行结果
     */
    doModuleEvent(eventName) {
        const foo = this[eventName];
        if (foo && typeof foo === 'function') {
            return foo.apply(this, [this.model]);
        }
    }
    /**
     * 设置props
     * @param props -   属性值
     * @param dom -     子模块对应渲染后节点
     */
    setProps(props, dom) {
        if (!props) {
            return;
        }
        const dataObj = props['$data'];
        delete props['$data'];
        //props数据复制到模块model
        if (dataObj) {
            for (const d of Object.keys(dataObj)) {
                this.model[d] = dataObj[d];
            }
        }
        //保留src dom
        this.srcDom = dom;
        //如果不存在旧的props，则change为true，否则初始化为false
        let change = false;
        if (!this.props) {
            change = true;
        }
        else {
            for (const k of Object.keys(props)) {
                if (props[k] !== this.props[k]) {
                    change = true;
                }
            }
        }
        //对于 MOUNTED 状态进行渲染
        if (change && this.state === EModuleState.MOUNTED) {
            Renderer.add(this);
        }
        //保存props
        this.props = props;
    }
    /**
     * 编译
     * 出现编译，表示
     */
    compile(templateStr) {
        var _a;
        this.children = [];
        //清理css url
        CssManager.clearModuleRules(this);
        //清除dom参数
        this.objectManager.clearAllDomParams();
        this.eventFactory.clear();
        this.domManager.vdomTree = new Compiler(this).compile(templateStr);
        if (!this.domManager.vdomTree) {
            return;
        }
        //添加从源dom传递的事件
        const root = this.domManager.vdomTree;
        if ((_a = this.srcDom) === null || _a === void 0 ? void 0 : _a.events) {
            if (root.events) {
                root.events = root.events.concat(this.srcDom.events);
            }
            else {
                root.events = this.srcDom.events;
            }
        }
        //增加编译后事件
        this.doModuleEvent('onCompile');
    }
    /**
     * 设置不渲染到根dom的属性集合
     * @param props -   待移除的属性名属组
     */
    setExcludeProps(props) {
        this.excludedProps = props;
    }
    /**
     * 按模块类名获取子模块
     * @remarks
     * 找到第一个满足条件的子模块，如果deep=true，则深度优先
     *
     * 如果attrs不为空，则同时需要匹配子模块属性
     *
     * @example
     * ```html
     *  <div>
     *      <Module1 />
     *      <!--other code-->
     *      <Module1 v1='a' v2='b' />
     *  </div>
     * ```
     * ```js
     *  const m = getModule('Module1',{v1:'a'},true);
     *  m 为模板中的第二个Module1
     * ```
     * @param name -    子模块类名或别名
     * @param attrs -   属性集合
     * @param deep -    是否深度获取
     * @returns         符合条件的子模块或undefined
     */
    getModule(name, attrs, deep) {
        if (!this.children) {
            return;
        }
        const cls = ModuleFactory.getClass(name);
        if (!cls) {
            return;
        }
        return find(this);
        /**
         * 查询
         * @param mdl -   模块
         * @returns     符合条件的子模块
         */
        function find(mdl) {
            for (const m of mdl.children) {
                if (m.constructor === cls) {
                    if (attrs) { //属性集合不为空
                        //全匹配标识
                        let matched = true;
                        for (const k of Object.keys(attrs)) {
                            if (!m.props || m.props[k] !== attrs[k]) {
                                matched = false;
                                break;
                            }
                        }
                        if (matched) {
                            return m;
                        }
                    }
                    else {
                        return m;
                    }
                }
                //递归查找
                if (deep) {
                    const r = find(m);
                    if (r) {
                        return r;
                    }
                }
            }
        }
    }
    /**
     * 获取模块类名对应的所有子模块
     * @param className -   子模块类名
     * @param deep -        深度查询
     */
    getModules(className, attrs, deep) {
        if (!this.children) {
            return;
        }
        const arr = [];
        find(this);
        return arr;
        /**
         * 查询
         * @param module -  模块
         */
        function find(module) {
            if (!module.children) {
                return;
            }
            for (const m of module.children) {
                if (attrs) { //属性集合不为空
                    //全匹配标识
                    let matched = true;
                    for (const k of Object.keys(attrs)) {
                        if (!m.props || m.props[k] !== attrs[k]) {
                            matched = false;
                            break;
                        }
                    }
                    if (matched) {
                        arr.push(m);
                    }
                }
                else {
                    arr.push(m);
                }
                //递归查找
                if (deep) {
                    find(m);
                }
            }
        }
    }
    /**
     * 监听model
     * @remarks
     * 参数个数可变，分为几种情况
     * 1 如果第一个参数为object，则表示为被监听的model：
     *      如果第二个为字符串或数组，则表示被监听的属性或属性数组，第三个为钩子函数
     *      如果第二个为函数，则表示钩子，表示深度监听
     * 2 如果第一个参数为属性名，即字符串或字符串数组，则第二个参数为钩子函数，此时监听对象为this.model
     * 3 如果第一个为function，则表示深度监听，此时监听对象为this.model
     * 注：当不指定监听属性时，则统一表示为深度监听，深度监听会影响渲染性能，不建议使用
     *
     * @param model -   模型或属性或钩子函数
     * @param key -     属性/属性数组/监听函数
     * @param func -    钩子函数
     * @returns         回收监听器函数，执行后取消监听
     */
    watch(model, key, func) {
        const tp = typeof model;
        if (tp === 'string' || Array.isArray(model)) { //字符串或数组
            return Watcher.watch(this, this.model, model, key);
        }
        else if (tp === 'object') { // 数据对象
            return Watcher.watch(this, model, key, func);
        }
        else if (tp === 'function') { // 钩子函数
            return Watcher.watch(this, this.model, model);
        }
    }
    /**
     * 设置模型属性值
     * @remarks
     * 参数个数可变，如果第一个参数为属性名，则第二个参数为属性值，默认model为根模型，否则按照参数说明
     *
     * @param model -     模型
     * @param key -       子属性，可以分级，如 name.firstName
     * @param value -     属性值
     */
    set(model, key, value) {
        if (typeof model === 'object') {
            ModelManager.set(model, key, value);
        }
        else {
            ModelManager.set(this.model, model, key);
        }
    }
    /**
     * 获取模型属性值
     * @remarks
     * 参数个数可变，如果第一个参数为属性名，默认model为根模型，否则按照参数说明
     *
     * @param model -   模型
     * @param key -     属性名，可以分级，如 name.firstName，如果为null，则返回自己
     * @returns         属性值
     */
    get(model, key) {
        if (typeof model === 'object') {
            return ModelManager.get(model, key);
        }
        else {
            return ModelManager.get(this.model, model);
        }
    }
    /**
     * 调用模块方法
     * @param methodName -  方法名
     * @param args -        参数
     */
    invokeMethod(methodName, ...args) {
        if (typeof this[methodName] === 'function') {
            return this[methodName](...args);
        }
    }
    /**
     * 根据条件获取渲染节点
     * @param params -  条件，dom key 或 props键值对
     * @returns         渲染节点
     */
    getRenderedDom(params) {
        return this.domManager.getRenderedDom(params);
    }
    /**
     * 根据条件获取html节点
     * @param params -  条件，dom key 或 props键值对
     * @returns         html node
     */
    getNode(params) {
        var _a;
        return (_a = this.domManager.getRenderedDom(params)) === null || _a === void 0 ? void 0 : _a.node;
    }
}

/**
 * 路由管理类
 */
class Router {
    /**
     * 构造器
     * @param basePath -          路由基础路径，显示的完整路径为 basePath + route.path
     * @param defaultEnter -      默认进入时事件函数，传递参数： module,离开前路径
     * @param defaultLeave -      默认离开时事件函数，传递参数： module,进入时路径
     */
    constructor(basePath, defaultEnter, defaultLeave) {
        /**
         * 根路由
         */
        this.root = new Route();
        /**
         * path等待链表
         */
        this.waitList = [];
        /**
         * 激活Dom map
         * key: path
         * value: object，格式为：
         * ```js
         *  {
         *      moduleId:dom所属模板模块id，
         *      model:对应model,
         *      field:激活字段名
         *  }
         * ```
         */
        // private activeModelMap: Map<string, object> = new Map();
        /**
         * 绑定到module的router指令对应的key，即router容器对应的key，格式为:
         * ```js
         *  {
         *      moduleId1:{
         *          //active节点
         *          activeDoms:[{
         *              dom:节点
         *              name:激活属性名
         *          }],
         *          dom:带router指令的dom节点
         *          //等待路由
         *          wait:route
         *      },
         *      moduleId2:...
         *  }
         * ```
         *  moduleId: router所属模块id
         */
        this.routerMap = new Map();
        this.basePath = basePath;
        this.onDefaultEnter = defaultEnter;
        this.onDefaultLeave = defaultLeave;
        //添加popstate事件
        window.addEventListener('popstate', () => {
            //根据state切换module
            const state = history.state;
            if (!state) {
                return;
            }
            this.startType = 1;
            this.go(state.url);
        });
    }
    /**
     * 跳转
     * @remarks
     * 只是添加到跳转列表，并不会立即进行跳转
     *
     * @param path -    路径
     * @param type -    启动路由类型，参考startType，默认0
     */
    go(path) {
        const me = this;
        // 当前路径的父路径不处理
        if (this.currentPath && this.currentPath.startsWith(path)) {
            return;
        }
        //添加路径到等待列表，已存在，不加入
        if (this.waitList.indexOf(path) === -1) {
            this.waitList.push(path);
        }
        checkRoot();
        /**
         * 检测routemodule是否已经存在，不存在则一致检查，直到出现为止
         */
        function checkRoot() {
            if (!me.rootModule) {
                setTimeout(checkRoot, 0);
            }
            else {
                me.load();
            }
        }
    }
    /**
     * 启动加载
     */
    load() {
        //在加载，或无等待列表，则返回
        if (this.waitList.length === 0) {
            return;
        }
        //从等待队列拿路径加载
        this.start(this.waitList.shift()).then(() => {
            //继续加载
            this.load();
        });
    }
    /**
     * 切换路由
     * @param path - 	路径
     */
    start(path) {
        var _a;
        return __awaiter(this, void 0, void 0, function* () {
            // 当前路径的父路径不处理
            if ((_a = this.currentPath) === null || _a === void 0 ? void 0 : _a.startsWith(path)) {
                return;
            }
            //保存旧path
            const oldPath = this.currentPath;
            //设置当前path
            this.currentPath = path;
            const diff = this.compare(oldPath, path);
            // 不存在上一级模块,则为主模块，否则为上一级模块
            let parentModule = diff[0] === null ? this.rootModule : yield this.getModule(diff[0]);
            //onleave事件，从末往前执行
            for (let i = diff[1].length - 1; i >= 0; i--) {
                const r = diff[1][i];
                if (!r.module) {
                    continue;
                }
                const module = yield this.getModule(r);
                if (Util.isFunction(this.onDefaultLeave)) {
                    this.onDefaultLeave(module, oldPath);
                }
                if (Util.isFunction(r.onLeave)) {
                    r.onLeave(module, oldPath);
                }
                // 取消挂载
                module.unmount();
            }
            if (diff[2].length === 0) { //路由相同，参数不同
                const route = diff[0];
                if (route !== null) {
                    yield this.getModule(route);
                    // 模块处理
                    this.handleRouteModule(route, diff[3] ? diff[3].module : null);
                }
            }
            else { //路由不同
                //加载模块
                for (let ii = 0; ii < diff[2].length; ii++) {
                    const route = diff[2][ii];
                    //路由不存在或路由没有模块（空路由）
                    if (!route || !route.module) {
                        continue;
                    }
                    const module = yield this.getModule(route);
                    // 模块处理
                    this.handleRouteModule(route, parentModule);
                    //默认全局路由enter事件
                    if (Util.isFunction(this.onDefaultEnter)) {
                        this.onDefaultEnter(module, path);
                    }
                    //当前路由进入事件
                    if (Util.isFunction(route.onEnter)) {
                        route.onEnter(module, path);
                    }
                    parentModule = module;
                }
            }
            //如果是history popstate或新路径是当前路径的子路径，则不加入history
            if (this.startType !== 1) {
                const path1 = (this.basePath || '') + path;
                //子路由或父路由，替换state
                if (path.startsWith(oldPath)) {
                    history.replaceState({ url: path1 }, '', path1);
                }
                else { //路径push进history
                    history.pushState({ url: path1 }, '', path1);
                }
            }
            //设置start类型为正常start
            this.startType = 0;
        });
    }
    /**
     * 获取module
     * @param route - 路由对象
     * @returns     路由对应模块
     */
    getModule(route) {
        return __awaiter(this, void 0, void 0, function* () {
            let module = route.module;
            //已经是模块实例
            if (typeof module === 'object') {
                return module;
            }
            //模块路径
            if (typeof module === 'string') {
                module = yield ModuleFactory.load(module);
            }
            //模块类
            if (typeof module === 'function') {
                route.module = ModuleFactory.get(module);
            }
            return route.module;
        });
    }
    /**
     * 比较两个路径对应的路由链
     * @param path1 - 	第一个路径
     * @param path2 - 	第二个路径
     * @returns 		数组 [父路由或不同参数的路由，需要销毁的路由数组，需要增加的路由数组，不同参数路由的父路由]
     */
    compare(path1, path2) {
        // 获取路由id数组
        let arr1 = null;
        let arr2 = null;
        if (path1) {
            //采用克隆方式复制，避免被第二个路径返回的路由覆盖参数
            arr1 = this.getRouteList(path1, true);
        }
        if (path2) {
            arr2 = this.getRouteList(path2);
        }
        let len = 0;
        if (arr1 !== null) {
            len = arr1.length;
        }
        if (arr2 !== null) {
            if (arr2.length < len) {
                len = arr2.length;
            }
        }
        else {
            len = 0;
        }
        //需要销毁的旧路由数组
        let retArr1 = [];
        //需要加入的新路由数组
        let retArr2 = [];
        let i = 0;
        for (i = 0; i < len; i++) {
            //找到不同路由开始位置
            if (arr1[i].id === arr2[i].id) {
                //比较参数
                if (JSON.stringify(arr1[i].data) !== JSON.stringify(arr2[i].data)) {
                    i++;
                    break;
                }
            }
            else {
                break;
            }
        }
        //旧路由改变数组
        if (arr1 !== null) {
            retArr1 = arr1.slice(i);
        }
        //新路由改变数组（相对于旧路由）
        if (arr2 !== null) {
            retArr2 = arr2.slice(i);
        }
        //上一级路由或参数不同的当前路由
        let p1 = null;
        //上二级路由或参数不同路由的上一级路由
        let p2 = null;
        if (arr2 && i > 0) {
            // 可能存在空路由，需要向前遍历
            for (let j = i - 1; j >= 0; j--) {
                if (!p1) {
                    if (arr2[j].module) {
                        p1 = arr2[j];
                        continue;
                    }
                }
                else if (!p2) {
                    if (arr2[j].module) {
                        p2 = arr2[j];
                        break;
                    }
                }
            }
        }
        return [p1, retArr1, retArr2, p2];
    }
    /**
     * 添加激活对象
     * @param moduleId -    模块id
     * @param dom -         可激活节点
     * @param field -       激活字段名
     */
    addActiveDom(module, dom) {
        if (!this.routerMap.has(module.id)) {
            this.routerMap.set(module.id, { activeDoms: [] });
        }
        const cfg = this.routerMap.get(module.id);
        if (!cfg.activeDoms) {
            cfg.activeDoms = [];
        }
        const index = cfg.activeDoms.findIndex(item => item.key === dom.key);
        if (index === -1) {
            cfg.activeDoms.push(dom);
        }
        else { //替换
            cfg.activeDoms.splice(index, 1, dom);
        }
    }
    /**
     * 设置路由元素激活属性
     * @param module -    模块
     * @param path -      路径
     * @returns
     */
    setActiveDom(module, path) {
        if (!this.routerMap.has(module.id)) {
            return;
        }
        const cfg = this.routerMap.get(module.id);
        if (!cfg.activeDoms) {
            return;
        }
        //与path一致的dom，设置其active=true，否则设置为false
        for (const dom of cfg.activeDoms) {
            if (dom.props['path'] === path) {
                dom.model[dom.props['active']] = true;
            }
            else {
                dom.model[dom.props['active']] = false;
            }
        }
    }
    /**
     * 路由模块相关处理
     * @param route -   路由
     * @param pm -      父模块
     */
    handleRouteModule(route, pm) {
        //设置参数
        const o = {
            path: route.path
        };
        if (!Util.isEmpty(route.data)) {
            o['data'] = route.data;
        }
        const module = route.module;
        module.model['$route'] = o;
        if (pm) {
            if (!this.routerMap.has(pm.id)) {
                this.routerMap.set(pm.id, {});
            }
            const cfg = this.routerMap.get(pm.id);
            //父模块router dom尚不存在，则添加到wait
            if (!cfg.dom) {
                cfg.wait = route;
            }
            else {
                this.setActiveDom(pm, route.fullPath);
            }
            this.prepModuleDom(pm, route);
        }
    }
    /**
     * 为route.module设置dom
     * @param module -  父模块
     * @param route -   路由
     */
    prepModuleDom(module, route) {
        var _a;
        if (!this.routerMap.has(module.id)) {
            return;
        }
        const cfg = this.routerMap.get(module.id);
        if (!cfg.dom) {
            return;
        }
        if (!cfg.dom.vdom.children) {
            cfg.dom.vdom.children = [];
        }
        const m = route.module;
        //dom key
        const key = m.id + '_r';
        const dom = (_a = cfg.dom.children) === null || _a === void 0 ? void 0 : _a.find(item => item.key === key);
        if (!dom) {
            const vdom = new VirtualDom('div', key, module);
            const d = new Directive('module');
            d.value = m.id;
            vdom.addDirective(d);
            cfg.dom.vdom.add(vdom);
            module.active();
        }
        else {
            m.srcDom = dom;
            m.active();
        }
    }
    /**
     * 获取路由数组
     * @param path - 	要解析的路径
     * @param clone - 是否clone，如果为false，则返回路由树的路由对象，否则返回克隆对象
     * @returns     路由对象数组
     */
    getRouteList(path, clone) {
        if (!this.root) {
            return [];
        }
        const pathArr = path.split('/');
        let node = this.root;
        let paramIndex = 0; //参数索引
        const retArr = [];
        let fullPath = ''; //完整路径
        let preNode = this.root; //前一个节点
        for (let i = 0; i < pathArr.length; i++) {
            const v = pathArr[i].trim();
            if (v === '') {
                continue;
            }
            let find = false;
            for (let j = 0; j < node.children.length; j++) {
                if (node.children[j].path === v) {
                    //设置完整路径
                    if (preNode !== this.root) {
                        preNode.fullPath = fullPath;
                        preNode.data = node.data;
                        retArr.push(preNode);
                    }
                    //设置新的查找节点
                    node = clone ? node.children[j].clone() : node.children[j];
                    //参数清空
                    node.data = {};
                    preNode = node;
                    find = true;
                    //参数索引置0
                    paramIndex = 0;
                    break;
                }
            }
            //路径叠加
            fullPath += '/' + v;
            //不是孩子节点,作为参数
            if (!find) {
                if (paramIndex < node.params.length) { //超出参数长度的废弃
                    node.data[node.params[paramIndex++]] = v;
                }
            }
        }
        //最后一个节点
        if (node !== this.root) {
            node.fullPath = fullPath;
            retArr.push(node);
        }
        return retArr;
    }
    /**
     * 注册路由容器
     * @param module -      router容器所属模块
     * @param dom -         路由模块 src dom
     */
    registRouter(module, dom) {
        if (!this.rootModule) {
            this.rootModule = module;
        }
        if (!this.routerMap.has(module.id)) {
            this.routerMap.set(module.id, { dom: dom });
        }
        const cfg = this.routerMap.get(module.id);
        cfg.dom = dom;
        //存在待处理路由
        if (cfg.wait) {
            this.prepModuleDom(module, cfg.wait);
            //执行后删除
            delete cfg.wait;
        }
    }
    /**
     * 尝试激活路径
     * @param path -  待激活的路径
     */
    activePath(path) {
        // 如果当前路径为空或待激活路径是当前路径的子路径
        if (!this.currentPath || path.startsWith(this.currentPath)) {
            this.go(path);
        }
    }
    /**
     * 获取根路由
     * @returns     根路由
     */
    getRoot() {
        return this.root;
    }
}

/**
 * module 元素
 * @remarks
 * module指令标签，用`<module name='class name' /> 代替 x-module='class name'`
 */
class MODULE extends DefineElement {
    constructor(node, module) {
        super(node, module);
        //类名
        const clazz = node.getProp('name');
        if (!clazz) {
            throw new NError('itemnotempty', NodomMessage.TipWords['element'], 'MODULE', 'className');
        }
        node.delProp('name');
        node.addDirective(new Directive('module', clazz));
    }
}
/**
 * for 元素
 * @remarks
 * repeat指令标签，用`<for cond={{your expression}} /> 代替 x-repeat={{your expression}}`
 */
class FOR extends DefineElement {
    constructor(node, module) {
        super(node, module);
        //条件
        const cond = node.getProp('cond');
        if (!cond) {
            throw new NError('itemnotempty', NodomMessage.TipWords['element'], 'FOR', 'cond');
        }
        node.delProp('cond');
        node.addDirective(new Directive('repeat', cond));
    }
}
/**
 * 递归元素
 * @remarks
 * recur指令标签，用`<recur cond='recur field' /> 代替 x-recur='recur field'`
 */
class RECUR extends DefineElement {
    constructor(node, module) {
        super(node, module);
        //条件
        const cond = node.getProp('cond');
        node.delProp('cond');
        node.addDirective(new Directive('recur', cond));
    }
}
/**
 * IF 元素
 * @remarks
 * if指令标签，用`<if cond={{your expression}} /> 代替 x-if={{your expression}}`
 */
class IF extends DefineElement {
    constructor(node, module) {
        super(node, module);
        //条件
        const cond = node.getProp('cond');
        if (!cond) {
            throw new NError('itemnotempty', NodomMessage.TipWords['element'], 'IF', 'cond');
        }
        node.delProp('cond');
        node.addDirective(new Directive('if', cond));
    }
}
/**
 * ELSE 元素
 * @remarks
 * else指令标签，用`<else/> 代替 x-else`
 */
class ELSE extends DefineElement {
    constructor(node, module) {
        super(node, module);
        node.addDirective(new Directive('else', null));
    }
}
/**
 * ELSEIF 元素
 * @remarks
 * elseif指令标签，用`<elseif cond={{your expression}} /> 代替 x-elseif={{your expression}}`
 */
class ELSEIF extends DefineElement {
    constructor(node, module) {
        super(node, module);
        //条件
        const cond = node.getProp('cond');
        if (!cond) {
            throw new NError('itemnotempty', NodomMessage.TipWords['element'], 'ELSEIF', 'cond');
        }
        node.delProp('cond');
        node.addDirective(new Directive('elseif', cond));
    }
}
/**
 * ENDIF 元素
 * @remarks
 * endif指令标签，用`<endif /> 代替 x-endif`
 */
class ENDIF extends DefineElement {
    constructor(node, module) {
        super(node, module);
        node.addDirective(new Directive('endif', null));
    }
}
/**
 * SHOW 元素
 * @remarks
 * show指令标签，用`<show cond={{your expression}} /> 代替 x-show={{your expression}}`
 */
class SHOW extends DefineElement {
    constructor(node, module) {
        super(node, module);
        //条件
        const cond = node.getProp('cond');
        if (!cond) {
            throw new NError('itemnotempty', NodomMessage.TipWords['element'], 'SHOW', 'cond');
        }
        node.delProp('cond');
        node.addDirective(new Directive('show', cond));
    }
}
/**
 * 插槽
 * @remarks
 * slot指令标签，用`<slot name='slotname' > 代替 x-slot='slotname'`
 */
class SLOT extends DefineElement {
    constructor(node, module) {
        super(node, module);
        //条件
        const cond = node.getProp('name') || 'default';
        node.delProp('name');
        node.addDirective(new Directive('slot', cond));
    }
}
/**
 * 路由
 * @remarks
 * route指令标签，用`<route path='routepath' > 代替 x-route='routepath'`
 */
class ROUTE extends DefineElement {
    constructor(node, module) {
        //默认标签为a
        if (!node.hasProp('tag')) {
            node.setProp('tag', 'a');
        }
        super(node, module);
        //条件
        const cond = node.getProp('path');
        if (!cond) {
            throw new NError('itemnotempty', NodomMessage.TipWords['element'], 'ROUTE', 'path');
        }
        node.addDirective(new Directive('route', cond));
    }
}
/**
 * 路由容器
 * @remarks
 * router指令标签，用`<router /> 代替 x-router`
 */
class ROUTER extends DefineElement {
    constructor(node, module) {
        super(node, module);
        node.addDirective(new Directive('router', null));
    }
}
//添加到自定义元素管理器
DefineElementManager.add([MODULE, FOR, RECUR, IF, ELSE, ELSEIF, ENDIF, SHOW, SLOT, ROUTE, ROUTER]);

/**
     * 指令类型初始化
     * @remarks
     * 每个指令类型都有一个名字、处理函数和优先级，处理函数`不能用箭头函数`
     * 处理函数在渲染时执行，包含两个参数 module(模块)、dom(目标虚拟dom)
     * 处理函数的this指向指令对象
     * 处理函数的返回值`true`表示继续，`false`表示后续指令不再执行，同时该节点不加入渲染树
     */
(function () {
    /**
     * module 指令
     * 用于指定该元素为模块容器，表示子模块
     * 用法 x-module='模块类名'
     */
    Nodom.createDirective('module', function (module, dom) {
        if (!this.value) {
            return false;
        }
        let m = module.objectManager.getDomParam(dom.key, '$savedModule');
        if (!m) {
            m = ModuleFactory.get(this.value);
            if (!m) {
                return false;
            }
            module.objectManager.setDomParam(dom.key, '$savedModule', m);
        }
        ModuleFactory.get(dom.moduleId).addChild(m);
        //保存到dom上，提升渲染性能
        dom.childModuleId = m.id;
        const role = m.constructor.name + ':' + m.id;
        if (!dom.props) {
            dom.props = { role: role };
        }
        else {
            dom.props['role'] = role;
        }
        //设置props
        const o = {};
        for (const p of Object.keys(dom.props)) {
            const v = dom.props[p];
            if (p[0] === '$') { //数据
                if (!o['$data']) {
                    o['$data'] = {};
                }
                o['$data'][p.substring(1)] = v;
                //删除属性
                delete dom.props[p];
            }
            else {
                o[p] = v;
            }
        }
        //传递给模块
        m.setProps(o, dom);
        return true;
    }, 8);
    /**
     *  model指令
     */
    Nodom.createDirective('model', function (module, dom) {
        const model = module.get(dom.model, this.value);
        if (model) {
            dom.model = model;
        }
        return true;
    }, 1);
    /**
     * 指令名 repeat
     * 描述：重复指令
     */
    Nodom.createDirective('repeat', function (module, dom) {
        const rows = this.value;
        // 无数据不渲染
        if (!Util.isArray(rows) || rows.length === 0) {
            return false;
        }
        const src = dom.vdom;
        //索引名
        const idxName = src.getProp('index');
        const parent = dom.parent;
        //禁用该指令
        this.disabled = true;
        //避免在渲染时对src设置了model，此处需要删除
        for (let i = 0; i < rows.length; i++) {
            if (!rows[i]) {
                continue;
            }
            if (idxName && typeof rows[i] === 'object') {
                rows[i][idxName] = i;
            }
            const d = Renderer.renderDom(module, src, rows[i], parent, rows[i].__key);
            //删除index属性
            if (idxName) {
                delete d.props['index'];
            }
        }
        //启用该指令
        this.disabled = false;
        return false;
    }, 2);
    /**
     * 递归指令
     * 作用：在dom内部递归，用于具有相同数据结构的节点递归生成
     * 递归指令不允许嵌套
     * name表示递归名字，必须与内部的recur标签的ref保持一致，名字默认为default
     * 典型模版
     * ```
     * <recur name='r1'>
     *      <element1>...</element1>
     *      <element2>...</element2>
     *      <recur ref='r1' />
     * </recur>
     * ```
     */
    Nodom.createDirective('recur', function (module, dom) {
        const src = dom.vdom;
        //当前节点是递归节点存放容器
        if (dom.props.hasOwnProperty('ref')) {
            //如果出现在repeat中，src为单例，需要在使用前清空子节点，避免沿用上次的子节点
            src.children = [];
            //递归存储名
            const name = '$recurs.' + (dom.props['ref'] || 'default');
            const node = module.objectManager.get(name);
            if (!node) {
                return true;
            }
            const model = dom.model;
            const cond = node.getDirective('recur');
            const m = model[cond.value];
            //不存在子层数组，不再递归
            if (!m) {
                return true;
            }
            //克隆，后续可以继续用
            const node1 = node.clone();
            node1.removeDirective('recur');
            dom.children || (dom.children = []);
            if (!Array.isArray(m)) { //非数组recur
                Renderer.renderDom(module, node1, m, dom, m.__key);
            }
            else { //数组内recur，依赖repeat得到model，repeat会取一次数组元素，所以需要dom model
                Renderer.renderDom(module, node1, model, dom, m['__key']);
            }
            //删除ref属性
            delete dom.props['ref'];
        }
        else { //递归节点
            const data = dom.model[this.value];
            if (!data) {
                return true;
            }
            //递归名，默认default
            const name = '$recurs.' + (dom.props['name'] || 'default');
            //删除name属性
            delete dom.props['name'];
            //保存递归定义的节点
            if (!module.objectManager.get(name)) {
                module.objectManager.set(name, src);
            }
        }
        return true;
    }, 2);
    /**
     * 指令名 if
     * 描述：条件指令
     */
    Nodom.createDirective('if', function (module, dom) {
        if (!dom.parent) {
            return;
        }
        module.objectManager.setDomParam(dom.parent.key, '$if', this.value);
        return this.value;
    }, 5);
    /**
     * 指令名 else
     * 描述：else指令
     */
    Nodom.createDirective('else', function (module, dom) {
        if (!dom.parent) {
            return;
        }
        return !module.objectManager.getDomParam(dom.parent.key, '$if');
    }, 5);
    /**
     * elseif 指令
     */
    Nodom.createDirective('elseif', function (module, dom) {
        if (!dom.parent) {
            return;
        }
        const v = module.objectManager.getDomParam(dom.parent.key, '$if');
        if (v === true) {
            return false;
        }
        else {
            if (!this.value) {
                return false;
            }
            else {
                module.objectManager.setDomParam(dom.parent.key, '$if', true);
            }
        }
        return true;
    }, 5);
    /**
     * elseif 指令
     */
    Nodom.createDirective('endif', function (module, dom) {
        if (!dom.parent) {
            return;
        }
        module.objectManager.removeDomParam(dom.parent.key, '$if');
        //endif 不显示
        return false;
    }, 5);
    /**
     * 指令名 show
     * 描述：显示指令
     */
    Nodom.createDirective('show', function (module, dom) {
        //show指令参数 {origin:通过style设置的初始display属性,rendered:是否渲染过}
        let showParam = module.objectManager.getDomParam(dom.key, '$show');
        //为false且未渲染过，则不渲染
        if (!this.value && (!showParam || !showParam['rendered'])) {
            return false;
        }
        if (!showParam) {
            showParam = {};
            module.objectManager.setDomParam(dom.key, '$show', showParam);
        }
        let style = dom.props['style'];
        const reg = /display\s*\:[\w\-]+/;
        let regResult;
        let display;
        if (style) {
            regResult = reg.exec(style);
            //保存第一个style display属性
            if (regResult !== null) {
                const ra = regResult[0].split(':');
                display = ra[1].trim();
                //保存第一个display属性
                if (!showParam['origin'] && display !== 'none') {
                    showParam['origin'] = display;
                }
            }
        }
        // 渲染标识，value为false且尚未进行渲染，则不渲染
        if (!this.value) {
            if (style) {
                if (display) {
                    //把之前的display替换为none
                    if (display !== 'none') {
                        style = style.substring(0, regResult.index) + 'display:none' + style.substring(regResult.index + regResult[0].length);
                    }
                }
                else {
                    style += ';display:none';
                }
            }
            else {
                style = 'display:none';
            }
        }
        else {
            //设置渲染标志
            showParam['rendered'] = true;
            if (display === 'none') {
                if (style) {
                    if (showParam['origin']) {
                        style = style.substring(0, regResult.index) + 'display:' + showParam['origin'] + style.substring(regResult.index + regResult[0].length);
                    }
                    else {
                        style = style.substring(0, regResult.index) + style.substring(regResult.index + regResult[0].length);
                    }
                }
            }
        }
        if (style) {
            dom.props['style'] = style;
        }
        return true;
    }, 5);
    /**
     * 指令名 field
     * 描述：字段指令
     */
    Nodom.createDirective('field', function (module, dom) {
        dom.assets || (dom.assets = {});
        //修正staticnum
        if (dom.staticNum === 0) {
            dom.staticNum = 1;
        }
        const dataValue = module.get(dom.model, this.value);
        if (dom.tagName === 'select') {
            dom.props['value'] = dataValue;
            //延迟设置value，避免option尚未渲染
            setTimeout(() => {
                const el = dom.node;
                if (el) {
                    el.value = dataValue;
                }
            }, 0);
        }
        else if (dom.tagName === 'input') {
            switch (dom.props['type']) {
                case 'radio':
                    const value = dom.props['value'];
                    dom.props['name'] = this.value;
                    if (dataValue == value) {
                        dom.props['checked'] = 'checked';
                        dom.assets['checked'] = true;
                    }
                    else {
                        delete dom.props['checked'];
                        dom.assets['checked'] = false;
                    }
                    break;
                case 'checkbox':
                    //设置状态和value
                    const yv = dom.props['yes-value'];
                    //当前值为yes-value
                    if (dataValue == yv) {
                        dom.props['value'] = yv;
                        dom.assets['checked'] = true;
                    }
                    else { //当前值为no-value
                        dom.props['value'] = dom.props['no-value'];
                        dom.assets['checked'] = false;
                    }
                    break;
                default:
                    const v = (dataValue !== undefined && dataValue !== null) ? dataValue : '';
                    dom.props['value'] = v;
                    dom.assets['value'] = v;
            }
        }
        else {
            const v = (dataValue !== undefined && dataValue !== null) ? dataValue : '';
            dom.props['value'] = v;
            dom.assets['value'] = v;
        }
        //设置dom参数，避免二次添加事件
        if (!module.objectManager.getDomParam(dom.vdom.key, '$addedFieldEvent')) {
            module.objectManager.setDomParam(dom.vdom.key, '$addedFieldEvent', true);
            const event = new NEvent(module, 'change', (model, dom) => {
                const el = dom.node;
                if (!el) {
                    return;
                }
                const type = dom.props['type'];
                const field = this.value;
                let v = el.value;
                //根据选中状态设置checkbox的value
                if (type === 'checkbox') {
                    if (dom.props['yes-value'] == v) {
                        v = dom.props['no-value'];
                    }
                    else {
                        v = dom.props['yes-value'];
                    }
                }
                else if (type === 'radio') {
                    if (!el.checked) {
                        v = undefined;
                    }
                }
                //修改字段值,需要处理.运算符
                module.set(model, field, v);
            });
            dom.vdom.addEvent(event, 0);
        }
        return true;
    }, 10);
    /**
     * route指令
     */
    Nodom.createDirective('route', function (module, dom) {
        if (!Nodom['$Router']) {
            throw new NError('uninit', NodomMessage.TipWords.route);
        }
        //a标签需要设置href
        if (dom.tagName === 'a') {
            dom.props['href'] = 'javascript:void(0)';
        }
        const v = this.value;
        dom.props['path'] = (v === undefined || v === null || v === '' || typeof v === 'string' && v.trim() === '') ? '' : v;
        //有激活属性
        const acName = dom.props['active'];
        //添加激活model
        if (acName) {
            const router = Nodom['$Router'];
            router.addActiveDom(module, dom);
            //如果有active属性，尝试激活路径
            if (dom.model[acName]) {
                router.activePath(this.value);
            }
        }
        //添加click事件,避免重复创建事件对象，创建后缓存
        if (!module.objectManager.getDomParam(dom.vdom.key, '$addedRouteEvent')) {
            module.objectManager.setDomParam(dom.vdom.key, '$addedRouteEvent', true);
            const event = new NEvent(module, 'click', null, function (model, d) {
                const path = d.props['path'];
                if (Util.isEmpty(path)) {
                    return;
                }
                Nodom['$Router'].go(path);
            });
            dom.vdom.addEvent(event);
        }
        return true;
    }, 10);
    /**
     * 增加router指令
     */
    Nodom.createDirective('router', function (module, dom) {
        const router = Nodom['$Router'];
        if (!router) {
            throw new NError('uninit', NodomMessage.TipWords.route);
        }
        router.registRouter(module, dom);
        return true;
    }, 10);
    /**
     * 插头指令
     * 用于模块中，可实现同名替换
     */
    Nodom.createDirective('slot', function (module, dom) {
        var _a;
        this.value || (this.value = 'default');
        const mid = dom.parent.childModuleId;
        //父dom有module指令，表示为替代节点，替换子模块中的对应的slot节点；否则为子模块定义slot节点
        if (mid) {
            const m = ModuleFactory.get(mid);
            //子模块不存在则不处理
            if (!m) {
                return false;
            }
            m.slots.set(this.value, dom);
            dom.slotModuleId = mid;
            //保持key带slot标识
            if (!dom.vdom.slotModuleId) {
                dom.key += 's';
                updateKey(dom.vdom, 's');
            }
            //innerrender，此次不渲染
            if ((_a = dom.vdom.props) === null || _a === void 0 ? void 0 : _a.has('innerrender')) {
                return false;
            }
            return true;
            /**
             * 更新虚拟dom key，避免在新模块中重复
             * @param vdom -    虚拟dom
             * @param key -     附加key
             */
            function updateKey(vdom, key) {
                vdom.key += key;
                vdom.slotModuleId = mid;
                //模块不处理子节点，子节点将以该模块id作为slot module id
                if (vdom.hasDirective('module')) {
                    return;
                }
                if (vdom.children) {
                    for (const c of vdom.children) {
                        updateKey(c, key);
                    }
                }
            }
        }
        else { //源slot节点
            const sdom = module.slots.get(this.value);
            if (sdom) {
                if (dom.vdom.hasProp('innerrender')) { //内部数据渲染
                    if (sdom.vdom.children && dom.parent) {
                        for (const c of sdom.vdom.children) {
                            Renderer.renderDom(module, c, dom.model, dom.parent, dom.key);
                        }
                    }
                }
                else { //替换为存储的已渲染节点
                    if ((sdom === null || sdom === void 0 ? void 0 : sdom.children) && dom.parent) {
                        for (const c of sdom.children) {
                            dom.parent.children.push(c);
                            c.parent = dom.parent;
                        }
                    }
                }
            }
            return false;
        }
    }, 5);
}());

/*
 * 消息js文件 中文文件
 */
const NodomUIMessage_zh = {
    uploadingText: '上传中...',
    pleaseSelect: '请选择...',
    ok: '确定',
    cancel: '取消',
    //date
    sunday: '日',
    monday: '一',
    tuesday: '二',
    wednesday: '三',
    thursday: '四',
    friday: '五',
    saturday: '六',
    today: '今天',
    //pagination
    total: '共',
    totalUnit: '条',
    pagePre: '第',
    page: '页',
    pageSize: '条/页',
    noData: '无数据',
    //表单验证
    form: {
        type: "请输入有效的{0}",
        unknown: "输入错误",
        require: "不能为空",
        number: "请输入数字",
        min: "值最小为{0}",
        max: "值最大为{0}",
        between: "值必须在{0}-{1}之间",
        maxLength: "长度不能大于{0}",
        minLength: "长度不能小于{0}",
        betweenLength: '长度必须在{0}-{1}之间',
        email: '请输入有效的email',
        url: '请输入有效的url',
        mobile: '请输入有效的手机号',
        date: "请输入有效日期格式，如：2022-01-01",
        time: "请输入有效时间格式，如：12:30:30",
        datetime: "请输入有效日期时间格式，如：2022-01-01",
        idno: '请输入18位有效身份证号',
        range: '第二个值不允许小于第一个值'
    }
};

/*
 * 消息js文件 英文文件
 */
const NodomUIMessage_en = {
    uploadingText: 'uploading...',
    pleaseSelect: 'please select...',
    ok: 'ok',
    cancel: 'cancel',
    //date
    sunday: 'sun',
    monday: 'mon',
    tuesday: 'tue',
    wednesday: 'wed',
    thursday: 'thu',
    friday: 'fri',
    saturday: 'sat',
    today: 'today',
    //pagination
    total: 'total',
    totalUnit: '',
    pagePre: 'page',
    page: '',
    pageSize: '/page',
    noData: 'no data',
    //表单验证
    form: {
        type: "{0} is invalid",
        unknown: "input error",
        require: "not allow empty",
        number: "is not a number",
        min: "min value is {0}",
        max: "max value is {0}",
        between: "value must between {0}-{1}",
        maxLength: "max length is {0}",
        minLength: "min length is {0}",
        betweenLength: 'input length must between {0}-{1}',
        email: 'invalid email',
        url: 'invalid url',
        mobile: 'invalid phone no',
        date: "invalid date,ex: 2022-01-01",
        time: "invalid time,ex: 12:30:30",
        datetime: "invalid date time,ex:2022-01-01",
        idno: 'input 18 bit IDNo',
        range: 'second value must be equal or bigger than first'
    }
};

/**
 * nodom组件库相关设置
 * @public
 */
class NodomUI {
    /**
     * 设置语言
     * @param lang -  语言
     */
    static setLang(lang) {
        switch (lang) {
            case 'en':
                this.messageCfg = NodomUIMessage_en;
                break;
            default:
                this.messageCfg = NodomUIMessage_zh;
        }
    }
    /**
     * 获取提示文字信息
     * @param name -  文字对应名称
     * @param args -  参数
     * @returns     文字内容
     */
    static getText(name, ...args) {
        const ta = name.includes('.') ? name.split('.') : [name];
        let o = this.messageCfg || NodomUIMessage_zh;
        let v;
        for (const p of ta) {
            v = o[p];
            if (v) {
                o = o[p];
            }
        }
        if (v && args.length > 0) {
            v = Util.compileStr(v, ...args);
        }
        return v;
    }
}

//关闭右键菜单
document.oncontextmenu = function (e) {
    e.preventDefault();
};
/**
 * 工具类
 * @public
 */
class UITool {
    /**
     * 去掉字符串的空格
     * @param src - 需处理的字符串
     */
    static clearSpace(src) {
        if (src && typeof src === 'string') {
            return src.replace(/\s+/g, '');
        }
        return src;
    }
    /**
     * 计算实际位置
     * @param el - 待计算的element
     * @returns  [x坐标,y坐标]
     */
    static getRelPos(el) {
        let x = el.offsetLeft - el.scrollLeft;
        let y = el.offsetTop - el.scrollTop;
        const offsetEl = el.offsetParent;
        while (el) {
            el = el.parentElement;
            x -= el.scrollLeft;
            y -= el.scrollTop;
            if (offsetEl === el || el === document.body) {
                break;
            }
        }
        return [x, y];
    }
    /**
     * 获取实际位置
     * @param el - 待计算的element
     * @returns  [x坐标,y坐标]
     */
    static getRealPos(el) {
        let x = el.offsetLeft - el.scrollLeft;
        let y = el.offsetTop - el.scrollTop;
        let offsetEl = el.offsetParent;
        while (el) {
            el = el.parentElement;
            x -= el.scrollLeft;
            y -= el.scrollTop;
            if (offsetEl === el) {
                x += el.offsetLeft;
                y += el.offsetTop;
                offsetEl = offsetEl.offsetParent;
            }
            if (el === document.body) {
                break;
            }
        }
        return [x, y];
    }
    /**
     * 计算位置
     * @param event - 鼠标事件
     * @param relPos - 相对位置：1下 2右
     * @param width - dom宽
     * @param height - dom高
     * @returns  [x,y,width,height]
     */
    static cacPosition(event, relPos, width, height) {
        const relEl = event.currentTarget;
        const rect = relEl.getBoundingClientRect();
        //box 阴影面padding为5
        const padding = 5;
        //页面高度和宽度
        const gwidth = window.innerWidth;
        const gheight = window.innerHeight;
        //相对位置
        let x;
        let y;
        if (relPos === 1) { //纵向
            x = rect.x - padding;
            y = rect.y + rect.height;
            if (y + height > gheight + padding) { //下方不够放
                if (y > gheight / 2 - padding) { //y过半，移到上方
                    y = rect.y - height;
                    if (y < 0) {
                        height += y - padding;
                        y = padding;
                    }
                }
                else { //在下边
                    if (y + height > gheight - padding) { //高度超出边界
                        height = gheight - y - padding * 2;
                    }
                }
            }
        }
        else { //横向
            x = rect.x + rect.width + padding;
            y = rect.y;
            if (x + width > gwidth - padding) { //右方不够放，放左放
                x = rect.x - width - padding;
            }
        }
        return [x | 0, y | 0, width | 0, height | 0];
    }
    /**
     * 将对象转换为属性字符串
     * @param obj - 要转换的 object
     * @returns     转换完成的 string
     */
    static toAttrString(obj) {
        let res = '';
        for (const [key, val] of Object.entries(obj)) {
            res += `${key}='${val}' `;
        }
        return res.trim();
    }
    /**
     * 鼠标拖动事件，获取拖动后位置（相对父元素）
     * @param event - 鼠标事件
     * @param el - 拖动事件的父Element
     * @param vertical - 是否竖直
     * @returns 拖动后位置
     * @beta
     */
    static getDisplacement(event, el, vertical) {
        const rect = el.getBoundingClientRect();
        let newL;
        if (vertical) {
            newL = rect.bottom - event.clientY;
        }
        else {
            newL = event.clientX - rect.left;
        }
        return newL;
    }
    /**
 * 鼠标拖动事件，获取拖动后位置（相对父元素）
 * @param event - 鼠标事件
 * @returns x,y - 鼠标指针相对于浏览器窗口左上角的水平距离和垂直距
 * @beta
 */
    static returnClientXY(event) {
        const x = event.clientX;
        const y = event.clientY;
        return { x, y };
    }
    /**
     * 调用通过props传递的方法
     * @param module -      当前模块
     * @param methodName -  方法名
     * @param params -      参数数组
     * @returns             方法实际返回值
     */
    static invokePropMethod(module, methodName, ...params) {
        const pm = ModuleFactory.get(module.srcDom.moduleId);
        if (!pm || !pm[methodName]) {
            return;
        }
        return pm[methodName](...params);
    }
}

/**
 * animation box 指令
 * value: 起始位置
 * top-left:左上
 * top-center: 中上
 * top-right:右上
 * center:中心
 * bottom-left:左下
 * bottom-center:中下
 * bottom-right:右下
 */
/**
 * animation box 指令
 * @public
 */
Nodom.createDirective('animationbox', function (module, dom) {
    const cls = this.value === 'horizontal' ? 'ui-expand-horizontal' : 'ui-expand-vertical';
    if (dom.props['class']) {
        dom.props['class'] += ' ' + cls;
    }
    else {
        dom.props['class'] = cls;
    }
    //默认控制数据项为__open
    const field = dom.props['field'] || '__open';
    //延迟设置高度
    setTimeout(() => {
        const el = dom.node;
        if (!el) {
            return;
        }
        const chd = el.children[0];
        if (el) {
            if (this.value === 'horizontal') {
                if (dom.model[field]) {
                    el.style.width = chd.offsetWidth + 'px';
                    el.style.height = chd.offsetHeight + 'px';
                }
                else {
                    el.style.width = '0px';
                }
            }
            else {
                if (dom.model[field]) {
                    el.style.height = chd.offsetHeight + 'px';
                }
                else {
                    el.style.height = '0px';
                }
            }
        }
    }, 0);
    return true;
}, 10);

/**
 * 属性配置
 * single: true/false 是否同时只展开一个
 */
/**
 * UIAccordion 折叠面板
 * @public
 */
class UIAccordion extends Module {
    constructor() {
        super(...arguments);
        /**
         * 子item数组
         */
        this.__items = [];
    }
    /**
     * 模板函数html
     * @privateRemarks
     */
    template(props) {
        this.__single = props.hasOwnProperty('single');
        if (this.__single) {
            this.setExcludeProps(['single']);
        }
        /**
         * 子菜单 type=0表示在当前菜单项下侧，否则表示右侧
         */
        return `
            <div class='ui-accordion'>
                <slot />
            </div>
        `;
    }
    /**
     * 添加item
     * @param item	待加入的accordion item
     */
    __addItem(item) {
        if (!this.__items.includes(item)) {
            this.__items.push(item);
        }
    }
}
/**
 * accordion item
 * 属性配置
 * title:	标题栏内容
 * opened: 	是否展开
 */
/**
 * UIAccordionItem 折叠子组件
 * @public
 */
class UIAccordionItem extends Module {
    /**
     * 模板函数
     * @privateRemarks
     */
    template(props) {
        if (props.hasOwnProperty('opened')) {
            this.model['__open'] = true;
        }
        //设置父accordion
        this.__accordion = ModuleFactory.get(this.srcDom.slotModuleId);
        //添加到父item集合
        this.__accordion.__addItem(this);
        delete props['opened'];
        this.setExcludeProps(['title', 'opened']);
        return `
            <div class='ui-accordion-item'>
                <div class='ui-accordion-title' e-click='__clickItem'>
                    ${props['title']}
                    <span class={{__open?'ui-expand-icon ui-expand-icon-open':'ui-expand-icon'}}></span>
                </div>
				<div x-animationbox>
					<div class='ui-accordion-content'>
						<slot />
					</div>
				</div>
            </div>
        `;
    }
    /**
     * 点击事件	展开/折叠
     * @param model - 对应model
     */
    __clickItem(model) {
        if (this.__accordion.__single) {
            for (const m of this.__accordion.__items) {
                if (m !== this) {
                    if (m.model['__open']) {
                        m.model['__open'] = false;
                    }
                }
                else {
                    m.model['__open'] = true;
                }
            }
        }
        else {
            model['__open'] = !model['__open'];
        }
    }
}
Nodom.registModule(UIAccordion, 'ui-accordion');
Nodom.registModule(UIAccordionItem, 'ui-accordion-item');

/**
 * 参数说明
 * title:           标题
 * icon:            图标
 * vertical-icon    图标位置 left top right bottom,默认left
 * theme:           主题 default active error success warn，默认default
 * size:            按钮size tiny normal large，默认normal
 * nobg:            不需要背景 true/false，默认false
 * circle:          圆形
 * disable:         禁用，true/false,默认false
 */
/**
 * UIButton 按钮
 * @public
 */
class UIButton extends Module {
    /**
     *模板函数html
     * @privateRemarks
     */
    template(props) {
        const arr = ['ui-btn'];
        if (props.hasOwnProperty('vertical-icon')) {
            arr.push('ui-btn-vert');
        }
        if (props.hasOwnProperty('disabled')) {
            arr.push('ui-btn-disable');
        }
        //字体
        arr.push('ui-btn-' + (props['size'] || 'normal'));
        let btnStr = '';
        //图标
        if (props['icon']) {
            btnStr = `<b class='ui-btn-icon ui-icon-${props['icon']}'/>`;
        }
        //无背景
        if (props.hasOwnProperty('nobg')) {
            arr.push('ui-btn-nobg');
        }
        else if (!props.hasOwnProperty('disabled')) {
            arr.push('ui-btn-' + (props['theme'] || 'default'));
        }
        //圆形
        if (props.hasOwnProperty('circle')) {
            arr.push('ui-btn-circle');
        }
        if (!props['title']) {
            arr.push('ui-btn-notext');
        }
        const disable = props.hasOwnProperty('disabled') ? 'disabled' : '';
        this.setExcludeProps(['theme', 'size', 'nobg', 'circle', 'icon', 'verticle-icon', 'title', 'disabled']);
        return `
            <button class='${arr.join(" ")}' ${disable}>
                ${btnStr} ${props['title'] || ''}
            </button>
        `;
    }
}
Nodom.registModule(UIButton, 'ui-button');

/**
 * BaseInput 绑定数据型父类组件
 * @public
 */
class BaseInput extends Module {
    /**
     * 重构
     */
    constructor() {
        super();
    }
    /**
     * 模板函数
     * @privateRemarks
     */
    template(props) {
        if (props['field']) {
            this.__field = props['field'];
            this.__onChange = props['onchange'];
            if (!this.__watched) {
                this.__watched = true;
                // 在父dom添加__value属性，父修改时，可驱动子模块渲染
                this.srcDom.vdom.setProp('__value', new Expression(this.__field));
                //添加监听
                this.watch('__value', (m, key, ov, nv) => {
                    this.__change(ov, nv);
                });
            }
            else { //第二次则直接赋值
                this.model['__value'] = this.props['__value'];
            }
        }
        this.setExcludeProps(['__value']);
        return null;
    }
    onBeforeFirstRender() {
        //第一渲染前获取srcDom的属性值，后续则直接从“__value”获取
        if (this.__field) {
            this.model['__value'] = this.get(this.srcDom.model, this.__field);
        }
    }
    /**
     * 更改值
     * 需要对父模块对应数据项进行更改
     * @param oldValue - 旧值
     * @param newValue - 新值
     */
    __change(oldValue, newValue) {
        if (!this.__field) {
            return;
        }
        //修改props值，避免二次渲染
        this.props['__value'] = newValue;
        //调用change事件
        if (this.__onChange) {
            UITool.invokePropMethod(this, this.__onChange, oldValue, newValue);
        }
        //更改父模块对应数据项
        this.set(this.srcDom.model, this.__field, newValue);
        //初始化显示
        this.__initValue();
    }
    /**
     * 初始化value，当模块依赖值进行初始化时有用
     */
    __initValue() { }
    /**
     * 获取值
     * @returns - 模块值
     */
    __getValue() {
        return this.model['__value'];
    }
}

/**
 * checkbox插件
 * 配置说明
 * field:       绑定父模块的字段
 * yes-value:   选中的值（可选），默认true
 * no-value:    不选中的值（可选），默认false
 * onChanged:    change事件方法
 */
/**
 * UICheckbox 复选框
 * @public
 */
class UICheckbox extends BaseInput {
    /**
     * 模板函数
     * @privateRemarks
     */
    template(props) {
        super.template(props);
        this.__yesValue = props['yes-value'] || true;
        this.__noValue = props['no-value'] || false;
        this.setExcludeProps(['field', 'yes-value', 'no-value']);
        this.__onChanged = props['onchanged'];
        this.setExcludeProps(['onchanged']);
        return `
            <span class={{'ui-checkbox' + (__value==this.__yesValue?' ui-checkbox-checked':'')}}>
                <span class='ui-checkbox-box' e-click='__clickCheck'>
                    <span class='ui-checkbox-inner' />
                </span>
                <span class={{'ui-checkbox-title'}}>
                    <slot/>
                </span>
            </span>
        `;
    }
    /**
     * 点击事件
     * @param model - 当前节点对应model
     * @param dom - virtual dom节点
     * @param evObj - NEvent对象
     * @param e - event对象
     */
    __clickCheck(model, dom, evObj, e) {
        model['__value'] = model['__value'] == this.__yesValue ? this.__noValue : this.__yesValue;
        if (this.__onChanged) {
            UITool.invokePropMethod(this, this.__onChanged, model, dom, evObj, e);
        }
    }
}
Nodom.registModule(UICheckbox, 'ui-checkbox');

/**
 * 配置项
 * field        绑定字段名
 * allow-clear  是否允许清空，无值属性
 * show-pwd     是否允许密码显示和隐藏方式切换，无值属性，type为password时有效
 * name         原生属性，参考input标签
 * type         原生属性，参考input标签，可设置为textarea
 * placeholder  原生属性，参考input标签
 * min          原生属性，参考input标签
 * max          原生属性，参考input标签
 * minlength    原生属性，参考input标签
 * maxlength    原生属性，参考input标签
 * disabled     原生属性，参考input标签
 * readonly     原生属性，参考input标签
 * rows         原生属性，参考textarea标签
 * cols         原生属性，参考textarea标签
 * resize       原生属性，参考textarea标签
 */
/**
 * UIInput 输入框
 * @public
 */
class UIInput extends BaseInput {
    /**
     * 模板函数
     */
    template(props) {
        super.template(props);
        // 属性集合
        const valueArr = [];
        //带值属性
        for (const f of ['name', 'placeholder', 'min', 'max', 'minlength', 'maxlength', 'size', 'rows', 'cols']) {
            if (props[f]) {
                valueArr.push(`${f}='${props[f]}'`);
            }
        }
        //不带值属性
        for (const f of ['disabled', 'readonly', 'autosize', 'autocomplete']) {
            if (props.hasOwnProperty(f)) {
                valueArr.push(`${f}='true'`);
            }
        }
        //设置类型
        if (!this.model['type']) {
            this.model['type'] = props.type || 'text';
        }
        //输入串
        let inputStr;
        if (props.resize || props.type === 'textarea') {
            inputStr = `<textarea x-field='__value' ${valueArr.join(' ')}/>`;
        }
        else {
            inputStr = `<input type={{type}} x-field='__value' ${valueArr.join(' ')}/>`;
        }
        //后置按钮串
        let btnStr = '';
        if (props.type === 'password' && props.hasOwnProperty('show-pwd')) {
            btnStr += "<b class={{type==='text'?'ui-input-hidepwd':'ui-input-seepwd'}} e-click='__seePwd'/>";
        }
        else if (props.hasOwnProperty('allow-clear')) {
            btnStr += "<b class='ui-input-clear' e-click='__clear'/>";
        }
        //给btnstr添加外部内容
        if (btnStr) {
            btnStr = `<div class='ui-input-buttons'>
                ${btnStr}
                </div>
            `;
        }
        this.setExcludeProps(['type', 'resize', 'field', 'placeholder', 'min', 'max', 'minlength', 'maxlength', 'size', 'rows', 'autosize', 'autocomplete', 'disabled', 'readonly', 'autosize', 'autocomplete']);
        //返回
        return `
            <div class='ui-input ${props.hasOwnProperty('disabled') ? 'ui-input-disabled' : ''}'>
                <div class='ui-input-before'>
                    <slot name='before'/>
                </div>
                ${inputStr}
                ${btnStr}
                <div class='ui-input-after'>
                    <slot name='after'/>
                </div>
            </div>
        `;
    }
    /**
     * 清除内容
     */
    __clear() {
        this.model['__value'] = '';
    }
    /**
     * 显示和隐藏password
     */
    __seePwd() {
        this.model['type'] = this.model['type'] === 'password' ? 'text' : 'password';
    }
}
//注册别名    
Nodom.registModule(UIInput, 'ui-input');

/**
 * 数据项
 * $data            树结构数据
 * 参数说明
 * field            如果树作为输入模式，需要设置对应字段，同时会显示复选框
 * value-field      数据项中用于取值的属性名，field存在时，不为空
 * icons            树节点图标，依次为为非叶子节点关闭状态，打开状态，叶子节点，如果只有两个，则表示非叶子节点和叶子节点，如果1个，则表示非叶子节点
 * onItemClick      节点点击事件
 */
/**
 * 树形控件
 * @public
 */
class UITree extends BaseInput {
    /**
     * 模版函数
     * @privateRemarks
     */
    template(props) {
        super.template(props);
        this.__valueField = props['value-field'];
        this.__onItemClick = props['onitemclick'];
        this.__onExpand = props['onexpand'];
        this.__onCollapse = props['oncollapse'];
        this.__icons = props['icons'] ? props['icons'].split(',').map(item => item.trim()) : undefined;
        const checkStr = props['field'] ? `
            <span class={{__genCheckCls(__state)}}  e-click='__checkItem'>
                <span class='ui-checkbox-box' >
                    <span class='ui-checkbox-inner' />
                </span>
            </span>
        ` : '';
        //设置不渲染到attribute的属性
        this.setExcludeProps(['icons', 'onitemclick', 'value-field', 'display-field', 'onexpand', 'oncollapse']);
        return `
            <div class='ui-tree' x-model='data'>
                <for cond={{children}} class='ui-tree-node-wrap'>
				    <div class='ui-tree-node'>
                        <b class={{__genArrowCls($model)}} e-click='__expandClose'></b>
                        ${props['icons'] ? "<b class={{__genFolderCls($model)}}></b>" : ""}
                        ${checkStr}
                        <span e-click='__clickItem'>
                            <slot innerRender/>
                        </span>
                    </div>
                    <recur cond='children' class='ui-expand-vertical' style={{'height:' + __cacHeight($model) + 'px'}}>
                        <div>
                            <div class='ui-tree-sub-wrap'>
                                <for cond={{children}} class='ui-tree-node-wrap'>
                                    <div class='ui-tree-node'>
                                        <b class={{__genArrowCls($model)}} e-click='__expandClose'></b>
                                        ${props['icons'] ? "<b class={{__genFolderCls($model)}}></b>" : ""}
                                        ${checkStr}
                                        <span e-click='__clickItem'>
                                            <slot innerRender/>
                                        </span>
                                    </div>
                                    <recur ref />
                                </for>
                            </div>
                        </div>
                    </recur>
                </for>
            </div>
        `;
    }
    /**
     * 创建选择框class
     * @param checked - 选中标识 true:选中  false:未选中
     * @returns         选择框class
     */
    __genCheckCls(checked) {
        const arr = ['ui-checkbox'];
        if (!checked) {
            arr.push('ui-checkbox-uncheck');
        }
        else if (checked === 1) {
            arr.push('ui-checkbox-checked');
        }
        else {
            arr.push('ui-checkbox-partchecked');
        }
        return arr.join(' ');
    }
    /**
     * 创建树左侧箭头class
     * @param model - 节点对应model
     * @returns         箭头(展开收拢)图标class
     */
    __genArrowCls(model) {
        const arr = [];
        if (model.children && model.children.length > 0) {
            arr.push('ui-expand-icon');
        }
        if (model.__open) {
            arr.push('ui-expand-icon-open');
        }
        return arr.join(' ');
    }
    /**
     * 显示文件夹图标
     * @param model - 节点对应model
     * @returns         文件夹图标class
     */
    __genFolderCls(model) {
        if (!this.__icons || this.__icons.length === 0) {
            return;
        }
        const isLeaf = !model.children || model.children.length === 0;
        const isOpen = model.__open;
        const arr = this.__icons;
        const arr1 = ['ui-tree-icon'];
        if (arr.length === 1) {
            arr1.push(isLeaf ? '' : 'ui-icon-' + arr[0]);
        }
        else if (arr.length === 2) {
            arr1.push('ui-icon-' + (isLeaf ? arr[1] : arr[0]));
        }
        else if (arr.length === 3) {
            if (isOpen) {
                arr1.push('ui-icon-' + (isLeaf ? arr[2] : arr[1]));
            }
            else {
                arr1.push('ui-icon-' + (isLeaf ? arr[2] : arr[0]));
            }
        }
        return arr1.join(' ');
    }
    /**
     * 计算展开菜单高度
     * @param model - 节点对应model
     * @returns     容器高度
     */
    __cacHeight(model) {
        let height = 0;
        const oneHeight = 26;
        if (!model.__open || !model.children) {
            return height;
        }
        cac(model.children);
        return height;
        /**
         * 计算子节点容器高度
         * @param chd - 子节点Array
         */
        function cac(chd) {
            for (const c of chd) {
                height += oneHeight;
                if (c.__open && c.children) {
                    cac(c.children);
                }
            }
        }
    }
    /**
     * 点击item事件
     * @param model - 当前节点对应model
     * @param dom - virtual dom节点
     * @param evObj - NEvent对象
     * @param e - event对象
     */
    __clickItem(model, dom, evObj, e) {
        if (this.__onItemClick) {
            UITool.invokePropMethod(this, this.__onItemClick, model, dom, evObj, e);
        }
    }
    /**
     * 展开关闭节点
     * @param model - 当前节点对应model
     */
    __expandClose(model) {
        model['__open'] = !model['__open'];
        if (this.__onExpand && model['__open']) {
            UITool.invokePropMethod(this, this.__onExpand, model);
        }
        else if (this.__onCollapse && !model['__open']) {
            UITool.invokePropMethod(this, this.__onCollapse, model);
        }
    }
    /**
     * checkbox 点击
     * @param model -     当前节点对应model
     */
    __checkItem(model) {
        let state = model.__state;
        state = state === 0 || state === 2 ? 1 : 0;
        this.__changeState(model, state);
        this.__updateState();
    }
    /**
     * 首次渲染事件
     */
    __initValue() {
        if (!this.__field || !this.__valueField) {
            return;
        }
        super.__initValue();
        this.__finishState();
        this.__updateState();
    }
    /**
     * 补全整棵树选中值(state=1)
     */
    __finishState() {
        const value = this.model['__value'];
        const valueField = this.__valueField;
        handleOne(this.model['data']);
        /**
         * 处理单个节点
         * @param m - model
         */
        function handleOne(m) {
            if (value.includes(m[valueField])) {
                if (m.children) {
                    for (const m1 of m.children) {
                        const v = m1[valueField];
                        if (!value.includes(v)) {
                            value.push(v);
                        }
                    }
                }
            }
            if (m.children) {
                for (const m1 of m.children) {
                    handleOne(m1);
                }
            }
        }
    }
    /**
     * 修改模型状态
     * @param model - 模型
     * @param state - 状态 0/1
     */
    __changeState(model, state) {
        const value = this.model['__value'];
        const valueField = this.__valueField;
        updValue(model);
        handleSub(model);
        handleParent(model);
        /**
         * 处理子节点
         * @param m - model
         */
        function handleSub(m) {
            if (m.children) {
                for (const m1 of m.children) {
                    updValue(m1);
                    handleSub(m1);
                }
            }
        }
        /**
         * 处理祖先
         * @param m - model
         * @returns
         */
        function handleParent(m) {
            if (!m.__parent || !m.__parent.__parent) {
                return;
            }
            m = m.__parent.__parent;
            if (state === 0) { //删除value中的父
                updValue(m);
            }
            else { //判断并确定是否添加父
                const all1 = m.children.find(item => !value.includes(item[valueField]));
                //子全为1，则添加值
                if (!all1) {
                    updValue(m);
                }
            }
            handleParent(m);
        }
        /**
         * 更新插件value
         * @param m - model
         */
        function updValue(m) {
            const v = m[valueField];
            const index = value.indexOf(v);
            if (state === 0) {
                if (index !== -1) {
                    value.splice(index, 1);
                }
            }
            else if (index === -1) {
                value.push(v);
            }
        }
    }
    /**
     * 更新整棵树
     */
    __updateState() {
        const value = this.model['__value'];
        const valueField = this.__valueField;
        updOne(this.model['data']);
        function updOne(model) {
            if (value.includes(model[valueField])) {
                model.__state = 1;
                //逆向处理父为state=2
                for (let m = model; m.__parent && m.__parent.__parent;) {
                    const p = m.__parent.__parent;
                    if (p.__state === 1) {
                        break;
                    }
                    else {
                        p.__state = 2;
                    }
                    m = p;
                }
            }
            else {
                model.__state = 0;
            }
            if (model.children) {
                for (const d of model.children) {
                    updOne(d);
                }
            }
        }
    }
}
//注册模块
Nodom.registModule(UITree, 'ui-tree');

/**
 * 参数说明
 * title:   标题
 * buttons: 按钮，以','分割，按钮事件以'|'分割,如：minus|clickMinus,close|clickClose
 */
/**
 * UIPanel 面板
 * @public
 */
class UIPanel extends Module {
    constructor() {
        super(...arguments);
        /**
         * @example
         * 事件对象
         * key:按钮名
         * value:事件名
         */
        this.eventMap = {};
    }
    /**
     * 模板函数
     * @privateRemarks
     */
    template(props) {
        let iconStr = '';
        let style = '';
        if (props['bgcolor']) {
            style += 'background-color:' + props['bgcolor'] + ';';
        }
        if (props['color']) {
            style += 'color:' + props['color'];
        }
        if (props['buttons']) {
            iconStr = "<div class='ui-panel-header-bar'>";
            const arr = props['buttons'].split(',');
            for (const icon of arr) {
                const a = icon.split('|');
                if (a.length === 1) {
                    iconStr += `<ui-button icon='${a[0]}' nobg/>`;
                }
                else if (a.length === 2) {
                    iconStr += `<ui-button icon='${a[0]}' nobg e-click='clickButton'/>`;
                    this.eventMap[a[0]] = a[1];
                }
            }
            iconStr += "</div>";
        }
        this.setExcludeProps(['title', 'buttons', 'bgcolor', 'color']);
        return `
            <div class='ui-panel'>
                <div class='ui-panel-header' style='${style}'>
                    <span class='ui-panel-title'>${props['title']}</span>
                    ${iconStr}
                </div>
                <div class='ui-panel-bodyct'>
                    <slot />
                </div>
            </div>
        `;
    }
    /**
     * 点击按钮
     * @param model - 对应model
     * @param dom - Virtual dom
     */
    clickButton(model, dom) {
        for (const p in this.eventMap) {
            if (dom.props['class'].indexOf('ui-icon-' + p) !== -1) {
                const pm = ModuleFactory.get(this.srcDom.slotModuleId);
                pm.invokeMethod(this.eventMap[p], pm.model);
                break;
            }
        }
    }
}
/**
 * UIPanelBody 面板主体
 * @public
 */
class UIPanelBody extends Module {
    /**
     * 模板函数
     * @privateRemarks
     */
    template() {
        return `
            <div class='ui-panel-body'>
                <slot />
            </div>
        `;
    }
}
/**
 * UIToolbar 面板工具栏
 * @public
 */
class UIToolbar extends Module {
    constructor() {
        super(...arguments);
        /**
         * 子按钮
         */
        this.modules = [UIButton];
    }
    /**
     * 模板函数
     * @privateRemarks
     */
    template() {
        return `
            <div class='ui-toolbar'>
                <slot />
            </div>
        `;
    }
}
/**
 * UIButtonGroup 面板按钮集
 * @public
 */
class UIButtonGroup extends Module {
    constructor() {
        super(...arguments);
        /**
         * 子按钮
         */
        this.modules = [UIButton];
    }
    /**
     * 模板函数
     * @privateRemarks
     */
    template() {
        return `
            <div class='ui-button-group'>
                <slot/>
            </div>
        `;
    }
}
//注册模块
Nodom.registModule(UIPanel, 'ui-panel');
Nodom.registModule(UIPanelBody, 'ui-panel-body');
Nodom.registModule(UIToolbar, 'ui-toolbar');
Nodom.registModule(UIButtonGroup, 'ui-button-group');

/**
 * dialog插件
 * 参数说明
 * title:           标题
 * closable:        是否显示close按钮
 * height:          高度
 * width:           宽度
 * onOpen:          dialog打开后事件方法名
 * onClose:         dialog关闭后事件方法名
 * onBeforeOpen:    dialog打开前事件名，如果对应方法返回true，则不打开
 * onBeforeClose:   dialog关闭前事件名，如果对应方法返回true，则不关闭
 */
/**
 * Dialog 对话框
 * @public
 */
class UIDialog extends Module {
    /**
     * 模板函数html
     * @privateRemarks
     */
    template(props) {
        this.__onOpen = props['onopen'];
        this.__onClose = props['onclose'];
        this.__onBeforeClose = props['onbeforeclose'];
        this.__onBeforeOpen = props['onbeforeopen'];
        let closeStr = '';
        if (props.hasOwnProperty('closable')) {
            closeStr = `<div class='ui-panel-header-bar'>
                            <b class='ui-dialog-close' e-click='__close'/>
                        </div>`;
        }
        this.setExcludeProps(['title', 'onopen', 'onclose', 'onbeforeopen', 'onbeforeclose', 'height', 'width', 'closable']);
        return `
            <div class={{'ui-dialog' + (__show?'':' ui-dialog-hide')}} >
                <div class={{'ui-mask' + (__show?'':' ui-mask-hide')}} />
                <div x-animationbox e-transitionend='__checkClose'>
                    <div class='ui-dialog-body'>
                        <div>
                            <div class='ui-panel' style='width:${props['width']};height:${props['height']}' >
                                <div class='ui-panel-header'>
                                    <span class='ui-panel-title'>${props['title']}</span>
                                    ${closeStr}
                                </div>
                                <div class='ui-panel-bodyct'>
                                    <slot />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        `;
    }
    /**
     * 模型
     * @privateRemarks
     */
    data() {
        return {
            __show: false,
            __open: false
        };
    }
    /**
     * 关闭dialog
     */
    __close() {
        if (this.__onBeforeClose && UITool.invokePropMethod(this, this.__onBeforeClose, this.model)) {
            return;
        }
        this.model['__open'] = false;
    }
    /**
     * 打开dialog
     */
    __open() {
        if (this.__onBeforeOpen && UITool.invokePropMethod(this, this.__onBeforeOpen, this.model)) {
            return;
        }
        this.model['__show'] = true;
        this.model['__open'] = true;
        if (this.__onOpen) {
            UITool.invokePropMethod(this, this.__onOpen, this.model);
        }
    }
    /**
     * 检查关闭
     * @param model - 当前节点对应model
     * @param dom - virtual dom节点
     * @param evObj - NEvent对象
     * @param e - event对象
     */
    __checkClose(model, dom, evObj, e) {
        if (e.target !== this.domManager.renderedTree.children[1].node) {
            return;
        }
        if (!this.model['__open']) {
            this.model['__show'] = false;
            if (this.__onClose) {
                UITool.invokePropMethod(this, this.__onClose, this.model);
            }
        }
    }
}
class UIDialogBody extends UIPanelBody {
}
//注册模块
Nodom.registModule(UIDialog, 'ui-dialog');
Nodom.registModule(UIDialogBody, 'ui-dialog-body');

/**
 * file上传插件
 * 配置项
 * field:           对应数据项名
 * value-field:        值数据项名，对应上传后返回的valueField
 * display-field:    显示数据项名，对应上传后返回的displayField
 * multiple:        是否支持多个文件，设置则表示上传多个文件
 * url-field:       上传后返回的文件url对应的字段名
 * upload-name:     上传名，与服务器接收保持一致
 * upload-url:        上传url
 * delete-url:        删除url
 * max-count:        最大上传数量，multiple设置时有效
 * file-type:        上传资源类型，如果为image，上传成功后显示缩略图，displayField为url对应项，否则显示文件名，对应数据项为displayField。
 */
/**
 * UIFile 文件
 * @public
 */
class UIFile extends BaseInput {
    /**
     * 模板函数html
     * @privateRemarks
     */
    template(props) {
        super.template(props);
        this.__uploadName = props['upload-name'];
        this.__valueField = props['value-field'];
        this.__displayField = props['display-field'];
        this.__multiple = props['multiple'];
        this.__uploadUrl = props['upload-url'];
        this.__deleteUrl = props['delete-url'];
        this.__maxCount = props['max-count'] ? parseInt(props['max-count']) : 0;
        this.__fileType = props['file-type'];
        this.__urlField = props['url-field'];
        this.__width = props['width'] || '100%';
        this.__height = props['height'] || '100%';
        this.setExcludeProps(['multiple', 'value-field', 'display-field', 'upload-url', 'delete-url', 'filet-ype', 'max-count', 'width', 'height', 'url-field', 'upload-name']);
        //根据不同类型显示不同结果串
        const showStr = this.__fileType === 'image' ? `<img src={{${this.__urlField}}} />` : `<div>{{${this.__displayField}}}</div>`;
        const singleStr = !this.__multiple ? 'ui-file-single' : '';
        return `
            <div class="ui-file ${singleStr}" >
                <for class='ui-file-showct' cond={{__value}} >
                    <a class="ui-file-content" target="blank" href={{${this.__urlField}}} style='width:${this.__width};height:${this.__height}'>
                        ${showStr}
                    </a>
                    <b class="ui-file-del" e-click='__delete'/>
                </for>
                <div class="ui-file-uploadct"  style='width:${this.__width};height:${this.__height}'
                    x-show={{!__value || __value.length===0 || this.__multiple && (this.__maxCount === 0 || this.__maxCount > __value.length)}}>
                    <if cond={{__uploading}}>
                        上传中...
                    </if>
                    <else>
                        <div class="ui-file-toupload">
                            <span class='ui-file-add'>+</span>
                        </div>
                        <input type="file" class="ui-file-input" ${this.__multiple ? 'multiple' : ''} e-change='__changeFile'/>
                    </else>
                </div>
            </div>
        `;
    }
    /**
     * 模型
     * @privateRemarks
     */
    data() {
        return {
            __uploading: false,
            __uploadingText: NodomUI.getText('uploadingText')
        };
    }
    /**
     * 文件修改
     * @param model - 对应模型
     * @param dom - virtual dom节点
     */
    __changeFile(model, dom) {
        const el = dom.node;
        if (!el.files) {
            return;
        }
        model.__uploading = true;
        const form = new FormData();
        for (let i = 0; i < el.files.length; i++) {
            form.append(this.__uploadName, el.files[i]);
        }
        //提交请求
        Nodom.request({
            url: this.__uploadUrl,
            method: 'POST',
            params: form,
            header: {
                'Content-Type': 'multipart/form-data'
            },
            type: 'json'
        }).then((r) => {
            //上传显示
            model.__uploading = false;
            if (!model.__value) {
                model.__value = [r];
            }
            else {
                model.__value.push(r);
            }
        });
    }
    /**
     * 删除上传文件
     * @param model - 对应模型
     */
    __delete(model) {
        if (!this.__deleteUrl || !this.__valueField) {
            return;
        }
        const param = {};
        param[this.__valueField] = model[this.__valueField];
        Nodom.request({
            url: this.__deleteUrl,
            method: 'GET',
            params: param,
            header: {
                'Content-Type': 'multipart/form-data'
            },
            type: 'json'
        });
        //移除
        const ind = this.model['__value'].findIndex(item => item[this.__valueField] === model[this.__valueField]);
        if (ind !== -1) {
            this.model['__value'].splice(ind, 1);
            Renderer.add(this);
        }
    }
}
Nodom.registModule(UIFile, 'ui-file');

/**
 * 校验器类
 * @public
 */
class UIValidator {
    /**
     * 验证方法
     * @param type -    类型（参考rules）
     * @param value -   验证值
     * @param args -    参数
     * @returns  字符串: 验证错误，undefined: 验证通过，null: 不存在此验证类型
     */
    static verify(type, value, ...args) {
        if (UIValidator.rules[type]) {
            return UIValidator.rules[type].apply(null, [value, ...args]);
        }
        return null;
    }
    /**
     * 添加校验器
     * @param type -      验证类型
     * @param handler -   验证方法(参数顺序为value,p1,p2,p3,p4)
     * @param msg -       验证失败消息，支持\{0..3\}传递参数p1,p2,p3,p4
     */
    static addValidator(type, handler, msg) {
        UIValidator.msgs[type] = msg;
        UIValidator.rules[type] = handler;
    }
    /**
     * 转换消息
     * @param type - 消息类型
     * @param v1 - 参数1
     * @param v2 - 参数2
     * @returns
     */
    static getMsg(type, v1, v2) {
        let v = NodomUI.getText('form.' + type, v1, v2);
        //如果没从通用类型中找到，则从自定义中查找
        if (!v) {
            v = this.msgs[type];
        }
        return v;
    }
    /**
     * 检查是否为空串
     * @param value - 检测值
     * @returns
     */
    static checkEmpty(value) {
        return value === undefined || value === null || value === "" || Array.isArray(value) && value.length === 0;
    }
    /**
     * 检测是否在range范围
     * @param value - 检测值
     * @returns
     */
    static checkRange(value) {
        if (!value || !Array.isArray(value)) {
            return true;
        }
        if (typeof value[0] === 'string' && value[0].indexOf('-') === -1 && value[0].indexOf(':') === -1) {
            value[0] = Number(value[0]);
        }
        if (typeof value[1] === 'string' && value[1].indexOf('-') === -1 && value[1].indexOf(':') === -1) {
            value[1] = Number(value[1]);
        }
        return value.length === 2 && value[0] <= value[1];
    }
}
/**
 * 自定义验证信息
 */
UIValidator.msgs = {};
//验证规则
UIValidator.rules = {
    require(value) {
        if (UIValidator.checkEmpty(value)) {
            return UIValidator.getMsg('require');
        }
    },
    number(value) {
        if (UIValidator.checkEmpty(value)) {
            return;
        }
        if (isNaN(Number(value))) {
            return UIValidator.getMsg('number');
        }
    },
    min(value, v1) {
        if (UIValidator.checkEmpty(value)) {
            return;
        }
        if (value && Number(value) < v1) {
            return UIValidator.getMsg('min', v1);
        }
    },
    max(value, v1) {
        if (UIValidator.checkEmpty(value)) {
            return;
        }
        if (value && Number(value) > v1) {
            return UIValidator.getMsg('max', v1);
        }
    },
    between(value, v1, v2) {
        if (UIValidator.checkEmpty(value)) {
            return;
        }
        if (value) {
            const num = Number(value);
            if (num < v1 || num > v2) {
                return UIValidator.getMsg('between', v1, v2);
            }
        }
    },
    minLength(value, v1) {
        if (UIValidator.checkEmpty(value)) {
            return;
        }
        if (value && value.length < v1) {
            return UIValidator.getMsg('minLength', v1);
        }
    },
    maxLength(value, v1) {
        if (UIValidator.checkEmpty(value)) {
            return;
        }
        if (value && value.length > v1) {
            return UIValidator.getMsg('maxLength', v1);
        }
    },
    betweenLength(value, v1, v2) {
        if (UIValidator.checkEmpty(value)) {
            return;
        }
        if (value && value.length < v1 || value.length > v2) {
            return UIValidator.getMsg('betweenLength', v1, v2);
        }
    },
    email(value) {
        if (UIValidator.checkEmpty(value)) {
            return;
        }
        if (!/^\w+\S*@[\w\d]+(\.\w+)+$/.test(value)) {
            return UIValidator.getMsg('email');
        }
    },
    url(value) {
        if (UIValidator.checkEmpty(value)) {
            return;
        }
        if (!/^(https?|ftp):\/\/[\w\d]+\..*/.test(value)) {
            return UIValidator.getMsg('url');
        }
    },
    mobile(value) {
        if (UIValidator.checkEmpty(value)) {
            return;
        }
        if (!/^1[3-9]\d{9}$/.test(value)) {
            return UIValidator.getMsg('mobile');
        }
    },
    date(value) {
        if (UIValidator.checkEmpty(value)) {
            return;
        }
        if (!/^\d{4}[-\/](0[1-9]|1[0-2])[-\/](0[1-9]|[12]\d|3[01])$/.test(value)) {
            return UIValidator.getMsg('date');
        }
    },
    datetime(value) {
        if (UIValidator.checkEmpty(value)) {
            return;
        }
        if (!/^\d{4}[-\/](0[1-9]|1[0-2])[-\/](0[1-9]|[12]\d|3[01])\s+([0-1]?[0-9]|2[0-3]):([0-5][0-9]):([0-5][0-9])$/.test(value)) {
            return UIValidator.getMsg('datetime');
        }
    },
    time(value) {
        if (UIValidator.checkEmpty(value)) {
            return;
        }
        if (!/([0-1]?[0-9]|2[0-3]):([0-5][0-9]):([0-5][0-9])$/.test(value)) {
            return UIValidator.getMsg('time');
        }
    },
    idno(value) {
        if (UIValidator.checkEmpty(value)) {
            return;
        }
        if (!/^[1-9]\d{7}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])\d{3}$|^[1-9]\d{5}[1-9]\d{3}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])\d{3}([0-9]|X)$/.test(value)) {
            return UIValidator.getMsg('idno');
        }
    },
    range(value) {
        if (UIValidator.checkRange(value)) {
            return;
        }
        return UIValidator.getMsg('range');
    }
};

/**
 * form 插件
 * 配置项:
 * label-width   label所占宽度
 * unit-width    单位(m,m/s,...)所占宽度
 */
/**
 * UIForm 表单
 * @public
 */
class UIForm extends Module {
    constructor() {
        super(...arguments);
        /**
         * 子item数组
         */
        this.__formItems = [];
    }
    /**
     * 模板函数
     * @privateRemarks
     */
    template(props) {
        this.__labelWidth = props['label-width'] || '100px';
        this.__unitWidth = props['unit-width'] || undefined;
        this.setExcludeProps(['label-width', 'unit-width']);
        return `
            <div class='ui-form'>
                <slot />
            </div>
        `;
    }
    /**
     * 添加form item
     * @param item -  form item
     * @returns
     */
    __addItem(item) {
        if (this.__formItems.includes(item)) {
            return;
        }
        this.__formItems.push(item);
    }
    /**
     * 校验
     */
    __verify() {
        let r = true;
        for (const item of this.__formItems) {
            if (!item.__verify()) {
                r = false;
            }
        }
        return r;
    }
    /**
     * 清除错误提示
     */
    __clearErrorTips() {
        for (const item of this.__formItems) {
            item.__clearErrorTip();
        }
    }
}
/**
 * form item
 * 配置项
 *  label：     输入项label
 *  field：     绑定字段
 *  required：  是否必填
 *  invalid-msg: 校验失败消息，其中内置校验器有自己的消息，method方式也会返回错误消息，但是设置了invalidMsg，则其它消息失效
 *  validator： 校验器，支持内置校验器(参见UIValidator.rules)，regexp(正则表达式)，method(模块方法校验)
 *              使用方式 validator='validatorName:param1:param2'，其中validatorName为内置校验器名，regexp和method
 *              param1,param2为校验器参数，可选
 *              检验器示例:
 *                  between:1:10            校验输入项是否为1-10之间
 *                  method:check            通过模块的check方法进行校验，check参数为(value,model)，其中value为对应项值，model为对应model
 *                  regexp:`^[a-z]{5,20}$`  通过正则表达式进行校验，校验数据项是否为 5-20个小写字母
 */
/**
 * UIFormItem 表单子项组件
 * @public
 */
class UIFormItem extends Module {
    /**
     * 模板函数
     * @privateRemarks
     */
    template(props) {
        const parent = ModuleFactory.get(this.srcDom.slotModuleId);
        const itemStyle = props['item-width'] ? "style='width:" + props['item-width'] + "'" : '';
        const labelStyle = "style='width:" + parent.__labelWidth + "'";
        this.__field = props['field'];
        this.__addValidator(props);
        this.setExcludeProps(['unit', 'field', 'required', 'validator', 'invalid-msg', 'label']);
        //单位串
        const unitStyle = parent.__unitWidth ? `style='width:${parent.__unitWidth}'` : undefined;
        let unitStr = '';
        if (props['unit']) {
            unitStr = `<span class='ui-form-item-unit' ${unitStyle}>${props['unit']}</span>`;
        }
        else if (unitStyle) {
            unitStr = `<span class='ui-form-item-unit' ${unitStyle}></span>`;
        }
        //验证串，需要设置margin-left宽度，保证与输入框对齐
        const validStr = this.__validator.length > 0 ? `
            <div class='ui-form-item-tip' x-show={{__errorMessage!==undefined}} style='margin-left:${parent.__labelWidth}'>
                <b class='ui-form-item-tip-icon' />
                <span class='ui-form-item-tip-text'>{{__errorMessage}}</span>
            </div>` : '';
        return `
            <div class='ui-col ui-form-item'>
                <div class={{'ui-form-item-content' + (__errorMessage?' ui-form-item-error':'')}} ${itemStyle}>
                    <label ${labelStyle}>${props['label'] ? props['label'] : ''} </label>
                    <slot/>
                    ${unitStr}
                </div>
                ${validStr}
            </div>
        `;
    }
    /**
      * 添加校验器
      * @param props -  属性
      */
    __addValidator(props) {
        let firstAdd = false;
        //校验器已存在，则不再添加
        if (!this.__validator) {
            firstAdd = true;
            this.__validator = [];
        }
        const validArr = this.__validator;
        if (this.__field) {
            //添加require
            if (props.hasOwnProperty('required') && !validArr.find(item => item.type === 'require')) {
                validArr.push({ type: 'require' });
            }
            if (props.validator) {
                const item = props.validator;
                const ind = item.indexOf(':');
                if (ind !== -1) {
                    const type = item.substring(0, ind);
                    const params = [];
                    if (!validArr.find(ii => ii.type === type)) {
                        //需要处理数字
                        if (['min', 'max', 'between', 'minLength', 'maxLength', 'betweenLength'].includes(type)) {
                            const pa = item.substring(ind + 1).split(':');
                            for (const p of pa) {
                                params.push(parseInt(p));
                            }
                            validArr.push({ type: type, params: params, msg: props.invalidMsg });
                        }
                        else if ('regexp' === type) { //正则表达式
                            validArr.push({
                                type: type,
                                reg: new RegExp(item.substring(ind + 1)),
                                msg: props.invalidMsg
                            });
                        }
                        else if ('method' === type) {
                            validArr.push({
                                type: type,
                                method: item.substring(ind + 1),
                                msg: props.invalidMsg
                            });
                        }
                    }
                }
                else {
                    if (!validArr.find(ii => ii.type === item)) {
                        validArr.push({ type: item, msg: props.invalidMsg });
                    }
                }
            }
        }
        //第一次需要监听
        if (firstAdd && validArr.length !== 0) {
            //添加监听
            this.watch(this.srcDom.model, this.__field, (m, k, ov, nv) => {
                this.__verify(nv);
            });
        }
    }
    /**
     * 校验
     * @param value - 值
     * @returns         true 通过 false 失败
     */
    __verify(value) {
        if (this.__validator.length === 0) {
            return true;
        }
        value = value || this.get(this.srcDom.model, this.__field);
        let msg, r;
        for (const v of this.__validator) {
            if (v.type === 'regexp') {
                if (!v.reg.test(value)) {
                    msg = v.msg || UIValidator.getMsg('unknown');
                    break;
                }
            }
            else if (v.type === 'method') {
                r = this.invokeMethod(v.method, value, this.srcDom.model);
                if (r) {
                    msg = v.msg || r;
                    break;
                }
            }
            else {
                if (v.params) {
                    r = UIValidator.verify(v.type, value, ...v.params);
                }
                else {
                    r = UIValidator.verify(v.type, value);
                }
                if (r) {
                    msg = v.msg || r;
                    break;
                }
            }
        }
        this.model['__errorMessage'] = msg;
        return msg === undefined;
    }
    /**
     * 清除错误提示
     */
    __clearErrorTip() {
        this.model['__errorMessage'] = undefined;
    }
    /**
     * 第一次渲染前事件
     * @privateRemarks
     */
    onBeforeFirstRender() {
        ModuleFactory.get(this.srcDom.slotModuleId).__addItem(this);
    }
}
Nodom.registModule(UIForm, 'ui-form');
Nodom.registModule(UIFormItem, 'ui-form-item');

/**
 * 表格组件
 * 配置参数
 *  $data           表格数据
 *  row-alt         行颜色交替标志，不用设置值
 *  grid-line       网格线类型，包括cols(列) rows(行) both(行列)，默认无
 *  fix-head        是否固定表头，默认false
 *  checkable       是否显示复选框，默认false
 *  single          支持单选，当配置checkable时有效，默认false
 *  onSelectChange  选中更改时触发事件，只针对单行选中有效，传入参数为当前行model，对于头部check框选中无效
 *  onRowClick      行单击事件
 *  onRowDblClick   行双击事件
 *  onRowExpand     行展开事件
 *  onRowCollapse   行从展开到闭合时事件
 */
/**
 * UIGrid 表格
 * @public
 */
class UIGrid extends Module {
    constructor() {
        super(...arguments);
        /**
         * 表头选中状态 0未选中 1选中 2部分选中
         */
        this.__headCheck = 0;
        /**
         * 列集合
         */
        this.__columns = [];
    }
    /**
     * 模板函数
     * @privateRemarks
     */
    template(props) {
        //列集合为空，进行slot初始化
        if (this.__columns.length === 0) {
            return "<div><slot/></div>";
        }
        this.__rowAlt = props.hasOwnProperty('row-alt');
        this.__fixHead = props.hasOwnProperty('fix-head');
        this.__gridLine = props['grid-line'];
        this.__singleSelect = props.hasOwnProperty('single');
        this.__onSelectChange = props.onselectchange;
        this.__onRowClick = props.onrowclick;
        this.__onRowDblClick = props.onrowdblclick;
        this.__onRowExpand = props.onrowexpand;
        this.__onRowCollapse = props.onrowcollapse;
        let expandStr = '';
        let expandHeadStr = '';
        let checkStr = '';
        let checkHeadStr = '';
        this.__width = 0;
        //行展开容器字符串
        let expandCtStr = '';
        /**
         * 是否可展开
         */
        if (this.__expandDom) {
            expandStr = `<div class='ui-grid-row-item ui-grid-icon' e-click='__clickExpand'>
                            <b class={{__open?'ui-expand-icon ui-expand-icon-open':'ui-expand-icon'}}/>
                        </div>`;
            expandHeadStr = `<div class='ui-grid-row-item ui-grid-icon'></div>`;
            expandCtStr = `<div x-animationbox><div class='ui-grid-expand'></div></div>`;
            this.__width += 25;
        }
        /**
         * 是否带有复选框
         */
        if (props.hasOwnProperty('checkable')) {
            checkStr = `<div class='ui-grid-row-item ui-grid-icon'>
                            <span class={{__genCheckCls(__checked)}} e-click='__clickCheck'>
                                <span class='ui-checkbox-box' >
                                    <span class='ui-checkbox-inner' />
                                </span>
                            </span>
                        </div>`;
            checkHeadStr = `<div class='ui-grid-row-item ui-grid-icon'>
                        <span class={{__genCheckCls(this.__headCheck)}} e-click='__clickHeadCheck'>
                            <span class='ui-checkbox-box' >
                                <span class='ui-checkbox-inner' />
                            </span>
                        </span>
                    </div>`;
            this.__width += 25;
        }
        for (const col of this.__columns) {
            const w = col['width'];
            if (w) {
                if (this.__width >= 0) {
                    this.__width += w;
                }
            }
            else {
                //flex 不计算宽度
                this.__width = -1;
            }
        }
        //设置不渲染属性
        this.setExcludeProps(['grid-line', 'checkable', 'row-alt', 'fix-head', 'onselectchange', 'onrowclick', 'onrowdblclick']);
        return `
            <div class={{__genGridCls()}} style={{__genGridWidth()}}>
                <div class='ui-grid-head' style={{__genWidthStyle()}}>
                    <div class='ui-grid-row' >
                        ${expandHeadStr}
                        ${checkHeadStr}
                    </div>
                </div>
                <div class='ui-grid-bodyct' e-scroll='__scrollBody' >
                    <div class={{__genBodyCls()}} style={{__genWidthStyle()}}>
                        <for cond={{__genData()}} class='ui-grid-rowct'>
                            <div class='ui-grid-row' e-click='__rowClick' e-dblclick='__dblClick'>
                                ${expandStr}
                                ${checkStr}
                            </div>
                            ${expandCtStr}
                        </for>
                    </div>
                </div>
            </div>
        `;
    }
    /**
     * 编译后事件，动态添加列到body
     */
    onCompile() {
        if (this.domManager.vdomTree.children[1]) {
            //head col容器
            const headCt = this.domManager.vdomTree.children[0].children[0];
            //body col容器
            const bodyCt = this.domManager.vdomTree.children[1].children[0].children[0].children[0];
            for (const col of this.__columns) {
                if (headCt) {
                    this.__genNewKey(col.headDom, col);
                    headCt.add(col.headDom);
                }
                if (bodyCt) {
                    this.__genNewKey(col.bodyDom, col);
                    bodyCt.add(col.bodyDom);
                }
            }
            //处理展开节点
            if (this.__expandDom) {
                const ct = this.domManager.vdomTree.children[1].children[0].children[0].children[1].children[0];
                if (ct) {
                    ct.children = this.__expandDom.node.children;
                    for (const c of ct.children) {
                        this.__genNewKey(c, 'ex');
                    }
                }
            }
        }
    }
    /**
     * 生产grid class
     * @returns     grid class
     */
    __genGridCls() {
        const arr = ['ui-grid'];
        if (this.__fixHead) {
            arr.push("ui-grid-fixhead");
        }
        if (this.__gridLine === 'rows') {
            arr.push('ui-grid-row-line');
        }
        else if (this.__gridLine === 'cols') {
            arr.push('ui-grid-col-line');
        }
        else if (this.__gridLine === 'both') {
            arr.push('ui-grid-all-line');
        }
        if (this.props['class']) {
            arr.push(this.props['class']);
        }
        return arr.join(' ');
    }
    /**
     * 产生grid width style，用于body和head
     * @returns style样式
     */
    __genWidthStyle() {
        return this.__width > 0 ? 'width:' + this.__width + 'px' : '';
    }
    /**
     * 获取表格宽度
     * @returns
     */
    __genGridWidth() {
        return this.__width > 0 ? 'max-width:' + (this.__width + 10) + 'px' : '';
    }
    /**
     * 产生body css
     * @returns css串
     */
    __genBodyCls() {
        const arr = ['ui-grid-body'];
        if (this.__rowAlt) {
            arr.push("ui-grid-rowalt");
        }
        return arr.join(' ');
    }
    /**
     * 生成checkbox class
     * @param  st - 状态 0未选中 1全选中 2部分选中
     * @returns     checkbox 的class
     */
    __genCheckCls(st) {
        const arr = ['ui-checkbox'];
        if (!st) {
            arr.push('ui-checkbox-uncheck');
        }
        else if (st === 1) {
            arr.push('ui-checkbox-checked');
        }
        else {
            arr.push('ui-checkbox-partchecked');
        }
        return arr.join(' ');
    }
    /**
     * 点击expand
     * @param model - 对应模型
     */
    __clickExpand(model) {
        model['__open'] = !model['__open'];
        if (this.__onRowExpand && model['__open']) {
            UITool.invokePropMethod(this, this.__onRowExpand, model);
        }
        else if (this.__onRowCollapse && !model['__open']) {
            UITool.invokePropMethod(this, this.__onRowCollapse, model);
        }
    }
    /**
     * 行点击
     * @param model - 当前节点对应model
     * @param dom - virtual dom节点
     * @param evObj - NEvent对象
     * @param e - event对象
     */
    __rowClick(model, dom, evObj, e) {
        UITool.invokePropMethod(this, this.__onRowClick, model, dom, evObj, e);
    }
    /**
     * 行双击
     * @param model - 当前节点对应model
     * @param dom - virtual dom节点
     * @param evObj - NEvent对象
     * @param e - event对象
     */
    __dblClick(model, dom, evObj, e) {
        UITool.invokePropMethod(this, this.__onRowDblClick, model, dom, evObj, e);
    }
    /**
     * 点击头部checkbox
     */
    __clickHeadCheck() {
        if (this.__singleSelect) {
            return;
        }
        const st = this.__headCheck === 1 ? 0 : 1;
        this.__headCheck = st;
        if (!this.model['data'] || this.model['data'].length === 0) {
            return;
        }
        //更新行checkbox状态
        for (const m of this.model['data']) {
            m['__checked'] = st;
        }
    }
    /**
     * 点击行 checkbox
     * @param model - 点击项model
     */
    __clickCheck(model) {
        //单选，需要清理之前选中项
        if (this.__singleSelect) {
            if (this.__selectedModel) {
                this.__selectedModel['__checked'] = 0;
            }
            model['__checked'] = 1;
            this.__headCheck = 2;
            this.__selectedModel = model;
        }
        else {
            model['__checked'] = model['__checked'] ? 0 : 1;
            //修改表头checkbox选中状态
            const rows = this.model['data'];
            const arr = rows.filter(item => item.__checked === 1);
            if (arr.length === rows.length) {
                this.__headCheck = 1;
            }
            else if (arr.length === 0) {
                this.__headCheck = 0;
            }
            else {
                this.__headCheck = 2;
            }
        }
        if (this.__onSelectChange) {
            UITool.invokePropMethod(this, this.__onSelectChange, model);
        }
    }
    /**
     * 设置排序字段
     * @param field - 待排序字段
     * @param type -  1升序  -1降序
     */
    __setSortField(field, type) {
        if (field === this.__sortField) {
            //现在是升序，则解除排序，否则设置为升序
            if (this.model['__sortObj'][field] === type) {
                delete this.model['__sortObj'][field];
            }
            else {
                this.model['__sortObj'][field] = type;
            }
        }
        else { //不是排序字段
            //删除之前的排序字段
            if (this.__sortField) {
                delete this.model['__sortObj'][this.__sortField];
            }
            //设置新排序字段
            this.model['__sortObj'][field] = type;
            this.__sortField = field;
        }
    }
    /**
     * 创建数组数据
     * @returns     新数据
     */
    __genData() {
        //不排序，则直接返回
        if (!this.__sortable) {
            return this.model['data'];
        }
        //新建一个数组进行操作
        const rows = this.model['data'].slice(0);
        if (this.__sortField) {
            const v = this.model['__sortObj'][this.__sortField];
            rows.sort((a, b) => a[this.__sortField] > b[this.__sortField] ? v : -v);
        }
        return rows;
    }
    /**
     * 滚动表格body
     * @param model - 当前节点对应model
     * @param dom - virtual dom节点
     * @param evObj - NEvent对象
     * @param e - event对象
     */
    __scrollBody(model, dom, evObj, e) {
        if (!this.__fixHead) {
            return;
        }
        const el = e.currentTarget;
        const left = el.scrollLeft;
        this.getNode(this.domManager.vdomTree.children[0].key).style.transform = 'translateX(-' + left + 'px)';
    }
    /**
     * 添加记录
     * @param rows - 数据(数组)
     */
    __addRow(rows) {
        if (!this.model['data']) {
            this.model['data'] = [];
        }
        if (Array.isArray(rows)) {
            for (const r of rows) {
                this.model['data'].push(r);
            }
        }
        else {
            this.model['data'].push(rows);
        }
    }
    /**
     * 删除记录
     * @param param -     对象参数，用于查找符合该参数条件的所有数据
     */
    __removeRow(param) {
        if (!this.__checkData()) {
            return;
        }
        for (let i = 0; i < this.model['data'].length; i++) {
            const item = this.model['data'][i];
            //找到标志
            let finded = true;
            for (const k of Object.keys(param)) {
                if (param[k] !== this.get(item, k)) {
                    finded = false;
                    break;
                }
            }
            if (finded) {
                this.model['data'].splice(i--, 1);
            }
        }
    }
    /**
     * 移除所选行
     * @param rows -    待删除的行
     */
    __removeRows(rows) {
        if (!this.__checkData()) {
            return;
        }
        const rows1 = this.model['data'];
        for (let r of rows) {
            //查找
            const index = rows1.findIndex(item => item === r);
            if (index === -1) {
                return;
            }
            //移除
            rows1.splice(index, 1);
        }
    }
    /**
     * 移除选中行
     */
    __removeSelectedRows() {
        if (!this.__checkData()) {
            return;
        }
        const rows = this.model['data'];
        for (let i = 0; i < rows.length; i++) {
            if (rows[i].__checked) {
                rows.splice(i--, 1);
            }
        }
    }
    /**
     * 获取选中行
     * @returns     选中的记录集
     */
    __getSelectedRows() {
        if (!this.__checkData()) {
            return;
        }
        return this.model['data'].filter(item => item.__checked);
    }
    /**
     *  取消选择
     */
    __unselect() {
        if (!this.__checkData()) {
            return;
        }
        for (let o of this.model['data']) {
            o.__checked = false;
        }
    }
    /**
     * 全部选中
     */
    __selectAll() {
        for (let o of this.model['data']) {
            o.__checked = false;
        }
        for (let o of this.model['data']) {
            o.__checked = true;
        }
    }
    /**
     * 添加列
     * @param col - UIGridCol
     */
    __addColumn(col) {
        //如果存在不添加
        if ((!this.__columns.find(item => item === col))) {
            this.__columns.push(col);
            if (col['sortable'] && col['field']) {
                this.__sortable = true;
                if (!this.model['__sortObj']) {
                    this.model['__sortObj'] = {};
                }
            }
            //没有修改数据，需要强制渲染
            Renderer.add(this);
        }
    }
    /**
     * 设置展开节点
     * @param expand -    expand 组件实例
     */
    __setExpandDom(expand) {
        this.__expandDom = expand;
    }
    /**
     * 清空列
     * @param startIndex -  开始索引，参考Array.splice方法参数
     * @param count -       删除数量，参考Array.splice方法参数
     */
    __clearColumns(startIndex, count) {
        if (startIndex !== undefined && count !== undefined) {
            this.__columns.splice(startIndex, count);
        }
        else {
            this.__columns = [];
        }
    }
    /**
     * 为dom及其子节点设置新key
     * @param dom - 待设置key的dom节点
     */
    __genNewKey(dom, col) {
        dom.key += '_' + (typeof col === 'string' ? col : col.id);
        if (dom.children) {
            for (const c of dom.children) {
                this.__genNewKey(c, col);
            }
        }
    }
    /**
     * 检查数据grid数据是有有效
     * @returns     true/false
     */
    __checkData() {
        return !this.model['data'] || !Array.isArray(this.model['data']);
    }
}
/**
 * grid列项组件
 * 配置参数
 *  title           列标题
 *  width           宽度，不带单位，使用时直接按照px计算，如果不设置，则默认flex:1，如果自动铺满，最后一列不设置宽度
 *  sortable        是否支持排序，true/false
 *  field           如果sortable，则需要设置，以该字段排序，如果没设置列显示内容，则默认显示此项对应的值
 *  head-style     表头列样式
 *  body-style      表body列样式
 */
/**
 * UIGridCol 表格列项组件
 *  @public
 */
class UIGridCol extends Module {
    /**
     * 模板函数
     * @privateRemarks
     */
    template(props) {
        //隐藏节点不添加
        if (props.hasOwnProperty('hidden')) {
            return;
        }
        const w = props['width'];
        if (w !== undefined) {
            this['width'] = parseInt(w);
        }
        this['title'] = props['title'];
        this['field'] = props['field'],
            this['width'] = props['width'] ? parseInt(props['width']) : 0;
        this['sortable'] = props['sortable'] === 'true' || props['sortable'] === true;
        this['fixed'] = props['fixed'] === 'right' ? 'right' : props.hasOwnProperty('fixed') ? 'left' : undefined;
        const style = 'style="' + (this['width'] ? 'width:' + this['width'] + 'px' : 'flex:1') + ';';
        let headStyle = style;
        let bodyStyle = style;
        const headAlign = '';
        if (props['head-style']) {
            headStyle += props['head-style'];
        }
        if (props['body-style']) {
            bodyStyle += props['body-style'];
        }
        headStyle += '"';
        bodyStyle += '"';
        const clazz = `class="ui-grid-row-item${this['fixed'] ? (this['fixed'] === 'left' ? ' ui-grid-sticky-left' : ' ui-grid-sticky-right') : ''}"`;
        //排序节点
        const sortStr = this['sortable'] && this['field'] ?
            `<div class='ui-grid-sort' field='${this['field']}'>
                <b class={{'ui-grid-sort-raise' + (__sortObj['${this['field']}']===1?' ui-grid-sort-pressed':'')}} e-click='raiseSort' />
                <b class={{'ui-grid-sort-down' + (__sortObj['${this['field']}']===-1?' ui-grid-sort-pressed':'')}} e-click='downSort'/>
            </div>` : '';
        //两个节点 0:header列节点  1:body列节点
        return `
            <div>
                <div ${clazz} ${headStyle} ${headAlign}>${this['title']} ${sortStr}</div>
                <div ${clazz} ${bodyStyle}></div>
            </div>
        `;
    }
    /**
     * 编译事件
     * @privateRemarks
     */
    onCompile() {
        let node;
        if (this.srcDom.vdom.children && this.srcDom.vdom.children.length > 0) {
            node = this.srcDom.vdom.children[0];
            node.removeDirective('slot');
        }
        else { //默认为field
            node = new VirtualDom();
            node.expressions = [new Expression(this['field'])];
        }
        this.domManager.vdomTree.children[1].children = [node];
        this.headDom = this.domManager.vdomTree.children[0];
        this.bodyDom = this.domManager.vdomTree.children[1];
        ModuleFactory.get(this.srcDom.slotModuleId).__addColumn(this);
        //清空vdomtree，避免渲染
        this.domManager.vdomTree = null;
    }
    /**
     * 升序
     */
    raiseSort() {
        ModuleFactory.get(this.srcDom.slotModuleId).__setSortField(this['field'], 1);
    }
    /**
     * 降序
     */
    downSort() {
        ModuleFactory.get(this.srcDom.slotModuleId).__setSortField(this['field'], -1);
    }
}
/**
 * UIGridExpand 行展开内容
 * @public
 */
class UIGridExpand extends Module {
    /**
     * 模板函数
     * @privateRemarks
     */
    template() {
        this.node = this.srcDom.vdom.children[0];
        this.node.removeDirective('slot');
        ModuleFactory.get(this.srcDom.slotModuleId).__setExpandDom(this);
        return null;
    }
}
//注册模块
Nodom.registModule(UIGrid, 'ui-grid');
Nodom.registModule(UIGridCol, 'ui-grid-col');
Nodom.registModule(UIGridExpand, 'ui-grid-expand');

/**
 * 配置说明
 * $data：          列表数据数组
 * field:           绑定父模块的字段
 * value-field：    值字段名
 * disable-field：  禁用字段名
 * onItemClick：    点击事件
 */
/**
 * UIList 列表
 * @public
 */
class UIList extends BaseInput {
    /**
     * 模板函数
     * @privateRemarks
     */
    template(props) {
        super.template(props);
        this.__multiple = props.hasOwnProperty('multiple');
        this.__valueField = props['value-field'];
        this.__disableField = props['disable-field'];
        this.__onItemClick = props.onitemclick;
        let disableCtx = '';
        if (this.__disableField) {
            disableCtx = "+ (" + this.__disableField + "?' ui-list-item-disable':'')";
        }
        this.setExcludeProps(['field', 'multiple', 'value-field', 'display', 'disable-field', 'onitemclick']);
        return `
            <div class="ui-list">
                <div x-repeat={{data}} class={{'ui-list-item' + (__selected?' ui-list-item-active':'')  ${disableCtx} }} e-click='__clickItem'>
                    <div class='ui-list-itemcontent'>
                        <slot innerRender/>
                    </div>
                    <b class="ui-list-icon"></b>
                </div>
            </div>
        `;
    }
    /**
     * 点击item
     * @param model - 当前节点对应model
     * @param dom - virtual dom节点
     * @param evObj - NEvent对象
     * @param e - event对象
     */
    __clickItem(model, dom, evObj, e) {
        if (!this.__field || this.__disableField && model[this.__disableField]) {
            return;
        }
        const rows = this.model['data'];
        let value;
        const v = model[this.__valueField];
        model['__selected'] = !model['__selected'];
        /**
         * 是否可多项
         */
        if (this.__multiple) {
            value = this.model['__value'] || [];
            if (!Array.isArray(value)) {
                value = [value];
            }
            const index = value.indexOf(v);
            if (model['__selected']) {
                if (index === -1) {
                    value.push(v);
                }
            }
            else {
                if (index !== -1) {
                    value.splice(index, 1);
                }
            }
        }
        else {
            if (model['__selected']) {
                for (const d of rows) {
                    if (d !== model) {
                        d.__selected = false;
                    }
                }
                this.model['__value'] = v;
            }
            else {
                this.model['__value'] = undefined;
            }
        }
        //触发itemclick事件
        if (this.__onItemClick) {
            UITool.invokePropMethod(this, this.__onItemClick, model, dom, evObj, e);
        }
    }
    /**
     * 设置值
     */
    __initValue() {
        super.__initValue();
        const rows = this.model['data'];
        if (!this.__field || !Array.isArray(rows)) {
            return;
        }
        let value = this.model['__value'];
        if (this.__multiple) {
            if (!Array.isArray(value)) {
                value = [value];
            }
            for (const m of rows) {
                m['__selected'] = value.includes(m[this.__valueField]);
            }
        }
        else {
            for (const m of rows) {
                m['__selected'] = value === m[this.__valueField];
            }
        }
    }
}
Nodom.registModule(UIList, 'ui-list');

/**
 * $data：          列表数据数组
 * field:           绑定父模块的字段
 * value-field：    值字段名
 * disable-field：  禁用字段名
 */
/**
 * UIListTransfer 穿梭框
 * @public
 */
class UIListTransfer extends BaseInput {
    /**
     * 模板函数
     * @privateRemarks
     */
    template(props) {
        super.template(props);
        this.__valueField = props['value-field'];
        this.__disableField = props['disable-field'];
        //删除多余属性
        this.setExcludeProps(['field', 'value-field', 'disable-field']);
        let disableCtx = '';
        if (this.__disableField) {
            disableCtx = "+ (" + this.__disableField + "?' ui-list-item-disable':'')";
        }
        return `
            <div class="ui-listtransfer">
                <div class='ui-list'>
                    <div x-repeat={{__getLeftRows()}} class={{'ui-list-item' + (__selected?' ui-list-item-active':'')  ${disableCtx} }} 
                        e-click='__clickItem'>
                        <div class='ui-list-itemcontent'>
                            <slot innerRender/>
                        </div>
                        <b class="ui-list-icon"></b>
                    </div>
                </div>
                <div class='ui-listtransfer-btngrp'>
                    <div>
                    <ui-button icon='double-arrow-right' e-click='__rightClick1'/>
                    <ui-button icon='arrow-right' e-click='__rightClick'/>
                    <ui-button icon='arrow-left' e-click='__leftClick'/>
                    <ui-button icon='double-arrow-left' e-click='__leftClick1'/>
                    </div>
                </div>
                <div class='ui-list'>
                    <div x-repeat={{__selectedRows}} class={{'ui-list-item' + (__selected?' ui-list-item-active':'')  ${disableCtx} }} 
                        e-click='__clickItem'>
                        <div class='ui-list-itemcontent'>
                            <slot innerRender/>
                        </div>
                        <b class="ui-list-icon"></b>
                    </div>
                </div>
            </div>
        `;
    }
    /**
     * 模型
     * @privateRemarks
     */
    data() {
        return {
            __selectedRows: []
        };
    }
    /**
     * 渲染前事件
     * @privateRemarks
     */
    onBeforeRender() {
        if (this.model['__value']) {
            this.__setValue(this.model['__value']);
        }
    }
    /**
     * 获取左边列表数据
     * @returns
     */
    __getLeftRows() {
        if (!this.model['data']) {
            return;
        }
        return this.model['data'].filter(item => !item.__valued);
    }
    /**
     * 点击item
     * @param model - 点击dom的model
     */
    __clickItem(model) {
        if (this.__disableField && model[this.__disableField]) {
            return;
        }
        model['__selected'] = !model['__selected'];
    }
    /**
     * 设置值
     * @param value - 需设置的值
     */
    __setValue(value) {
        const rows = this.model['data'];
        if (!this.__field || !Array.isArray(rows)) {
            return;
        }
        if (!Array.isArray(value)) {
            value = [value];
        }
        //值相同则不执行
        if (value.join(',') === this.__getSelectedValue().join(',')) {
            return;
        }
        for (const m of rows) {
            if (value.indexOf(m[this.__valueField]) !== -1) {
                this.__addSelect(m);
            }
            else {
                this.__removeSelect(m);
            }
        }
        this.__value = value;
    }
    /**
     * 把选中节点传递到右边
     */
    __rightClick() {
        if (!this.model['data']) {
            return;
        }
        this.model['data'].filter(item => item.__selected).forEach(r => {
            this.__addSelect(r);
        });
    }
    /**
     * 把所有节点传递到右边
     */
    __rightClick1() {
        if (!this.model['data']) {
            return;
        }
        this.model['data'].filter(item => (!this.__disableField || !item[this.__disableField]) && !item.__valued).forEach(r => {
            this.__addSelect(r);
        });
    }
    /**
     * 把选中节点传递到左边
     */
    __leftClick() {
        const rows = this.model['__selectedRows'].filter(item => item.__selected);
        if (rows) {
            for (let i = 0; i < rows.length; i++) {
                if (this.__removeSelect(rows[i])) {
                    i--;
                }
            }
        }
    }
    /**
     * 把所有节点传递到左边
     */
    __leftClick1() {
        for (let i = 0; i < this.model['__selectedRows'].length; i++) {
            if (this.__removeSelect(this.model['__selectedRows'][i])) {
                i--;
            }
        }
    }
    /**
     * 添加选中
     * @param m - 选中的model
     */
    __addSelect(m) {
        m.__valued = true;
        m.__selected = false;
        if (!this.model['__selectedRows'].find(item => item[this.__valueField] === m[this.__valueField])) {
            this.model['__selectedRows'].push(m);
            this.__updateValue();
        }
    }
    /**
     * 移除选中
     * @param m - 选中的model
     */
    __removeSelect(m) {
        m.__valued = false;
        m.__selected = false;
        const index = this.model['__selectedRows'].indexOf(m);
        if (index !== -1) {
            this.model['__selectedRows'].splice(index, 1);
            this.__updateValue();
            return true;
        }
        return false;
    }
    /**
     * 更新值
     * @returns
     */
    __updateValue() {
        if (!this.__valueField || !this.__field) {
            return;
        }
        const v = this.__getSelectedValue();
        //设置值
        this.set(this.srcDom.model, this.__field, v);
    }
    /**
     * 获取值
     * @returns 值数组
     */
    __getSelectedValue() {
        const v = [];
        for (const r of this.model['__selectedRows']) {
            v.push(r[this.__valueField]);
        }
        return v;
    }
}
Nodom.registModule(UIListTransfer, 'ui-listtransfer');

/**
 * 配置项：
 *  radius  圈的半径
 *  color   圆点颜色
 */
/**
 * UILoading 加载
 *  @public
 */
class UILoading extends Module {
    constructor() {
        super(...arguments);
        /**
         * 显示数，每open一次，+1，每close一次，-1。close时检查是否为0，为0则关闭
         */
        this.__openCount = 0;
    }
    /**
     * 模板函数
     * @privateRemarks
     */
    template(props) {
        this.__color = props.color;
        //圈大小，默认半径40
        this.__radius = props.radius ? parseInt(props.radius) : 40;
        const width = this.__radius * 2;
        this.setExcludeProps(['radius', 'color']);
        return `
            <div class={{"ui-loading" + (__open?'':' ui-loading-hide')}}>
                <div class={{'ui-mask' + (__open?'':' ui-mask-hide')}} />
                <if cond={{__url}}>
                    <img src={{__url}} />
                </if>
                <else>
                    <canvas role='ui-loading-canvas' width='${width}' height='${width}'
                    style='width:${width}px;height:${width}px' />
                </else>
            </div>
        `;
    }
    /**
     * 打开loading
     * @param limit - 停留时间/动画url地址，如果为0或不填，则表示不自动关闭，如果为字符串，则为动画url地址
     * @param imgUrl - 动画url地址
     */
    __show(limit, imgUrl) {
        const me = this;
        const rootEl = this.domManager.renderedTree.node;
        if (!rootEl) {
            return;
        }
        const canvas = rootEl.querySelector('canvas');
        this.model['__open'] = true;
        let url;
        //设置了自动关闭，则到时关闭
        if (limit) {
            const tp = typeof limit;
            if (tp === 'number') {
                setTimeout(() => {
                    this.__close();
                }, limit);
            }
            else if (tp === 'string') {
                url = limit;
            }
        }
        if (imgUrl) {
            url = imgUrl;
        }
        //已经开启了loading，则不再加载
        if (this.__openCount++ !== 0) {
            return;
        }
        //存在url，则使用自定义动画，不绘制canvas
        if (url) {
            this.model['__url'] = url;
            return;
        }
        const width = canvas.width;
        const circleCount = 6;
        const ctx = canvas.getContext('2d');
        const centerx = me.__radius;
        const centery = me.__radius;
        loop();
        /**
         * 循环绘制
         */
        function loop() {
            if (!me.model['__open']) {
                return;
            }
            const radius1 = 6;
            const radius = me.__radius - radius1;
            let angle = Math.PI / 2;
            const circleArr = [];
            loop1();
            setTimeout(loop, 1500);
            /**
             * 单次绘制
             */
            function loop1() {
                if (!me.model['__open']) {
                    return;
                }
                ctx.clearRect(0, 0, width, width);
                ctx.fillStyle = me.__color;
                if (circleArr.length < circleCount) {
                    circleArr.push(true);
                }
                let overNum = 0;
                for (let i = 0; i < circleArr.length; i++) {
                    let a = angle - i * Math.PI / circleCount;
                    if (a > Math.PI * 5 / 2) {
                        overNum++;
                        a = Math.PI * 5 / 2;
                    }
                    //绘制路径                
                    ctx.beginPath();
                    ctx.arc(centerx - radius * Math.cos(a), centery - radius * Math.sin(a), radius1 - i, 0, 360);
                    ctx.closePath();
                    ctx.fill();
                }
                angle += Math.PI / circleCount;
                if (overNum < circleCount) {
                    setTimeout(loop1, 60);
                }
            }
        }
    }
    /**
     * 关闭loading
     * @param force - 是否强制完全关闭，如果为true，则不计算openCount，直接关闭
     */
    __close(force) {
        if (force) { //强制关闭
            this.model['__open'] = false;
            this.__openCount = 0;
            delete this.model['__url'];
        }
        else if (--this.__openCount === 0) {
            this.model['__open'] = false;
            delete this.model['__url'];
        }
    }
}
//注册插件
Nodom.registModule(UILoading, 'ui-loading');
/**
 * 获取加载框类
 * @public
 */
function nuiloading() {
    return ModuleFactory.getMain().getModule('uiloading');
}

/**
 * 数据项
 * $data             菜单结构数据
 * 参数说明
 * vertical:        是否纵向菜单
 * onItemClick:     菜单项点击事件
 * width:           菜单宽度（px，针对纵向菜单或横向菜单二级及以上菜单）,默认150
 * bgcolor:         菜单背景色（可选）
 * color:           菜单文字色（可选）
 * active-bgcolor:   激活菜单项背景色（可选）
 * active-color:     激活菜单项纹紫色（可选）
 */
/**
 * UIMenu 菜单
 * @public
 */
class UIMenu extends Module {
    /**
     * 模板函数
     * @privateRemarks
     */
    template(props) {
        this.__vertical = props.hasOwnProperty('vertical');
        this.__onItemClick = props['onitemclick'];
        this.__width = props['width'] && props['width'] !== '' ? parseInt(props['width']) : 150;
        //存储样式
        const styleArr1 = [];
        if (props['bgcolor']) {
            styleArr1.push('background-color:' + props['bgcolor']);
        }
        if (props['color']) {
            styleArr1.push('color:' + props['color']);
        }
        const menuStyle = styleArr1.join(';');
        const styleArr2 = [];
        if (props['active-bgcolor']) {
            styleArr2.push('background-color:' + props['active-bgcolor']);
        }
        if (props['active-color']) {
            styleArr2.push('color:' + props['active-color']);
        }
        //激活样式
        let activeClass = '';
        let activeStyle;
        if (styleArr2.length > 0) {
            activeStyle = "(__active?'" + styleArr2.join(";") + "':'" + styleArr1.join(";") + "')";
        }
        else if (styleArr1.length > 0) {
            activeStyle = "(__active?'':'" + styleArr1.join(";") + "')";
        }
        else {
            //默认激活样式
            activeClass = "+ (__active?' ui-menu-active':'')";
        }
        //菜单style
        let style1 = "style={{" + (this.__vertical ? "'padding-left:' + (__level * 20) + 'px;'" : '');
        if (activeStyle) {
            style1 += " + " + activeStyle;
        }
        style1 += "}}";
        const iconStr = `<b x-show={{children&&children.length>0}} class={{'ui-menu-subicon' + (__open?' ui-expand-icon-open':'')}}/>`;
        this.setExcludeProps(['width', 'onitemclick', 'vertical', 'bgcolor', 'color', 'active-bgcolor', 'active-color']);
        //是否纵向
        if (this.__vertical) { //纵向菜单
            return `
                <div class='ui-menu ui-menu-vert' style='${menuStyle}'>
                    <div class='ui-menu-subct-expand'>
                        <for cond={{data}} class='ui-menu-node-wrap' e-click='__clickMenu:nopopo'>
                            <div class={{'ui-menu-node' ${activeClass}}}
                                ${style1}
                                e-click='__clickMenu:nopopo'>
                                <slot innerRender/>
                                ${iconStr}
                            </div>
                            <recur cond='children' class='ui-expand-vertical' style={{'height:' + __cacHeight(__open,children) + 'px'}} >
                                <div style='${menuStyle}'>
                                    <for cond={{children}}  class='ui-menu-node-wrap'  e-click='__clickMenu:nopopo'>
                                        <div class={{'ui-menu-node' ${activeClass}}} 
                                            ${style1}>
                                            <slot innerRender/>
                                            ${iconStr}
                                        </div>
                                        <recur ref />
                                    </for>
                                </div>
                            </recur>
                        </for>
                    </div>
                </div>
            `;
        }
        else { //横向菜单
            return `
                <div class='ui-menu' style='${menuStyle}'>
                    <div class='ui-menu-hori-first'>
                        <for cond={{data}} 
                            class='ui-menu-node-wrap ui-menu-first' 
                            e-mouseenter='__expandMenu'  
                            e-mouseleave='__closeMenu'
                            e-click='__clickMenu:nopopo'>
                            <div class={{'ui-menu-node' ${activeClass}}}
                                ${style1}
                                e-click='__clickMenu:nopopo'>
                                <slot innerRender/>
                            </div>
                            <recur cond='children'
                                e-mouseenter='__expandMenu'
                                e-mouseleave='__closeMenu'
                                e-click='__clickMenu:nopopo'
                                class='ui-menu-subct-wrap'
                                x-animationbox
                                style={{__genPopStyle(children,__open,__x,__y)}}>
                                <div class='ui-menu-subct-pop'>
                                    <div style='${menuStyle}'>
                                        <for cond={{children}}  
                                            class='ui-menu-node-wrap'  
                                            e-mouseenter='__expandMenu'  
                                            e-mouseleave='__closeMenu'
                                            e-click='__clickMenu:nopopo' >
                                            <div class={{'ui-menu-node' ${activeClass}}} 
                                                ${style1}>
                                                <slot innerRender/>
                                                ${iconStr}
                                            </div>
                                            <recur ref />
                                        </for>
                                    </div>
                                </div>
                            </recur>
                        </for>
                    </div>
                </div>
            `;
        }
    }
    /**
     * 点击item事件
     * @param model - 当前dom对应model
     * @param dom - virtual dom节点
     * @param evObj - NEvent对象
     * @param e -event对象
     */
    __clickMenu(model, dom, evObj, e) {
        //处理显示和隐藏
        if (model.children && this.__vertical) {
            model['__open'] = !model['__open'];
        }
        //激活菜单（纵向菜单有效）
        this.__setActive(model);
        //click事件
        if (this.__onItemClick) {
            UITool.invokePropMethod(this, this.__onItemClick, model, dom, evObj, e);
        }
    }
    /**
     * 叶子结点激活
     * @param model - 对应model
     */
    __setActive(model) {
        if (!model.children) {
            if (this.__activeModel) {
                this.__activeModel['__active'] = false;
            }
            model['__active'] = true;
            this.__activeModel = model;
        }
    }
    /**
     * 计算展开菜单高度
     * @param open - 是否展开
     * @param children - 孩子节点
     * @returns     容器高度
     */
    __cacHeight(open, children) {
        let height = 0;
        const oneHeight = 40;
        if (!open || !children) {
            return height;
        }
        cac(children);
        return height;
        function cac(chd) {
            for (const c of chd) {
                height += oneHeight;
                if (c.__open && c.children) {
                    cac(c.children);
                }
            }
        }
    }
    /**
     * 生成popmenu style
     * @param model - 对应model
     * @returns     style串
     */
    __genPopStyle(children, open, x, y) {
        const height = open && children && children.length ? children.length * 40 : 0;
        return `width:${this.__width}px;height:${height || 0}px;left:${x || 0}px;top:${y || 0}px`;
    }
    /**
     * 展开关闭节点
     * @param model - 当前dom对应model
     * @param dom - virtual dom节点
     * @param evObj - NEvent对象
     * @param e - event对象
     */
    __expandMenu(model, dom, evObj, e) {
        if (model['__open'] || !model['children'] || model['children'].length === 0) {
            return;
        }
        model['__open'] = true;
        if (!this.__vertical) {
            this.__cacLoc(model, dom, evObj, e);
        }
    }
    /**
     * 关闭子菜单
     * @param model - 当前dom对应model
     */
    __closeMenu(model) {
        model['__open'] = false;
    }
    /**
     * 计算位置
     * @param model - 模型
     * @param dom - 当前节点
     * @param evObj - event object
     * @param e - html event
     */
    __cacLoc(model, dom, evObj, e) {
        if (!model.children) {
            return;
        }
        //横向菜单 一级菜单的子菜单为下侧，否则为右侧
        const pos = UITool.cacPosition(e, dom.props['class'].includes('-first') ? 1 : 2, this.__width, model.children.length * 30 + 20);
        model.__x = pos[0];
        model.__y = pos[1];
    }
    /**
     * 渲染前事件
     * @privateRemarks
     */
    onBeforeRender() {
        const me = this;
        //设置level
        if (!this.__vertical || !this.model['data']) {
            return;
        }
        for (const d of this.model['data']) {
            if (!d.__level) {
                setLevel(d, 1);
            }
        }
        //设置active
        if (!this.__activeModel) {
            for (const d of this.model['data']) {
                if (setActive(d)) {
                    return;
                }
            }
        }
        /**
         * 设置level
         * @param data - 菜单结构数据
         * @param level - 层级
         * @returns
         */
        function setLevel(data, level) {
            if (data.__level) {
                return;
            }
            data.__level = level;
            if (data.children) {
                for (const d of data.children) {
                    setLevel(d, level + 1);
                }
            }
        }
        /**
         * 设置激活对象
         * @param data - 菜单结构数据
         * @returns
         */
        function setActive(data) {
            if (data.__active) {
                me.__setActive(data);
                return data;
            }
            if (data.children) {
                for (const d of data.children) {
                    setActive(d);
                }
            }
        }
    }
}
//注册模块
Nodom.registModule(UIMenu, 'ui-menu');

/**
 * 参数说明
 * total       total值
 * ps-array：   页面大小数组，数组字符串，默认[10,20,30,50]
 * show-total:  显示总记录数
 * show-jump:   显示跳转
 * show-num:    显示页面号的数量，默认10
 * big-step:    一大步移动页数，默认5
 * onChange:    页号或页面大小改变时执行方法名
 */
/**
 * UIPagination 分页
 * @public
 */
class UIPagination extends Module {
    /**
     * 模板函数
     * @privateRemarks
     */
    template(props) {
        var _a, _b;
        this.__watch();
        this.model['total'] = props['total'] || 0;
        this.__onChange = props['onchange'];
        this.__showNum = props['show-num'] || 10;
        this.__bigStep = props['big-step'] || 5;
        //只在首次执行template才需要设置pageNo和pageSize
        (_a = this.model)['pageNo'] || (_a['pageNo'] = props['page-no']);
        (_b = this.model)['pageSize'] || (_b['pageSize'] = props['page-size']);
        //页面大小选择列表
        let pageArrStr = '';
        if (props['ps-array'] && Array.isArray(props['ps-array'])) {
            this.__pageSizeArray = props['ps-array'].map(item => { return { size: item, text: item + NodomUI.getText('pageSize') }; });
            pageArrStr = `
                <ui-select style='width:120px' $data={{this.__pageSizeArray}} value-field='size' display-field='text' field='pageSize'>
                    {{text}}
                </ui-select>
            `;
        }
        //共*条
        let totalStr = '';
        if (props.hasOwnProperty('show-total')) {
            totalStr = `<div class='ui-pagination-total-wrap'>${NodomUI.getText('total')}<span class="ui-pagination-total">{{total}}</span>${NodomUI.getText('totalUnit')}</div>`;
        }
        //跳转到
        let goStr = '';
        if (props.hasOwnProperty('show-jump')) {
            goStr = `<div class="ui-pagination-go">
                    ${NodomUI.getText('pagePre')}<ui-input type="number" field='pageNo' min='1' max={{pageCount}} />${NodomUI.getText('page')}
                </div>`;
        }
        //设置不渲染属性
        this.setExcludeProps(['onchange', 'show-num', 'show-total', 'show-num', 'show-jump', 'show-go', 'ps-array', 'big-step', 'page-no', 'page-size']);
        return `
            <div class='ui-pagination'>
                ${totalStr}
                <if cond={{total>0}} class='ui-pagination-data-wrap' tag='div'>
                    ${pageArrStr}
                    <div class="ui-pagination-page-wrap">
                        <b class={{'ui-pagination-leftarrow1' + (pageNo===1?' ui-pagination-disable':'')}}  e-click='__reduceMore'/>
                        <b class={{'ui-pagination-leftarrow' + (pageNo===1?' ui-pagination-disable':'')}}  e-click='__reduceOne'/>
                        <span x-repeat={{pages}}
                            class={{'ui-pagination-page' + (this.model.pageNo===page?' ui-pagination-active':'')}}
                            e-click='__clickPage'>
                            {{page}}
                        </span>
                        <b class={{'ui-pagination-rightarrow' + (pageNo===pageCount?' ui-pagination-disable':'')}} e-click='__addOne'/>
                        <b class={{'ui-pagination-rightarrow1' + (pageNo===pageCount?' ui-pagination-disable':'')}}  e-click='__addMore'/>
                    </div>
                    ${goStr}
                </if>
                <else>
                    ${NodomUI.getText('noData')}
                </else>
            </div>
        `;
    }
    /**
     * 模型
     * @privateRemarks
     */
    data() {
        return {
            pageNo: 0,
            pageSize: 0,
            total: 0,
            pages: []
        };
    }
    /**
     * 页号修改钩子
     */
    __changePage() {
        if (!this.model['pageNo'] || !this.model['pageSize']) {
            return;
        }
        //事件回调
        if (this.__onChange) {
            UITool.invokePropMethod(this, this.__onChange, this.model['pageNo'], this.model['pageSize']);
        }
    }
    __watch() {
        if (!this.__watched) {
            this.watch(['pageNo', 'pageSize', 'total'], (model, key, ov, nv) => {
                if (key === 'pageNo') {
                    this.model['pageNo'] = parseInt(nv + '');
                    this.__changePage();
                }
                else if (key === 'pageSize') {
                    this.model['pageCount'] = Math.ceil(model['total'] / this.model['pageSize']);
                    this.__changePage();
                }
                else if (key === 'total') {
                    this.model['pageCount'] = Math.ceil(nv / this.model['pageSize']);
                }
                this.__cacPages();
            });
            this.__watched = true;
        }
    }
    /**
     * 设置页面
     * @param page - 页面号
     */
    __setPage(page) {
        if (!page) {
            page = this.model['pageNo'];
        }
        const count = this.model['pageCount'];
        if (page > count) {
            page = count;
        }
        else if (page < 1) {
            page = 1;
        }
        this.model['pageNo'] = page;
    }
    /**
     * 点击页
     * @param model - 当前节点model
     */
    __clickPage(model) {
        this.__setPage(model.page);
    }
    /**
     * 页号减1
     */
    __reduceOne() {
        if (this.model['pageNo'] === 1) {
            return;
        }
        this.__setPage(this.model['pageNo'] - 1);
    }
    /**
     * 页号加1
     */
    __addOne() {
        if (this.model['pageNo'] === this.model['pageCount']) {
            return;
        }
        this.__setPage(this.model['pageNo'] + 1);
    }
    /**
     * 页号减bigStep
     */
    __reduceMore() {
        if (this.model['pageNo'] === 1) {
            return;
        }
        this.__setPage(this.model['pageNo'] - this.__bigStep);
    }
    /**
     * 页号加bigStep
     */
    __addMore() {
        if (this.model['pageNo'] === this.model['pageCount']) {
            return;
        }
        this.__setPage(this.model['pageNo'] + this.__bigStep);
    }
    /**
     * 计算最大最小页号
     */
    __cacPages() {
        const step = this.__showNum / 2 | 0;
        let minPage;
        let maxPage;
        const count = this.model['pageCount'] || 0;
        if (count === 0) {
            minPage = 0;
            maxPage = 0;
            this.model['pages'] = [];
            return;
        }
        else if (count <= this.__showNum) {
            minPage = 1;
            maxPage = count;
        }
        else { //页面数大于显示数
            minPage = this.model['pageNo'] - step;
            maxPage = this.model['pageNo'] + step;
            if (maxPage - minPage === this.__showNum) {
                maxPage--;
            }
            //处理page范畴
            if (minPage > count) {
                minPage = count - this.__showNum + 1;
                maxPage = count;
            }
            else if (minPage < 1) {
                maxPage += 1 - minPage;
                minPage = 1;
            }
            if (maxPage < 1) {
                minPage = 1;
                maxPage = this.__showNum;
            }
            else if (maxPage > count) {
                minPage = count - this.__showNum + 1;
                maxPage = count;
            }
        }
        //重新计算pages
        let pages = this.model['pages'];
        if (pages.length === 0 || pages[0].page !== minPage || pages[pages.length - 1].page !== maxPage) {
            let finded = false;
            let pno = this.model['pageNo'];
            pages = [];
            for (let i = minPage; i <= maxPage; i++) {
                pages.push({ page: i });
                if (pno === i) {
                    finded = true;
                }
            }
            this.model['pages'] = pages;
            if (!finded) {
                //pageNo不在max min内，需要重新设置
                if (pno < minPage) {
                    pno = minPage;
                }
                else if (pno > maxPage) {
                    pno = maxPage;
                }
                this.model['pageNo'] = pno;
            }
        }
    }
}
//注册模块
Nodom.registModule(UIPagination, 'ui-pagination');

/**
 * UIRadioGroup 单选框集
 * @public
 */
class UIRadioGroup extends BaseInput {
    constructor() {
        super(...arguments);
        /**
         * 子radio数组
         */
        this.__radios = [];
    }
    /**
     * 模板函数
     * @privateRemarks
     */
    template(props) {
        super.template(props);
        return `
            <div class='ui-radiogroup'>
                <slot />
            </div>
        `;
    }
    /**
     * 设置当前子model
     * @param radio - 子radio
     */
    __setCurrent(radio) {
        if (this.__current) {
            this.__current.model.checked = false;
        }
        radio.model.checked = true;
        this.__current = radio;
        this.model['__value'] = radio.value;
    }
    /**
     * 添加子model
     * @param radio -  子radio
     * @returns
     */
    __add(radio) {
        if (this.__radios.includes(radio)) {
            return;
        }
        this.__radios.push(radio);
        if (radio.value == this.model['__value']) {
            this.__setCurrent(radio);
        }
        else {
            radio.model.checked = false;
        }
    }
    /**
     * 初始化数据
     */
    __initValue() {
        for (const r of this.__radios) {
            if (r.value == this.model['__value'] && r !== this.__current) {
                this.__setCurrent(r);
            }
        }
    }
}
/**
 * 配置说明
 * field:           绑定父模块的字段
 */
/**
 * UIRadio 单选框
 * @public
 */
class UIRadio extends Module {
    /**
     * 模板函数
     * @privateRemarks
     */
    template(props) {
        this.value = props.value;
        return `
            <span class={{'ui-radio ' + (checked?'ui-radio-active':'ui-radio-unactive')}} e-click='__click'>
                <b/>
                <span>${props.title}</span>
            </span>    
        `;
    }
    /**
     * 第一次渲染前事件
     * @privateRemarks
     */
    onBeforeFirstRender() {
        ModuleFactory.get(this.srcDom.slotModuleId).__add(this);
    }
    /**
     * 点击事件
     * @param model - 模型
     */
    __click() {
        ModuleFactory.get(this.srcDom.slotModuleId).__setCurrent(this);
    }
}
Nodom.registModule(UIRadioGroup, 'ui-radiogroup');
Nodom.registModule(UIRadio, 'ui-radio');

/**
 * 参数说明
 * $data:           rows和cols定义
 * $value:          对应的数据数组
 * display-field:    用于显示的字段，rows和cols保持已知
 * value-field:      值字段，rows和cols保持已知
 */
/**
 * UIRelationMap 关系图
 * @public
 */
class UIRelationMap extends BaseInput {
    /**
     * 模板函数
     * @privateRemarks
     */
    template(props) {
        super.template(props);
        this.__valueField = props['value-field'];
        this.__displayField = props['display-field'];
        this.setExcludeProps(['display-field', 'value-field', 'left-title', 'top-title']);
        return `
            <table class='ui-relationmap' x-model='arr' >
                <tr class="ui-relationmap-head">
                    <td>
                        <span>${props['left-title']}</span>
                        \\
                        <span>${props['top-title']}</span>
                    </td> 
                    <td x-repeat={{cols}}>{{${this.__displayField}}}</td>
                </tr>
                <tr class='ui-relationmap-row' x-repeat={{rows}}>
                    <td>{{${this.__displayField}}}</td>
                    <td x-repeat={{__valueArray}} e-click='__clickItem'>
                        <b class={{__checked?'ui-icon-select':''}}/>
                    </td>
                </tr>
            </table>
        `;
    }
    /**
     * 初始化值
     */
    __initValue() {
        const model = this.model;
        if (!model['data']) {
            return;
        }
        if (!model['__value']) {
            model['__value'] = [];
        }
        const rowsArray = [];
        for (let row of model['data'].rows) {
            // 不能新建引用类型数据，否则死循环，原因未知
            // row['__valueArray'] = [];
            const valueArray = [];
            for (const col of model['data'].cols) {
                let find = false;
                for (const v of model['__value']) {
                    if (v && row[this['__valueField']] === v[0] && col[this['__valueField']] === v[1]) {
                        find = true;
                        break;
                    }
                }
                valueArray.push({ __checked: find, row: row[this['__valueField']], col: col[this['__valueField']] });
                row = { id: row.id, title: row.title, __valueArray: valueArray };
            }
            rowsArray.push(row);
        }
        model['arr'] = { cols: model['data'].cols, rows: rowsArray };
    }
    /**
     * 点击dom
     * @param model - dom对应model
     */
    __clickItem(model) {
        const index = this.model['__value'].findIndex(item => item[0] === model.row && item[1] === model.col);
        if (model.__checked) {
            if (index !== -1) {
                this.model['__value'].splice(index, 1);
            }
        }
        else if (index === -1) {
            this.model['__value'].push([model.row, model.col]);
        }
    }
    __hover(item) {
    }
    __leave() {
    }
    /**
     * 渲染前事件
     * @privateRemarks
     */
    onBeforeRender() {
        this.__initValue();
    }
}
//注册模块
Nodom.registModule(UIRelationMap, 'ui-relationmap');

/**
 * 配置说明
 * $data：              列表数据数组
 * field:               绑定父模块的字段
 * value-field：        值字段
 * disable-field：      禁用字段（表示记录项不可点击）
 * list-width:          下拉框宽度，默认为select宽度，单位px
 * list-height:         下拉框最大高度，单位px
 * allow-search          是否需要输入搜索
 */
/**
 * UISelect 下拉框
 * @public
 */
class UISelect extends BaseInput {
    /**
     * 模板函数
     * @privateRemarks
     */
    template(props) {
        super.template(props);
        this.__multiple = props.hasOwnProperty('multiple');
        this.__valueField = props['value-field'];
        this.__displayField = props['display-field'];
        this.__disableField = props['disable-field'];
        this.__listWidth = props['list-width'] ? parseInt(props['list-width'] + '') : undefined;
        this.__allowEmpty = props.hasOwnProperty('allow-empty');
        this.__allowSearch = props.hasOwnProperty('allow-search');
        let heightStyle = '';
        if (props['list-height']) {
            this.__listHeight = parseInt(props['list-height'] + '');
            heightStyle = "max-height:" + this.__listHeight + "px;";
        }
        let disableCtx = '';
        if (props['disable-field']) {
            disableCtx = "+ (" + props['disable-field'] + "?' ui-select-item-disable':'')";
        }
        let styleStr = '';
        let selectStr = '';
        /*if (this.__allowSearch) {
            styleStr = ` <div class='ui-select-input-wrap' e-click='__toggleBox:capture' e-keyup='__openBox'>
            <div class='ui-select-show '>
                 <input type="text" x-field="__content"    e-keyup='__filterItem' e-click='__changeInput'>
                </input>
            </div>`;
            selectStr = `
            <div class={{'ui-select-item' + (__selected?' ui-select-item-active':'')  ${disableCtx} }}
            x-repeat={{data}} e-click='__clickItem' x-show={{!__hidden}}>
            `;

        } else {*/
        styleStr = `
                <div class="ui-select-input-wrap" e-click='__toggleBox:captrue' >
                    <div class="ui-select-show" >{{showText}}</div>`;
        selectStr = `
                <div class={{'ui-select-item' + (__selected?' ui-select-item-active':'')  ${disableCtx} }}
                 x-repeat={{data}} e-click='__clickItem'>
             `;
        // }
        this.setExcludeProps(['allow-empty', 'list-width', 'list-height', 'disable-field', 'value-field', 'display-field', 'multiple', 'allow-search']);
        return `
            <div class="ui-select" e-mouseleave='__closeBox'>
                ${styleStr}
                    <b class='ui-expand-icon ui-expand-icon-open'/>
                </div>

            <div class='ui-select-list-wrap' x-animationbox style={{'left:' + __x + 'px;top:' + __y + 'px;width:' + __width + 'px;'}}>
                <div class='ui-select-list'   style={{'${heightStyle}height:' + __height + 'px'}}>
                    <div>
                        ${selectStr}
                            <div class='ui-select-itemcontent'>
                                <slot innerRender/>
                            </div>
                            <b class='ui-select-icon'/>
                        </div>
                    </div>
                </div>
            </div>
        </div>  
            `;
    }
    /**
     * 模型
     * @privateRemarks
     */
    data() {
        return {
            __open: false,
            rows: [],
            showText: '',
            __x: 0,
            __y: 0,
            __width: 0,
            __height: 0,
            __content: ''
        };
    }
    /**
    * 选中后控制输入框
    * @param model - 对应model
    */
    __changeInput(model) {
        this.model['__content'] = '';
        for (const m of model['data']) {
            m['__selected'] = false;
        }
        for (const m of model['data']) {
            m['__hidden'] = false;
        }
    }
    /**
     * 输入内容进行筛选
     * @param model -     模型
     * @param dom -       当前节点
     * @param ev -        event object
     * @param e -         html event
     */
    __filterItem(model, dom, ev, e) {
        const rows = this.model['data'];
        if (!rows) {
            return;
        }
        this.__hidden = false;
        if (this.__disableField && model[this.__disableField]) {
            return;
        }
        const serachInput = e.target;
        const seachValue = serachInput.value;
        for (const m of rows) {
            if (m !== model) {
                if (m[this.__displayField].includes(seachValue) && !m[this.__disableField]) {
                    m['__hidden'] = false;
                }
                else {
                    m['__hidden'] = true;
                }
            }
        }
    }
    /**
     * 交替 list box
     * @param model - 当前节点对应model
     * @param dom - virtual dom节点
     * @param evObj - NEvent对象
     * @param e - event对象
     */
    __toggleBox(model, dom, evObj, e) {
        if (this.model['__open']) {
            this.__closeBox();
        }
        else {
            this.__openBox(model, dom, evObj, e);
        }
    }
    /**
     * 打开list box
     * @param model - 当前节点对应model
     * @param dom - virtual dom节点
     * @param evObj - NEvent对象
     * @param e - event对象
     */
    __openBox(model, dom, evObj, e) {
        model['__width'] = this.__listWidth ? this.__listWidth : e.currentTarget.offsetWidth + 10;
        this.__cacLoc(model, dom, evObj, e);
        this.model['__open'] = true;
    }
    /**
     * 关闭list box
     */
    __closeBox() {
        this.model['__open'] = false;
    }
    /**
     * 点击item
     * @param model - 点击的item对应model
     * @returns
     */
    __clickItem(model) {
        if (this.__disableField && model[this.__disableField]) {
            return;
        }
        let value;
        if (this.__multiple) {
            model.__selected = !model.__selected;
            //如果allowEmpty，则选中实际元素时，第一个元素取消选中，否则第一个元素选中
            value = [];
            const index = this.__allowEmpty ? 1 : 0;
            for (let i = index; i < this.model['data'].length; i++) {
                const m = this.model['data'][i];
                if (m.__selected) {
                    value.push(m[this.__valueField]);
                }
            }
            if (this.__allowEmpty) {
                //未选中实际节点，则空选择项被选中，否则空选择项取消选中
                if (value.length === 0) {
                    this.model['data'][0].__selected = true;
                }
                else {
                    this.model['data'][0].__selected = false;
                }
            }
        }
        else {
            if (model['__selected']) {
                return;
            }
            model['__selected'] = true;
            for (const m of this.model['data']) {
                if (m !== model) {
                    m['__selected'] = false;
                }
            }
            this.__closeBox();
            value = model[this.__valueField];
            this.model["__content"] = model[this.__displayField];
        }
        this.model['__value'] = value;
        this.model['__isEdit'] = false;
    }
    /**
     * 设置值
     * @returns
     */
    __initValue() {
        const emptyText = NodomUI.getText('pleaseSelect');
        super.__initValue();
        if (!Array.isArray(this.model['data'])) {
            this.model['data'] = [];
        }
        let value = this.model['__value'];
        //如果允许空，则需要在数据最前面加上"请选择"项
        if (this.__allowEmpty) {
            const rows = this.model['data'];
            //数组为空或者第一项不为空，都需要添加“请选择”项
            if (rows.length === 0 || rows[0][this.__displayField] !== emptyText) {
                const o = {};
                o[this.__valueField] = undefined;
                o[this.__displayField] = emptyText;
                this.model['data'].unshift(o);
            }
            if (!value || value.length === 0) {
                rows[0]['__selected'] = true;
            }
            else {
                rows[0]['__selected'] = false;
            }
        }
        const rows = this.model['data'];
        if (!Array.isArray(rows)) {
            return;
        }
        //清除选择项
        for (const r of rows) {
            r.__selected = false;
        }
        let showArr = [];
        if (this.__multiple) {
            if (!Array.isArray(value)) {
                value = [value];
            }
            for (const m of rows) {
                if (value.indexOf(m[this.__valueField]) !== -1) {
                    m.__selected = true;
                    showArr.push(m[this.__displayField]);
                }
            }
        }
        else {
            for (const m of rows) {
                if (value === m[this.__valueField]) {
                    m.__selected = true;
                    showArr.push(m[this.__displayField]);
                }
                else {
                    m.__selected = false;
                }
            }
        }
        if (showArr.length === 0 && this.__allowEmpty) {
            showArr = [NodomUI.getText('pleaseSelect')];
        }
        this.model['showText'] = showArr.join(',');
    }
    /**
     * 计算位置
     * @param model - 当前节点对应model
     * @param dom - virtual dom节点
     * @param evObj - NEvent对象
     * @param e - event对象
     */
    __cacLoc(model, dom, evObj, e) {
        if (!model.data) {
            return;
        }
        let h = model.data.filter((item) => !item.__hidden).length * 30;
        if (h > this.__listHeight) {
            h = this.__listHeight;
        }
        const target = e.currentTarget;
        const pos = UITool.cacPosition(e, 1, target.clientWidth, h);
        model.__x = pos[0];
        model.__y = pos[1];
        model.__height = pos[3] + 2;
    }
    /**
     * 渲染前事件
     * @privateRemarks
     */
    onBeforeFirstRender() {
        super.onBeforeFirstRender();
        const rows = this.model['data'];
        if (rows) {
            for (const m of rows) {
                m.__hidden = false;
            }
        }
        //增加"请选择"项
        // if (this.__field && this.__allowEmpty) {
        //     if (!this.model['__value']) {
        //         this.__initValue();
        //     }
        // }
        //修改下拉列表数据，需要更新
        this.watch('data', (m, k, ov, nv) => {
            this.__initValue();
        });
    }
}
//注册模块
Nodom.registModule(UISelect, 'ui-select');

/**
 * 配置项
 * active-color:    激活状态颜色（可选）
 * inactive-color:  非激活状态颜色（可选）
 * values:          值数组，第一个元素为关闭时的值，第二个元素为打开时的值，默认为[false,true]
 */
/**
 * UISwitch 开关
 * @public
 */
class UISwitch extends BaseInput {
    /**
     * 模板函数
     * @privateRemarks
     */
    template(props) {
        super.template(props);
        if (props.hasOwnProperty('active-color')) {
            this.__activeColor = props['active-color'];
        }
        if (props.hasOwnProperty('inactive-color')) {
            this.__inactiveColor = props['inactive-color'];
        }
        if (props.values && Array.isArray(props.values) && props.values.length === 2) {
            this.__values = props.values;
        }
        else {
            this.__values = [false, true];
        }
        this.setExcludeProps(['active-color', 'inactive-color']);
        return `
            <div class="ui-switch" e-click='__click'>
                <div class={{__genClass()}} style={{__genStyle()}}>
                    <span class='ui-switch-circle'></span>
                </div>
            </div>
        `;
    }
    /**
     * 打开/关闭状态切换
     */
    __genClass() {
        return 'ui-switch-slider' + (this.model['__value'] === this.__values[1] ? ' ui-switch-checked' : ' ui-switch-unchecked');
    }
    /**
     * 自定义颜色
     */
    __genStyle() {
        if (this.__activeColor && this.__inactiveColor) {
            return 'background-color:' + (this.model['__value'] === this.__values[1] ? this.__activeColor : this.__inactiveColor);
        }
    }
    /**
     * 点击
     */
    __click() {
        this.model['__value'] = this.model['__value'] === this.__values[1] ? this.__values[0] : this.__values[1];
        this.__genClass();
    }
}
//注册模块
Nodom.registModule(UISwitch, 'ui-switch');

/**
 * date 插件
 * 配置说明：
 * use-timestamp:   是否使用时间戳，如果使用，则对应value为数字，否则为日期串
 * format:          日期格式，参考ISO 日期字符串，默认为 yyyy-MM-dd
 */
/**
 * UIDate 日期选择器
 * @public
 */
class UIDate extends BaseInput {
    /**
     * 模板函数html
     * @privateRemarks
     */
    template(props) {
        super.template(props);
        this.__useTimestamp = props.hasOwnProperty('use-timestamp');
        this.__format = props['format'] || 'yyyy-MM-dd';
        return `
            <div class='ui-datetime' e-mouseleave='__closeBox' >
                <div class='ui-datetime-field'  e-click='__toggleBox'>
                    <input value={{__toDateStr()}} />
                    <b class='ui-datetime-date' />
                </div>
                <div x-animationbox class='ui-datetime-picker-wrap' 
                    style={{'left:' + __x + 'px;top:' + __y + 'px;width:' + __width + 'px;'}}>
                    <div class='ui-datetime-picker'>
                        <div>
                            <div class='ui-datetime-tbl'>
                                <div class='ui-datetime-datetbl'>
                                    <div class='ui-datetime-ymct'>
                                        <b class='ui-datetime-leftarrow1' e-click='__subYear' />
                                        <b class='ui-datetime-leftarrow' e-click='__subMonth' />
                                        <span class='ui-datetime-ym'>{{ year + '/' + month }}</span>
                                        <b class='ui-datetime-rightarrow' e-click='__addMonth' />
                                        <b class='ui-datetime-rightarrow1' e-click='__addYear' />
                                    </div>
                                    <div class='ui-datetime-weekdays'>
                                        <span>${NodomUI.getText('sunday')}</span>
                                        <span>${NodomUI.getText('monday')}</span>
                                        <span>${NodomUI.getText('tuesday')}</span>
                                        <span>${NodomUI.getText('wednesday')}</span>
                                        <span>${NodomUI.getText('thursday')}</span>
                                        <span>${NodomUI.getText('friday')}</span>
                                        <span>${NodomUI.getText('saturday')}</span>
                                    </div>
                                    <div class='ui-datetime-dates'>
                                        <span x-repeat={{__dates}} class={{(selected?'ui-datetime-selected':'') + (disable?'ui-datetime-disable':'')}}
                                            e-click='__clickDate'
                                        >{{date}}</span>
                                    </div>
                                </div>
                            </div>
                            <div class='ui-datetime-btnct'>
                                <ui-button title='${NodomUI.getText('today')}' e-click='__today'/>
                                <ui-button theme='active' title='${NodomUI.getText('ok')}' e-click='__okClick'/>
                            </div>
                        </div>
                    </div>
                </div>
             </div>
        `;
    }
    /**
     * 模型
     * @privateRemarks
     */
    data() {
        return {
            __open: false,
            __dates: []
        };
    }
    /**
     * 交替 list box
     * @param model - 对应model
     * @param dom - virtual dom节点
     * @param evObj - NEvent对象
     * @param e - event对象
     */
    __toggleBox(model, dom, evObj, e) {
        if (this.model['__open']) {
            this.__closeBox();
        }
        else {
            this.__openBox(model, dom, evObj, e);
        }
    }
    /**
     * 打开list box
     * @param model - 对应model
     * @param dom - virtual dom节点
     * @param evObj - NEvent对象
     * @param e - event对象
     */
    __openBox(model, dom, evObj, e) {
        this.__setDate(this.model['__value']);
        this.__cacLoc(model, dom, evObj, e);
        this.model['__open'] = true;
        // (<UIAnimation>this.getModule('uianimation')).__open();
    }
    /**
     * 关闭list box
     */
    __closeBox() {
        this.model['__open'] = false;
        // (<UIAnimation>this.getModule('uianimation')).__close();
    }
    /**
     * 点击item
     * @param model - 对应model
     */
    __clickDate(model) {
        if (!this.__field || model.disable) {
            return;
        }
        this.model['date'] = model.date;
        this.__setDate(this.model['year'] + '-' + this.model['month'] + '-' + this.model['date']);
    }
    /**
     * 设置当前日期
     */
    __today() {
        this.__setDate();
    }
    /**
     * 确定按钮
     */
    __okClick() {
        const dStr = this.model['year'] + '-' + (this.model['month'] > 9 ? this.model['month'] : ('0' + this.model['month'])) + '-' + (this.model['date'] > 9 ? this.model['date'] : '0' + this.model['date']);
        if (this.__useTimestamp) {
            this.model['__value'] = new Date(dStr).getTime();
        }
        else {
            this.model['__value'] = dStr;
        }
        this.__closeBox();
    }
    /**
     * 转换为日期串
     */
    __toDateStr() {
        if (!this.model['__value']) {
            return;
        }
        const d = typeof this.model['__value'] === 'string' ? new Date(this.model['__value']).getTime() : this.model['__value'];
        return Util.formatDate(d, this.__format);
    }
    /**
     * 设置日期或时间
     * @param str - 待设置值(日期串或时间戳)
     */
    __setDate(str) {
        let date;
        let d; //日期串对应日期
        if (str && str !== '') {
            date = new Date(str);
            if (date.toString() !== 'Invalid Date') {
                d = date.getDate();
            }
        }
        if (!d) {
            date = new Date();
            d = date.getDate();
        }
        this.model['date'] = d;
        this.__genDates(date.getFullYear(), date.getMonth() + 1);
    }
    /**
     * 计算位置
     * @param model - 模型
     * @param dom - 当前节点
     * @param evObj - event object
     * @param e -  html event
     */
    __cacLoc(model, dom, evObj, e) {
        const height = Math.ceil(this.model['__dates'].length / 7) * 40 + 164;
        const pos = UITool.cacPosition(e, 1, 312, height);
        model.__x = pos[0];
        model.__y = pos[1];
        model.__width = 312;
    }
    /**
     * 产生日期数组
     * @param year - 年
     * @param month - 月
     */
    __genDates(year, month) {
        this.model['year'] = year;
        this.model['month'] = month;
        //获取当日
        const cda = new Date();
        const cy = cda.getFullYear();
        const cm = cda.getMonth() + 1;
        const cd = cda.getDate();
        const days = this.__cacMonthDays(year, month);
        const dayArr = [];
        let date = new Date(year + '-' + month + '-1');
        //周几
        let wd = date.getDay();
        const lastMonthDays = this.__cacMonthDays(year, month, -1);
        //补充1号对应周前几天日期
        for (let d = lastMonthDays, i = 0; i < wd; i++, d--) {
            dayArr.unshift({
                disable: true,
                selected: false,
                date: d
            });
        }
        //当月日期
        for (let i = 1; i <= days; i++) {
            dayArr.push({
                date: i,
                selected: this.model['year'] === year && this.model['month'] === month && this.model['date'] === i,
                today: cy === year && cm === month && cd === i
            });
        }
        //下月日期
        date = new Date(year + '-' + month + '-' + days);
        //周几
        wd = date.getDay();
        for (let i = wd + 1; i <= 6; i++) {
            dayArr.push({
                disable: true,
                selected: false,
                date: i - wd
            });
        }
        this.model['__dates'] = dayArr;
        this.model['days'] = dayArr;
    }
    /**
     * 计算一个月的天数
     * @param year - 年
     * @param month - 月
     * @param disMonth - 相差月数
     */
    __cacMonthDays(year, month, disMonth) {
        if (disMonth) {
            month += disMonth;
        }
        if (month <= 0) {
            year--;
            month += 12;
        }
        else if (month > 12) {
            year++;
            month -= 12;
        }
        if ([1, 3, 5, 7, 8, 10, 12].includes(month)) {
            return 31;
        }
        else if (month !== 2) {
            return 30;
        }
        else if (year % 400 === 0 || year % 4 === 0 && year % 100 !== 0) {
            return 29;
        }
        else {
            return 28;
        }
    }
    /**
     * 修改月份
     * @param distance - 差异量
     */
    __changeMonth(distance) {
        let year = this.model['year'];
        let month = this.model['month'];
        month += distance;
        if (month <= 0) {
            year--;
            month += 12;
        }
        else if (month > 12) {
            year++;
            month -= 12;
        }
        if (month <= 0) {
            year--;
            month += 12;
        }
        else if (month > 12) {
            year++;
            month -= 12;
        }
        this.__genDates(year, month);
    }
    /**
     * 年份-1
     */
    __subYear() {
        this.__changeMonth(-12);
    }
    /**
     * 年份+1
     */
    __addYear() {
        this.__changeMonth(12);
    }
    /**
     * 月份-1
     */
    __subMonth() {
        this.__changeMonth(-1);
    }
    /**
     * 月份+1
     */
    __addMonth() {
        this.__changeMonth(1);
    }
}
Nodom.registModule(UIDate, 'ui-date');

/**
 * bgcolor:         默认背景色
 * color:           默认前景色
 * active-bgcolor:  激活背景色
 * active-color:    激活前景色
 * onTabChange:     tab页切换钩子函数，页面切换时调用
 */
/**
 * UITab 标签页
 * @public
 */
class UITab extends Module {
    constructor() {
        super(...arguments);
        /**
         * 拖动参数
         */
        this.__dragParam = { dx: 0, x: 0 };
    }
    /**
     * 模板函数
     * @privateRemarks
     */
    template(props) {
        this.__onTabClose = props.ontabclose;
        this.__onTabChange = props.ontabchange;
        let tabStyle = '';
        let activeStyle = '';
        if (props['active-bgcolor']) {
            activeStyle += 'background-color:' + props['active-bgcolor'];
        }
        //激活前景色，如果没设置，但是设置了默认前景色，则使用默认前景色
        const ac = props['active-color'] || props.color;
        if (ac) {
            if (activeStyle !== '') {
                activeStyle += ';';
            }
            activeStyle += 'color:' + ac;
        }
        let defaultStyle = '';
        if (props.bgcolor) {
            defaultStyle += 'background-color:' + props.bgcolor;
        }
        if (props.color) {
            if (defaultStyle !== '') {
                defaultStyle += ';';
            }
            defaultStyle += 'color:' + props.color;
        }
        if (activeStyle) {
            tabStyle = `style={{active?'${activeStyle}':'${defaultStyle}'}}`;
        }
        else if (defaultStyle) {
            tabStyle = `style={{!active?'${props.defaultstyle}':''}}`;
        }
        const headBg = props.bgcolor ? "style='background-color:" + props.bgcolor + "'" : '';
        this.setExcludeProps(['activebgcolor', 'activecolor', 'bgcolor', 'color', 'ontabchange', 'ontabclose']);
        return `
            <div class='ui-tab'>
                <div class='ui-tab-headct' ${headBg} >
                    <div class='ui-tab-head' e-drag='__dragHead' e-mousedown='__dragStart' e-mousemove='__drag' e-mouseup='__dragEnd' e-mouseleave='__dragEnd'>
                        <for cond={{tabs}} index='idx' class={{'ui-tab-item' + (active?' ui-tab-item-active':'')}} e-click='__clickTab' ${tabStyle}>
                            {{title}}
                            <b class="ui-tab-close" x-show={{closable}} e-click='__closeTab:nopopo'/>
                        </for>
                    </div>
                </div>
                <div class='ui-tab-body'>
                    <slot />
                </div>
            </div>
        `;
    }
    /**
     * 模型
     * @privateRemarks
     */
    data() {
        return {
            tabs: []
        };
    }
    /**
     * 添加tab
     * @param cfg - tab配置项
     */
    __addTab(cfg) {
        if (this.model['tabs'].find(item => item.tab === cfg.tab)) {
            return;
        }
        this.model['tabs'].push(cfg);
        this.__setDefaultActive();
    }
    /**
     * 点击tab
     * @param model - 对应节点model
     */
    __clickTab(model) {
        this.__activeTab(model);
    }
    /**
     * 切换到目标tab
     * @param tab - 目标tab
     * @returns
     */
    __changeTo(tab) {
        if (!tab || tab === this.__currentTab || tab && this.__currentTab && tab.idx && tab.idx === this.__currentTab['idx']) {
            return;
        }
        if (this.__currentTab) {
            this.__currentTab.active = false;
            this.__currentTab.tab.__hide();
        }
        tab.active = true;
        if (this.__onTabChange) {
            UITool.invokePropMethod(this, this.__onTabChange, this.__currentTab, tab);
        }
        tab.tab.__show();
        this.__currentTab = tab;
    }
    /**
     * 获取tab
     * @param data - tab config对象 或 title 或index
     * @returns  tab
     */
    __getTab(data) {
        switch (typeof data) {
            case 'number':
                return this.model['tabs'][data];
            case 'string':
                return this.model['tabs'].find((item) => item.title === data);
            default:
                return data;
        }
    }
    /**
     * 激活新tab
     * @param data - tab config对象 或 title 或index
     */
    __activeTab(data) {
        this.__changeTo(this.__getTab(data));
    }
    /**
     * 关闭页签
     * @param data - tab config对象 或 title 或index
     * @returns
     */
    __closeTab(data) {
        //最后一个不删除
        if (this.model['tabs'].length === 1) {
            return;
        }
        const tab = this.__getTab(data);
        if (!tab) {
            return;
        }
        const index = tab.idx;
        if (index >= 0 && index < this.model['tabs'].length) {
            //执行tabclose事件
            if (this.__onTabClose) {
                UITool.invokePropMethod(this, this.__onTabClose, tab);
            }
            //移除
            this.model['tabs'].splice(index, 1);
            //被删除为当前tab，需要切换当前tab
            if (this.__currentTab && index === this.__currentTab['idx']) {
                if (index === this.model['tabs'].length) { //最后一个
                    this.__changeTo(this.model['tabs'][index - 1]);
                }
                else { //取后面一个
                    this.__changeTo(this.model['tabs'][index]);
                }
            }
        }
    }
    /**
     * 关闭所有tab
     */
    __closeAll() {
        this.model['tabs'] = [];
    }
    /**
     * 头部拖动开始
     * @param model - 当前节点对应model
     * @param dom - virtual dom节点
     * @param evObj - NEvent对象
     * @param e - event对象
     */
    __dragStart(model, dom, evObj, e) {
        const el = e.currentTarget;
        const pel = el.parentElement;
        //设置宽度
        let w = 0;
        for (const d of el.children) {
            w += d.offsetWidth;
        }
        el.style.width = (w + 1) + 'px';
        //不比父宽，不处理
        if (el.offsetWidth < pel.offsetWidth) {
            return;
        }
        this.__dragParam.x = e.pageX;
    }
    /**
     * 拖动
     * @param model - 当前节点对应model
     * @param dom - virtual dom节点
     * @param evObj - NEvent对象
     * @param e - event对象
     * @returns
     */
    __drag(model, dom, evObj, e) {
        if (!this.__dragParam.x) {
            return;
        }
        this.__move(e);
    }
    /**
     * 拖动停止
     * @param model - 当前节点对应model
     * @param dom - virtual dom节点
     * @param evObj - NEvent对象
     * @param e - event对象
     */
    __dragEnd(model, dom, evObj, e) {
        this.__move(e);
        delete this.__dragParam.x;
    }
    /**
     * 移动
     * @param e - 事件对象
     * @returns
     */
    __move(e) {
        if (!this.__dragParam.x) {
            return;
        }
        const dx = e.pageX - this.__dragParam.x;
        if (Math.abs(dx) < 2) {
            return;
        }
        this.__dragParam.dx += dx;
        if (this.__dragParam.dx > 0) {
            this.__dragParam.dx = 0;
        }
        else {
            const el = e.currentTarget;
            const pel = el.parentElement;
            if (el.offsetWidth + this.__dragParam.dx < pel.offsetWidth) {
                this.__dragParam.dx = pel.offsetWidth - el.offsetWidth;
            }
        }
        this.__dragParam.x = e.pageX;
        e.currentTarget.style.transform = 'translateX(' + this.__dragParam.dx + 'px)';
    }
    __setDefaultActive() {
        const tabs = this.model['tabs'];
        let atab;
        if (tabs && tabs.length > 0) {
            for (let i = tabs.length - 1; i >= 0; i--) {
                const tab = tabs[i];
                // 最后一个active tab优先
                if (tab.active) {
                    if (!atab) { // 
                        atab = tab;
                    }
                    else { //找到active tab后，另外的active置false
                        tab.active = false;
                    }
                }
            }
            //如果没有设置active tab，默认第一个
            this.__changeTo(atab || tabs[0]);
        }
    }
}
/**
 * 配置说明
 * title    tab标题
 * closable 是否可关闭
 * active   是否处于打开状态
 */
/**
 * UTabItem 标签页个体
 * @public
 */
class UITabItem extends Module {
    /**
     * 模板函数
     * @privateRemarks
     */
    template(props) {
        this.__active = (props.active === 'true' || props.active === true);
        this.__closable = (props.closable === 'true' || props.closable === true);
        this.__title = props.title;
        this.setExcludeProps(['title', 'active', 'closable']);
        return `
            <div x-show={{show}}>
                <slot/>
            </div>
        `;
    }
    /**
     * 模型
     * @privateRemarks
     */
    data() {
        return {
            show: false
        };
    }
    /**
     * 渲染前
     */
    onBeforeFirstRender() {
        //添加到父
        const pm = ModuleFactory.get(this.srcDom.slotModuleId);
        if (pm.constructor !== UITab) {
            return;
        }
        //追加到ui tab插件
        pm.__addTab({
            title: this.__title,
            active: this.__active,
            closable: this.__closable,
            tab: this
        });
    }
    /**
     * 隐藏
     */
    __hide() {
        this.model['show'] = false;
    }
    /**
     * 显示
     */
    __show() {
        this.model['show'] = true;
        this.active();
    }
}
//注册模块
Nodom.registModule(UITab, 'ui-tab');
Nodom.registModule(UITabItem, 'ui-tab-item');

/**
 * bgcolor          默认t背景色
 * color            默认前景色
 * active-bgcolor    激活背景色
 * active-color      激活前景色
 * tabs             页签对象数组
 *  包括：
 *      title       页签标题
 *      active      是否激活
 *      closable    是否可关闭
 *      path        路由路径（需先定义路由）
 * onChange         tab切换时事件，this指向RouterTab插件
 *      参数1：      切换前tab，可能为undefined
 *      参数2:       切换后tab
 * onTabClose       tab关闭事件，this指向RouterTab插件
 *      参数1：      被关闭tab
 */
/**
 * UIRouterTab 路由标签页
 * @public
 */
class UIRouterTab extends Module {
    constructor() {
        super(...arguments);
        /**
         * 拖动参数
         */
        this.__dragParam = { dx: 0, x: 0 };
    }
    /**
     * 模板函数
     * @privateRemarks
     */
    template(props) {
        this.__onTabClose = props.ontabclose;
        this.__onTabChange = props.ontabchange;
        let tabStyle = '';
        let activeStyle = '';
        if (props['active-bgcolor']) {
            activeStyle += 'background-color:' + props['active-bgcolor'];
        }
        //激活前景色，如果没设置，但是设置了默认前景色，则使用默认前景色
        const ac = props['active-color'] || props.color;
        if (ac) {
            if (activeStyle !== '') {
                activeStyle += ';';
            }
            activeStyle += 'color:' + ac;
        }
        let defaultStyle = '';
        if (props['bg-color']) {
            defaultStyle += 'background-color:' + props['bg-color'];
        }
        if (props.color) {
            if (defaultStyle !== '') {
                defaultStyle += ';';
            }
            defaultStyle += 'color:' + props.color;
        }
        if (activeStyle) {
            tabStyle = `style={{active?'${activeStyle}':'${defaultStyle}'}}`;
        }
        else if (defaultStyle) {
            tabStyle = `style={{!active?'${props.defaultstyle}':''}}`;
        }
        const headBg = props.bgcolor ? "style='background-color:" + props.bgcolor + "'" : '';
        this.setExcludeProps(['active-bgcolor', 'active-color', 'bgcolor', 'color', 'onchange', 'ontabclose']);
        return `
            <div class='ui-tab'>
                <div class='ui-tab-headct' ${headBg}>
                    <div class='ui-tab-head' e-drag='__dragHead' e-mousedown='__dragStart' e-mousemove='__drag' e-mouseup='__dragEnd' e-mouseleave='__dragEnd'>
                        <for cond={{tabs}} class={{'ui-tab-item' + (active?' ui-tab-item-active':'')}}  x-route={{path}} active='active' index='idx' ${tabStyle}>
                            {{title}}
                            <b class="ui-tab-close" x-show={{closable}} e-click='__closeTab:nopopo'/>
                        </for>
                    </div>
                </div>
                <div class='ui-tab-body' x-router>
                </div>
            </div>
        `;
    }
    /**
     * 模型
     * @privateRemarks
     */
    data() {
        return {
            tabs: []
        };
    }
    /**
     * 头部拖动开始
     * @param model - 对应model
     * @param dom - Virtual dom
     * @param evObj - NEvent对象
     * @param e - event对象
     */
    __dragStart(model, dom, evObj, e) {
        const el = e.currentTarget;
        const pel = el.parentElement;
        //设置宽度
        let w = 0;
        for (const d of el.children) {
            w += d.offsetWidth;
        }
        el.style.width = (w + 1) + 'px';
        //不比父宽，不处理
        if (el.offsetWidth < pel.offsetWidth) {
            return;
        }
        this.__dragParam.x = e.pageX;
    }
    /**
     * 拖动
     * @param model - 对应model
     * @param dom - Virtual dom
     * @param evObj - NEvent对象
     * @param e - event对象
     * @returns
     */
    __drag(model, dom, evObj, e) {
        if (!this.__dragParam.x) {
            return;
        }
        this.__move(e);
    }
    /**
     * 拖动停止
     * @param model - 对应model
     * @param dom - Virtual dom
     * @param evObj - NEvent对象
     * @param e - event对象
     */
    __dragEnd(model, dom, evObj, e) {
        this.__move(e);
        delete this.__dragParam.x;
    }
    /**
     * 移动
     * @param e - event对象
     * @returns
     */
    __move(e) {
        if (!this.__dragParam.x) {
            return;
        }
        const dx = e.pageX - this.__dragParam.x;
        if (Math.abs(dx) < 2) {
            return;
        }
        this.__dragParam.dx += dx;
        if (this.__dragParam.dx > 0) {
            this.__dragParam.dx = 0;
        }
        else {
            const el = e.currentTarget;
            const pel = el.parentElement;
            if (el.offsetWidth + this.__dragParam.dx < pel.offsetWidth) {
                this.__dragParam.dx = pel.offsetWidth - el.offsetWidth;
            }
        }
        this.__dragParam.x = e.pageX;
        e.currentTarget.style.transform = 'translateX(' + this.__dragParam.dx + 'px)';
    }
    /**
     * 清除多余active
     */
    __cleanActive() {
        const tabs = this.model['tabs'];
        let atab;
        if (tabs && tabs.length > 0) {
            for (let i = tabs.length - 1; i >= 0; i--) {
                const tab = tabs[i];
                // 最后一个active tab优先
                if (tab.active) {
                    if (!atab) { // 
                        atab = tab;
                    }
                    else { //找到activetab后，另外的active置false
                        tab.active = false;
                    }
                }
            }
            //如果没有设置active tab，默认第一个
            this.__changeTo(atab || tabs[0]);
        }
    }
    /**
     * 切换到目标tab
     * @param tab - 目标tab
     * @returns
     */
    __changeTo(tab) {
        var _a;
        if (!tab || tab.idx === ((_a = this.__currentTab) === null || _a === void 0 ? void 0 : _a.idx)) {
            return;
        }
        if (this.__currentTab) {
            this.__currentTab.active = false;
        }
        tab.active = true;
        if (this.__onTabChange) {
            UITool.invokePropMethod(this, this.__onTabChange, this.__currentTab, tab);
        }
        this.__currentTab = tab;
        if (!Nodom['$Router']) {
            Nodom.use(Router);
        }
        Nodom['$Router'].go(tab.path);
    }
    /**
     * 手动active
     * @param data - model或title或索引号
     */
    __activeTab(data) {
        let tab;
        switch (typeof data) {
            case 'number':
                tab = this.model['tabs'][data];
                break;
            case 'string':
                tab = this.model['tabs'].find((item) => item.title === data);
                break;
            default:
                tab = this.model['tabs'].find((item) => item === data);
        }
        this.__changeTo(tab);
    }
    /**
     * 关闭页签
     * @param model - TabItem
     * @returns
     */
    __closeTab(model) {
        //最后一个不删除
        if (this.model['tabs'].length === 1) {
            return;
        }
        const index = this.model['tabs'].findIndex(item => item === model);
        if (index >= 0 || index < this.model['tabs'].length) {
            model = this.model['tabs'][index];
            //执行tabclose事件
            if (this.__onTabClose) {
                UITool.invokePropMethod(this, this.__onTabClose, this.model['tabs'][index]);
            }
            //移除
            this.model['tabs'].splice(index, 1);
            //被删除为当前tab，需要切换当前tab
            if (model === this.__currentTab) {
                if (index === this.model['tabs'].length) { //最后一个
                    this.__changeTo(this.model['tabs'][index - 1]);
                }
                else { //取后面一个
                    this.__changeTo(this.model['tabs'][index]);
                }
            }
        }
    }
    /**
     * 渲染前事件
     * @privateRemarks
     */
    onBeforeRender() {
        this.__cleanActive();
    }
}
//注册模块
Nodom.registModule(UIRouterTab, 'ui-routertab');

/**
 * radio group插件
 */
class UIRange extends BaseInput {
    template(props) {
        super.template(props);
        this.__type = props.type || 'number';
        let type = this.__type;
        if (type === 'datetime') {
            type = 'datetime-local';
        }
        return `
            <div class='ui-range'>
                <input type='${type}' x-field='data1'/>
                <span> - </span>
                <input type='${type}' x-field='data2'/>
            </div>
        `;
    }
    onBeforeFirstRender() {
        super.onBeforeFirstRender();
        const formItem = ModuleFactory.get(this.srcDom.slotModuleId);
        if (formItem instanceof UIFormItem) {
            formItem.__addValidator({
                validator: 'range'
            });
        }
        this.watch(['data1', 'data2'], (m, k, ov, nv) => {
            this.model['__value'] = (k === 'data1' ? [nv, this.model['data2']] : [this.model['data1'], nv]);
        });
    }
    __initValue() {
        const value = this.model['__value'];
        if (value && value.length === 2) {
            this.model['data1'] = value[0];
            this.model['data2'] = value[1];
        }
    }
}
Nodom.registModule(UIRange, 'ui-range');

/**
 * UITip 信息提示
 * @public
 */
class UITip extends Module {
    /**
     * 模板函数
     * @privateRemarks
     */
    template(props) {
        super.template(props);
        //四个位置
        const arr = ['top', 'right', 'bottom', 'left'];
        //位置item串
        let str = '';
        for (const d of arr) {
            str += `
                <div class='ui-tip ui-tip-${d}'>
                    <for cond={{${d}Data}}
                        x-animationbox e-transitionend='__removeHide'>
                        <div class='ui-tip-wrap'>
                            <div>
                                <div class={{'ui-tip-item' + (theme?(' ui-box-' + theme):'')}} style={{'width:' + width + 'px'}}>
                                    <b x-show={{showIcon}} class={{icon?('ui-icon-' + icon):''}} />
                                    <span class='ui-tip-content'>{{text}}</span>
                                    <b class='ui-tip-close' e-click='__close' x-show={{closable}}/>
                                </div>
                            </div>
                        </div>
                    </for>
                </div>
            `;
        }
        return `
            <div class='ui-tip'>
                ${str}
            </div>
        `;
    }
    /**
     * 模型
     * @privateRemarks
     */
    data() {
        return {
            topData: [],
            rightData: [],
            bottomData: [],
            leftData: []
        };
    }
    /**
     * 关闭
     * @param model - 对应节点的model
     */
    __close(model) {
        model['__open'] = false;
    }
    /**
     * 移除hide item
     * @param model - 当前节点对应model
     * @param dom - virtual dom节点
     * @param evObj - NEvent对象
     * @param e - event对象
     */
    __removeHide(model, dom, evObj, e) {
        if (e.target !== this.domManager.renderedTree.children[0].node)
            return;
        for (const data of ['topData', 'rightData', 'bottomData', 'leftData']) {
            const rows = this.model[data];
            if (!rows) {
                continue;
            }
            for (let i = 0; i < rows.length; i++) {
                if (!rows[i]['__open']) {
                    rows.splice(i--, 1);
                }
            }
        }
    }
}
//注册模块
Nodom.registModule(UITip, 'ui-tip');
/**
 * UITipManager UITip管理器
 * @public
 */
class UITipManager {
    /**
     * 添加tip
     * @param cfg - tip配置项
     */
    static addTip(cfg) {
        cfg.position || (cfg.position = 'top');
        cfg.theme || (cfg.theme = 'default');
        cfg.id = this.id++;
        const mdl = this.getUITip();
        const rows = mdl.model[cfg.position + 'Data'];
        //排他
        if (cfg.exclusive) {
            rows.splice(0, rows.length);
        }
        cfg.showIcon = cfg.hasOwnProperty('icon');
        cfg['__open'] = true;
        //设置默认width
        cfg.width || (cfg.width = 300);
        //不显示关闭按钮，则设置自动关闭时间
        if (!cfg.closable) {
            //设置关闭时间
            const t = new Date().getTime();
            cfg.closeTime = t + (cfg.timeout || 2000);
            //添加到待关闭队列
            this.tobeClosed.push(cfg);
            //排序
            this.tobeClosed.sort((a, b) => { return a.closeTime < b.closeTime ? -1 : 1; });
            //设置延迟清理
            setTimeout(() => { UITipManager.removeTip(); }, (this.tobeClosed[0].closeTime - t));
        }
        rows.push(cfg);
    }
    /**
     * 移除tip
     */
    static removeTip() {
        const mdl = this.getUITip();
        if (!mdl) {
            return;
        }
        if (this.tobeClosed.length === 0) {
            return;
        }
        const t = new Date().getTime();
        //第一个时间不超时，则后续不超时
        if (this.tobeClosed[0].closeTime > t) {
            return;
        }
        for (let i = 0; i < this.tobeClosed.length; i++) {
            const d = this.tobeClosed[i];
            //从待关闭队列移除
            this.tobeClosed.splice(i--, 1);
            const rows = mdl.model[d.position + 'Data'];
            //从uitip数据移除
            const ind = rows.findIndex(item => item.id === d.id);
            if (ind !== -1) {
                rows[ind]['__open'] = false;
            }
        }
        if (this.tobeClosed.length > 0) {
            //设置延迟清理
            setTimeout(() => { UITipManager.removeTip(); }, (this.tobeClosed[0].closeTime - t));
        }
    }
    /**
     * 获取uiTip插件
     * @returns     uiTip插件
     */
    static getUITip() {
        const root = ModuleFactory.getMain();
        for (const m of root.children) {
            if (m instanceof UITip) {
                return m;
            }
        }
        return null;
    }
}
/**
 * 待关闭tip数组
 */
UITipManager.tobeClosed = [];
/**
 * tip id
 */
UITipManager.id = 0;
/**
 * 暴露tip函数
 * @param cfg -tip 配置
 * @public
 */
function nuitip(cfg) {
    UITipManager.addTip(cfg);
}

/**
 * UIMessageBox 消息框
 * @public
 */
class UIMessageBox extends Module {
    /**
     * 模板函数
     * @privateRemarks
     */
    template() {
        let btnStr = '';
        let index = 0;
        if (this.__buttons) {
            for (const b of this.__buttons) {
                let methodName = b.callback;
                if (typeof b.callback === 'function') {
                    //设定方法名和回调函数
                    methodName = '__genmethod__' + index++;
                    this[methodName] = (model, dom, ev, e) => {
                        b.callback.call(this, this.model, dom, ev, e);
                        this.__close();
                    };
                }
                btnStr += `<ui-button title='${b.text}' theme='${b.theme || "default"}' e-click='${methodName}'></ui-button>`;
            }
        }
        return `
            <div class={{'ui-dialog ui-messagebox' + (__show?'':' ui-dialog-hide')}}>
                <div class={{'ui-mask' + (__open?'':' ui-mask-hide')}} />
                <div x-animationbox class='ui-messagebox-wrap' e-transitionend='__checkClose'>
                    <div>
                        <div class='ui-messagebox-body'>
                            <div class='ui-messagebox-content'>
                                <div x-if={{type==='prompt'}}>
                                    <div class='ui-messagebox-text'>{{text}}</div>
                                    <ui-input field='answer'/>
                                </div>
                                <div x-else>
                                    <div class='ui-messagebox-text'>{{text}}</div>
                                </div>
                            </div>
                            <div class='ui-messagebox-btn-wrap'>
                                ${btnStr}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        `;
    }
    /**
     * 模型
     * @privateRemarks
     */
    data() {
        return {
            __show: false,
            __open: false,
            answer: ''
        };
    }
    /**
     * 关闭窗口
     */
    __close() {
        this.model['__open'] = false;
    }
    /**
     * 检查关闭状态（transition结束后）
     * @param model - 当前节点对应model
     * @param dom - virtual dom节点
     * @param evObj - NEvent对象
     * @param e - event对象
     */
    __checkClose(model, dom, evObj, e) {
        if (e.target !== this.domManager.renderedTree.children[1].node) {
            return;
        }
        if (!this.model['__open']) {
            this.model['__show'] = false;
        }
    }
}
/**
 * UIMessageBoxManager 消息框管理器
 * @public
 */
class UIMessageBoxManager {
    /**
     * confirm 对话框
     * @param cfg - confirm 配置
     * @returns
     */
    static confirm(cfg) {
        const mdl = ModuleFactory.getMain().getModule('UIMessageBox');
        if (!mdl) {
            return;
        }
        //修改状态，避免不被渲染
        mdl.model['text'] = cfg.text;
        mdl.model['type'] = 'confirm';
        mdl.model['__open'] = true;
        mdl.model['__show'] = true;
        mdl.__buttons = [{
                text: cfg.cancelTitle || NodomUI.getText('cancel'),
                theme: 'default',
                callback: '__close'
            }];
        if (Array.isArray(cfg.buttons)) {
            mdl.__buttons = mdl.__buttons.concat(cfg.buttons);
        }
    }
    /**
     * alert 对话框
     * @param cfg - alert 配置
     * @returns
     */
    static alert(cfg) {
        const mdl = ModuleFactory.getMain().getModule('UIMessageBox');
        if (!mdl) {
            return;
        }
        //修改状态，避免不被渲染
        mdl.model['text'] = cfg.text;
        mdl.model['type'] = 'alert';
        mdl.model['__open'] = true;
        mdl.model['__show'] = true;
        mdl.__buttons = [{ text: NodomUI.getText('ok'), theme: 'active', callback: '__close' }];
    }
    /**
     * prompt 对话框
     * @param cfg - prompt配置
     */
    static prompt(cfg) {
        const mdl = ModuleFactory.getMain().getModule('UIMessageBox');
        if (!mdl) {
            return;
        }
        //修改状态，避免不被渲染
        mdl.model['text'] = cfg.text;
        mdl.model['answer'] = cfg.answer;
        mdl.model['type'] = 'prompt';
        mdl.model['__open'] = true;
        mdl.model['__show'] = true;
        mdl.__buttons = [{ text: cfg.cancelTitle || NodomUI.getText('cancel'), theme: 'default', callback: '__close' }];
        if (Array.isArray(cfg.buttons)) {
            mdl.__buttons = mdl.__buttons.concat(cfg.buttons);
        }
    }
}
/**
 * confirm创建函数
 * @param cfg - 配置项
 * @example
 * ```js
 *  {
 *      text:消息内容,
 *      icon:消息图标,
 *      buttons:[{
 *          text:按钮标题,
 *          callback:回调函数
 *      },]
 *  }
 * ```
 * @public
 */
function nuiconfirm(cfg) {
    UIMessageBoxManager.confirm(cfg);
}
/**
 * alert 创建函数
 * @param cfg - 配置项
 * @example
 * ```js
 *  {
 *      text:消息内容,
 *      icon:消息图标
 *  }
 * ```
 * @public
 */
function nuialert(cfg) {
    UIMessageBoxManager.alert(cfg);
}
/**
 * confirm创建函数
 * @param cfg - 配置项
 * @example
 * ```json
 * {
 *      text:消息内容,
 *      answer:默认回答,
 *      icon:消息图标,
 *      buttons:[{
 *          text:按钮标题,
 *          callback:回调函数
 *      },...]
 *  }
 * ```
 * @public
 */
function nuiprompt(cfg) {
    UIMessageBoxManager.prompt(cfg);
}
//注册插件
Nodom.registModule(UIMessageBox, 'ui-messagebox');

/**
 * 配置参数
 *  vertical    是否为纵向分布，默认false（横向分布）
 */
/**
 * UILayout 布局
 *  @public
 */
class UILayout extends Module {
    template(props) {
        this.setExcludeProps(['vertical']);
        return `
            <div class="ui-layout ${props.hasOwnProperty('vertical') ? 'ui-layout-vert' : 'ui-layout-hori'}" >
                <slot/>
            </div>
        `;
    }
}
/**
 * UILayoutTop 顶部布局
 * @public
 */
class UILayoutTop extends Module {
    template() {
        return `
            <div class='ui-layout-top'>
                <slot/>
            </div>
        `;
    }
}
/**
 * UILayoutLeft 左侧布局
 * 必须设置宽度
 * @public
 */
class UILayoutLeft extends Module {
    /**
     * 模板函数
     * @privateRemarks
     */
    template(props) {
        const sizeStr = props.hasOwnProperty('sizable') ? `
            <div class='ui-layout-spliter' 
                e-mousedown='__dragStart'  
            ></div>` : "";
        return `
            <div class='ui-layout-left'>
                <div class='ui-layout-content'><slot/></div>
                ${sizeStr}
            </div>
        `;
    }
    /**
     * 拖动开始事件
     * @param model - 当前节点对应model
     * @param dom - virtual dom节点
     * @param evObj - NEvent对象
     * @param e - event对象
     */
    __dragStart(model, dom, evObj, e) {
        e.preventDefault();
        const el = e.currentTarget.parentElement;
        const width = el.offsetWidth;
        const initX = e.pageX;
        //添加mousemove事件
        window.addEventListener('mousemove', moveEvent);
        //添加mouseup事件
        window.addEventListener('mouseup', upEvent);
        /**
         * mousemove事件钩子
         * @param e - 鼠标事件
         */
        function moveEvent(e) {
            el.style.width = (e.pageX - initX + width) + 'px';
        }
        /**
         * mouseup 事件钩子
         */
        function upEvent() {
            window.removeEventListener('mousemove', moveEvent);
            window.removeEventListener('mouseup', upEvent);
        }
    }
}
/**
 * UILayoutCenter 中心布局
 * @public
 */
class UILayoutCenter extends Module {
    /**
     * 模板函数
     * @privateRemarks
     */
    template() {
        return `
            <div class='ui-layout-center'>
                <slot/>
            </div>
        `;
    }
}
/**
 * UILayoutRight 右部布局
 * @public
 */
class UILayoutRight extends Module {
    /**
     * 模板函数
     * @privateRemarks
     */
    template(props) {
        const sizeStr = props.hasOwnProperty('sizable') ? `
            <div class='ui-layout-spliter' 
                e-mousedown='__dragStart'  
            ></div>` : "";
        return `
            <div class='ui-layout-right'>
                ${sizeStr}
                <div class='ui-layout-content'><slot/></div>
            </div>
        `;
    }
    /**
     * 拖动开始事件
     * @param model -     模型
     * @param dom -       dom节点
     * @param evObj -     nodom event对象
     * @param event -     html event对象
     */
    __dragStart(model, dom, evObj, event) {
        event.preventDefault();
        const el = event.currentTarget.parentElement;
        const width = el.offsetWidth;
        const initX = event.pageX;
        //添加mousemove事件
        window.addEventListener('mousemove', moveEvent);
        //添加mouseup事件
        window.addEventListener('mouseup', upEvent);
        /**
         * mousemove事件钩子
         * @param e - 鼠标事件
         */
        function moveEvent(e) {
            el.style.width = (initX - e.pageX + width) + 'px';
        }
        /**
         * mouseup 事件狗子
         */
        function upEvent() {
            window.removeEventListener('mousemove', moveEvent);
            window.removeEventListener('mouseup', upEvent);
        }
    }
}
/**
 * UILayoutBottom 底部布局
 * @public
 */
class UILayoutBottom extends Module {
    /**
     * 模板函数
     * @privateRemarks
     */
    template() {
        return `
            <div class='ui-layout-bottom'>
                <slot/>
            </div>
        `;
    }
}
//注册模块
Nodom.registModule(UILayout, 'ui-layout');
Nodom.registModule(UILayoutTop, 'ui-layout-top');
Nodom.registModule(UILayoutLeft, 'ui-layout-left');
Nodom.registModule(UILayoutCenter, 'ui-layout-center');
Nodom.registModule(UILayoutRight, 'ui-layout-right');
Nodom.registModule(UILayoutBottom, 'ui-layout-bottom');

/**
 * 参数说明
 * autoplay:        是否自动切换，无值属性
 * interval:        切换时间，单位为ms，默认500
 * ani-type:        动画效果  'scroll' | 'fade'
 * init-index:      默认展示的索引
 * arrow-type:      箭头的显示方式  'always' | 'hover'
 * show-indicator:  是否展示指示器
 * onChange:        切换时的回调函数 `(oldIdx, newIdx) => void`
 */
/**
 * UICarousel 轮播图
 * @public
 */
class UICarousel extends Module {
    constructor() {
        super(...arguments);
        /**
         * 当前索引号
         */
        this.__currentIndex = -1;
        /**
         * items 集合
         */
        this.__items = [];
    }
    /**
     * 模型
     * @privateRemarks
     */
    data() {
        return {
            __count: 0,
            __indicators: [] //索引圆圈
        };
    }
    /**
     * 模板函数
     * @privateRemarks
     */
    template(props) {
        this.__interval = props['interval'] ? parseInt(props['interval'] + '') : 500;
        this.__autoplay = props.hasOwnProperty('autoplay');
        this.__animation = props['ani-type'] || 'scroll';
        this.__initIndex = props['init-index'] ? parseInt(props['init-index'] + '') : 0;
        this.__onChange = props['onchange'];
        const showIndicator = props.hasOwnProperty('show-indicator');
        const arrowType = props['arrow-type'] || 'alaways';
        let __indicatorstr = '';
        /**
         * 是否显示指示器
         */
        if (showIndicator) {
            __indicatorstr = `<ul class='ui-carousel-indicator-wrap'>
            <li x-repeat={{__indicators}} index='idx' class={{'ui-carousel-indicator' + (__isCurr ? ' ui-carousel-indicator-active' : '')}} 
              style='transition-duration:${this.__interval}ms;' 
              e-click='__goto' />
          </ul>`;
        }
        this.setExcludeProps(['autoplay', 'ani-type', 'init-index', 'arrow-type', 'show-indicator', 'onchange']);
        return `
      <div class='ui-carousel'
          e-mouseenter='__handleMouseEnter'
          e-mouseleave='__handleMouseLeave'>
          <div class='ui-carousel-wrap'>
            <slot />
          </div>
          ${__indicatorstr}
          <div x-show={{arrowType !== 'never' && __count}} ${arrowType === 'hover' ? ' class="ui-carousel-arrow-hover"' : ''}>
            <div class='ui-carousel-arrow-left' e-click='__prev'>
              <b class='ui-icon-arrow-left'></b>
            </div>
            <div class='ui-carousel-arrow-right' e-click='__next'>
              <b class='ui-icon-arrow-right'></b>
            </div>
          </div>
      </div>
    `;
    }
    /**
     * 播放下一张图片
     * @param next - 下一数据
     */
    __go(next) {
        if (this.__items.length === 0 || this.__currentIndex === next || this.__animating) {
            return;
        }
        this.__moved = true;
        if (this.__onChange) {
            UITool.invokePropMethod(this, this.__onChange, this.__currentIndex, next);
        }
        //设置初始currentindex和sliding标志
        if (this.__currentIndex === -1) {
            this.__currentIndex = 0;
            this.__animating = false;
        }
        else {
            this.__animating = true;
        }
        if (this.__animation === 'scroll') {
            this.__doScroll(next);
        }
        else {
            this.__doFade(next);
        }
        const c = next % this.__items.length;
        //设置indicator选中状态
        for (let i = 0; i < this.__items.length; i++) {
            this.model['__indicators'][i].__isCurr = i === c;
        }
        //设置当前索引  
        this.__currentIndex = (next + this.__items.length) % this.__items.length;
    }
    /**
     * 执行scroll
     * @param next - 下一数据
     */
    __doScroll(next) {
        //计算位置
        if (next > this.__currentIndex) { //右滑动
            for (let i = this.__currentIndex; i <= next; i++) {
                const loc1 = 'transform:translateX(' + -(this.__currentIndex - i) * 100 + '%)';
                const loc2 = 'transform:translateX(' + -((next - i) * 100) + '%)';
                this.__items[i % this.__items.length].__setLoc(i === next, loc1, loc2);
            }
        }
        else { //左滑动
            for (let i = this.__currentIndex; i >= next; i--) {
                const loc1 = 'transform:translateX(' + -(this.__currentIndex - i) * 100 + '%)';
                const loc2 = 'transform:translateX(' + -((next - i) * 100) + '%)';
                this.__items[(i + this.__items.length) % this.__items.length].__setLoc(i === next, loc1, loc2);
            }
        }
    }
    /**
     * 执行fade
     * @param next - 下一数据
     */
    __doFade(next) {
        const index1 = (this.__currentIndex + this.__items.length) % this.__items.length;
        const index2 = (next + this.__items.length) % this.__items.length;
        //避免最开始的状态 opacity设置为0
        if (index1 !== index2) {
            this.__items[index1].__setLoc(false, 'opacity:1', 'opacity:0');
        }
        this.__items[index2].__setLoc(true, 'opacity:0', 'opacity:1');
    }
    /**
     * 到指定页
     * @param model -模型
     */
    __goto(model) {
        this.__go(model.idx);
    }
    /**
     * 右按钮事件
     */
    __next() {
        this.__go(this.__currentIndex + 1);
    }
    /**
     * 左按钮事件
     */
    __prev() {
        this.__go(this.__currentIndex - 1);
    }
    /**
     * 结束自动播放
     */
    __stop() {
        if (this.__timer) {
            clearInterval(this.__timer);
            delete this.__timer;
        }
    }
    /**
     * 自动播放
     */
    __play() {
        this.__timer = setInterval(() => {
            this.__next();
        }, this.__interval);
    }
    /**
     * 鼠标进入，autoplay时有效
     */
    __handleMouseEnter() {
        if ((this.__autoplay)) {
            this.__stop();
        }
    }
    /**
     * 鼠标离开，autoplay时有效
     */
    __handleMouseLeave() {
        if (this.__autoplay) {
            this.__play();
        }
    }
    /**
     * 添加item
     * @param item - 需被添加的item
     */
    __addItem(item) {
        if (!this.__items.includes(item)) {
            this.__items.push(item);
            this.model['__indicators'].push({ isCurr: false });
        }
        this.model['__count'] = this.__items.length;
    }
    /**
     * 渲染前事件
     * @privateRemarks
     */
    onBeforeRender() {
        if (!this.__moved && this.__items.length > this.__initIndex) {
            this.__go(this.__initIndex);
        }
    }
    /**
     * 挂载后事件
     * @privateRemarks
     */
    onMount() {
        if (this.__autoplay) {
            this.__play();
        }
    }
}
/**
 * UICarouselItem 轮播图个体
 * @public
 */
class UICarouselItem extends Module {
    /**
     * 模板函数
     * @privateRemarks
     */
    template() {
        const pmodule = ModuleFactory.get(this.srcDom.slotModuleId);
        pmodule.__addItem(this);
        return `
      <div class={{'ui-carousel-item' + (__isCurr?' ui-carousel-item-active':'') + (__isAnimation?' ui-carousel-item-animation':'')}}
        style={{__loc + ';transition-duration:${pmodule.__interval}ms'}} 
        e-transitionend='__animationEnd'>
        <slot/>
      </div>`;
    }
    /**
     * 设置位置
     * @param flag -  是否为当前节点
     * @param loc1 -  初始位置
     * @param loc2 -  结束位置
     */
    __setLoc(flag, loc1, loc2) {
        this.model['__isCurr'] = flag;
        this.model['__isAnimation'] = true;
        this.model['__loc'] = loc1;
        this.__toLoc = loc2;
    }
    /**
     * 动画结束
     * @param model -  模型
     */
    __animationEnd(model) {
        //关闭动画
        model.__isAnimation = false;
        //设置父模块的滑动标识
        ModuleFactory.get(this.srcDom.slotModuleId).__animating = false;
    }
    /**
     * 更新事件
     * @privateRemarks
     */
    onUpdate() {
        // 需要延迟，因为此时尚在渲染流程中，无法添加到渲染队列进行下次渲染
        setTimeout(() => {
            if (this.__toLoc) {
                this.model['__loc'] = this.__toLoc;
                delete this.__toLoc;
            }
        }, 0);
    }
    /**
     * 挂载事件
     * @privateRemarks
     */
    onMount() {
        // 需要延迟，因为此时尚在渲染流程中，无法添加到渲染队列进行下次渲染
        setTimeout(() => {
            if (this.__toLoc) {
                this.model['__loc'] = this.__toLoc;
                delete this.__toLoc;
            }
        }, 0);
    }
}
Nodom.registModule(UICarousel, 'ui-carousel');
Nodom.registModule(UICarouselItem, 'ui-carousel-item');

/**
 * 参数说明
 * percent:       当前进度
 * format:        内容模板函数 `(percent) => text | HtmlElementString`
 * hide-text:     是否隐藏文本
 * type:          类型 line | circle
 * size:          尺寸 small | normal | large
 * stroke-color:  已完成进度条色
 * trail-color:   未完成进度条色
 */
/**
 * UIProgress 进度条
 * @public
 */
class UIProgress extends Module {
    /**
     * 模板函数
     * @privateRemarks
     */
    template(props) {
        this.__strokeColor = props['stroke-color'];
        this.__trailColor = props['trail-color'];
        let percent = props['percent'] || 0;
        const size = props['size'] || 'small';
        const showInfo = !props.hasOwnProperty('hide-text');
        const format = props['format'] || (() => "{{percent}}%");
        this.__type = props['type'] || 'line';
        //percent必须保持在0-100之间
        if (percent < 0) {
            percent = 0;
        }
        else if (percent > 100) {
            percent = 100;
        }
        const progress = this.__type === 'line' ?
            `<div class='ui-progress-bar'>
        <div class='ui-progress-outer'
          style={{'${this.__trailColor ? 'background-color: ' + this.__trailColor + ';' : ''}'}}>
          <div class='ui-progress-inner'
            style={{'width:' +  percent + '%;${this.__strokeColor ? 'background-color: ' + this.__strokeColor + ';' : ''}'}}></div>
        </div>
      </div>` : `<div class='ui-progress-round'>
        <div class='ui-progress-track'>
          <div class='ui-progress-wrapper'>
            <div class='ui-progress-left' style={{__getCircleStyle('left')}}/>
          </div>
          <div class='ui-progress-wrapper'>
            <div class='ui-progress-right' style={{__getCircleStyle('right')}}/>
          </div>
        </div>
      </div>`;
        //显示文本
        const progressInfo = showInfo ? `<div class='ui-progress-text'>${format()}</div>` : '';
        this.model['percent'] = percent;
        this.setExcludeProps(['type', 'percent', 'format', 'hide-text', 'size', 'stroke-color', 'trail-color']);
        return `<div
          class='ui-progress ui-progress-${size} ui-progress-${this.__type}'
          role='progressbar'
          aria-valuenow='{{percent}}'
          aria-valuemin='0'
          aria-valuemax='100'
        >
          ${progress}
          ${progressInfo}
      </div>`;
    }
    /**
     * 获取圆圈样式
     * @param part -  左半或右办
     * @returns style 样式内容
     */
    __getCircleStyle(part) {
        const perc = this.model['percent'];
        let style = '';
        if (part === 'right' && perc <= 50) {
            style = `transform: rotate(${perc * 3.6 + 45}deg);`;
        }
        else if (part === 'left' && perc <= 50) {
            style = 'transform: rotate(-45deg);';
        }
        else if (part === 'right' && perc > 50) {
            style = 'transform: rotate(225deg);';
        }
        else {
            style = `transform: rotate(${(perc - 50) * 3.6 - 45}deg);`;
        }
        if (this.__strokeColor) {
            style += 'border-bottom-color: ' + this.__strokeColor + ';' + 'border-' + (part === 'left' ? 'right' : 'left') + '-color: ' + this.__strokeColor + ';';
        }
        if (this.__trailColor) {
            style += 'border-top-color: ' + this.__trailColor + ';' + 'border-' + part + '-color: ' + this.__trailColor + ';';
        }
        return style;
    }
}
//注册模块
Nodom.registModule(UIProgress, 'ui-progress');

/******************************************************************************
Copyright (c) Microsoft Corporation.

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
PERFORMANCE OF THIS SOFTWARE.
***************************************************************************** */

function __rest(s, e) {
  var t = {};
  for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
      t[p] = s[p];
  if (s != null && typeof Object.getOwnPropertySymbols === "function")
      for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
          if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
              t[p[i]] = s[p[i]];
      }
  return t;
}

typeof SuppressedError === "function" ? SuppressedError : function (error, suppressed, message) {
  var e = new Error(message);
  return e.name = "SuppressedError", e.error = error, e.suppressed = suppressed, e;
};

const formatBlock = 'formatBlock';
const queryCommandState = command => document.queryCommandState(command);
const queryCommandValue = command => document.queryCommandValue(command);
const exec = (command, value = null) => document.execCommand(command, false, value);
/**
 * 参数说明
 * separator              文本默认分隔符
 * configs                功能配置, 控制富文本组件的功能  `Array<string | config>`
 * onChange               内容改变时的回调函数 `(html) => void`
 * class
 * style
 */
/**
 * UIRichEditor 富文本
 * @alpha
 */
class UIRichEditor extends Module {
    data() {
        return {
            configArray: null,
        };
    }
    /**
     * 模板函数
     * @privateRemarks
     */
    template(props) {
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        const { configs, class: ca } = props, others = __rest(props, ["configs", "class"]);
        const defaultConfig = {
            bold: {
                icon: '<b>B</b>',
                title: '粗体',
                state: () => queryCommandState('bold'),
                result: () => exec('bold')
            },
            italic: {
                icon: '<i>I</i>',
                title: '斜体',
                state: () => queryCommandState('italic'),
                result: () => exec('italic')
            },
            underline: {
                icon: '<u>U</u>',
                title: '下划线',
                state: () => queryCommandState('underline'),
                result: () => exec('underline')
            },
            strikethrough: {
                icon: '<strike>S</strike>',
                title: '删除线',
                state: () => queryCommandState('strikeThrough'),
                result: () => exec('strikeThrough')
            },
            heading1: {
                icon: '<b>H<sub>1</sub></b>',
                title: '一级标题',
                result: () => exec(formatBlock, '<h1>')
            },
            heading2: {
                icon: '<b>H<sub>2</sub></b>',
                title: '二级标题',
                result: () => exec(formatBlock, '<h2>')
            },
            heading3: {
                icon: '<b>H<sub>3</sub></b>',
                title: '三级标题',
                result: () => exec(formatBlock, '<h3>')
            },
            paragraph: {
                icon: 'P',
                title: '段落',
                result: () => exec(formatBlock, '<p>')
            },
            quote: {
                icon: '“ ”',
                title: '引用',
                result: () => exec(formatBlock, '<blockquote>')
            },
            olist: {
                icon: '#',
                title: '有序列表',
                result: () => exec('insertOrderedList')
            },
            ulist: {
                icon: '<b>·</b>',
                title: '无序列表',
                result: () => exec('insertUnorderedList')
            },
            code: {
                icon: '代码',
                title: '代码块',
                result: () => exec(formatBlock, '<pre>')
            },
            line: {
                icon: '<b>—</b>',
                title: '分割线',
                result: () => exec('insertHorizontalRule')
            },
            link: {
                icon: '🔗',
                title: '插入链接',
                result: () => {
                    const url = window.prompt('请输入链接的URL');
                    if (url)
                        exec('createLink', url);
                }
            },
            image: {
                icon: '📷',
                title: '插入图片',
                result: () => {
                    const url = window.prompt('请输入图片的URL');
                    if (url)
                        exec('insertImage', url);
                }
            }
        };
        if (this.model['configArray'] === null) {
            this.model['configArray'] = configs ? configs.map(config => {
                if (typeof config === 'string') {
                    return Object.assign(Object.assign({}, defaultConfig[config]), { select: false });
                }
                else if (defaultConfig[config.name]) {
                    return Object.assign(Object.assign(Object.assign({}, defaultConfig[config.name]), config), { select: false });
                }
                else {
                    return Object.assign(Object.assign({}, config), { select: false });
                }
            }) : Object.keys(defaultConfig).map(config => (Object.assign(Object.assign({}, defaultConfig[config]), { select: false })));
        }
        const renderToolBar = () => {
            return this.model['configArray'].map((config, index) => `<button
          type='button'
          class={{'ui-rich-editor-button${config.select ? " ui-rich-editor-button-selected" : ""}'}}
          title='${config.title}'
          id='${index}'
          e-click='__handleBarItemClick'
        >${config.icon}</button>`).join(' ');
        };
        this.setExcludeProps(['separator', 'configs', 'onChange']);
        return `<div
      class='ui-rich-editor ${ca || ""}'
      ${UITool.toAttrString(others)}
    >
      <div class='ui-rich-editor-toolbar'>
        ${renderToolBar()}
      </div>
      <div
        class='ui-rich-editor-content'
        contenteditable='true'
        e-input='__handleContentInput'
        e-keydown='__handleContentKeydown'
        e-keyup='__handleContentUp'
        e-mouseup='__handleContentUp'
      ></div>
    </div>`;
    }
    /**
     * 处理输入
     * @param model - 模型
     * @param dom - 当前节点
     * @param evObj - event object
     * @param e - html event
     */
    __handleContentInput(model, dom, evObj, e) {
        const separator = this.props['separator'] || 'div';
        const onChange = this.props['onchange'];
        const contentDom = e.target;
        const firstChild = e.target.firstChild;
        if (firstChild && firstChild.nodeType === 3)
            exec(formatBlock, `<${separator}>`);
        else if (contentDom.innerHTML === '<br>')
            contentDom.innerHTML = '';
        onChange && onChange(contentDom.innerHTML);
    }
    /**
     * 当按下enter
     * @param model - 模型
     * @param dom - 当前节点
     * @param evObj - event object
     * @param e - html event
     */
    __handleContentKeydown(model, dom, evObj, e) {
        const separator = this.props['separator'] || 'div';
        if (e.key === 'Enter' && queryCommandValue(formatBlock) === 'blockquote') {
            setTimeout(() => exec(formatBlock, `<${separator}>`), 0);
        }
    }
    /**
     * 点击
     * @param model - 模型
     * @param dom - 当前节点
     * @param evObj - event object
     * @param e - html event
     */
    __handleBarItemClick(model, dom, evObj, e) {
        const config = this.model['configArray'][dom.props.id];
        config.result() && e.target.parentNode.parentNode.children[1].focus();
        if (config.state) {
            config.select = config.state();
        }
    }
    /**
     * 设置
     */
    __handleContentUp() {
        for (const config of this.model['configArray']) {
            if (config.state) {
                config.select = config.state();
            }
        }
    }
}
Nodom.registModule(UIRichEditor, 'ui-rich-editor');

/**
 * 参数说明
 * animated:       是否开启动画
 * loading:        是否处于加载状态，false 时展示子组件
 * avater:         是否显示头像
 * title:          是否显示标题
 * text:           是否显示段落
 * row:            段落数量
 * class
 * style
 */
/**
 * UISkeleton 骨架屏
 * @alpha
 */
class UISkeleton extends Module {
    /**
     * 模板函数
     * @privateRemarks
     */
    template(props) {
        const animated = props.hasOwnProperty('animated') || false;
        const title = props.hasOwnProperty('title') || true;
        const avater = props.hasOwnProperty('avater') || false;
        const text = props.hasOwnProperty('text') || true;
        const rows = props['rows'] ? parseInt(props['rows'] + '') : 3;
        const rightContent = [];
        const leftContent = [];
        if (avater) {
            leftContent.push(`<ui-skeleton-item type='circle' />`);
        }
        if (title) {
            rightContent.push(`<ui-skeleton-item type='title' />`);
        }
        if (text) {
            for (let i = 0; i < rows - 1; i++) {
                rightContent.push(`<ui-skeleton-item type='text' />`);
            }
            rightContent.push(`<ui-skeleton-item type='text' style='width: 65%' />`);
        }
        this.model['loading'] = props['loading'];
        this.setExcludeProps(['loading', 'animated', 'avater', 'title', 'text', 'row']);
        return `
          <div>
            <div ${animated ? "class='ui-skeleton-animated'" : ''} x-if={{loading}}>
              <slot name='skeleton'>
                <div style='display: flex'>
                  <div>
                    ${leftContent.join(' ')}
                  </div>
                  <div style='margin-left: 12px; flex: 1'>
                    ${rightContent.join(' ')}
                  </div>
                </div>
              </slot>
            </div>
            <div x-else>
              <slot />
            </div>
          </div>    
        `;
    }
}
/**
 * 参数说明
 * type:  'title' | 'circle' | 'rect'  | 'text' | 'img';
 * class
 * style
 */
/**
 * UISkeletonItem 骨架屏子组件
 * @alpha
 */
class UISkeletonItem extends Module {
    /**
     * 模板函数
     * @privateRemarks
     */
    template(props) {
        const type = props['type'] || 'text';
        this.setExcludeProps(['type']);
        return `<span class='ui-skeleton-base ui-skeleton-${type}'></span>`;
    }
}
Nodom.registModule(UISkeleton, 'ui-skeleton');
Nodom.registModule(UISkeletonItem, 'ui-skeleton-item');

/**
 *  UISlider 滑动条
 *  @public
 */
class UISlider extends BaseInput {
    /**
     *
     * @param props - 传递进来的参数，具体见顶部
     * @returns 模板字符串，根据此进行渲染
     */
    template(props) {
        super.template(props);
        this.__max = props['max'] || 100;
        this.__min = props['min'] || 0;
        this.__vertical = props.hasOwnProperty('vertical');
        this.__disabled = props.hasOwnProperty('disabled');
        this.__step = props['step'] || 1;
        // 是否显示tick
        this.__showStep = props['show-step'] || false;
        // 根据是否设置了step来判断是否生成tick
        this.__hasTick = props.hasOwnProperty('step') || false;
        // 竖直情况下，获得滑动条高度
        if (this.__vertical) {
            this.__height = props['height'];
        }
        this.model['showStep'] = this.__showStep;
        //设置class
        let className = 'ui-slider-normol ui-slider-' + (this.__vertical ? 'vertical' : 'normal');
        className += ' ' + (this.__disabled ? 'ui-slider-disabled' : '');
        let ticks = '';
        if (this.__hasTick) {
            ticks += `<div class='ui-slider-ticks'>`;
            const length = this.__max - this.__min;
            let stepValue = 0;
            for (let i = this.__step; i < length; i += this.__step) {
                stepValue = i / length * 100;
                if (this.__vertical) {
                    ticks += `<div class='ui-slider-tick' style="bottom: ${stepValue}%;"/>`;
                }
                else {
                    ticks += `<div class='ui-slider-tick'  style="left: ${stepValue}%;"/>`;
                }
            }
            ticks += `</div>`;
        }
        this.setExcludeProps(['height', 'max', 'min', 'vertical', 'disabled', 'step', 'show-step']);
        return `
                <div class='${className}' style='${this.__vertical ? 'height:' + this.__height + 'px;' : ''}' e-mousedown='__onSliderDown'>
                    <div class='ui-slider-bar' style={{(this.__vertical ? 'height:' : 'width:') +this.__currentPosition() + "%;" ;}}/>
                    ${ticks}
                    <div class='ui-slider-dot' e-mousedown='__dragStart' style={{(this.__vertical ? 'bottom:' : 'left:')  + this.__currentPosition() + "%;"}}/>
                </div>
        `;
    }
    /**
     * 初始化参数对象
     */
    onInit() {
        this.__initData = {
            startPos: {
                x: 0,
                y: 0
            },
            currentPos: {
                x: 0,
                y: 0
            },
            newPosition: 0,
            startPosition: 0,
            highWayLength: 0
        };
    }
    /**
     * 挂载后钩子函数，只有挂载后才能获取对应ELement以及相关属性，此处完成slider的初始化
     */
    onMount() {
        //获取元素及初始化slider
        this.__pel = this.domManager.renderedTree.node;
        this.__bel = this.domManager.renderedTree.children[0].node;
        // 是否显示ticks关乎到获取element的次序，也许querySelector是更好的选择。
        if (this.__hasTick) {
            this.__ticks = this.domManager.renderedTree.children[1].node;
            this.__el = this.domManager.renderedTree.children[2].node;
        }
        else {
            this.__el = this.domManager.renderedTree.children[1].node;
        }
        this.__initSlider();
    }
    /**
     * 渲染前钩子函数，此处设置对showStep属性的监听
     */
    onBeforeRender() {
        //监听showStep
        this.watch('showStep', () => {
            this.__updateTicks();
        });
    }
    /**
     * model数据
     * @returns showStep - 设置showStep以便设置监听函数
     */
    data() {
        return {
            showStep: false,
        };
    }
    /**
     * 初始化绑定的field，若不存在则为0
     */
    __initValue() {
        if (isNaN(this.model['__value']))
            this.model['__value'] = 0;
    }
    /**
     * 获取初始化slider所需参数
     */
    __initSlider() {
        // 实际value
        this.__initData.newPosition = this.__currentPosition();
        // 获取轨道长度
        this.__initData.highWayLength = this.__pel[`client${this.__vertical ? 'Height' : 'Width'}`];
        this.__setPosition(this.__initData.newPosition);
    }
    /**
     * 计算目标位置
     * @returns 目标位置百分比
     */
    __currentPosition() {
        return ((this.model['__value'] - this.__min) / (this.__max - this.__min)) * 100;
    }
    /**
     * 拖拽事件
     * @param model - 当前节点对应model
     * @param dom - virtual dom节点
     * @param evObj - NEvent对象
     * @param e - event对象
     */
    __dragStart(model, dom, evObj, e) {
        e.preventDefault();
        //获取鼠标点击位置
        this.__initData.startPos = UITool.returnClientXY(e);
        // 获取滑块初始位置
        this.__initData.startPosition = this.__currentPosition();
        this.__initData.newPosition = this.__initData.startPosition;
        this.__dragging = true;
        this.__isClick = true;
        const that = this;
        const onDragging = (event) => {
            if (this.__disabled)
                return;
            if (that.__dragging) {
                that.__isClick = false;
                const initData = that.__initData;
                // 拖动过程中鼠标的位置
                initData.currentPos = UITool.returnClientXY(event);
                const { currentPos, startPos } = initData;
                // 计算被拖动的距离
                let diff;
                if (this.__vertical) {
                    diff = startPos.y - currentPos.y;
                }
                else {
                    diff = currentPos.x - startPos.x;
                }
                that.__initData.newPosition = initData.startPosition + ((diff / initData.highWayLength) * 100);
                // 计算出[min,max]范围的value，并移动滑块和滑块条
                that.__setPosition(that.__initData.newPosition);
            }
        };
        // 拖动结束，注销事件
        const onDragEnd = () => {
            if (that.__dragging) {
                that.__dragging = false;
                // 移除事件
                window.removeEventListener('mousemove', onDragging);
                window.removeEventListener('mousedown', onDragEnd);
            }
        };
        // 创建拖拽流程事件
        window.addEventListener('mousemove', onDragging);
        window.addEventListener('mouseup', onDragEnd);
    }
    /**
     * 点击轨道事件
     * @param model - 当前节点对应model
     * @param dom - virtual dom节点
     * @param evObj - NEvent对象
     * @param e - event对象
     */
    __onSliderDown(model, dom, evObj, e) {
        if (this.__disabled)
            return;
        // 获取点击后位置
        let newL = UITool.getDisplacement(e, this.__pel, this.__vertical);
        newL = (newL / this.__initData.highWayLength) * 100;
        // 点击轨道超出范围则取消
        if (newL < 0 || newL > 100)
            return;
        this.__setPosition(newL);
    }
    /**
     * 根据范围修改value，并决定每步步长(如设置step)
     * @param newL - 滑块拖拽后位置
     */
    __setPosition(newL) {
        if (newL === null || isNaN(newL))
            return;
        // 超出范围修正
        if (newL < 0)
            newL = 0;
        else if (newL > 100)
            newL = 100;
        // 实际步长
        const lengthPerStep = 100 / ((this.__max - this.__min) / this.__step);
        // 步数
        const steps = Math.round(newL / lengthPerStep);
        // 实际值
        const value = steps * lengthPerStep * (this.__max - this.__min) * 0.01 + this.__min;
        // 更新value
        this.model['__value'] = parseFloat(value.toPrecision(3));
        //更新ticks
        this.__updateTicks();
    }
    /**
     * 更新tick状态，如果要求显示，则会根据滑块所处位置，更新其之前的tick颜色
     */
    __updateTicks() {
        if (!this.__hasTick)
            return;
        const value = this.__currentPosition() / 10;
        const ticks = this.__ticks.childNodes;
        if (!this.__showStep) {
            for (let i = 0; i < ticks.length; i++) {
                ticks[i].className = 'ui-slider-tick';
            }
        }
        else {
            for (let i = 0; i < ticks.length; i++) {
                if (i < value) {
                    ticks[i].className = 'ui-slider-tick ui-slider-tick-active';
                }
                else {
                    ticks[i].className = 'ui-slider-tick';
                }
            }
        }
    }
}
//注册模块
Nodom.registModule(UISlider, 'ui-slider');

/**
 * 参数说明
 * size： 尺寸，normal,small,large
 * max： 最大评分值
 * readonly： 只读
 * allow-half：允许半选
 * touchable：支持滑动手势
 * color：自定义颜色
 */
/**
 * UIRating 评分
 * @beta
 */
class UIRating extends BaseInput {
    /**
     * 模板函数
     * @privateRemarks
     */
    template(props) {
        super.template(props);
        this.model['__starsArray'] = [];
        this.__size = props['size'] || 'normal';
        this.__max = props['max'] || 5;
        this.__color = props['color'];
        if (props.hasOwnProperty('readonly')) {
            this.__readonly = true;
        }
        if (props.hasOwnProperty('allow-half')) {
            this.__allowHalf = true;
        }
        if (props.hasOwnProperty('touchable')) {
            this.__touchable = true;
        }
        this.__pushStarsArr();
        //设置不渲染到attribute的属性
        this.setExcludeProps(['size', 'max', 'readonly', 'allow-half', 'touchable', 'color']);
        return `
          <div class='container'> 
              <div class='ui-rating'>
                  <span x-repeat={{__starsArray}} index="idx" ${this.__readonly ? '' : 'e-mousedown="__drag"'}  ${this.__touchable ? 'e-mousemove="__drag"' : ''}>
                    <input type='radio' class={{this.__renderStars(idx)}} style={{this.__changeColor(idx)}}/>
                  </span>
              </div>
              <span class='ui-rating-title'>
                  <slot/>
              </span>
          </div>
      `;
    }
    /**
     * 模型
     * @privateRemarks
     */
    data() {
        return {
            __starsArray: [],
        };
    }
    /**
     * 生成评分星星数组
     */
    __pushStarsArr() {
        let star = 1;
        if (this.__allowHalf)
            star = 0.5;
        for (let i = star; i <= this.__max; i += star) {
            this.model['__starsArray'].push({ i });
        }
    }
    /**
     * 渲染激活的评分星星
     * @param idx - 当前节点索引
     * @returns  激活的评分星星class
     */
    __renderStars(idx) {
        return `ui-rating-star ${this.model['__starsArray'][idx].i <= this.model['__value'] ? 'ui-rating-star-filled' : ''}  ${'ui-rating-' + this.__size} ${this.__allowHalf ? 'ui-rating-star-half-filled' : ''}`;
    }
    /**
     * 自定义颜色
     * @param idx -   当前节点索引
     * @returns  自定义颜色style
     */
    __changeColor(idx) {
        return `${this.model['__starsArray'][idx].i <= this.model['__value'] && this.__color ? `color: ${this.__color}` : ''}`;
    }
    /**
     * 鼠标点击
     * @param model - 当前节点对应model
     */
    __drag(model) {
        this.model['__value'] = model.i;
    }
}
Nodom.registModule(UIRating, 'ui-rating');

/**
 * drawer插件
 * 参数说明
 * title:           标题
 * closable:        是否显示close按钮
 * modal:           控制是否显示遮罩层
 * showconfirm:     控制是否显示确认关闭消息框
 * size:            控制抽屉的大小
 * direction:       控制抽屉弹出的方向
 * onInner          点击抽屉时事件
 * onOpen:          drawer打开后事件方法名
 * onClose:         drawer关闭后事件方法名
 * onBeforeOpen:    drawer打开前事件名，如果对应方法返回true，则不打开
 * onBeforeClose:   drawer关闭前事件名，如果对应方法返回true，则不关闭
 */
/**
 * UIDrawer 抽屉
 * @beta
 */
class UIDrawer extends Module {
    /**
     * 模板函数html
     * @privateRemarks
     */
    template(props) {
        this.__onInner = props['oninner'];
        this.__onOpen = props['onopen'];
        this.__onClose = props['onclose'];
        this.__onBeforeClose = props['onbeforeclose'];
        this.__onBeforeOpen = props['onbeforeopen'];
        let closeStr = '';
        if (props.hasOwnProperty('closable')) {
            closeStr = `<div class='ui-pannel-header-bar'>
                            <b class='ui-drawer-close' e-click='__close'></b>
                        </div>`;
        }
        const boxType = props['direction'] === 'left' || props['direction'] === 'right' ? 'horizontal' : 'vertical';
        const boxPosition = props['direction'] === 'left' || props['direction'] === 'right' ? `height:100%;${props['direction']}:0;` : `width:100%;${props['direction']}:0;`;
        const style = boxType === 'horizontal' ? `width:${props['size']};height:100%` : `height:${props['size']};width:100%`;
        const modal = props.hasOwnProperty('modal') ? 'display:none' : '';
        const showconfirm = props.hasOwnProperty('showconfirm') ? '__showConfirm' : '__close';
        this.setExcludeProps(['title', 'onopen', 'onclose', 'closable', 'direction', 'size', 'modal', 'showconfirm']);
        return `
            <div class={{'ui-drawer' + (__show?'':' ui-dialog-hide')}} e-click='${showconfirm}'>
                <div class={{'ui-mask' + (__show?'':' ui-mask-hide')}} style='${modal}'/>
                <div x-animationbox='${boxType}' e-transitionend='__checkClose' style='position:absolute;${boxPosition}' > 
                    <div class="ui-drawer-content" style='padding:0px;${style}'>
                        <div style='${style}'>
                            <div class='ui-panel' style='${style}' e-click="__inner:nopopo">
                                <div class='ui-panel-header'>
                                    <span class='ui-panel-title'>${props['title']}</span>
                                    ${closeStr}
                                </div>
                                <div class='ui-panel-bodyct'>
                                    <slot />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        `;
    }
    /**
     * 模型
     * @privateRemarks
     */
    data() {
        return {
            __show: false,
            __open: false,
        };
    }
    /**
     * 点击drawer外部时，弹出消息框询问是否关闭
     */
    __showConfirm() {
        nuiconfirm({
            text: '确定关闭？',
            icon: 'prompt',
            buttons: [
                {
                    text: '确定',
                    theme: 'active',
                    callback: () => {
                        if (this.__onBeforeClose && UITool.invokePropMethod(this, this.__onBeforeClose, this.model)) {
                            return;
                        }
                        this.model['__open'] = false;
                    }
                }
            ]
        });
    }
    /**
     * 点击drawer时触发
     */
    __inner() {
        if (this.__onInner) {
            UITool.invokePropMethod(this, this.__onInner, this.model);
        }
    }
    /**
     * 关闭drawer
     */
    __close() {
        if (this.__onBeforeClose && UITool.invokePropMethod(this, this.__onBeforeClose, this.model)) {
            return;
        }
        this.model['__open'] = false;
    }
    /**
     * 打开drawer
     */
    __open() {
        if (this.__onBeforeOpen && UITool.invokePropMethod(this, this.__onBeforeOpen, this.model)) {
            return;
        }
        this.model['__show'] = true;
        this.model['__open'] = true;
        if (this.__onOpen) {
            UITool.invokePropMethod(this, this.__onOpen, this.model);
        }
    }
    /**
     * 检查关闭
     * @param model - 当前节点对应model
     * @param dom - virtual dom节点
     * @param evObj - NEvent对象
     * @param e - event对象
     */
    __checkClose(model, dom, evObj, e) {
        if (e.target !== this.domManager.renderedTree.children[1].node) {
            return;
        }
        if (!this.model['__open']) {
            this.model['__show'] = false;
            if (this.__onClose) {
                UITool.invokePropMethod(this, this.__onClose, this.model);
            }
        }
    }
}
class UIDrawerBody extends UIPanelBody {
}
//注册模块
Nodom.registModule(UIDrawer, 'ui-drawer');
Nodom.registModule(UIDrawerBody, 'ui-drawer-body');

/**
 * 标记插件
 * 配置参数
 * value:       标记内容
 * max:         展示max+字样
 * dot:         显示小红点
 * active:      右上角标记带跳动效果
 * type:        标记背景颜色
 * icon         自定义标记图标，值参考图教程
 * color        自定义标记背景颜色
 */
/**
 * UIBadge 标记
 * @beta
 */
class UIBadge extends Module {
    /**
模板函数
     * @privateRemarks
     */
    template(props) {
        //基础样式
        const clsArr = ['ui-badge-sup'];
        this.__value = props['value'];
        //自定义颜色
        let selfColor = '';
        if (props.hasOwnProperty('dot')) {
            clsArr.push("dot");
        }
        if (props.hasOwnProperty("active")) {
            clsArr.push("active");
        }
        if (props['max']) {
            this.__value = `${props['max']}+`;
        }
        //如果传递过来的是图标，不设置背景
        if (props.hasOwnProperty("icon")) {
            this.__icon = true;
        }
        else {
            this.__icon = false;
            //如果传入了自定义颜色，则设置
            if (props['color']) {
                selfColor += `background-color:${props['color']};`;
            }
            else {
                //如果没有传color，看是否有type,没有就默认颜色
                clsArr.push(`ui-badge-sup-${props['type'] || 'error'}`);
            }
        }
        this.setExcludeProps(['value', 'type', 'dot', 'max', 'active', 'icon', 'color']);
        return `
            <div class="ui-badge">
                <slot/>
                <sup class='${clsArr.join(" ")}' style='${selfColor}' x-if={{!this.__icon}}>
                    ${this.__value || ''} 
                </sup>
                <sup class='${clsArr.join(" ")}' style='${selfColor}' x-else>
                   <b class="${props['icon']}">
                </sup>
            </div>
        `;
    }
}
Nodom.registModule(UIBadge, 'ui-badge');

/**
 * 数据项
 * $data            步骤数组数据
 * 配置参数
 * current          当前步骤位置
 * direction        步骤条方向 vertical/horizontal 默认horizontal
 * finish-status    完成时状态 active/success/warn/error
 * process-status   进行时状态
 * success-select   完成状态图标
 */
/**
 * UISteps 步骤条
 * @beta
 */
class UISteps extends Module {
    /**
     * 模板函数
     * @privateRemarks
     */
    template(props) {
        this.__finishColor = props['finish-status'] || '';
        this.__processColor = props['process-status'] || '';
        this.__direction = props.hasOwnProperty("direction") ? `${props['direction']}` : "horizontal";
        this.__useSelect = props.hasOwnProperty('success-select');
        /**
         * 设置步骤条方向
         */
        const stepsStyle = (this.__direction === "horizontal") ? 'ui-steps-horizontal' : 'ui-steps-vertical';
        this.setExcludeProps(['current', 'direction']);
        //设置当前步骤索引
        let cur = typeof props['current'] === 'string' ? parseInt(props['current']) : props['current'];
        this.__setCurrent(cur);
        return `
            <div class='ui-steps ${stepsStyle}'>
                <for cond={{data}}  class={{__genStepClass(__status)}} index='idx'>
                    <!-- 头部 -->
                    <div class="ui-step-head">
                        <!-- 节点 -->
                        <div class="ui-step-text" x-if={{!icon}}>
                            <div class={{'ui-step-inner ' + __checked}}>
                                {{__getIndex(__status,idx)}}
                            </div>
                        </div>
                        <div class="ui-step-icon" x-else>
                            <div class={{'ui-step-inner ' + icon}}>
                            </div>
                        </div>
                        <!-- 线条 -->
                        <div class="ui-step-line">
                            <b class="ui-step-line-inner" />
                        </div>
                    </div>
                    <!-- 内容区域 -->
                    <div class="ui-step-main">
                        <div class="ui-step-title" x-show={{title}}>
                            {{title}}
                        </div>
                        <div class="ui-step-description" x-show={{desc}}>
                            {{desc}}
                        </div>
                    </div>
                </for>
            </div>
        `;
    }
    /**
     * 初始化步骤节点状态
     * @param cur - 当前节点
     */
    __setCurrent(cur) {
        if (cur === this.__current) {
            return;
        }
        this.__current = cur;
        const array = this.model['data'];
        if (!Array.isArray(array)) {
            return;
        }
        for (let i = 0; i < array.length; i++) {
            const status = i < cur ? 1 : (i === cur ? 2 : 3);
            //为每个节点设置初始状态样式
            this.__setStatus(status, array[i]);
        }
    }
    /**
     * 设置每个步骤节点状态
     * @param status -  状态类型
     * @param node -    节点对象
     */
    __setStatus(status, node) {
        node.__status = status;
        //如果成功状态并且用户设置了成功的样式,则显示为"√"，否则默认显示数字
        if (status === 1 && this.__useSelect) {
            node.__checked = 'ui-icon-select';
        }
        //如果当前节点设置了样式为error，则显示为“x”
        else if (status === 2 && this.__processColor === 'error') {
            node.__checked = 'ui-icon-cross';
        }
        else {
            node.__checked = '';
        }
    }
    /**
     * 设置每个step节点样式
     * @param status -  状态值
     * @returns         class值
     */
    __genStepClass(status) {
        const clsArr = ['ui-step'];
        switch (status) {
            // 完成时状态
            case 1:
                clsArr.push(`ui-step-success ${this.__finishColor}`);
                break;
            //当前状态
            case 2:
                clsArr.push(`ui-step-process ${this.__processColor}`);
                break;
            //等待状态
            case 3:
                clsArr.push(`ui-step-wait`);
        }
        return clsArr.join(' ');
    }
    /**
     * 设置节点显示图标还是数字
     * @param status -  状态类型
     * @param idx -     索引
     * @returns         节点显示值
     */
    __getIndex(status, idx) {
        if ((status === 1 && this.__useSelect) || (status === 2 && this.__processColor === 'error')) {
            return '';
        }
        return idx + 1;
    }
}
//注册模块
Nodom.registModule(UISteps, 'ui-steps');

/**
 * 配置参数
 * $data         数据
 * reverse       是否需要反转
 * color-field         自定义颜色
 * size-field          自定义尺寸 四种：small、normal、big、hollow(空心圆)
 * icon-field          自定义图标
 * place-ment     时间戳位置 true:"top"/false:"bottom" 默认为bottom
 */
/**
 * UITimeLine 时间轴
 * @beta
 */
class UITimeLine extends BaseInput {
    /**
     * 模板函数
     * @privateRemarks
     */
    template(props) {
        super.template(props);
        this.__colorField = props['color-field'];
        this.__sizeField = props['size-field'];
        this.__iconField = props['icon-field'];
        this.__placeMent = props.hasOwnProperty("place-ment") ? true : false;
        this.__reverse = props.hasOwnProperty("reverse");
        const contentStr = props.hasOwnProperty("place-ment") ? `
        <div class="ui-itemcontent">
               <div class="ui-card-shadow">
                    <div class="ui-card-body">
                        <h4>{{ttitle}}</h4>
                        <p>{{tdesc}} {{tdate}} {{ttime}}</p>
                    </div>
               </div>
        </div>
    ` : `<div class="ui-itemcontent">
             {{ttitle}}
        </div>`;
        this.setExcludeProps(['color-field', 'size-field', 'icon-field', 'placeMent', 'reverse']);
        return `
        <div>
            <ui-radiogroup  field='__evalue' x-show={{this.__reverse}}>
                    <ui-radio value='1'title="正序"   e-click="__sortAsc"/>
                    <ui-radio value='-1' title="逆序" e-click="__sortDesc"/>
        </ui-radiogroup >
        </div>
        <div class="ui-timeline">
        <div class="ui-timeline-block" >
               <ul class="ui-timeline ui-timeline-reverse" x-repeat={{data}}>
                    <li class="ui-timeline-item">
                        <div class="ui-itemtail" ></div>
                        <b class={{__genClass(${this.__iconField},${this.__sizeField})}} style={{__genStyle(${this.__colorField})}}/>
                        <div class="ui-itemwrapper">
                            <div class="ui-itemtimestamp-top" x-show={{this.__placeMent}}>{{tdate}}</div>
                            ${contentStr}
                            <div class="ui-itemtimestamp-bottom" x-show={{!this.__placeMent}}>{{tdate}}</div>
                        </div>
                       
                    </li>
               </ul>

            </div>
        </div>
            
        `;
    }
    /**
     * 模型
     */
    data() {
        return {
            __evalue: 1,
            __flag: true
        };
    }
    /**
     * 更改颜色
     * @param color - 状态类型
     * @returns 自定义背景颜色样式
     */
    __genStyle(color) {
        return this.__colorField ? 'background-color:' + (color) : '';
    }
    /**
     * 自定义大小和图标
     * @param icon - 图标类型
     * @param size - 大小类型
     * @returns 自定义图标、大小样式
    */
    __genClass(icon, size) {
        if (icon) {
            return 'ui-icon ' + ('ui-icon-' + (icon));
        }
        else {
            return 'ui-itemnode ui-itemnode--' + (size ? size : 'normal');
        }
    }
    /**
     * 进行正序展示
     */
    __sortAsc() {
        if (!this.model['__flag']) {
            const row = this.model['data'];
            row.reverse();
            this.model['data'] = row;
            this.model['__flag'] = true;
        }
    }
    /**
     * 进行逆序展示
     */
    __sortDesc() {
        if (this.model['__flag']) {
            const row = this.model['data'];
            row.reverse();
            this.model['data'] = row;
            this.model['__flag'] = false;
        }
    }
}
//注册模块
Nodom.registModule(UITimeLine, 'ui-timeline');

/**
 * color工具类，作为转换颜色格式
 * @alpha
 */
class Color {
    constructor(options) {
        /**
         * 色相
         */
        this._hue = 0;
        /**
         * 饱和度
         */
        this._saturation = 100;
        /**
         * 亮度
         */
        this._value = 100;
        /**
         * 透明度
         */
        this._alpha = 100;
        this.enableAlpha = false;
        this.format = 'hex';
        this.value = '';
        //初始化
        for (const option in options) {
            this[option] = options[option];
        }
        if (options.value) {
            this.fromString(options.value);
        }
        else {
            this.doOnChange();
        }
    }
    set(prop, value) {
        if (arguments.length === 1 && typeof prop === 'object') {
            for (const p in prop) {
                this.set(p, prop[p]);
            }
            return;
        }
        this[`_${prop}`] = value;
        this.doOnChange();
    }
    get(propName) {
        if (propName === 'alpha') {
            return Math.floor(this[`_${propName}`]);
        }
        return this[`_${propName}`];
    }
    toRgb() {
        return hsv2rgb(this._hue, this._saturation, this._value);
    }
    fromString(value) {
        if (!value) {
            this._hue = 0;
            this._saturation = 100;
            this._value = 100;
            this.doOnChange();
            return;
        }
        const fromHSV = (h, s, v) => {
            this._hue = Math.max(0, Math.min(360, h));
            this._saturation = Math.max(0, Math.min(100, s));
            this._value = Math.max(0, Math.min(100, v));
            this.doOnChange();
        };
        if (value.includes('hsl')) {
            const parts = value
                .replace(/hsla|hsl|\(|\)/gm, '')
                .split(/\s|,/g)
                .filter((val) => val !== '')
                .map((val, index) => index > 2 ? Number.parseFloat(val) : Number.parseInt(val, 10));
            if (parts.length === 4) {
                this._alpha = parseFloat(parts[3].toString()) * 100;
            }
            else if (parts.length === 3) {
                this._alpha = 100;
            }
            if (parts.length >= 3) {
                const { h, s, v } = hsl2hsv(parts[0], parts[1], parts[2]);
                fromHSV(h, s, v);
            }
        }
        else if (value.includes('hsv')) {
            const parts = value
                .replace(/hsva|hsv|\(|\)/gm, '')
                .split(/\s|,/g)
                .filter((val) => val !== '')
                .map((val, index) => index > 2 ? Number.parseFloat(val) : Number.parseInt(val, 10));
            if (parts.length === 4) {
                this._alpha = parseFloat(parts[3].toString()) * 100;
            }
            else if (parts.length === 3) {
                this._alpha = 100;
            }
            if (parts.length >= 3) {
                fromHSV(parts[0], parts[1], parts[2]);
            }
        }
        else if (value.includes('rgb')) {
            const parts = value
                .replace(/rgba|rgb|\(|\)/gm, '')
                .split(/\s|,/g)
                .filter((val) => val !== '')
                .map((val, index) => index > 2 ? Number.parseFloat(val) : Number.parseInt(val, 10));
            if (parts.length === 4) {
                this._alpha = parseFloat(parts[3].toString()) * 100;
            }
            else if (parts.length === 3) {
                this._alpha = 100;
            }
            if (parts.length >= 3) {
                const { h, s, v } = rgb2hsv(parts[0], parts[1], parts[2]);
                fromHSV(h, s, v);
            }
        }
        else if (value.includes('#')) {
            const hex = value.replace('#', '').trim();
            if (!/^[0-9a-fA-F]{3}$|^[0-9a-fA-F]{6}$|^[0-9a-fA-F]{8}$/.test(hex))
                return;
            let r, g, b;
            if (hex.length === 3) {
                r = parseHexChannel(hex[0] + hex[0]);
                g = parseHexChannel(hex[1] + hex[1]);
                b = parseHexChannel(hex[2] + hex[2]);
            }
            else if (hex.length === 6 || hex.length === 8) {
                r = parseHexChannel(hex.slice(0, 2));
                g = parseHexChannel(hex.slice(2, 4));
                b = parseHexChannel(hex.slice(4, 6));
            }
            if (hex.length === 8) {
                this._alpha = (parseHexChannel(hex.slice(6)) / 255) * 100;
            }
            else if (hex.length === 3 || hex.length === 6) {
                this._alpha = 100;
            }
            const { h, s, v } = rgb2hsv(r, g, b);
            fromHSV(h, s, v);
        }
    }
    doOnChange() {
        const { _hue, _saturation, _value, _alpha, format } = this;
        if (this.enableAlpha) {
            switch (format) {
                case 'hsl': {
                    const hsl = hsv2hsl(_hue, _saturation / 100, _value / 100);
                    this.value = `hsla(${_hue}, ${Math.round(hsl[1] * 100)}%, ${Math.round(hsl[2] * 100)}%, ${this.get('alpha') / 100})`;
                    break;
                }
                case 'hsv': {
                    this.value = `hsva(${_hue}, ${Math.round(_saturation)}%, ${Math.round(_value)}%, ${this.get('alpha') / 100})`;
                    break;
                }
                case 'hex': {
                    this.value = `${toHex(hsv2rgb(_hue, _saturation, _value))}${hexOne((_alpha * 255) / 100)}`;
                    break;
                }
                default: {
                    const { r, g, b } = hsv2rgb(_hue, _saturation, _value);
                    this.value = `rgba(${r}, ${g}, ${b}, ${this.get('alpha') / 100})`;
                }
            }
        }
        else {
            switch (format) {
                case 'hsl': {
                    const hsl = hsv2hsl(_hue, _saturation / 100, _value / 100);
                    this.value = `hsl(${_hue}, ${Math.round(hsl[1] * 100)}%, ${Math.round(hsl[2] * 100)}%)`;
                    break;
                }
                case 'hsv': {
                    this.value = `hsv(${_hue}, ${Math.round(_saturation)}%, ${Math.round(_value)}%)`;
                    break;
                }
                case 'rgb': {
                    const { r, g, b } = hsv2rgb(_hue, _saturation, _value);
                    this.value = `rgb(${r}, ${g}, ${b})`;
                    break;
                }
                default: {
                    this.value = toHex(hsv2rgb(_hue, _saturation, _value));
                }
            }
        }
    }
    compare(color) {
        return (Math.abs(color._hue - this._hue) < 2 &&
            Math.abs(color._saturation - this._saturation) < 1 &&
            Math.abs(color._value - this._value) < 1 &&
            Math.abs(color._alpha - this._alpha) < 1);
    }
}
const hsv2hsl = function (hue, sat, val) {
    return [
        hue,
        (sat * val) / ((hue = (2 - sat) * val) < 1 ? hue : 2 - hue) || 0,
        hue / 2,
    ];
};
const isOnePointZero = function (n) {
    return typeof n === 'string' && n.includes('.') && Number.parseFloat(n) === 1;
};
const isPercentage = function (n) {
    return typeof n === 'string' && n.includes('%');
};
// Take input from [0, n] and return it as [0, 1]
const bound01 = function (value, max) {
    if (isOnePointZero(value))
        value = '100%';
    const processPercent = isPercentage(value);
    value = Math.min(max, Math.max(0, Number.parseFloat(`${value}`)));
    // Automatically convert percentage into number
    if (processPercent) {
        value = Number.parseInt(`${value * max}`, 10) / 100;
    }
    // Handle floating point rounding errors
    if (Math.abs(value - max) < 0.000001) {
        return 1;
    }
    // Convert into [0, 1] range if it isn't already
    return (value % max) / Number.parseFloat(max);
};
const INT_HEX_MAP = {
    10: 'A',
    11: 'B',
    12: 'C',
    13: 'D',
    14: 'E',
    15: 'F',
};
const hexOne = (value) => {
    value = Math.min(Math.round(value), 255);
    const high = Math.floor(value / 16);
    const low = value % 16;
    return `${INT_HEX_MAP[high] || high}${INT_HEX_MAP[low] || low}`;
};
const toHex = function ({ r, g, b }) {
    if (Number.isNaN(+r) || Number.isNaN(+g) || Number.isNaN(+b))
        return '';
    return `#${hexOne(r)}${hexOne(g)}${hexOne(b)}`;
};
const HEX_INT_MAP = {
    A: 10,
    B: 11,
    C: 12,
    D: 13,
    E: 14,
    F: 15,
};
const parseHexChannel = function (hex) {
    if (hex.length === 2) {
        return ((HEX_INT_MAP[hex[0].toUpperCase()] || +hex[0]) * 16 +
            (HEX_INT_MAP[hex[1].toUpperCase()] || +hex[1]));
    }
    return HEX_INT_MAP[hex[1].toUpperCase()] || +hex[1];
};
const hsl2hsv = function (hue, sat, light) {
    sat = sat / 100;
    light = light / 100;
    let smin = sat;
    const lmin = Math.max(light, 0.01);
    // let sv
    // let v
    light *= 2;
    sat *= light <= 1 ? light : 2 - light;
    smin *= lmin <= 1 ? lmin : 2 - lmin;
    const v = (light + sat) / 2;
    const sv = light === 0 ? (2 * smin) / (lmin + smin) : (2 * sat) / (light + sat);
    return {
        h: hue,
        s: sv * 100,
        v: v * 100,
    };
};
// `rgbToHsv`
// Converts an RGB color value to HSV
// *Assumes:* r, g, and b are contained in the set [0, 255] or [0, 1]
// *Returns:* { h, s, v } in [0,1]
const rgb2hsv = (r, g, b) => {
    r = bound01(r, 255);
    g = bound01(g, 255);
    b = bound01(b, 255);
    const max = Math.max(r, g, b);
    const min = Math.min(r, g, b);
    let h;
    const v = max;
    const d = max - min;
    const s = max === 0 ? 0 : d / max;
    if (max === min) {
        h = 0; // achromatic
    }
    else {
        switch (max) {
            case r: {
                h = (g - b) / d + (g < b ? 6 : 0);
                break;
            }
            case g: {
                h = (b - r) / d + 2;
                break;
            }
            case b: {
                h = (r - g) / d + 4;
                break;
            }
        }
        h /= 6;
    }
    return { h: h * 360, s: s * 100, v: v * 100 };
};
// `hsvToRgb`
// Converts an HSV color value to RGB.
// *Assumes:* h is contained in [0, 1] or [0, 360] and s and v are contained in [0, 1] or [0, 100]
// *Returns:* { r, g, b } in the set [0, 255]
const hsv2rgb = function (h, s, v) {
    h = bound01(h, 360) * 6;
    s = bound01(s, 100);
    v = bound01(v, 100);
    const i = Math.floor(h);
    const f = h - i;
    const p = v * (1 - s);
    const q = v * (1 - f * s);
    const t = v * (1 - (1 - f) * s);
    const mod = i % 6;
    const r = [v, q, p, p, t, v][mod];
    const g = [t, v, v, q, p, p][mod];
    const b = [p, p, t, v, v, q][mod];
    return {
        r: Math.round(r * 255),
        g: Math.round(g * 255),
        b: Math.round(b * 255),
    };
};

/**
 * UIColorPicker 颜色采集器
 * @alpha
 */
class UIColorPicker extends BaseInput {
    /**
     * 模板函数
     * @privateRemarks
     */
    template(props) {
        super.template(props);
        return `
            <div class="ui-color-picker">
                 <div class="ui-color-picker-dropdown ui-color-picker-wrapper">
                    <ui-sv-panel field="color"/>
                    <ui-hue-slider field="color"/>
                </div>
                <div class="ui-color-picker-input">
                    <ui-input field="__value" readonly allow-clear/>
                </div>
            </div>
        `;
    }
    /**
     * 模型
     * @privateRemarks
     */
    data() {
        return {
            //color类保存颜色相关属性，并计算
            color: new Color({ enableAlpha: this.__enableAlpha, format: '', value: this.__value }),
            value: ''
        };
    }
    /**
     * 第一次渲染前事件
     * @privateRemarks
     */
    onBeforeFirstRender() {
        super.onBeforeFirstRender();
        this.model['__value'] = this.model['color'].value;
    }
    /**
     * 当svPanel或 hueSlider改变时，调用此函数更新value
     */
    __valueUpdate() {
        this.model['__value'] = this.model['color'].value;
    }
}
/**
 * 颜色采集板
 */
class UISvPanel extends BaseInput {
    template(props) {
        super.template(props);
        const bgc = this.model['background'];
        return `
            <div class='ui-sv-panel' style={{'background-color:' + '${bgc}'}} e-mousedown='__dragStart' >
              <div class='ui-sv-panel-white' />
              <div class='ui-sv-panel-black' />
              <div class='ui-sv-panel-cursor' style={{__cursorStyle()}}></div>  
            </div>
        `;
    }
    data() {
        return {
            cursorTop: 0,
            cursorLeft: 0,
            background: 'hsl(0, 100%, 50%)',
        };
    }
    /**
     * 设置取色点位置
     */
    __cursorStyle() {
        return 'top:' + this.model['cursorTop'] + 'px;' + 'left:' + this.model['cursorLeft'] + 'px';
    }
    /**
     * 开始拖拽
     * @param model -     当前节点对应model
     * @param dom -       virtual dom节点
     * @param eobj -      NEvent对象
     * @param e -         event对象
     */
    __dragStart(model, dom, eobj, e) {
        e.preventDefault();
        let isDrag = true;
        document.addEventListener('mousemove', drag);
        document.addEventListener('mouseup', dragEnd);
        const that = this;
        /**
         * 拖拽
         * @param event - 事件对象
         */
        function drag(event) {
            if (isDrag) {
                handleEvent(event);
            }
        }
        /**
         * 拖拽结束或一次点击结束
         * @param event -
         */
        function dragEnd(event) {
            if (isDrag) {
                handleEvent(event);
                document.removeEventListener('mousemove', drag);
                document.removeEventListener('mouseup', dragEnd);
                isDrag = false;
            }
        }
        /**
         * 具体处理事件函数
         * @param event - 事件对象
         */
        function handleEvent(event) {
            //ui-sv-panel
            const el = that.domManager.renderedTree.node;
            const rect = el.getBoundingClientRect();
            const clientX = event.clientX;
            const clientY = event.clientY;
            //开始计算cursor的left和top
            let left = clientX - rect.left;
            let top = clientY - rect.top;
            left = Math.max(0, left);
            left = Math.min(left, rect.width);
            top = Math.max(0, top);
            top = Math.min(top, rect.height);
            //设置
            model.cursorTop = top;
            model.cursorLeft = left;
            //这里调用color类
            model.__value.set({
                saturation: (left / rect.width) * 100,
                value: 100 - (top / rect.height) * 100,
            });
            that.__update(false);
            //svPanel改变，更新colorPicker颜色值
            const cP = ModuleFactory.get(that.srcDom.slotModuleId);
            cP.invokeMethod('__valueUpdate');
        }
    }
    /**
     * 得到数值后更新
     * @param isSlider - 是否为slider调用
     */
    __update(isSlider) {
        //这里更新background-color等操作
        const color = this.model['__value'];
        if (!isSlider) {
            const saturation = color.get('saturation');
            const value = color.get('value');
            const el = this.domManager.renderedTree.node;
            const { clientWidth: width, clientHeight: height } = el;
            this.model['cursorLeft'] = (saturation * width) / 100;
            this.model['cursorTop'] = ((100 - value) * height) / 100;
        }
        this.model['background'] = `hsl(${color.get('hue')}, 100%, 50%)`;
    }
}
/**
 *  色相选择器
 */
class UIHueSlider extends BaseInput {
    template(props) {
        super.template(props);
        return `
            <div class="ui-hue-slider">
                <div class="ui-hue-slider-bar" e-mousedown='__clickBar'></div>
                <div class="ui-hue-slider-thumb" style={{__thumbStyle()}} e-mousedown='__dragStart'> </div>
            </div>
        `;
    }
    data() {
        return {
            thumbTop: 0,
        };
    }
    onMount() {
        this.__barEl = this.domManager.renderedTree.children[0].node;
        this.__El = this.domManager.renderedTree.children[1].node;
    }
    __thumbStyle() {
        return 'top:' + this.model['thumbTop'] + 'px';
    }
    /**
     * 点击Bar
     * @param model - 当前节点对应model
     * @param dom - virtual dom节点
     * @param evObj - NEvent对象
     * @param e - event对象
     */
    __clickBar(model, dom, evObj, e) {
        const target = e.target;
        if (target != this.__El) {
            this.__handleEvent(e);
        }
    }
    /**
     * 拖拽开始，获取鼠标位置及dot位置
     * @param model - 当前节点对应model
     * @param dom - virtual dom节点
     * @param evObj - NEvent对象
     * @param e - event对象
     */
    __dragStart(model, dom, evObj, e) {
        e.preventDefault();
        let isDrag = true;
        document.addEventListener('mousemove', drag);
        document.addEventListener('mouseup', dragEnd);
        const that = this;
        /**
         * 拖拽
         * @param event - 事件对象
         */
        function drag(event) {
            if (isDrag) {
                that.__handleEvent(event);
            }
        }
        /**
         * 拖拽结束或一次点击结束
         * @param event - 事件对象
         */
        function dragEnd(event) {
            if (isDrag) {
                that.__handleEvent(event);
                document.removeEventListener('mousemove', drag);
                document.removeEventListener('mouseup', dragEnd);
                isDrag = false;
            }
        }
    }
    /**
     * 处理事件
     * @param event - 事件对象
     */
    __handleEvent(event) {
        const el = this.domManager.renderedTree.node;
        const rect = el.getBoundingClientRect();
        const clientY = event.clientY;
        let top = clientY - rect.top;
        top = Math.min(top, rect.height - this.__El.offsetHeight / 2);
        top = Math.max(this.__El.offsetHeight / 2, top);
        const hue = Math.round(((top - this.__El.offsetHeight / 2) /
            (rect.height - this.__El.offsetHeight)) *
            360);
        const model = this.model;
        const color = model['__value'];
        color.set('hue', hue);
        model['thumbTop'] = top;
        //hue改变 设置svPanel背景
        const pmodule = ModuleFactory.get(this.srcDom.slotModuleId);
        const panel = pmodule.children[0];
        UITool.invokePropMethod(panel, '__update', true);
        //更新colorPicker颜色值
        UITool.invokePropMethod(pmodule, '__valueUpdate');
    }
    /**
     * 更新Thumb
     */
    getThumbTop() {
        const model = this.model;
        const color = model['__value'];
        const el = this.domManager.renderedTree.node;
        const hue = color.get('hue');
        model['thumbTop'] = Math.round((hue * (el.offsetHeight - this.__El.offsetHeight / 2)) / 360);
    }
}
Nodom.registModule(UIColorPicker, 'ui-color-picker');
Nodom.registModule(UISvPanel, 'ui-sv-panel');
Nodom.registModule(UIHueSlider, 'ui-hue-slider');

class UIAvatar extends Module {
    template(props) {
        this.__size = props["size"] || "normal";
        this.__shape = props["shape"] || "circle";
        // 图标字符串
        let btnStr = "";
        if (props["src"]) {
            this.__method = "image";
            this.__src = props["src"];
            this.__alt = props["alt"];
            btnStr = `<img src="${this.__src}" alt="${this.__alt}">`;
        }
        else if (props["icon"]) {
            this.__method = "icon";
            this.__icon = props["icon"] || "user";
            btnStr = `<b class='ui-icon-${this.__icon} ui-icon-size${this.__size}'/>`;
        }
        else {
            this.__method = "slot";
        }
        const clazz = `class = "ui-avatar ui-avatar-${this.__size} ui-avatar-${this.__shape}"`;
        //设置不渲染属性
        this.setExcludeProps(["__shape", "__src", "__alt", "__size", "__icon", "__method"]);
        return `
            <span ${clazz}>
                <div x-if={{this.__method == 'image'}}>
                    ${btnStr}
                </div>
                <div x-elseif={{this.__method == 'icon'}}>
                    ${btnStr}
                </div>
                <div v-elseif={{this.__method == 'slot'}}>
                  <slot />
                </div>
            </span>
        `;
    }
}
//注册模块
Nodom.registModule(UIAvatar, "ui-avatar");

class UIBreadcrumb extends Module {
    constructor() {
        super(...arguments);
        /**
         * 待显示的item
         */
        this.__items = [];
    }
    template(props) {
        // 分隔符
        this.__separator = props["separator"];
        // 图标分隔符
        this.__iconSeparator = props["icon-separator"];
        // 分隔符和图标分隔符都没有的话，优先为分隔符”/“
        if (!this.__separator && !this.__iconSeparator) {
            this.__separator = "/";
        }
        this.setExcludeProps(["separator"]);
        return `
            <div class='ui-breadcrumb'>
                <slot />
            </div>
        `;
    }
    /**
     * 添加item
     * @param item - 待加入的Breadcrumb Item
     */
    __addItem(item) {
        if (!this.__items.includes(item)) {
            const itemLength = this.__items.length;
            if (itemLength > 0) {
                this.__items[itemLength - 1].model["__needSeparator"] = true;
            }
            this.__items.push(item);
        }
    }
}
class UIBreadcrumbItem extends Module {
    template(props) {
        //设置父accordion
        this.__parent = ModuleFactory.get(this.srcDom.slotModuleId);
        //添加到父item集合
        this.__parent.__addItem(this);
        let separator = "";
        //分隔符和图标分隔符都有的话，优先为分隔符 其次是图标分隔符
        if (this.__parent.__separator) {
            separator = this.__parent.__separator;
        }
        else {
            separator = `<b class='ui-icon-${this.__parent.__iconSeparator} ui-icon-sizesmall'/>`;
        }
        if (props["title"]) {
            this.__title = props["title"];
        }
        this.__toPath = props["to"];
        this.setExcludeProps(["title", "to"]);
        let clazz = "class = '";
        if (this.__toPath || this.slots.size > 0) {
            clazz += "ui-breadcrumb-item-link'";
        }
        else {
            clazz += "ui-breadcrumb-item-unlink'";
        }
        return `
            <div class='ui-breadcrumb-item'>
                    <div ${clazz} e-click='__clickItem' x-if={{this.__title}}>
                        ${this.__title}
                    </div>
                    <div ${clazz} x-else>
                        <slot />
                    </div>
                    <div class="ui-breadcrumb-separator" x-if={{this.model.__needSeparator == true}}>
                        ${separator}
                    </div>
            </div>
        `;
    }
    /**
     * 点击事件	跳转路由
     */
    __clickItem() {
        if (this.__toPath) {
            Nodom["$Router"].go(this.__toPath);
        }
    }
}
Nodom.registModule(UIBreadcrumb, "ui-breadcrumb");
Nodom.registModule(UIBreadcrumbItem, "ui-breadcrumb-item");

class UIPopover extends Module {
    template(props) {
        //弹出位置默认为底部
        this.placement = props["placement"] || "bottom";
        const placementClazz = `data-placement="${this.placement}"`;
        //设置不渲染属性
        this.setExcludeProps(["placement", placementClazz]);
        return `
            <div class="ui-popover">
                <div class="ui-popover-trigger">
                    <slot name="trigger" />
                </div>
                <div class="ui-popover-content" ${placementClazz}>
                    <slot/>
                </div>
            </div>
        `;
    }
}
Nodom.registModule(UIPopover, "ui-popover");

class UICard extends Module {
    constructor() {
        super(...arguments);
        /**
         * 阴影
         */
        this.shadow = "always";
    }
    template(props) {
        this.width = props["width"] ? props["width"] : "";
        this.height = props["height"] ? props["height"] : "";
        let cardStyle = 'style="';
        if (this.width) {
            cardStyle += `width:${this.width};`;
        }
        if (this.height) {
            cardStyle += `height:${this.height};`;
        }
        cardStyle += '"';
        let bodyStyle = 'style="';
        if (props["body-style"]) {
            bodyStyle += props["body-style"];
        }
        bodyStyle += '"';
        if (props["shadow"]) {
            this.shadow = props["shadow"];
        }
        const clazz = `class="ui-card ${this.judgeShadow(this.shadow)}"`;
        //设置不渲染属性
        this.setExcludeProps(["width", "height", "shadow", "body-style"]);
        return `
            <div ${clazz} ${cardStyle}>
                <div class="ui-card-header" x-if={{this.slots.has("header")}}>
                    <slot name="header">{{ header }}</slot>
                </div>
                <div class="ui-card-body" ${bodyStyle}>
                    <slot />
                </div>
                <div class="ui-card-footer" x-if={{this.slots.has("footer")}}>
                    <slot name="footer">{{ footer }}</slot>
                </div>
            </div>
        `;
    }
    judgeShadow(shadow) {
        if (shadow === "always") {
            return "ui-card-always";
        }
        else if (shadow === "hover") {
            return "ui-card-hover";
        }
        else {
            return "";
        }
    }
}
//注册模块
Nodom.registModule(UICard, "ui-card");

export { Compiler, CssManager, DefineElement, DefineElementManager, DiffTool, Directive, DirectiveManager, DirectiveType, DomManager, EModuleState, EventFactory, Expression, GlobalCache, Model, ModelManager, Module, ModuleFactory, NCache, NError, NEvent, Nodom, NodomMessage, NodomMessage_en, NodomMessage_zh, NodomUI, Renderer, Route, Router, Scheduler, UIAccordion, UIAccordionItem, UIAvatar, UIBadge, UIBreadcrumb, UIBreadcrumbItem, UIButton, UIButtonGroup, UICard, UICarousel, UICarouselItem, UICheckbox, UIColorPicker, UIDate, UIDialog, UIDrawer, UIFile, UIForm, UIFormItem, UIGrid, UIGridCol, UIGridExpand, UIInput, UILayout, UILayoutBottom, UILayoutCenter, UILayoutLeft, UILayoutRight, UILayoutTop, UIList, UIListTransfer, UIMenu, UIMessageBox, UIMessageBoxManager, UIPagination, UIPanel, UIPanelBody, UIPopover, UIProgress, UIRadio, UIRadioGroup, UIRange, UIRating, UIRelationMap, UIRichEditor, UIRouterTab, UISelect, UISkeleton, UISkeletonItem, UISlider, UISteps, UISwitch, UITab, UITabItem, UITimeLine, UITip, UITipManager, UITool, UIToolbar, UITree, Util, VirtualDom, nuialert, nuiconfirm, nuiloading, nuiprompt, nuitip };
//# sourceMappingURL=nodomui.js.map
