import { BaseInput } from "./baseinput";
/**
 * file上传插件
 * 配置项
 * field:           对应数据项名
 * value-field:        值数据项名，对应上传后返回的valueField
 * display-field:    显示数据项名，对应上传后返回的displayField
 * multiple:        是否支持多个文件，设置则表示上传多个文件
 * url-field:       上传后返回的文件url对应的字段名
 * upload-name:     上传名，与服务器接收保持一致
 * upload-url:        上传url
 * delete-url:        删除url
 * max-count:        最大上传数量，multiple设置时有效
 * file-type:        上传资源类型，如果为image，上传成功后显示缩略图，displayField为url对应项，否则显示文件名，对应数据项为displayField。
 */
/**
 * UIFile 文件
 * @public
 */
export declare class UIFile extends BaseInput {
    /**
     * 上传字段名
     */
    private __uploadName;
    /**
     * 值数据项
     */
    private __valueField;
    /**
     * 显示数据项
     */
    private __displayField;
    /**
     * url字段名
     */
    private __urlField;
    /**
     * 是否允许上传多个
     */
    private __multiple;
    /**
     * 上传url
     */
    private __uploadUrl;
    /**
     * 删除url
     */
    private __deleteUrl;
    /**
     * 最大文件数
     */
    private __maxCount;
    /**
     * 文件类型
     * 如果为image，上传成功后显示缩略图，否则显示文件名，对应数据项为__displayField
     */
    private __fileType;
    /**
     * 每个显示项宽度
     */
    private __width;
    /**
     * 每个显示项高度
     */
    private __height;
    /**
     * 模板函数html
     * @privateRemarks
     */
    template(props: object): string;
    /**
     * 模型
     * @privateRemarks
     */
    data(): {
        __uploading: boolean;
        __uploadingText: string;
    };
    /**
     * 文件修改
     * @param model - 对应模型
     * @param dom - virtual dom节点
     */
    private __changeFile;
    /**
     * 删除上传文件
     * @param model - 对应模型
     */
    private __delete;
}
