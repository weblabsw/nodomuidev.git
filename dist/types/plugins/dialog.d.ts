import { Module } from 'nodom3';
/**
 * dialog插件
 * 参数说明
 * title:           标题
 * closable:        是否显示close按钮
 * height:          高度
 * width:           宽度
 * onOpen:          dialog打开后事件方法名
 * onClose:         dialog关闭后事件方法名
 * onBeforeOpen:    dialog打开前事件名，如果对应方法返回true，则不打开
 * onBeforeClose:   dialog关闭前事件名，如果对应方法返回true，则不关闭
 */
/**
 * Dialog 对话框
 * @public
 */
export declare class UIDialog extends Module {
    /**
     * 打开后事件方法名
     */
    private __onOpen;
    /**
     * 关闭后事件方法名
     */
    private __onClose;
    /**
     * 打开前事件方法名
     */
    private __onBeforeOpen;
    /**
     * 关闭前事件方法名
     */
    private __onBeforeClose;
    /**
     * 模板函数html
     * @privateRemarks
     */
    template(props?: object): string;
    /**
     * 模型
     * @privateRemarks
     */
    data(): {
        __show: boolean;
        __open: boolean;
    };
    /**
     * 关闭dialog
     */
    __close(): void;
    /**
     * 打开dialog
     */
    __open(): void;
    /**
     * 检查关闭
     * @param model - 当前节点对应model
     * @param dom - virtual dom节点
     * @param evObj - NEvent对象
     * @param e - event对象
     */
    __checkClose(model: any, dom: any, evObj: any, e: any): void;
}
