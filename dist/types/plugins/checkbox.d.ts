import { BaseInput } from "./baseinput";
/**
 * checkbox插件
 * 配置说明
 * field:       绑定父模块的字段
 * yes-value:   选中的值（可选），默认true
 * no-value:    不选中的值（可选），默认false
 * onChanged:    change事件方法
 */
/**
 * UICheckbox 复选框
 * @public
 */
export declare class UICheckbox extends BaseInput {
    /**
     * 选中时value，默认true
     */
    private __yesValue;
    /**
     * 未选中时value，默认false
     */
    private __noValue;
    /**
     * 选中时事件
     */
    private __onChanged;
    /**
     * 模板函数
     * @privateRemarks
     */
    template(props: any): string;
    /**
     * 点击事件
     * @param model - 当前节点对应model
     * @param dom - virtual dom节点
     * @param evObj - NEvent对象
     * @param e - event对象
     */
    __clickCheck(model: any, dom: any, evObj: any, e: any): void;
}
