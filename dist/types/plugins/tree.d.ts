import { BaseInput } from "./baseinput";
/**
 * 数据项
 * $data            树结构数据
 * 参数说明
 * field            如果树作为输入模式，需要设置对应字段，同时会显示复选框
 * display-field    数据项中用于显示的属性名
 * value-field      数据项中用于取值的属性名，field存在时，不为空
 * icons            树节点图标，依次为为非叶子节点关闭状态，打开状态，叶子节点，如果只有两个，则表示非叶子节点和叶子节点，如果1个，则表示非叶子节点
 * onItemClick      节点点击事件
 */
/**
 * 树形控件
 * @public
 */
export declare class UITree extends BaseInput {
    /**
     * 值字段名
     */
    private __valueField;
    /**
     * 显示字段名
     */
    private __displayField;
    /**
     * 节点图标
     */
    private __icons;
    /**
     * 节点点击事件名
     */
    private __onItemClick;
    /**
     * 节点展开事件
     */
    private __onExpand;
    /**
     * 节点闭合事件
     */
    private __onCollapse;
    /**
     * 模版函数
     * @privateRemarks
     */
    template(props?: object): string;
    /**
     * 创建选择框class
     * @param checked - 选中标识 true:选中  false:未选中
     * @returns         选择框class
     */
    private __genCheckCls;
    /**
     * 创建树左侧箭头class
     * @param model - 节点对应model
     * @returns         箭头(展开收拢)图标class
     */
    private __genArrowCls;
    /**
     * 显示文件夹图标
     * @param model - 节点对应model
     * @returns         文件夹图标class
     */
    private __genFolderCls;
    /**
     * 计算展开菜单高度
     * @param model - 节点对应model
     * @returns     容器高度
     */
    private __cacHeight;
    /**
     * 点击item事件
     * @param model - 当前节点对应model
     * @param dom - virtual dom节点
     * @param evObj - NEvent对象
     * @param e - event对象
     */
    private __clickItem;
    /**
     * 展开关闭节点
     * @param model - 当前节点对应model
     */
    private __expandClose;
    /**
     * checkbox 点击
     * @param model -     当前节点对应model
     */
    private __checkItem;
    /**
     * 首次渲染事件
     */
    protected __initValue(): void;
    /**
     * 补全整棵树选中值(state=1)
     */
    private __finishState;
    /**
     * 修改模型状态
     * @param model - 模型
     * @param state - 状态 0/1
     */
    private __changeState;
    /**
     * 更新整棵树
     */
    private __updateState;
}
