import { BaseInput } from "./baseinput";
import Color from "./color";
/**
 * UIColorPicker 颜色采集器
 * @alpha
 */
export declare class UIColorPicker extends BaseInput {
    /**
     * 是否启用透明度
     */
    private __enableAlpha;
    /**
     * 颜色默认值
     */
    private __value;
    /**
     * 模板函数
     * @privateRemarks
     */
    template(props: any): string;
    /**
     * 模型
     * @privateRemarks
     */
    data(): {
        color: Color;
        value: string;
    };
    /**
     * 第一次渲染前事件
     * @privateRemarks
     */
    onBeforeFirstRender(): void;
    /**
     * 当svPanel或 hueSlider改变时，调用此函数更新value
     */
    __valueUpdate(): void;
}
