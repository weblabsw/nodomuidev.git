import { Module } from "nodom3";
/**
 * 参数说明
 * autoplay:        是否自动切换，无值属性
 * interval:        切换时间，单位为ms，默认500
 * ani-type:        动画效果  'scroll' | 'fade'
 * init-index:      默认展示的索引
 * arrow-type:      箭头的显示方式  'always' | 'hover'
 * show-indicator:  是否展示指示器
 * onChange:        切换时的回调函数 `(oldIdx, newIdx) => void`
 */
/**
 * UICarousel 轮播图
 * @public
 */
export declare class UICarousel extends Module {
    /**
     * 定时器
     */
    __timer: unknown;
    /**
     * 当前索引号
     */
    __currentIndex: number;
    /**
     * items 集合
     */
    private __items;
    /**
     * 是否处于动画中
     */
    __animating: boolean;
    /**
     * 滑动间隔(ms)
     */
    __interval: number;
    /**
     * 自动滚屏
     */
    __autoplay: boolean;
    /**
     * 动画方式
     */
    __animation: string;
    /**
     * 初始位置
     */
    private __initIndex;
    /**
     * 是否移动（执行go）
     */
    private __moved;
    /**
     * change方法
     */
    private __onChange;
    /**
     * 模型
     * @privateRemarks
     */
    data(): {
        __count: number;
        __indicators: any[];
    };
    /**
     * 模板函数
     * @privateRemarks
     */
    template(props?: object): string;
    /**
     * 播放下一张图片
     * @param next - 下一数据
     */
    private __go;
    /**
     * 执行scroll
     * @param next - 下一数据
     */
    private __doScroll;
    /**
     * 执行fade
     * @param next - 下一数据
     */
    private __doFade;
    /**
     * 到指定页
     * @param model -模型
     */
    private __goto;
    /**
     * 右按钮事件
     */
    private __next;
    /**
     * 左按钮事件
     */
    private __prev;
    /**
     * 结束自动播放
     */
    private __stop;
    /**
     * 自动播放
     */
    private __play;
    /**
     * 鼠标进入，autoplay时有效
     */
    private __handleMouseEnter;
    /**
     * 鼠标离开，autoplay时有效
     */
    private __handleMouseLeave;
    /**
     * 添加item
     * @param item - 需被添加的item
     */
    __addItem(item: UICarouselItem): void;
    /**
     * 渲染前事件
     * @privateRemarks
     */
    onBeforeRender(): void;
    /**
     * 挂载后事件
     * @privateRemarks
     */
    onMount(): void;
}
/**
 * UICarouselItem 轮播图个体
 * @public
 */
export declare class UICarouselItem extends Module {
    private __toLoc;
    /**
     * 模板函数
     * @privateRemarks
     */
    template(): string;
    /**
     * 设置位置
     * @param flag -  是否为当前节点
     * @param loc1 -  初始位置
     * @param loc2 -  结束位置
     */
    __setLoc(flag: any, loc1: any, loc2: any): void;
    /**
     * 动画结束
     * @param model -  模型
     */
    private __animationEnd;
    /**
     * 更新事件
     * @privateRemarks
     */
    onUpdate(): void;
    /**
     * 挂载事件
     * @privateRemarks
     */
    onMount(): void;
}
