import { EventMethod, Module } from "nodom3";
/**
 * messagebox按钮类型
 * @public
 */
declare type UIMessageButton = {
    callback: EventMethod | string;
    text: string;
    methodName?: string;
    theme?: string;
};
/**
 * UIMessageBox 消息框
 * @public
 */
export declare class UIMessageBox extends Module {
    /**
     * 应用组件的按钮
     */
    __buttons: UIMessageButton[];
    /**
     * 模板函数
     * @privateRemarks
     */
    template(): string;
    /**
     * 模型
     * @privateRemarks
     */
    data(): {
        __show: boolean;
        __open: boolean;
        answer: string;
    };
    /**
     * 关闭窗口
     */
    __close(): void;
    /**
     * 检查关闭状态（transition结束后）
     * @param model - 当前节点对应model
     * @param dom - virtual dom节点
     * @param evObj - NEvent对象
     * @param e - event对象
     */
    __checkClose(model: any, dom: any, evObj: any, e: any): void;
}
/**
 * UIMessageBoxManager 消息框管理器
 * @public
 */
export declare class UIMessageBoxManager {
    /**
     * confirm 对话框
     * @param cfg - confirm 配置
     * @returns
     */
    static confirm(cfg: any): void;
    /**
     * alert 对话框
     * @param cfg - alert 配置
     * @returns
     */
    static alert(cfg: any): void;
    /**
     * prompt 对话框
     * @param cfg - prompt配置
     */
    static prompt(cfg: any): void;
}
/**
 * confirm创建函数
 * @param cfg - 配置项
 * @example
 * ```js
 *  {
 *      text:消息内容,
 *      icon:消息图标,
 *      buttons:[{
 *          text:按钮标题,
 *          callback:回调函数
 *      },]
 *  }
 * ```
 * @public
 */
export declare function nuiconfirm(cfg: any): void;
/**
 * alert 创建函数
 * @param cfg - 配置项
 * @example
 * ```js
 *  {
 *      text:消息内容,
 *      icon:消息图标
 *  }
 * ```
 * @public
 */
export declare function nuialert(cfg: any): void;
/**
 * confirm创建函数
 * @param cfg - 配置项
 * @example
 * ```json
 * {
 *      text:消息内容,
 *      answer:默认回答,
 *      icon:消息图标,
 *      buttons:[{
 *          text:按钮标题,
 *          callback:回调函数
 *      },...]
 *  }
 * ```
 * @public
 */
export declare function nuiprompt(cfg: any): void;
export {};
