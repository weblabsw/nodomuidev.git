import { BaseInput } from "./baseinput";
/**
 *  配置说明：
 *  field 绑定滑块百分比值，默认为0
 *  max 最大值，默认为100
 *  min 最小值，默认为0
 *  vertical 是否竖向
 *  height 滑动条高度，竖向模式必填
 *  step 滑动步数
 *  show-step 显示间断点
 */
/**
 *  UISlider 滑动条
 *  @public
 */
export declare class UISlider extends BaseInput {
    /**
     * 滑动条精确值最大值，可设定，默认100
     */
    private __max;
    /**
     * 滑动条精确值最小值，可设定，默认0
     */
    private __min;
    /**
     * 是否竖向模式
     */
    private __vertical;
    /**
     * 如果是竖向模式，则需要设定设定，默认'200px'
     */
    private __height;
    /**
     * 是否禁用滑动
     */
    private __disabled;
    /**
     * 滑动条每次移动间隔
     */
    private __step;
    /**
     * 是否显示步点
     */
    private __showStep;
    /**
     *  滑动条实际长度
     */
    private __currentLen;
    /**
     *  父element
     */
    private __pel;
    /**
     * 滑块的element
     */
    private __el;
    /**
     *  轨道的element
     */
    private __bel;
    /**
     * 是否拖拽
     */
    private __isDrag;
    /**
     * 模板函数
     * @privateRemarks
     */
    template(props?: object): string;
    /**
     * 渲染前
     */
    onBeforeRender(): void;
    /**
     * 挂载后
     */
    onMount(): void;
    /**
     * 初始化绑定的field，若不存在则为0
     */
    protected __initValue(): void;
    /**
     * 获取初始化slider所需参数
     */
    private __initSlider;
    /**
     * 拖拽开始，获取鼠标位置及dot位置
     * @param model - 当前节点对应model
     * @param dom - virtual dom节点
     * @param evObj - NEvent对象
     * @param e - event对象
     */
    private __dragStart;
    /**
     * 计算位置
     * @param value - 百分比值
     * @returns 实际位置值
     */
    private __calPosition;
    /**
     * 设置拖动后效果
     * @param value - 滑块应处位置的百分比值
     */
    private __setDisplace;
    /**
     * 设置滑动条移动效果
     * @param scale - 移动后，滑块所处的百分比值
     */
    private __setPosition;
    /**
     * 鼠标点击slider（没有点击dot的前提）
     * @param model - 当前节点对应model
     * @param dom - virtual dom节点
     * @param evObj - NEvent对象
     * @param e - event对象
     */
    private __onSliderClick;
    /**
     * 处理拖动事件，计算位置相关
     * @param event - 鼠标事件
     */
    private __handleEvent;
}
