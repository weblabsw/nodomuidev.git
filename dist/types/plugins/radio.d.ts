import { Module } from "nodom3";
import { BaseInput } from "./baseinput";
/**
 * UIRadioGroup 单选框集
 * @public
 */
export declare class UIRadioGroup extends BaseInput {
    /**
     * 当前model
     */
    private __current;
    /**
     * 子radio数组
     */
    private __radios;
    /**
     * 模板函数
     * @privateRemarks
     */
    template(props: any): string;
    /**
     * 设置当前子model
     * @param radio - 子radio
     */
    __setCurrent(radio: any): void;
    /**
     * 添加子model
     * @param radio -  子radio
     * @returns
     */
    __add(radio: any): void;
    /**
     * 初始化数据
     */
    protected __initValue(): void;
}
/**
 * 配置说明
 * field:           绑定父模块的字段
 */
/**
 * UIRadio 单选框
 * @public
 */
export declare class UIRadio extends Module {
    /**
     * radio选择时的值
     */
    value: any;
    /**
     * 模板函数
     * @privateRemarks
     */
    template(props: any): string;
    /**
     * 第一次渲染前事件
     * @privateRemarks
     */
    onBeforeFirstRender(): void;
    /**
     * 点击事件
     * @param model - 模型
     */
    __click(): void;
}
