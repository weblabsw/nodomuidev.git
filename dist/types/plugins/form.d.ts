import { Module } from "nodom3";
/**
 * form 插件
 * 配置项:
 * label-width   label所占宽度
 * unit-width    单位(m,m/s,...)所占宽度
 */
/**
 * UIForm 表单
 * @public
 */
export declare class UIForm extends Module {
    /**
     * label宽度
     */
    __labelWidth: string;
    /**
     * 单位框宽度
     */
    __unitWidth: string;
    /**
     * 子item数组
     */
    private __formItems;
    /**
     * 模板函数
     * @privateRemarks
     */
    template(props?: object): string;
    /**
     * 添加form item
     * @param item -  form item
     * @returns
     */
    __addItem(item: UIFormItem): void;
    /**
     * 校验
     */
    __verify(): boolean;
    /**
     * 清除错误提示
     */
    __clearErrorTips(): void;
}
/**
 * form item
 * 配置项
 *  label：     输入项label
 *  field：     绑定字段
 *  required：  是否必填
 *  invalid-msg: 校验失败消息，其中内置校验器有自己的消息，method方式也会返回错误消息，但是设置了invalidMsg，则其它消息失效
 *  validator： 校验器，支持内置校验器(参见UIValidator.rules)，regexp(正则表达式)，method(模块方法校验)
 *              使用方式 validator='validatorName:param1:param2'，其中validatorName为内置校验器名，regexp和method
 *              param1,param2为校验器参数，可选
 *              检验器示例:
 *                  between:1:10            校验输入项是否为1-10之间
 *                  method:check            通过模块的check方法进行校验，check参数为(value,model)，其中value为对应项值，model为对应model
 *                  regexp:`^[a-z]{5,20}$`  通过正则表达式进行校验，校验数据项是否为 5-20个小写字母
 */
/**
 * UIFormItem 表单子项组件
 * @public
 */
export declare class UIFormItem extends Module {
    /**
     * 校验器
     */
    private __validator;
    /**
     * 绑定字段
     */
    private __field;
    /**
     * 模板函数
     * @privateRemarks
     */
    template(props?: object): string;
    /**
      * 添加校验器
      * @param props -  属性
      */
    __addValidator(props: any): void;
    /**
     * 校验
     * @param value - 值
     * @returns         true 通过 false 失败
     */
    __verify(value?: unknown): boolean;
    /**
     * 清除错误提示
     */
    __clearErrorTip(): void;
    /**
     * 第一次渲染前事件
     * @privateRemarks
     */
    onBeforeFirstRender(): void;
}
