import { Module } from "nodom3";
/**
 * 工具类
 * @public
 */
export declare class UITool {
    /**
     * 去掉字符串的空格
     * @param src - 需处理的字符串
     */
    static clearSpace(src: string): string;
    /**
     * 计算实际位置
     * @param el - 待计算的element
     * @returns  [x坐标,y坐标]
     */
    static getRelPos(el: HTMLElement): number[];
    /**
     * 获取实际位置
     * @param el - 待计算的element
     * @returns  [x坐标,y坐标]
     */
    static getRealPos(el: HTMLElement): number[];
    /**
     * 计算位置
     * @param event - 鼠标事件
     * @param relPos - 相对位置：1下 2右
     * @param width - dom宽
     * @param height - dom高
     * @returns  [x,y,width,height]
     */
    static cacPosition(event: MouseEvent, relPos: number, width: number, height: number): number[];
    /**
     * 将对象转换为属性字符串
     * @param obj - 要转换的 object
     * @returns     转换完成的 string
     */
    static toAttrString(obj: object): string;
    /**
     * 鼠标拖动事件，获取拖动后位置（相对父元素）
     * @param event - 鼠标事件
     * @param el - 拖动事件的父Element
     * @param vertical - 是否竖直
     * @returns 拖动后位置
     * @beta
     */
    static getDisplacement(event: MouseEvent, el: HTMLElement, vertical: boolean): number;
    /**
 * 鼠标拖动事件，获取拖动后位置（相对父元素）
 * @param event - 鼠标事件
 * @returns x,y - 鼠标指针相对于浏览器窗口左上角的水平距离和垂直距
 * @beta
 */
    static returnClientXY(event: MouseEvent): {
        x: number;
        y: number;
    };
    /**
     * 调用通过props传递的方法
     * @param module -      当前模块
     * @param methodName -  方法名
     * @param params -      参数数组
     * @returns             方法实际返回值
     */
    static invokePropMethod(module: Module, methodName: string, ...params: any[]): any;
}
