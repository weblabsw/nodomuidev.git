import { Module, Nodom, ModuleFactory } from 'nodom3';
/**
 * 属性配置
 * single: true/false 是否同时只展开一个
 */
/**
 * UIAccordion 折叠面板
 * @public
 */
export class UIAccordion extends Module {
    /**
     * 模板函数html
     * @privateRemarks
     */
    template(props) {
        this.__single = props.hasOwnProperty('single');
        this.setExcludeProps(['single']);
        /**
         * 子菜单 type=0表示在当前菜单项下侧，否则表示右侧
         */
        return `
            <div class='ui-accordion'>
                <slot></slot>
            </div>
        `;
    }
}
/**
 * accordion item
 * 属性配置
 * title:	标题栏内容
 * opened: 	是否展开
 */
/**
 * UIAccordionItem 折叠子组件
 * @public
 */
export class UIAccordionItem extends Module {
    /**
     * 模板函数
     * @privateRemarks
     */
    template(props) {
        if (props.hasOwnProperty('opened')) {
            this.model['__open'] = true;
        }
        delete props['opened'];
        this.setExcludeProps(['title', 'opened']);
        return `
            <div class='ui-accordion-item'>
                <div class='ui-accordion-title' e-click='__clickItem'>
                    ${props['title']}
                    <span class={{__open?'ui-expand-icon ui-expand-icon-open':'ui-expand-icon'}}></span>
                </div>
				<div x-animationbox>
					<div class='ui-accordion-content'>
						<slot></slot>
					</div>
				</div>
            </div>
        `;
    }
    /**
     * 点击事件	展开/折叠
     * @param model - 对应model
     */
    __clickItem(model) {
        const module = this.getParent();
        if (module['__single']) {
            for (const mid of module.children) {
                const m = ModuleFactory.get(mid);
                if (mid !== this.id) {
                    if (m.model['__open']) {
                        m.model['__open'] = false;
                    }
                }
                else {
                    m.model['__open'] = true;
                }
            }
        }
        else {
            model['__open'] = !model['__open'];
        }
    }
}
Nodom.registModule(UIAccordion, 'ui-accordion');
Nodom.registModule(UIAccordionItem, 'ui-accordion-item');
//# sourceMappingURL=accordion.js.map