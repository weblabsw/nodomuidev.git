import { BaseInput } from "./baseinput";
/**
 *  配置说明：
 *  field 绑定滑块百分比值，默认为0
 *  max 最大值，默认为100
 *  min 最小值，默认为0
 *  vertical 是否竖向
 *  height 滑动条高度，竖向模式必填
 *  step 滑动步数
 *  show-step 是否显示间断点，必须在设置了step的前提下才生效
 */
/**
 * 拖拽事件
 * @param startPos - 鼠标点击起始位置（clientX,clientY）
 * @param currentPos - 拖拽后鼠标位置（clientX,clientY）
 * @param newPosition - 拖拽后滑块应到达的位置
 * @param startPosition - 滑块起始位置
 * @param highWayLength - 轨道长度
 */
export declare type initData = {
    /**
     * 鼠标点击起始位置（clientX,clientY）
     */
    startPos: {
        x: number;
        y: number;
    };
    /**
     * 拖拽后鼠标位置（clientX,clientY）
     */
    currentPos: {
        x: number;
        y: number;
    };
    /**
     * 拖拽后滑块应到达的位置
     */
    newPosition: number;
    /**
     * 滑块起始位置
     */
    startPosition: number;
    /**
     * 轨道长度
     */
    highWayLength: number;
};
/**
 *  UISlider 滑动条
 *  @public
 */
export declare class UISlider extends BaseInput {
    /**
     * 滑块计算移动参数对象
     */
    private __initData;
    /**
     * 滑动条精确值最大值，可设定，默认100
     */
    private __max;
    /**
     * 滑动条精确值最小值，可设定，默认0
     */
    private __min;
    /**
     * 是否竖向模式
     */
    private __vertical;
    /**
     * 如果是竖向模式，则需要设定设定，默认'200px'
     */
    private __height;
    /**
     * 是否禁用滑动
     */
    private __disabled;
    /**
     * 滑动条每次移动间隔
     */
    private __step;
    /**
     * 是否有tick,只有设置step值为true
     */
    private __hasTick;
    /**
     * 是否显示步点
     */
    private __showStep;
    /**
     *  轨道的element
     */
    private __pel;
    /**
     * dot的element
     */
    private __el;
    /**
     *  bar的element
     */
    private __bel;
    /**
     * tick们的element
     */
    private __ticks;
    /**
     * 是否拖拽
     */
    private __dragging;
    /**
    * 是否点击，防止拖拽后点击轨道造成的偏移
    */
    private __isClick;
    /**
     *
     * @param props - 传递进来的参数，具体见顶部
     * @returns 模板字符串，根据此进行渲染
     */
    template(props?: object): string;
    /**
     * 初始化参数对象
     */
    onInit(): void;
    /**
     * 挂载后钩子函数，只有挂载后才能获取对应ELement以及相关属性，此处完成slider的初始化
     */
    onMount(): void;
    /**
     * 渲染前钩子函数，此处设置对showStep属性的监听
     */
    onBeforeRender(): void;
    /**
     * model数据
     * @returns showStep - 设置showStep以便设置监听函数
     */
    data(): {
        showStep: boolean;
    };
    /**
     * 初始化绑定的field，若不存在则为0
     */
    protected __initValue(): void;
    /**
     * 获取初始化slider所需参数
     */
    private __initSlider;
    /**
     * 计算目标位置
     * @returns 目标位置百分比
     */
    private __currentPosition;
    /**
     * 拖拽事件
     * @param model - 当前节点对应model
     * @param dom - virtual dom节点
     * @param evObj - NEvent对象
     * @param e - event对象
     */
    private __dragStart;
    /**
     * 点击轨道事件
     * @param model - 当前节点对应model
     * @param dom - virtual dom节点
     * @param evObj - NEvent对象
     * @param e - event对象
     */
    private __onSliderDown;
    /**
     * 根据范围修改value，并决定每步步长(如设置step)
     * @param newL - 滑块拖拽后位置
     */
    private __setPosition;
    /**
     * 更新tick状态，如果要求显示，则会根据滑块所处位置，更新其之前的tick颜色
     */
    private __updateTicks;
}
