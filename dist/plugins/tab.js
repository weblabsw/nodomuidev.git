import { Module, Nodom } from "nodom3";
/**
 * bgcolor:         默认背景色
 * color:           默认前景色
 * active-bgcolor:  激活背景色
 * active-color:    激活前景色
 * onTabChange:     tab页切换钩子函数，页面切换时调用
 */
/**
 * UITab 标签页
 * @public
 */
export class UITab extends Module {
    constructor() {
        super(...arguments);
        /**
         * 拖动参数
         */
        this.__dragParam = { dx: 0, x: 0 };
    }
    /**
     * 模板函数
     * @privateRemarks
     */
    template(props) {
        this.__onTabClose = props.ontabclose;
        this.__onTabChange = props.ontabchange;
        let tabStyle = '';
        let activeStyle = '';
        if (props['active-bgcolor']) {
            activeStyle += 'background-color:' + props['active-bgcolor'];
        }
        //激活前景色，如果没设置，但是设置了默认前景色，则使用默认前景色
        const ac = props['active-color'] || props.color;
        if (ac) {
            if (activeStyle !== '') {
                activeStyle += ';';
            }
            activeStyle += 'color:' + ac;
        }
        let defaultStyle = '';
        if (props.bgcolor) {
            defaultStyle += 'background-color:' + props.bgcolor;
        }
        if (props.color) {
            if (defaultStyle !== '') {
                defaultStyle += ';';
            }
            defaultStyle += 'color:' + props.color;
        }
        if (activeStyle) {
            tabStyle = `style={{active?'${activeStyle}':'${defaultStyle}'}}`;
        }
        else if (defaultStyle) {
            tabStyle = `style={{!active?'${props.defaultstyle}':''}}`;
        }
        const headBg = props.bgcolor ? "style='background-color:" + props.bgcolor + "'" : '';
        this.setExcludeProps(['activebgcolor', 'activecolor', 'bgcolor', 'color', 'onchange', 'ontabclose']);
        return `
            <div class='ui-tab'>
                <div class='ui-tab-headct' ${headBg} >
                    <div class='ui-tab-head' e-drag='__dragHead' e-mousedown='__dragStart' e-mousemove='__drag' e-mouseup='__dragEnd' e-mouseleave='__dragEnd'>
                        <for cond={{tabs}} class={{'ui-tab-item' + (active?' ui-tab-item-active':'')}} e-click='__clickTab' ${tabStyle}>
                            {{title}}
                            <b class="ui-tab-close" x-show={{closable}} e-click='__closeTab:nopopo'/>
                        </for>
                    </div>
                </div>
                <div class='ui-tab-body'>
                    <slot />
                </div>
            </div>
        `;
    }
    /**
     * 模型
     * @privateRemarks
     */
    data() {
        return {
            tabs: []
        };
    }
    /**
     * 添加tab
     * @param cfg - tab配置项
     */
    __addTab(cfg) {
        if (this.model['tabs'].find(item => item.tab === cfg.tab)) {
            return;
        }
        this.model['tabs'].push(cfg);
        this.__setDefaultActive();
    }
    /**
     * 点击tab
     * @param model - 对应节点model
     */
    __clickTab(model) {
        this.__activeTab(model);
    }
    /**
     * 切换到目标tab
     * @param tab - 目标tab
     * @returns
     */
    __changeTo(tab) {
        if (!tab || tab === this.__currentTab) {
            return;
        }
        if (this.__currentTab) {
            this.__currentTab.active = false;
            this.__currentTab.tab.__hide();
        }
        tab.active = true;
        if (this.__onTabChange) {
            this.invokeOuterMethod(this.__onTabChange, this.__currentTab, tab);
        }
        tab.tab.__show();
        this.__currentTab = tab;
    }
    /**
     * 获取tab
     * @param data - tab config对象 或 title 或index
     * @returns  tab
     */
    __getTab(data) {
        switch (typeof data) {
            case 'number':
                return this.model['tabs'][data];
            case 'string':
                return this.model['tabs'].find((item) => item.title === data);
            default:
                return data;
        }
    }
    /**
     * 激活新tab
     * @param data - tab config对象 或 title 或index
     */
    __activeTab(data) {
        this.__changeTo(this.__getTab(data));
    }
    /**
     * 关闭页签
     * @param data - tab config对象 或 title 或index
     * @returns
     */
    __closeTab(data) {
        //最后一个不删除
        if (this.model['tabs'].length === 1) {
            return;
        }
        const tab = this.__getTab(data);
        if (!tab) {
            return;
        }
        const index = this.model['tabs'].findIndex(item => item === tab);
        if (index >= 0 || index < this.model['tabs'].length) {
            //执行tabclose事件
            if (this.__onTabClose) {
                this.invokeOuterMethod(this.__onTabClose, tab);
            }
            //移除
            this.model['tabs'].splice(index, 1);
            //被删除为当前tab，需要切换当前tab
            if (tab === this.__currentTab) {
                if (index === this.model['tabs'].length) { //最后一个
                    this.__changeTo(this.model['tabs'][index - 1]);
                }
                else { //取后面一个
                    this.__changeTo(this.model['tabs'][index]);
                }
            }
        }
    }
    /**
     * 头部拖动开始
     * @param model - 当前节点对应model
     * @param dom - virtual dom节点
     * @param evObj - NEvent对象
     * @param e - event对象
     */
    __dragStart(model, dom, evObj, e) {
        const el = e.currentTarget;
        const pel = el.parentElement;
        //设置宽度
        let w = 0;
        for (const d of el.children) {
            w += d.offsetWidth;
        }
        el.style.width = (w + 1) + 'px';
        //不比父宽，不处理
        if (el.offsetWidth < pel.offsetWidth) {
            return;
        }
        this.__dragParam.x = e.pageX;
    }
    /**
     * 拖动
     * @param model - 当前节点对应model
     * @param dom - virtual dom节点
     * @param evObj - NEvent对象
     * @param e - event对象
     * @returns
     */
    __drag(model, dom, evObj, e) {
        if (!this.__dragParam.x) {
            return;
        }
        this.__move(e);
    }
    /**
     * 拖动停止
     * @param model - 当前节点对应model
     * @param dom - virtual dom节点
     * @param evObj - NEvent对象
     * @param e - event对象
     */
    __dragEnd(model, dom, evObj, e) {
        this.__move(e);
        delete this.__dragParam.x;
    }
    /**
     * 移动
     * @param e - 事件对象
     * @returns
     */
    __move(e) {
        if (!this.__dragParam.x) {
            return;
        }
        const dx = e.pageX - this.__dragParam.x;
        if (Math.abs(dx) < 2) {
            return;
        }
        this.__dragParam.dx += dx;
        if (this.__dragParam.dx > 0) {
            this.__dragParam.dx = 0;
        }
        else {
            const el = e.currentTarget;
            const pel = el.parentElement;
            if (el.offsetWidth + this.__dragParam.dx < pel.offsetWidth) {
                this.__dragParam.dx = pel.offsetWidth - el.offsetWidth;
            }
        }
        this.__dragParam.x = e.pageX;
        e.currentTarget.style.transform = 'translateX(' + this.__dragParam.dx + 'px)';
    }
    __setDefaultActive() {
        const tabs = this.model['tabs'];
        let atab;
        if (tabs && tabs.length > 0) {
            for (let i = tabs.length - 1; i >= 0; i--) {
                const tab = tabs[i];
                // 最后一个active tab优先
                if (tab.active) {
                    if (!atab) { // 
                        atab = tab;
                    }
                    else { //找到active tab后，另外的active置false
                        tab.active = false;
                    }
                }
            }
            //如果没有设置active tab，默认第一个
            this.__changeTo(atab || tabs[0]);
        }
    }
}
/**
 * 配置说明
 * title    tab标题
 * closable 是否可关闭
 * active   是否处于打开状态
 */
/**
 * UTabItem 标签页个体
 * @public
 */
export class UTabItem extends Module {
    /**
     * 模板函数
     * @privateRemarks
     */
    template(props) {
        this.__active = (props.active === 'true' || props.active === true);
        this.__closable = (props.closable === 'true' || props.closable === true);
        this.__title = props.title;
        this.setExcludeProps(['title', 'active', 'closable']);
        return `
            <div x-show={{show}}>
                <slot/>
            </div>
        `;
    }
    /**
     * 模型
     * @privateRemarks
     */
    data() {
        return {
            show: false
        };
    }
    /**
     * 渲染前
     */
    onBeforeFirstRender() {
        //添加到父
        const pm = this.getParent();
        if (pm.constructor !== UITab) {
            return;
        }
        //追加到ui tab插件
        pm.__addTab({
            title: this.__title,
            active: this.__active,
            closable: this.__closable,
            tab: this
        });
    }
    /**
     * 隐藏
     */
    __hide() {
        this.model['show'] = false;
    }
    /**
     * 显示
     */
    __show() {
        this.model['show'] = true;
    }
}
//注册模块
Nodom.registModule(UITab, 'ui-tab');
Nodom.registModule(UTabItem, 'ui-tab-item');
//# sourceMappingURL=tab.js.map