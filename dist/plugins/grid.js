import { Nodom, Module, VirtualDom, Expression, Renderer } from "nodom3";
/**
 * 表格组件
 * 配置参数
 *  $data           表格数据
 *  row-alt         行颜色交替标志，不用设置值
 *  grid-line       网格线类型，包括cols(列) rows(行) both(行列)，默认无
 *  fix-head        是否固定表头，默认false
 *  checkable       是否显示复选框，默认false
 *  single          支持单选，当配置checkable时有效，默认false
 *  onSelectChange  选中更改时触发事件，只针对单行选中有效，传入参数为当前行model，对于头部check框选中无效
 *  onRowClick      行单击事件
 *  onRowDblClick   行双击事件
 *  onRowExpand     行展开事件
 *  onRowCollapse   行从展开到闭合时事件
 */
/**
 * UIGrid 表格
 * @public
 */
export class UIGrid extends Module {
    constructor() {
        super(...arguments);
        /**
         * 表头选中状态 0未选中 1选中 2部分选中
         */
        this.__headCheck = 0;
        /**
         * 列集合
         */
        this.__columns = [];
    }
    /**
     * 模板函数
     * @privateRemarks
     */
    template(props) {
        //列集合为空，进行slot初始化
        if (this.__columns.length === 0) {
            return "<div><slot/></div>";
        }
        this.__rowAlt = props.hasOwnProperty('row-alt');
        this.__fixHead = props.hasOwnProperty('fix-head');
        this.__gridLine = props['grid-line'];
        this.__singleSelect = props.hasOwnProperty('single');
        this.__onSelectChange = props.onselectchange;
        this.__onRowClick = props.onrowclick;
        this.__onRowDblClick = props.onrowdblclick;
        this.__onRowExpand = props.onrowexpand;
        this.__onRowCollapse = props.onrowcollapse;
        let expandStr = '';
        let expandHeadStr = '';
        let checkStr = '';
        let checkHeadStr = '';
        this.__width = 0;
        //行展开容器字符串
        let expandCtStr = '';
        /**
         * 是否可展开
         */
        if (this.__expandDom) {
            expandStr = `<div class='ui-grid-row-item ui-grid-icon' e-click='__clickExpand'>
                            <b class={{__open?'ui-expand-icon ui-expand-icon-open':'ui-expand-icon'}}/>
                        </div>`;
            expandHeadStr = `<div class='ui-grid-row-item ui-grid-icon'></div>`;
            expandCtStr = `<div x-animationbox><div class='ui-grid-expand'></div></div>`;
            this.__width += 25;
        }
        /**
         * 是否带有复选框
         */
        if (props.hasOwnProperty('checkable')) {
            checkStr = `<div class='ui-grid-row-item ui-grid-icon'>
                            <span class={{__genCheckCls(__checked)}} e-click='__clickCheck'>
                                <span class='ui-checkbox-box' >
                                    <span class='ui-checkbox-inner' />
                                </span>
                            </span>
                        </div>`;
            checkHeadStr = `<div class='ui-grid-row-item ui-grid-icon'>
                        <span class={{__genCheckCls(this.__headCheck)}} e-click='__clickHeadCheck'>
                            <span class='ui-checkbox-box' >
                                <span class='ui-checkbox-inner' />
                            </span>
                        </span>
                    </div>`;
            this.__width += 25;
        }
        for (const col of this.__columns) {
            const w = col['width'];
            if (w) {
                if (this.__width >= 0) {
                    this.__width += w;
                }
            }
            else {
                //flex 不计算宽度
                this.__width = -1;
            }
        }
        //删除多余属性
        this.setExcludeProps(['grid-line', 'checkable', 'row-alt', 'fix-head', 'onselectchange', 'onrowclick', 'onrowdblclick']);
        return `
            <div class={{__genGridCls()}} style={{__genGridWidth()}}>
                <div class='ui-grid-head' style={{__genWidthStyle()}}>
                    <div class='ui-grid-row' >
                        ${expandHeadStr}
                        ${checkHeadStr}
                    </div>
                </div>
                <div class='ui-grid-bodyct' e-scroll='__scrollBody' >
                    <div class={{__genBodyCls()}} style={{__genWidthStyle()}}>
                        <for cond={{__genData()}} class='ui-grid-rowct'>
                            <div class='ui-grid-row' e-click='__rowClick' e-dblclick='__dblClick'>
                                ${expandStr}
                                ${checkStr}
                            </div>
                            ${expandCtStr}
                        </for>
                    </div>
                </div>
            </div>
        `;
    }
    /**
     * 编译后事件，动态添加列到body
     */
    onCompile() {
        if (this.domManager.vdomTree.children[1]) {
            //head col容器
            const headCt = this.domManager.vdomTree.children[0].children[0];
            //body col容器
            const bodyCt = this.domManager.vdomTree.children[1].children[0].children[0].children[0];
            for (const col of this.__columns) {
                if (headCt) {
                    this.__genNewKey(col.headDom);
                    headCt.add(col.headDom);
                }
                if (bodyCt) {
                    this.__genNewKey(col.bodyDom);
                    bodyCt.add(col.bodyDom);
                }
            }
            //处理展开节点
            if (this.__expandDom) {
                const ct = this.domManager.vdomTree.children[1].children[0].children[0].children[1].children[0];
                if (ct) {
                    ct.children = this.__expandDom.node.children;
                    for (const c of ct.children) {
                        this.__genNewKey(c);
                    }
                }
            }
        }
    }
    /**
     * 生产grid class
     * @returns     grid class
     */
    __genGridCls() {
        const arr = ['ui-grid'];
        if (this.__fixHead) {
            arr.push("ui-grid-fixhead");
        }
        if (this.__gridLine === 'rows') {
            arr.push('ui-grid-row-line');
        }
        else if (this.__gridLine === 'cols') {
            arr.push('ui-grid-col-line');
        }
        else if (this.__gridLine === 'both') {
            arr.push('ui-grid-all-line');
        }
        if (this.props['class']) {
            arr.push(this.props['class']);
        }
        return arr.join(' ');
    }
    /**
     * 产生grid width style，用于body和head
     * @returns style样式
     */
    __genWidthStyle() {
        return this.__width > 0 ? 'width:' + this.__width + 'px' : '';
    }
    /**
     * 获取表格宽度
     * @returns
     */
    __genGridWidth() {
        return this.__width > 0 ? 'max-width:' + (this.__width + 10) + 'px' : '';
    }
    /**
     * 产生body css
     * @returns css串
     */
    __genBodyCls() {
        const arr = ['ui-grid-body'];
        if (this.__rowAlt) {
            arr.push("ui-grid-rowalt");
        }
        return arr.join(' ');
    }
    /**
     * 生成checkbox class
     * @param  st - 状态 0未选中 1全选中 2部分选中
     * @returns     checkbox 的class
     */
    __genCheckCls(st) {
        const arr = ['ui-checkbox'];
        if (!st) {
            arr.push('ui-checkbox-uncheck');
        }
        else if (st === 1) {
            arr.push('ui-checkbox-checked');
        }
        else {
            arr.push('ui-checkbox-partchecked');
        }
        return arr.join(' ');
    }
    /**
     * 点击expand
     * @param model - 对应模型
     */
    __clickExpand(model) {
        model['__open'] = !model['__open'];
        if (this.__onRowExpand && model['__open']) {
            this.invokeOuterMethod(this.__onRowExpand, model);
        }
        else if (this.__onRowCollapse && !model['__open']) {
            this.invokeOuterMethod(this.__onRowCollapse, model);
        }
    }
    /**
     * 行点击
     * @param model - 当前节点对应model
     * @param dom - virtual dom节点
     * @param evObj - NEvent对象
     * @param e - event对象
     */
    __rowClick(model, dom, evObj, e) {
        this.invokeOuterMethod(this.__onRowClick, model, dom, evObj, e);
    }
    /**
     * 行双击
     * @param model - 当前节点对应model
     * @param dom - virtual dom节点
     * @param evObj - NEvent对象
     * @param e - event对象
     */
    __dblClick(model, dom, evObj, e) {
        this.invokeOuterMethod(this.__onRowDblClick, model, dom, evObj, e);
    }
    /**
     * 点击头部checkbox
     */
    __clickHeadCheck() {
        if (this.__singleSelect) {
            return;
        }
        const st = this.__headCheck === 1 ? 0 : 1;
        this.__headCheck = st;
        if (!this.model['data'] || this.model['data'].length === 0) {
            return;
        }
        //更新行checkbox状态
        for (const m of this.model['data']) {
            m['__checked'] = st;
        }
    }
    /**
     * 点击行 checkbox
     * @param model - 点击项model
     */
    __clickCheck(model) {
        //单选，需要清理之前选中项
        if (this.__singleSelect) {
            if (this.__selectedModel) {
                this.__selectedModel['__checked'] = false;
            }
            model['__checked'] = 1;
            this.__headCheck = 2;
            this.__selectedModel = model;
        }
        else {
            model['__checked'] = model['__checked'] ? 0 : 1;
            //修改表头checkbox选中状态
            const rows = this.model['data'];
            const arr = rows.filter(item => item.__checked === 1);
            if (arr.length === rows.length) {
                this.__headCheck = 1;
            }
            else if (arr.length === 0) {
                this.__headCheck = 0;
            }
            else {
                this.__headCheck = 2;
            }
        }
        if (this.__onSelectChange) {
            this.invokeOuterMethod(this.__onSelectChange, model);
        }
    }
    /**
     * 设置排序字段
     * @param field - 待排序字段
     * @param type -  1升序  -1降序
     */
    __setSortField(field, type) {
        if (field === this.__sortField) {
            //现在是升序，则解除排序，否则设置为升序
            if (this.model['__sortObj'][field] === type) {
                delete this.model['__sortObj'][field];
            }
            else {
                this.model['__sortObj'][field] = type;
            }
        }
        else { //不是排序字段
            //删除之前的排序字段
            if (this.__sortField) {
                delete this.model['__sortObj'][this.__sortField];
            }
            //设置新排序字段
            this.model['__sortObj'][field] = type;
            this.__sortField = field;
        }
    }
    /**
     * 创建数组数据
     * @returns     新数据
     */
    __genData() {
        //不排序，则直接返回
        if (!this.__sortable) {
            return this.model['data'];
        }
        //新建一个数组进行操作
        const rows = this.model['data'].slice(0);
        if (this.__sortField) {
            const v = this.model['__sortObj'][this.__sortField];
            rows.sort((a, b) => a[this.__sortField] > b[this.__sortField] ? v : -v);
        }
        return rows;
    }
    /**
     * 滚动表格body
     * @param model - 当前节点对应model
     * @param dom - virtual dom节点
     * @param evObj - NEvent对象
     * @param e - event对象
     */
    __scrollBody(model, dom, evObj, e) {
        if (!this.__fixHead) {
            return;
        }
        const el = e.currentTarget;
        const left = el.scrollLeft;
        this.getElement(this.domManager.vdomTree.children[0].key).style.transform = 'translateX(-' + left + 'px)';
    }
    /**
     * 添加记录
     * @param rows - 数据(数组)
     */
    __addRow(rows) {
        if (!this.model['data']) {
            this.model['data'] = [];
        }
        if (Array.isArray(rows)) {
            for (const r of rows) {
                this.model['data'].push(r);
            }
        }
        else {
            this.model['data'].push(rows);
        }
    }
    /**
     * 删除记录
     * @param param -     对象参数，用于查找符合该参数条件的所有数据
     */
    __removeRow(param) {
        if (!this.model['data']) {
            return;
        }
        for (let i = 0; i < this.model['data'].length; i++) {
            const item = this.model['data'][i];
            //找到标志
            let finded = true;
            for (const k of Object.keys(param)) {
                if (param[k] !== this.get(item, k)) {
                    finded = false;
                    break;
                }
            }
            if (finded) {
                this.model['data'].splice(i--, 1);
            }
        }
    }
    /**
     * 获取选中记录
     * @returns     选中的记录集
     */
    __getSelectedRows() {
        if (!this.model['data']) {
            return;
        }
        return this.model['data'].filter(item => item.__checked);
    }
    /**
     * 添加列
     * @param col - UIGridCol
     */
    __addColumn(col) {
        //如果存在不添加
        if ((!this.__columns.find(item => item === col))) {
            this.__columns.push(col);
            if (col['sortable'] && col['field']) {
                this.__sortable = true;
                if (!this.model['__sortObj']) {
                    this.model['__sortObj'] = {};
                }
            }
            //没有修改数据，需要强制渲染
            Renderer.add(this);
        }
    }
    /**
     * 设置展开节点
     * @param expand -    expand 组件实例
     */
    __setExpandDom(expand) {
        this.__expandDom = expand;
    }
    /**
     * 清空列
     */
    __clearColumns() {
        this.__columns = [];
    }
    /**
     * 为dom及其子节点设置新key
     * @param dom - 待设置key的dom节点
     */
    __genNewKey(dom) {
        dom.key = this.getDomKeyId();
        if (dom.children) {
            for (const c of dom.children) {
                this.__genNewKey(c);
            }
        }
    }
}
/**
 * grid列项组件
 * 配置参数
 *  title           列标题
 *  width           宽度，不带单位，使用时直接按照px计算，如果不设置，则默认flex:1，如果自动铺满，最后一列不设置宽度
 *  sortable        是否支持排序，true/false
 *  field           如果sortable，则需要设置，以该字段排序，如果没设置列显示内容，则默认显示此项对应的值
 *  head-style     表头列样式
 *  body-style      表body列样式
 */
/**
 * UIGridCol 表格列项组件
 *  @public
 */
export class UIGridCol extends Module {
    /**
     * 模板函数
     * @privateRemarks
     */
    template(props) {
        //隐藏节点不添加
        if (props.hasOwnProperty('hidden')) {
            return;
        }
        const w = props['width'];
        if (w !== undefined) {
            this['width'] = parseInt(w);
        }
        this['title'] = props['title'];
        this['field'] = props['field'],
            this['width'] = props['width'] ? parseInt(props['width']) : 0;
        this['sortable'] = props['sortable'] === 'true' || props['sortable'] === true;
        this['fixed'] = props['fixed'] === 'right' ? 'right' : props.hasOwnProperty('fixed') ? 'left' : undefined;
        const style = 'style="' + (this['width'] ? 'width:' + this['width'] + 'px' : 'flex:1') + ';';
        let headStyle = style;
        let bodyStyle = style;
        const headAlign = '';
        if (props['head-style']) {
            headStyle += props['head-style'];
        }
        if (props['body-style']) {
            bodyStyle += props['body-style'];
        }
        headStyle += '"';
        bodyStyle += '"';
        const clazz = `class="ui-grid-row-item${this['fixed'] ? (this['fixed'] === 'left' ? ' ui-grid-sticky-left' : ' ui-grid-sticky-right') : ''}"`;
        //排序节点
        const sortStr = this['sortable'] && this['field'] ?
            `<div class='ui-grid-sort' field='${this['field']}'>
                <b class={{'ui-grid-sort-raise' + (__sortObj['${this['field']}']===1?' ui-grid-sort-pressed':'')}} e-click='raiseSort' />
                <b class={{'ui-grid-sort-down' + (__sortObj['${this['field']}']===-1?' ui-grid-sort-pressed':'')}} e-click='downSort'/>
            </div>` : '';
        //两个节点 0:header列节点  1:body列节点
        return `
            <div>
                <div ${clazz} ${headStyle} ${headAlign}>${this['title']} ${sortStr}</div>
                <div ${clazz} ${bodyStyle}></div>
            </div>
        `;
    }
    /**
     * 编译事件
     * @privateRemarks
     */
    onCompile() {
        let node;
        if (this.srcDom.vdom.children && this.srcDom.vdom.children.length > 0) {
            node = this.srcDom.vdom.children[0];
            node.removeDirective('slot');
        }
        else { //默认为field
            node = new VirtualDom();
            node.expressions = [new Expression(this['field'])];
        }
        this.domManager.vdomTree.children[1].children = [node];
        this.headDom = this.domManager.vdomTree.children[0];
        this.bodyDom = this.domManager.vdomTree.children[1];
        this.getParent().__addColumn(this);
        //清空vdomtree，避免渲染
        this.domManager.vdomTree = null;
    }
    /**
     * 升序
     */
    raiseSort() {
        this.getParent().__setSortField(this['field'], 1);
    }
    /**
     * 降序
     */
    downSort() {
        this.getParent().__setSortField(this['field'], -1);
    }
}
/**
 * UIGridExpand 行展开内容
 * @public
 */
export class UIGridExpand extends Module {
    /**
     * 模板函数
     * @privateRemarks
     */
    template() {
        this.node = this.srcDom.vdom.children[0];
        this.node.removeDirective('slot');
        this.getParent().__setExpandDom(this);
        return null;
    }
}
//注册模块
Nodom.registModule(UIGrid, 'ui-grid');
Nodom.registModule(UIGridCol, 'ui-grid-col');
Nodom.registModule(UIGridExpand, 'ui-grid-expand');
//# sourceMappingURL=grid.js.map