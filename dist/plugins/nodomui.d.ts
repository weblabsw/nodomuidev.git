/**
 * nodom组件库相关设置
 * @public
 */
export declare class NodomUI {
    private static messageCfg;
    /**
     * 设置语言
     * @param lang -  语言
     */
    static setLang(lang: string): void;
    /**
     * 获取提示文字信息
     * @param name -  文字对应名称
     * @param args -  参数
     * @returns     文字内容
     */
    static getText(name: string, ...args: any[]): string;
}
