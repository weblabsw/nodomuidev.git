import { Module } from "nodom3";
/**
 * BaseInput 绑定数据型父类组件
 * @public
 */
export declare class BaseInput extends Module {
    /**
     * 对应字段
     */
    protected __field: string;
    /**
     * change事件方法名
     */
    private __onChange;
    /**
     * 启用watch标志
     */
    private __watched;
    /**
     * 重构
     */
    constructor();
    /**
     * 模板函数
     * @privateRemarks
     */
    template(props?: object): string;
    onBeforeFirstRender(): void;
    /**
     * 更改值
     * 需要对父模块对应数据项进行更改
     * @param oldValue - 旧值
     * @param newValue - 新值
     */
    __change(oldValue: unknown, newValue: unknown): void;
    /**
     * 初始化value，当模块依赖值进行初始化时有用
     */
    protected __initValue(): void;
    /**
     * 获取值
     * @returns - 模块值
     */
    __getValue(): any;
}
