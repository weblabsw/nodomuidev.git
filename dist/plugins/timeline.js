import { Nodom } from "nodom3";
import { BaseInput } from "./baseinput";
/**
 * 配置参数
 * $data         数据
 * reverse       是否需要反转
 * color-field         自定义颜色
 * size-field          自定义尺寸 四种：small、normal、big、hollow(空心圆)
 * icon-field          自定义图标
 * place-ment     时间戳位置 true:"top"/false:"bottom" 默认为bottom
 */
/**
 * UITimeLine 时间轴
 * @beta
 */
export class UITimeLine extends BaseInput {
    /**
     * 模板函数
     * @privateRemarks
     */
    template(props) {
        super.template(props);
        this.__colorField = props['color-field'];
        this.__sizeField = props['size-field'];
        this.__iconField = props['icon-field'];
        this.__placeMent = props.hasOwnProperty("place-ment") ? true : false;
        this.__reverse = props.hasOwnProperty("reverse");
        const contentStr = props.hasOwnProperty("place-ment") ? `
        <div class="ui-itemcontent">
               <div class="ui-card-shadow">
                    <div class="ui-card-body">
                        <h4>{{ttitle}}</h4>
                        <p>{{tdesc}} {{tdate}} {{ttime}}</p>
                    </div>
               </div>
        </div>
    ` : `<div class="ui-itemcontent">
             {{ttitle}}
        </div>`;
        this.setExcludeProps(['color-field', 'size-field', 'icon-field', 'placeMent', 'reverse']);
        return `
        <div>
            <ui-radiogroup  field='__evalue' x-show={{this.__reverse}}>
                    <ui-radio value='1'title="正序"   e-click="__sortAsc"/>
                    <ui-radio value='-1' title="逆序" e-click="__sortDesc"/>
        </ui-radiogroup >
        </div>
        <div class="ui-timeline">
        <div class="ui-timeline-block" >
               <ul class="ui-timeline ui-timeline-reverse" x-repeat={{data}}>
                    <li class="ui-timeline-item">
                        <div class="ui-itemtail" ></div>
                        <b class={{__genClass(${this.__iconField},${this.__sizeField})}} style={{__genStyle(${this.__colorField})}}/>
                        <div class="ui-itemwrapper">
                            <div class="ui-itemtimestamp-top" x-show={{this.__placeMent}}>{{tdate}}</div>
                            ${contentStr}
                            <div class="ui-itemtimestamp-bottom" x-show={{!this.__placeMent}}>{{tdate}}</div>
                        </div>
                       
                    </li>
               </ul>

            </div>
        </div>
            
        `;
    }
    /**
     * 模型
     */
    data() {
        return {
            __evalue: 1,
            __flag: true
        };
    }
    /**
     * 更改颜色
     * @param color - 状态类型
     * @returns 自定义背景颜色样式
     */
    __genStyle(color) {
        return this.__colorField ? 'background-color:' + (color) : '';
    }
    /**
     * 自定义大小和图标
     * @param icon - 图标类型
     * @param size - 大小类型
     * @returns 自定义图标、大小样式
    */
    __genClass(icon, size) {
        if (icon) {
            return 'ui-icon ' + ('ui-icon-' + (icon));
        }
        else {
            return 'ui-itemnode ui-itemnode--' + (size ? size : 'normal');
        }
    }
    /**
     * 进行正序展示
     */
    __sortAsc() {
        if (!this.model['__flag']) {
            const row = this.model['data'];
            row.reverse();
            this.model['data'] = row;
            this.model['__flag'] = true;
        }
    }
    /**
     * 进行逆序展示
     */
    __sortDesc() {
        if (this.model['__flag']) {
            const row = this.model['data'];
            row.reverse();
            this.model['data'] = row;
            this.model['__flag'] = false;
        }
    }
}
//注册模块
Nodom.registModule(UITimeLine, 'ui-timeline');
//# sourceMappingURL=timeline.js.map