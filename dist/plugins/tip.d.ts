import { Module } from "nodom3";
import { ITipCfg } from "./types";
/**
 * UITip 信息提示
 * @public
 */
export declare class UITip extends Module {
    /**
     * 模板函数
     * @privateRemarks
     */
    template(props: any): string;
    /**
     * 模型
     * @privateRemarks
     */
    data(): {
        topData: any[];
        rightData: any[];
        bottomData: any[];
        leftData: any[];
    };
    /**
     * 关闭
     * @param model - 对应节点的model
     */
    __close(model: any): void;
    /**
     * 移除hide item
     * @param model - 当前节点对应model
     * @param dom - virtual dom节点
     * @param evObj - NEvent对象
     * @param e - event对象
     */
    __removeHide(model: any, dom: any, evObj: any, e: any): void;
}
/**
 * UITipManager UITip管理器
 * @public
 */
export declare class UITipManager {
    /**
     * 待关闭tip数组
     */
    private static tobeClosed;
    /**
     * tip id
     */
    private static id;
    /**
     * 添加tip
     * @param cfg - tip配置项
     */
    static addTip(cfg: ITipCfg): void;
    /**
     * 移除tip
     */
    static removeTip(): void;
    /**
     * 获取uiTip插件
     * @returns     uiTip插件
     */
    private static getUITip;
}
/**
 * 暴露tip函数
 * @param cfg -tip 配置
 * @public
 */
export declare function nuitip(cfg: ITipCfg): void;
