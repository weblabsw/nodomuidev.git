import { Nodom } from "nodom3";
import { BaseInput } from "./baseinput";
/**
 * 配置说明
 * $data：          列表数据数组
 * field:           绑定父模块的字段
 * value-field：    值字段名
 * disable-field：  禁用字段名
 * onItemClick：    点击事件
 */
/**
 * UIList 列表
 * @public
 */
export class UIList extends BaseInput {
    /**
     * 模板函数
     * @privateRemarks
     */
    template(props) {
        super.template(props);
        this.__multiple = props.hasOwnProperty('multiple');
        this.__valueField = props['value-field'];
        this.__disableField = props['disable-field'];
        this.__onItemClick = props.onitemclick;
        let disableCtx = '';
        if (props.disablefield) {
            disableCtx = "+ (" + props.disablefield + "?' ui-list-item-disable':'')";
        }
        this.setExcludeProps(['field', 'multiple', 'value-field', 'disable-field', 'onitemclick']);
        return `
            <div class="ui-list">
                <div x-repeat={{data}} class={{'ui-list-item' + (__selected?' ui-list-item-active':'')  ${disableCtx} }} e-click='__clickItem'>
                    <div class='ui-list-itemcontent'>
                        <slot innerRender/>
                    </div>
                    <b class="ui-list-icon"></b>
                </div>
            </div>
        `;
    }
    /**
     * 点击item
     * @param model - 当前节点对应model
     * @param dom - virtual dom节点
     * @param evObj - NEvent对象
     * @param e - event对象
     */
    __clickItem(model, dom, evObj, e) {
        if (!this.__field || this.__disableField && model[this.__disableField]) {
            return;
        }
        const rows = this.model['data'];
        let value;
        const v = model[this.__valueField];
        model['__selected'] = !model['__selected'];
        /**
         * 是否可多项
         */
        if (this.__multiple) {
            value = this.model['__value'] || [];
            if (!Array.isArray(value)) {
                value = [value];
            }
            const index = value.indexOf(v);
            if (model['__selected']) {
                if (index === -1) {
                    value.push(v);
                }
            }
            else {
                if (index !== -1) {
                    value.splice(index, 1);
                }
            }
        }
        else {
            if (model['__selected']) {
                for (const d of rows) {
                    if (d !== model) {
                        d.__selected = false;
                    }
                }
                this.model['__value'] = v;
            }
            else {
                this.model['__value'] = undefined;
            }
        }
        //触发itemclick事件
        if (this.__onItemClick) {
            this.invokeOuterMethod(this.__onItemClick, model, dom, evObj, e);
        }
    }
    /**
     * 设置值
     */
    __initValue() {
        super.__initValue();
        const rows = this.model['data'];
        if (!this.__field || !Array.isArray(rows)) {
            return;
        }
        let value = this.model['__value'];
        if (this.__multiple) {
            if (!Array.isArray(value)) {
                value = [value];
            }
            for (const m of rows) {
                m['__selected'] = value.includes(m[this.__valueField]);
            }
        }
        else {
            for (const m of rows) {
                m['__selected'] = value === m[this.__valueField];
            }
        }
    }
}
Nodom.registModule(UIList, 'ui-list');
//# sourceMappingURL=list.js.map