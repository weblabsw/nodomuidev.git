import { BaseInput } from './baseinput';
/**
 * date 插件
 * 配置说明：
 * use-timestamp:   是否使用时间戳，如果使用，则对应value为数字，否则为日期串
 * format:          日期格式，参考ISO 日期字符串，默认为 yyyy-MM-dd
 */
/**
 * UIDate 日期选择器
 * @public
 */
export declare class UIDate extends BaseInput {
    /**
     * 使用时间戳，如果设置该值，则返回时，返回时间戳，否则返回日期串
     */
    private __useTimestamp;
    /**
     * 日期样式
     */
    private __format;
    /**
     * 模板函数html
     * @privateRemarks
     */
    template(props: any): string;
    /**
     * 模型
     * @privateRemarks
     */
    data(): {
        __open: boolean;
        __dates: any[];
    };
    /**
     * 交替 list box
     * @param model - 对应model
     * @param dom - virtual dom节点
     * @param evObj - NEvent对象
     * @param e - event对象
     */
    private __toggleBox;
    /**
     * 打开list box
     * @param model - 对应model
     * @param dom - virtual dom节点
     * @param evObj - NEvent对象
     * @param e - event对象
     */
    private __openBox;
    /**
     * 关闭list box
     */
    private __closeBox;
    /**
     * 点击item
     * @param model - 对应model
     */
    private __clickDate;
    /**
     * 设置当前日期
     */
    private __today;
    /**
     * 确定按钮
     */
    private __okClick;
    /**
     * 转换为日期串
     */
    private __toDateStr;
    /**
     * 设置日期或时间
     * @param str - 待设置值(日期串或时间戳)
     */
    private __setDate;
    /**
     * 计算位置
     * @param model - 模型
     * @param dom - 当前节点
     * @param evObj - event object
     * @param e -  html event
     */
    private __cacLoc;
    /**
     * 产生日期数组
     * @param year - 年
     * @param month - 月
     */
    private __genDates;
    /**
     * 计算一个月的天数
     * @param year - 年
     * @param month - 月
     * @param disMonth - 相差月数
     */
    private __cacMonthDays;
    /**
     * 修改月份
     * @param distance - 差异量
     */
    private __changeMonth;
    /**
     * 年份-1
     */
    private __subYear;
    /**
     * 年份+1
     */
    private __addYear;
    /**
     * 月份-1
     */
    private __subMonth;
    /**
     * 月份+1
     */
    private __addMonth;
}
