import { Module } from 'nodom3';
/**
 * UIStepNode 节点类型
 * @alpha
 */
export declare type UIStepNode = {
    __status: number;
    __checked: string;
};
/**
 * 数据项
 * $data            步骤数组数据
 * 配置参数
 * current          当前步骤位置
 * direction        步骤条方向 vertical/horizontal 默认horizontal
 * finish-status    完成时状态 active/success/warn/error
 * process-status   进行时状态
 * success-select   完成状态图标
 */
/**
 * UISteps 步骤条
 * @beta
 */
export declare class UISteps extends Module {
    /**
     * 当前状态
     */
    private __current;
    /**
     * 方向
     */
    private __direction;
    /**
     * 完成步骤样式
     */
    private __finishColor;
    /**
     * 当前步骤样式
     */
    private __processColor;
    /**
     * 成功时的图标显示状态
     */
    private __useSelect;
    /**
     * 模板函数
     * @privateRemarks
     */
    template(props?: object): string;
    /**
     * 初始化步骤节点状态
     * @param cur - 当前节点
     */
    private __setCurrent;
    /**
     * 设置每个步骤节点状态
     * @param status -  状态类型
     * @param node -    节点对象
     */
    private __setStatus;
    /**
     * 设置每个step节点样式
     * @param status -  状态值
     * @returns         class值
     */
    private __genStepClass;
    /**
     * 设置节点显示图标还是数字
     * @param status -  状态类型
     * @param idx -     索引
     * @returns         节点显示值
     */
    private __getIndex;
}
