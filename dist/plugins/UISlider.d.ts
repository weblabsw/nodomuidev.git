import { BaseInput } from "./baseinput";
export declare class UISlider extends BaseInput {
    private __max;
    private __min;
    private __vertical;
    private __disabled;
    private __height;
    private __isClick;
    private __dragging;
    private __initData;
    private __pel;
    /**
     * 滑块的element
     */
    private __el;
    /**
     *  轨道的element
     */
    private __bel;
    template(props?: object): string;
    onBeforeRender(): void;
    onInit(): void;
    onMount(): void;
    private __currentPosition;
    private __dragStart;
    private __diplay;
}
