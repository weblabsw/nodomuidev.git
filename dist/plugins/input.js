import { Nodom } from "nodom3";
import { BaseInput } from "./baseinput";
/**
 * 配置项
 * field        绑定字段名
 * allow-clear  是否允许清空，无值属性
 * show-pwd     是否允许密码显示和隐藏方式切换，无值属性，type为password时有效
 * name         原生属性，参考input标签
 * type         原生属性，参考input标签，可设置为textarea
 * placeholder  原生属性，参考input标签
 * min          原生属性，参考input标签
 * max          原生属性，参考input标签
 * minlength    原生属性，参考input标签
 * maxlength    原生属性，参考input标签
 * disabled     原生属性，参考input标签
 * readonly     原生属性，参考input标签
 * rows         原生属性，参考textarea标签
 * cols         原生属性，参考textarea标签
 * resize       原生属性，参考textarea标签
 */
/**
 * UIInput 输入框
 * @public
 */
export class UIInput extends BaseInput {
    /**
     * 模板函数
     */
    template(props) {
        super.template(props);
        // 属性集合
        const valueArr = [];
        //带值属性
        for (const f of ['name', 'placeholder', 'min', 'max', 'minlength', 'maxlength', 'size', 'rows', 'cols']) {
            if (props[f]) {
                valueArr.push(`${f}='${props[f]}'`);
            }
        }
        //不带值属性
        for (const f of ['disabled', 'readonly', 'autosize', 'autocomplete']) {
            if (props.hasOwnProperty(f)) {
                valueArr.push(`${f}='true'`);
            }
        }
        //设置类型
        if (!this.model['type']) {
            this.model['type'] = props.type || 'text';
        }
        //输入串
        let inputStr;
        if (props.resize || props.type === 'textarea') {
            inputStr = `<textarea x-field='__value' ${valueArr.join(' ')}/>`;
        }
        else {
            inputStr = `<input type={{type}} x-field='__value' ${valueArr.join(' ')}/>`;
        }
        //后置按钮串
        let btnStr = '';
        if (props.type === 'password' && props.hasOwnProperty('show-pwd')) {
            btnStr += "<b class={{type==='text'?'ui-input-hidepwd':'ui-input-seepwd'}} e-click='__seePwd'/>";
        }
        else if (props.hasOwnProperty('allow-clear')) {
            btnStr += "<b class='ui-input-clear' e-click='__clear'/>";
        }
        //给btnstr添加外部内容
        if (btnStr) {
            btnStr = `<div class='ui-input-buttons'>
                ${btnStr}
                </div>
            `;
        }
        this.setExcludeProps(['type', 'resize', 'field', 'placeholder', 'min', 'max', 'minlength', 'maxlength', 'size', 'rows', 'autosize', 'autocomplete', 'disabled', 'readonly', 'autosize', 'autocomplete']);
        //返回
        return `
            <div class='ui-input ${props.hasOwnProperty('disabled') ? 'ui-input-disabled' : ''}'>
                <div class='ui-input-before'>
                    <slot name='before'/>
                </div>
                ${inputStr}
                ${btnStr}
                <div class='ui-input-after'>
                    <slot name='after'/>
                </div>
            </div>
        `;
    }
    /**
     * 清除内容
     */
    __clear() {
        this.model['__value'] = '';
    }
    /**
     * 显示和隐藏password
     */
    __seePwd() {
        this.model['type'] = this.model['type'] === 'password' ? 'text' : 'password';
    }
}
//注册别名    
Nodom.registModule(UIInput, 'ui-input');
//# sourceMappingURL=input.js.map