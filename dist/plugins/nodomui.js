import { NodomUIMessage_zh } from "./locales/msg_zh";
import { NodomUIMessage_en } from "./locales/msg_en";
import { Util } from "nodom3";
/**
 * nodom组件库相关设置
 * @public
 */
export class NodomUI {
    /**
     * 设置语言
     * @param lang -  语言
     */
    static setLang(lang) {
        switch (lang) {
            case 'en':
                this.messageCfg = NodomUIMessage_en;
                break;
            default:
                this.messageCfg = NodomUIMessage_zh;
        }
    }
    /**
     * 获取提示文字信息
     * @param name -  文字对应名称
     * @param args -  参数
     * @returns     文字内容
     */
    static getText(name, ...args) {
        const ta = name.includes('.') ? name.split('.') : [name];
        let o = this.messageCfg || NodomUIMessage_zh;
        let v;
        for (const p of ta) {
            v = o[p];
            if (v) {
                o = o[p];
            }
        }
        if (v && args.length > 0) {
            v = Util.compileStr(v, ...args);
        }
        return v;
    }
}
//# sourceMappingURL=nodomui.js.map