import { BaseInput } from "./baseinput";
/**
 * 配置参数
 * $data         数据
 * reverse       是否需要反转
 * color-field         自定义颜色
 * size-field          自定义尺寸 四种：small、normal、big、hollow(空心圆)
 * icon-field          自定义图标
 * place-ment     时间戳位置 true:"top"/false:"bottom" 默认为bottom
 */
/**
 * UITimeLine 时间轴
 * @beta
 */
export declare class UITimeLine extends BaseInput {
    /**
     *是否需要反转
     */
    private __reverse;
    /**
     *自定义颜色
     */
    private __colorField;
    /**
     *自定义尺寸
     */
    private __sizeField;
    /**
     *自定义图标
     */
    private __iconField;
    /**
     *时间戳位置
     */
    private __placeMent;
    /**
     * 模板函数
     * @privateRemarks
     */
    template(props: any): string;
    /**
     * 模型
     */
    data(): {
        __evalue: number;
        __flag: boolean;
    };
    /**
     * 更改颜色
     * @param color - 状态类型
     * @returns 自定义背景颜色样式
     */
    private __genStyle;
    /**
     * 自定义大小和图标
     * @param icon - 图标类型
     * @param size - 大小类型
     * @returns 自定义图标、大小样式
    */
    private __genClass;
    /**
     * 进行正序展示
     */
    private __sortAsc;
    /**
     * 进行逆序展示
     */
    private __sortDesc;
}
