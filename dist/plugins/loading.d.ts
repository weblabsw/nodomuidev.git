import { Module } from "nodom3";
/**
 * 配置项：
 *  radius  圈的半径
 *  color   圆点颜色
 */
/**
 * UILoading 加载
 *  @public
 */
declare class UILoading extends Module {
    /**
     * 显示数，每open一次，+1，每close一次，-1。close时检查是否为0，为0则关闭
     */
    private __openCount;
    /**
     * 颜色
     */
    private __color;
    /**
     * 半径
     */
    private __radius;
    /**
     * 模板函数
     * @privateRemarks
     */
    template(props: any): string;
    /**
     * 打开loading
     * @param limit - 停留时间/动画url地址，如果为0或不填，则表示不自动关闭，如果为字符串，则为动画url地址
     * @param imgUrl - 动画url地址
     */
    __show(limit?: number, imgUrl?: string): void;
    /**
     * 关闭loading
     * @param force - 是否强制完全关闭，如果为true，则不计算openCount，直接关闭
     */
    __close(force?: boolean): void;
}
/**
 * 获取加载框类
 * @public
 */
export declare function nuiloading(): UILoading;
export {};
