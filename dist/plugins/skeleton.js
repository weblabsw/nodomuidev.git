import { Module, Nodom } from "nodom3";
/**
 * 参数说明
 * animated:       是否开启动画
 * loading:        是否处于加载状态，false 时展示子组件
 * avater:         是否显示头像
 * title:          是否显示标题
 * text:           是否显示段落
 * row:            段落数量
 * class
 * style
 */
/**
 * UISkeleton 骨架屏
 * @alpha
 */
export class UISkeleton extends Module {
    /**
     * 模板函数
     * @privateRemarks
     */
    template(props) {
        const animated = props.hasOwnProperty('animated') || false;
        const title = props.hasOwnProperty('title') || true;
        const avater = props.hasOwnProperty('avater') || false;
        const text = props.hasOwnProperty('text') || true;
        const rows = props['rows'] ? parseInt(props['rows'] + '') : 3;
        const rightContent = [];
        const leftContent = [];
        if (avater) {
            leftContent.push(`<ui-skeleton-item type='circle' />`);
        }
        if (title) {
            rightContent.push(`<ui-skeleton-item type='title' />`);
        }
        if (text) {
            for (let i = 0; i < rows - 1; i++) {
                rightContent.push(`<ui-skeleton-item type='text' />`);
            }
            rightContent.push(`<ui-skeleton-item type='text' style='width: 65%' />`);
        }
        this.model['loading'] = props['loading'];
        this.setExcludeProps(['loading', 'animated', 'avater', 'title', 'text', 'row']);
        return `
          <div>
            <div ${animated ? "class='ui-skeleton-animated'" : ''} x-if={{loading}}>
              <slot name='skeleton'>
                <div style='display: flex'>
                  <div>
                    ${leftContent.join(' ')}
                  </div>
                  <div style='margin-left: 12px; flex: 1'>
                    ${rightContent.join(' ')}
                  </div>
                </div>
              </slot>
            </div>
            <div x-else>
              <slot />
            </div>
          </div>    
        `;
    }
}
/**
 * 参数说明
 * type:  'title' | 'circle' | 'rect'  | 'text' | 'img';
 * class
 * style
 */
/**
 * UISkeletonItem 骨架屏子组件
 * @alpha
 */
export class UISkeletonItem extends Module {
    /**
     * 模板函数
     * @privateRemarks
     */
    template(props) {
        const type = props['type'] || 'text';
        this.setExcludeProps(['type']);
        return `<span class='ui-skeleton-base ui-skeleton-${type}'></span>`;
    }
}
Nodom.registModule(UISkeleton, 'ui-skeleton');
Nodom.registModule(UISkeletonItem, 'ui-skeleton-item');
//# sourceMappingURL=skeleton.js.map