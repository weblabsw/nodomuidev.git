import { Module } from "nodom3";
/**
 * 参数说明
 * separator              文本默认分隔符
 * configs                功能配置, 控制富文本组件的功能  `Array<string | config>`
 * onChange               内容改变时的回调函数 `(html) => void`
 * class
 * style
 */
/**
 * UIRichEditor 富文本
 * @alpha
 */
export declare class UIRichEditor extends Module {
    data(): {
        configArray: any;
    };
    /**
     * 模板函数
     * @privateRemarks
     */
    template(props?: object): string;
    /**
     * 处理输入
     * @param model - 模型
     * @param dom - 当前节点
     * @param evObj - event object
     * @param e - html event
     */
    private __handleContentInput;
    /**
     * 当按下enter
     * @param model - 模型
     * @param dom - 当前节点
     * @param evObj - event object
     * @param e - html event
     */
    private __handleContentKeydown;
    /**
     * 点击
     * @param model - 模型
     * @param dom - 当前节点
     * @param evObj - event object
     * @param e - html event
     */
    private __handleBarItemClick;
    /**
     * 设置
     */
    __handleContentUp(): void;
}
