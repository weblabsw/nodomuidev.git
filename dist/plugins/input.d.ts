import { BaseInput } from "./baseinput";
/**
 * 配置项
 * field        绑定字段名
 * allow-clear  是否允许清空，无值属性
 * show-pwd     是否允许密码显示和隐藏方式切换，无值属性，type为password时有效
 * name         原生属性，参考input标签
 * type         原生属性，参考input标签，可设置为textarea
 * placeholder  原生属性，参考input标签
 * min          原生属性，参考input标签
 * max          原生属性，参考input标签
 * minlength    原生属性，参考input标签
 * maxlength    原生属性，参考input标签
 * disabled     原生属性，参考input标签
 * readonly     原生属性，参考input标签
 * rows         原生属性，参考textarea标签
 * cols         原生属性，参考textarea标签
 * resize       原生属性，参考textarea标签
 */
/**
 * UIInput 输入框
 * @public
 */
export declare class UIInput extends BaseInput {
    /**
     * 模板函数
     */
    template(props: any): string;
    /**
     * 清除内容
     */
    private __clear;
    /**
     * 显示和隐藏password
     */
    private __seePwd;
}
