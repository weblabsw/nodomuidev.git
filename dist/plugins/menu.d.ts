import { Module } from "nodom3";
/**
 * 数据项
 * $data             菜单结构数据
 * 参数说明
 * vertical:        是否纵向菜单
 * onItemClick:     菜单项点击事件
 * width:           菜单宽度（px，针对纵向菜单或横向菜单二级及以上菜单）,默认150
 * bgcolor:         菜单背景色（可选）
 * color:           菜单文字色（可选）
 * active-bgcolor:   激活菜单项背景色（可选）
 * active-color:     激活菜单项纹紫色（可选）
 */
/**
 * UIMenu 菜单
 * @public
 */
export declare class UIMenu extends Module {
    /**
     * 节点点击事件名
     */
    private __onItemClick;
    /**
     * 菜单宽度
     */
    private __width;
    /**
     * 菜单类型: horizontal vertical，默认vertical
     */
    private __vertical;
    /**
     * 激活dom model
     */
    private __activeModel;
    /**
     * 模板函数
     * @privateRemarks
     */
    template(props?: object): string;
    /**
     * 点击item事件
     * @param model - 当前dom对应model
     * @param dom - virtual dom节点
     * @param evObj - NEvent对象
     * @param e -event对象
     */
    private __clickMenu;
    /**
     * 叶子结点激活
     * @param model - 对应model
     */
    private __setActive;
    /**
     * 计算展开菜单高度
     * @param open - 是否展开
     * @param children - 孩子节点
     * @returns     容器高度
     */
    private __cacHeight;
    /**
     * 生成popmenu style
     * @param model - 对应model
     * @returns     style串
     */
    private __genPopStyle;
    /**
     * 展开关闭节点
     * @param model - 当前dom对应model
     * @param dom - virtual dom节点
     * @param evObj - NEvent对象
     * @param e - event对象
     */
    private __expandMenu;
    /**
     * 关闭子菜单
     * @param model - 当前dom对应model
     */
    private __closeMenu;
    /**
     * 计算位置
     * @param model - 模型
     * @param dom - 当前节点
     * @param evObj - event object
     * @param e - html event
     */
    private __cacLoc;
    /**
     * 渲染前事件
     * @privateRemarks
     */
    onBeforeRender(): void;
}
