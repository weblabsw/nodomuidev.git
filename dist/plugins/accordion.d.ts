import { Module } from 'nodom3';
/**
 * 属性配置
 * single: true/false 是否同时只展开一个
 */
/**
 * UIAccordion 折叠面板
 * @public
 */
export declare class UIAccordion extends Module {
    /**
     * 单选标志
     */
    __single: boolean;
    /**
     * 子item数组
     */
    __items: UIAccordionItem[];
    /**
     * 模板函数html
     * @privateRemarks
     */
    template(props?: object): string;
    /**
     * 添加item
     * @param item	待加入的accordion item
     */
    __addItem(item: UIAccordionItem): void;
}
/**
 * accordion item
 * 属性配置
 * title:	标题栏内容
 * opened: 	是否展开
 */
/**
 * UIAccordionItem 折叠子组件
 * @public
 */
export declare class UIAccordionItem extends Module {
    /**
     * 父accordion
     */
    private __accordion;
    /**
     * 模板函数
     * @privateRemarks
     */
    template(props?: object): string;
    /**
     * 点击事件	展开/折叠
     * @param model - 对应model
     */
    private __clickItem;
}
