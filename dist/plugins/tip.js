import { Module, ModuleFactory, Nodom } from "nodom3";
/**
 * UITip 信息提示
 * @public
 */
export class UITip extends Module {
    /**
     * 模板函数
     * @privateRemarks
     */
    template(props) {
        super.template(props);
        //四个位置
        const arr = ['top', 'right', 'bottom', 'left'];
        //位置item串
        let str = '';
        for (const d of arr) {
            str += `
                <div class='ui-tip ui-tip-${d}'>
                    <for cond={{${d}Data}}
                        x-animationbox e-transitionend='__removeHide'>
                        <div class='ui-tip-wrap'>
                            <div>
                                <div class={{'ui-tip-item' + (theme?(' ui-box-' + theme):'')}} style={{'width:' + width + 'px'}}>
                                    <b x-show={{showIcon}} class={{icon?('ui-icon-' + icon):''}} />
                                    <span class='ui-tip-content'>{{text}}</span>
                                    <b class='ui-tip-close' e-click='__close' x-show={{closable}}/>
                                </div>
                            </div>
                        </div>
                    </for>
                </div>
            `;
        }
        return `
            <div class='ui-tip'>
                ${str}
            </div>
        `;
    }
    /**
     * 模型
     * @privateRemarks
     */
    data() {
        return {
            topData: [],
            rightData: [],
            bottomData: [],
            leftData: []
        };
    }
    /**
     * 关闭
     * @param model - 对应节点的model
     */
    __close(model) {
        model['__open'] = false;
    }
    /**
     * 移除hide item
     * @param model - 当前节点对应model
     * @param dom - virtual dom节点
     * @param evObj - NEvent对象
     * @param e - event对象
     */
    __removeHide(model, dom, evObj, e) {
        if (e.target !== this.getElement(this.domManager.vdomTree.children[0].key))
            return;
        for (const data of ['topData', 'rightData', 'bottomData', 'leftData']) {
            const rows = this.model[data];
            if (!rows) {
                continue;
            }
            for (let i = 0; i < rows.length; i++) {
                if (!rows[i]['__open']) {
                    rows.splice(i--, 1);
                }
            }
        }
    }
}
//注册模块
Nodom.registModule(UITip, 'ui-tip');
/**
 * UITipManager UITip管理器
 * @public
 */
export class UITipManager {
    /**
     * 添加tip
     * @param cfg - tip配置项
     */
    static addTip(cfg) {
        cfg.position || (cfg.position = 'top');
        cfg.theme || (cfg.theme = 'default');
        cfg.id = this.id++;
        const mdl = this.getUITip();
        const rows = mdl.model[cfg.position + 'Data'];
        //排他
        if (cfg.exclusive) {
            rows.splice(0, rows.length);
        }
        cfg.showIcon = cfg.hasOwnProperty('icon');
        cfg['__open'] = true;
        //设置默认width
        cfg.width || (cfg.width = 300);
        //不显示关闭按钮，则设置自动关闭时间
        if (!cfg.closable) {
            //设置关闭时间
            const t = new Date().getTime();
            cfg.closeTime = t + (cfg.timeout || 2000);
            //添加到待关闭队列
            this.tobeClosed.push(cfg);
            //排序
            this.tobeClosed.sort((a, b) => { return a.closeTime < b.closeTime ? -1 : 1; });
            //设置延迟清理
            setTimeout(() => { UITipManager.removeTip(); }, (this.tobeClosed[0].closeTime - t));
        }
        rows.push(cfg);
    }
    /**
     * 移除tip
     */
    static removeTip() {
        const mdl = this.getUITip();
        if (!mdl) {
            return;
        }
        if (this.tobeClosed.length === 0) {
            return;
        }
        const t = new Date().getTime();
        //第一个时间不超时，则后续不超时
        if (this.tobeClosed[0].closeTime > t) {
            return;
        }
        for (let i = 0; i < this.tobeClosed.length; i++) {
            const d = this.tobeClosed[i];
            //从待关闭队列移除
            this.tobeClosed.splice(i--, 1);
            const rows = mdl.model[d.position + 'Data'];
            //从uitip数据移除
            const ind = rows.findIndex(item => item.id === d.id);
            if (ind !== -1) {
                rows[ind]['__open'] = false;
            }
        }
        if (this.tobeClosed.length > 0) {
            //设置延迟清理
            setTimeout(() => { UITipManager.removeTip(); }, (this.tobeClosed[0].closeTime - t));
        }
    }
    /**
     * 获取uiTip插件
     * @returns     uiTip插件
     */
    static getUITip() {
        const root = ModuleFactory.getMain();
        for (const mid of root.children) {
            const m = ModuleFactory.get(mid);
            if (m instanceof UITip) {
                return m;
            }
        }
        return null;
    }
}
/**
 * 待关闭tip数组
 */
UITipManager.tobeClosed = [];
/**
 * tip id
 */
UITipManager.id = 0;
/**
 * 暴露tip函数
 * @param cfg -tip 配置
 * @public
 */
export function nuitip(cfg) {
    UITipManager.addTip(cfg);
}
//# sourceMappingURL=tip.js.map