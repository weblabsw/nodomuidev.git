/**
 * 校验器类
 * @public
 */
export declare class UIValidator {
    /**
     * 自定义验证信息
     */
    static msgs: {};
    private static rules;
    /**
     * 验证方法
     * @param type -    类型（参考rules）
     * @param value -   验证值
     * @param args -    参数
     * @returns  字符串: 验证错误，undefined: 验证通过，null: 不存在此验证类型
     */
    static verify(type: string, value: unknown, ...args: any[]): any;
    /**
     * 添加校验器
     * @param type -      验证类型
     * @param handler -   验证方法(参数顺序为value,p1,p2,p3,p4)
     * @param msg -       验证失败消息，支持\{0..3\}传递参数p1,p2,p3,p4
     */
    static addValidator(type: any, handler: any, msg: any): void;
    /**
     * 转换消息
     * @param type - 消息类型
     * @param v1 - 参数1
     * @param v2 - 参数2
     * @returns
     */
    static getMsg(type: any, v1?: any, v2?: any): string;
    /**
     * 检查是否为空串
     * @param value - 检测值
     * @returns
     */
    private static checkEmpty;
    /**
     * 检测是否在range范围
     * @param value - 检测值
     * @returns
     */
    private static checkRange;
}
