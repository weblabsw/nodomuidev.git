<!-- Do not edit this file. It is automatically generated by API Documenter. -->

[Home](./index.md) &gt; [nodomui](./nodomui.md) &gt; [UITool](./nodomui.uitool.md) &gt; [getRealPos](./nodomui.uitool.getrealpos.md)

## UITool.getRealPos() method

获取实际位置

**Signature:**

```typescript
static getRealPos(el: HTMLElement): number[];
```

## Parameters

|  Parameter | Type | Description |
|  --- | --- | --- |
|  el | HTMLElement | 待计算的element |

**Returns:**

number\[\]

\[x坐标,y坐标\]

