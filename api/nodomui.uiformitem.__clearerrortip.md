<!-- Do not edit this file. It is automatically generated by API Documenter. -->

[Home](./index.md) &gt; [nodomui](./nodomui.md) &gt; [UIFormItem](./nodomui.uiformitem.md) &gt; [\_\_clearErrorTip](./nodomui.uiformitem.__clearerrortip.md)

## UIFormItem.\_\_clearErrorTip() method

清除错误提示

**Signature:**

```typescript
__clearErrorTip(): void;
```
**Returns:**

void

