<!-- Do not edit this file. It is automatically generated by API Documenter. -->

[Home](./index.md) &gt; [nodomui](./nodomui.md) &gt; [UIForm](./nodomui.uiform.md) &gt; [\_\_verify](./nodomui.uiform.__verify.md)

## UIForm.\_\_verify() method

校验

**Signature:**

```typescript
__verify(): boolean;
```
**Returns:**

boolean

