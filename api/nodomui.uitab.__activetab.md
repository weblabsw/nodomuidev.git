<!-- Do not edit this file. It is automatically generated by API Documenter. -->

[Home](./index.md) &gt; [nodomui](./nodomui.md) &gt; [UITab](./nodomui.uitab.md) &gt; [\_\_activeTab](./nodomui.uitab.__activetab.md)

## UITab.\_\_activeTab() method

激活新tab

**Signature:**

```typescript
__activeTab(data: TabItem | string | number): void;
```

## Parameters

|  Parameter | Type | Description |
|  --- | --- | --- |
|  data | [TabItem](./nodomui.tabitem.md) \| string \| number | tab config对象 或 title 或index |

**Returns:**

void

