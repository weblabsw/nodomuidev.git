<!-- Do not edit this file. It is automatically generated by API Documenter. -->

[Home](./index.md) &gt; [nodomui](./nodomui.md) &gt; [UIMenu](./nodomui.uimenu.md) &gt; [template](./nodomui.uimenu.template.md)

## UIMenu.template() method

模板函数

**Signature:**

```typescript
template(props?: object): string;
```

## Parameters

|  Parameter | Type | Description |
|  --- | --- | --- |
|  props | object | _(Optional)_ |

**Returns:**

string

