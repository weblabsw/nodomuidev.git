<!-- Do not edit this file. It is automatically generated by API Documenter. -->

[Home](./index.md) &gt; [nodomui](./nodomui.md) &gt; [UIMessageBox](./nodomui.uimessagebox.md)

## UIMessageBox class

UIMessageBox 消息框

**Signature:**

```typescript
export declare class UIMessageBox extends Module 
```
**Extends:** Module

## Properties

|  Property | Modifiers | Type | Description |
|  --- | --- | --- | --- |
|  [\_\_buttons](./nodomui.uimessagebox.__buttons.md) |  | UIMessageButton\[\] | 应用组件的按钮 |

## Methods

|  Method | Modifiers | Description |
|  --- | --- | --- |
|  [\_\_checkClose()](./nodomui.uimessagebox.__checkclose.md) |  | 检查关闭状态（transition结束后） |
|  [\_\_close()](./nodomui.uimessagebox.__close.md) |  | 关闭窗口 |
|  [data()](./nodomui.uimessagebox.data.md) |  | 模型 |
|  [template()](./nodomui.uimessagebox.template.md) |  | 模板函数 |

