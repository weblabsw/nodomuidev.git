<!-- Do not edit this file. It is automatically generated by API Documenter. -->

[Home](./index.md) &gt; [nodomui](./nodomui.md) &gt; [UICheckbox](./nodomui.uicheckbox.md) &gt; [\_\_clickCheck](./nodomui.uicheckbox.__clickcheck.md)

## UICheckbox.\_\_clickCheck() method

点击事件

**Signature:**

```typescript
__clickCheck(model: any, dom: any, evObj: any, e: any): void;
```

## Parameters

|  Parameter | Type | Description |
|  --- | --- | --- |
|  model | any | 当前节点对应model |
|  dom | any | virtual dom节点 |
|  evObj | any | NEvent对象 |
|  e | any | event对象 |

**Returns:**

void

