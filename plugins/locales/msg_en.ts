/*
 * 消息js文件 英文文件
 */
export const NodomUIMessage_en = {
    uploadingText:'uploading...',
    pleaseSelect:'please select...',
    ok:'ok',
    cancel:'cancel',
    //date
    sunday:'sun',
    monday:'mon',
    tuesday:'tue',
    wednesday:'wed',
    thursday:'thu',
    friday:'fri',
    saturday:'sat',
    today:'today',

    //pagination
    total:'total',
    totalUnit:'',
    pagePre:'page',
    page:'',
    pageSize:'/page',
    noData:'no data',
    //表单验证
    form:{
        type: "{0} is invalid",
        unknown: "input error",
        require: "not allow empty",
        number:"is not a number",
        min: "min value is {0}",
        max: "max value is {0}",
        between:"value must between {0}-{1}",
        maxLength:"max length is {0}",
        minLength:"min length is {0}",
        betweenLength:'input length must between {0}-{1}',
        email:'invalid email',
        url:'invalid url',
        mobile:'invalid phone no',
        date:"invalid date,ex: 2022-01-01",
        time:"invalid time,ex: 12:30:30",
        datetime:"invalid date time,ex:2022-01-01",
        idno:'input 18 bit IDNo',
        range:'second value must be equal or bigger than first'
    }
}
