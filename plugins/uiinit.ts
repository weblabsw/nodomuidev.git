import { Nodom, RenderedDom, Module} from "nodom3";

/**
 * animation box 指令
 * value: 起始位置
 * top-left:左上
 * top-center: 中上
 * top-right:右上
 * center:中心
 * bottom-left:左下
 * bottom-center:中下
 * bottom-right:右下
 */

/**
 * animation box 指令
 * @public
 */
 Nodom.createDirective(
    'animationbox',
    function(module: Module, dom: RenderedDom){
        const cls = this.value==='horizontal'?'ui-expand-horizontal':'ui-expand-vertical';
        if(dom.props['class']){
            dom.props['class'] += ' ' + cls;
        }else{
            dom.props['class'] = cls;
        }
        //默认控制数据项为__open
        const field = dom.props['field'] || '__open';
        //延迟设置高度
        setTimeout(()=>{
            const el = <HTMLElement>dom.node;
            if(!el){
                return;
            }
            const chd = <HTMLElement>el.children[0];
            if(el){
                if(this.value === 'horizontal'){
                    if(dom.model[field]){
                        el.style.width = chd.offsetWidth + 'px';
                        el.style.height = chd.offsetHeight + 'px';
                    }else{
                        el.style.width = '0px';
                    }
                }else{
                    if(dom.model[field]){
                        el.style.height = chd.offsetHeight + 'px';
                    }else{
                        el.style.height = '0px';
                    }
                }
            }
        },0);
        return true;
    },
    10
);