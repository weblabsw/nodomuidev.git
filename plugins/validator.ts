import { NodomUI } from "./nodomui";

/**
 * 校验器类
 * @public
 */
export class UIValidator {
    /**
     * 自定义验证信息
     */
    public static msgs = {}
    //验证规则
    private static rules = {
        require(value) {
            if (UIValidator.checkEmpty(value)) {
                return UIValidator.getMsg('require');
            }
        },
        number(value) {
            if (UIValidator.checkEmpty(value)) {
                return;
            }
            if (isNaN(Number(value))) {
                return UIValidator.getMsg('number');;
            }
        },
        min(value, v1) {
            if (UIValidator.checkEmpty(value)) {
                return;
            }
            if (value && Number(value) < v1) {
                return UIValidator.getMsg('min', v1);
            }
        },
        max(value, v1) {
            if (UIValidator.checkEmpty(value)) {
                return;
            }
            if (value && Number(value) > v1) {
                return UIValidator.getMsg('max', v1);
            }
        },
        between(value, v1, v2) {
            if (UIValidator.checkEmpty(value)) {
                return;
            }
            if (value) {
                const num = Number(value);
                if (num < v1 || num > v2) {
                    return UIValidator.getMsg('between', v1, v2);
                }
            }
        },
        minLength(value, v1) {
            if (UIValidator.checkEmpty(value)) {
                return;
            }
            if (value && value.length < v1) {
                return UIValidator.getMsg('minLength', v1);
            }
        },
        maxLength(value, v1) {
            if (UIValidator.checkEmpty(value)) {
                return;
            }
            if (value && value.length > v1) {
                return UIValidator.getMsg('maxLength', v1);
            }
        },
        betweenLength(value, v1, v2) {
            if (UIValidator.checkEmpty(value)) {
                return;
            }
            if (value && value.length < v1 || value.length > v2) {
                return UIValidator.getMsg('betweenLength', v1, v2);
            }
        },
        email(value) {
            if (UIValidator.checkEmpty(value)) {
                return;
            }
            if (!/^\w+\S*@[\w\d]+(\.\w+)+$/.test(value)) {
                return UIValidator.getMsg('email');
            }
        },
        url(value) {
            if (UIValidator.checkEmpty(value)) {
                return;
            }
            if (!/^(https?|ftp):\/\/[\w\d]+\..*/.test(value)) {
                return UIValidator.getMsg('url');
            }
        },
        mobile(value) {
            if (UIValidator.checkEmpty(value)) {
                return;
            }
            if (!/^1[3-9]\d{9}$/.test(value)) {
                return UIValidator.getMsg('mobile');
            }
        },
        date(value) {
            if (UIValidator.checkEmpty(value)) {
                return;
            }
            if (!/^\d{4}[-\/](0[1-9]|1[0-2])[-\/](0[1-9]|[12]\d|3[01])$/.test(value)) {
                return UIValidator.getMsg('date');
            }
        },
        datetime(value) {
            if (UIValidator.checkEmpty(value)) {
                return;
            }
            if (!/^\d{4}[-\/](0[1-9]|1[0-2])[-\/](0[1-9]|[12]\d|3[01])\s+([0-1]?[0-9]|2[0-3]):([0-5][0-9]):([0-5][0-9])$/.test(value)) {
                return UIValidator.getMsg('datetime');
            }
        },
        time(value) {
            if (UIValidator.checkEmpty(value)) {
                return;
            }
            if (!/([0-1]?[0-9]|2[0-3]):([0-5][0-9]):([0-5][0-9])$/.test(value)) {
                return UIValidator.getMsg('time');
            }
        },
        idno(value) {
            if (UIValidator.checkEmpty(value)) {
                return;
            }
            if (!/^[1-9]\d{7}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])\d{3}$|^[1-9]\d{5}[1-9]\d{3}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])\d{3}([0-9]|X)$/.test(value)) {
                return UIValidator.getMsg('idno');
            }
        },
        range(value) {
            if (UIValidator.checkRange(value)) {
                return;
            }
            return UIValidator.getMsg('range');
        }
    }

    /**
     * 验证方法
     * @param type -    类型（参考rules）
     * @param value -   验证值
     * @param args -    参数
     * @returns  字符串: 验证错误，undefined: 验证通过，null: 不存在此验证类型
     */
    public static verify(type: string, value: unknown, ...args) {
        if (UIValidator.rules[type]) {
            return UIValidator.rules[type].apply(null, [value, ...args]);
        }
        return null;
    }

    /**
     * 添加校验器
     * @param type -      验证类型
     * @param handler -   验证方法(参数顺序为value,p1,p2,p3,p4)
     * @param msg -       验证失败消息，支持\{0..3\}传递参数p1,p2,p3,p4
     */
    public static addValidator(type, handler, msg) {
        UIValidator.msgs[type] = msg;
        UIValidator.rules[type] = handler;
    }

    /**
     * 转换消息
     * @param type - 消息类型
     * @param v1 - 参数1
     * @param v2 - 参数2
     * @returns 
     */
    public static getMsg(type, v1?, v2?): string {
        let v = NodomUI.getText('form.' + type, v1, v2);
        //如果没从通用类型中找到，则从自定义中查找
        if (!v) {
            v = this.msgs[type];
        }
        return v;
    }

    /**
     * 检查是否为空串
     * @param value - 检测值
     * @returns 
     */
    private static checkEmpty(value): boolean {
        return value === undefined || value === null || value === "" || Array.isArray(value) && value.length === 0;
    }

    /**
     * 检测是否在range范围
     * @param value - 检测值
     * @returns 
     */
    private static checkRange(value): boolean {
        if (!value || !Array.isArray(value)) {
            return true;
        }
        if (typeof value[0] === 'string' && value[0].indexOf('-') === -1 && value[0].indexOf(':') === -1) {
            value[0] = Number(value[0]);
        }

        if (typeof value[1] === 'string' && value[1].indexOf('-') === -1 && value[1].indexOf(':') === -1) {
            value[1] = Number(value[1]);
        }

        return value.length === 2 && value[0] <= value[1];
    }
}