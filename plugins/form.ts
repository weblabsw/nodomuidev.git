import { Nodom, Module, ModuleFactory} from "nodom3";
import { UIValidator } from "./validator";

/**
 * form 插件
 * 配置项:
 * label-width   label所占宽度
 * unit-width    单位(m,m/s,...)所占宽度
 */

/**
 * UIForm 表单
 * @public
 */
export class UIForm extends Module{
    /**
     * label宽度
     */
    public __labelWidth:string;

    /**
     * 单位框宽度
     */
    public __unitWidth:string;

    /**
     * 子item数组
     */
    private __formItems:UIFormItem[] = [];

    /**
     * 模板函数
     * @privateRemarks
     */
    template(props?: object): string {
        this.__labelWidth = props['label-width'] || '100px';
        this.__unitWidth = props['unit-width'] || undefined;
        this.setExcludeProps(['label-width','unit-width']);
        return `
            <div class='ui-form'>
                <slot />
            </div>
        `;  
    }

    /**
     * 添加form item
     * @param item -  form item
     * @returns 
     */
    __addItem(item:UIFormItem){
        if(this.__formItems.includes(item)){
            return;
        }
        this.__formItems.push(item);
    }

    /**
     * 校验
     */
    __verify(){
        let r = true;
        for(const item of this.__formItems){
            if(!item.__verify()){
                r = false;
            }
        }
        return r;
    }

    /**
     * 清除错误提示
     */
    public __clearErrorTips(){
        for(const item of this.__formItems){
            item.__clearErrorTip();
        }
    }
}

/**
 * form item
 * 配置项
 *  label：     输入项label
 *  field：     绑定字段
 *  required：  是否必填
 *  invalid-msg: 校验失败消息，其中内置校验器有自己的消息，method方式也会返回错误消息，但是设置了invalidMsg，则其它消息失效
 *  validator： 校验器，支持内置校验器(参见UIValidator.rules)，regexp(正则表达式)，method(模块方法校验)
 *              使用方式 validator='validatorName:param1:param2'，其中validatorName为内置校验器名，regexp和method
 *              param1,param2为校验器参数，可选
 *              检验器示例:
 *                  between:1:10            校验输入项是否为1-10之间
 *                  method:check            通过模块的check方法进行校验，check参数为(value,model)，其中value为对应项值，model为对应model
 *                  regexp:`^[a-z]{5,20}$`  通过正则表达式进行校验，校验数据项是否为 5-20个小写字母
 */

/**
 * UIFormItem 表单子项组件
 * @public
 */
export class UIFormItem extends Module{
    /**
     * 校验器
     */
    private __validator:{type:string,msg?:string,reg?:RegExp,method?:string,params?:unknown[]}[];

    /**
     * 绑定字段
     */
    private __field:string;
    /**
     * 模板函数
     * @privateRemarks
     */
    template(props?: object): string {
        const parent:UIForm = <UIForm>ModuleFactory.get(this.srcDom.slotModuleId);
        const itemStyle = props['item-width']?"style='width:" +  props['item-width'] + "'":'';
        const labelStyle = "style='width:" +  parent.__labelWidth + "'"
        this.__field = props['field'];
        this.__addValidator(props);

        this.setExcludeProps(['unit','field','required','validator','invalid-msg','label']);
        //单位串
        const unitStyle = parent.__unitWidth?`style='width:${parent.__unitWidth}'`:undefined;
        let unitStr='';
        if(props['unit']){
            unitStr = `<span class='ui-form-item-unit' ${unitStyle}>${props['unit']}</span>`;
        }else if(unitStyle){
            unitStr = `<span class='ui-form-item-unit' ${unitStyle}></span>`;
        }
        //验证串，需要设置margin-left宽度，保证与输入框对齐
        const validStr = this.__validator.length>0?`
            <div class='ui-form-item-tip' x-show={{__errorMessage!==undefined}} style='margin-left:${parent.__labelWidth}'>
                <b class='ui-form-item-tip-icon' />
                <span class='ui-form-item-tip-text'>{{__errorMessage}}</span>
            </div>`:'';
        return `
            <div class='ui-col ui-form-item'>
                <div class={{'ui-form-item-content' + (__errorMessage?' ui-form-item-error':'')}} ${itemStyle}>
                    <label ${labelStyle}>${props['label']?props['label']:''} </label>
                    <slot/>
                    ${unitStr}
                </div>
                ${validStr}
            </div>
        `;
    }
   /**
     * 添加校验器
     * @param props -  属性
     */
    __addValidator(props){
        let firstAdd = false;
        //校验器已存在，则不再添加
        if(!this.__validator){
            firstAdd = true;
            this.__validator = [];
        }
        const validArr = this.__validator;
        
        if(this.__field){
            //添加require
            if(props.hasOwnProperty('required') && !validArr.find(item=>item.type==='require')){
                validArr.push({type:'require'});
            }
            if(props.validator){
                const item = props.validator;
                const ind = item.indexOf(':');
                if(ind !== -1){
                    const type = item.substring(0,ind);
                    const params = [];
                    if(!validArr.find(ii=>ii.type===type)){
                        //需要处理数字
                        if(['min','max','between','minLength','maxLength','betweenLength'].includes(type)){
                            const pa = item.substring(ind+1).split(':');
                            for(const p of pa){
                                params.push(parseInt(p));
                            }
                            validArr.push({type:type,params:params,msg:props.invalidMsg});
                        }else if('regexp' === type){ //正则表达式
                            validArr.push({
                                type:type,
                                reg:new RegExp(item.substring(ind+1)),
                                msg:props.invalidMsg 
                            });
                        }else if('method' === type){
                            validArr.push({
                                type:type,
                                method:item.substring(ind+1),
                                msg:props.invalidMsg
                            });
                        }
                    }
                }else{
                    if(!validArr.find(ii=>ii.type===item)){
                        validArr.push({type:item,msg:props.invalidMsg});
                    }
                }
            }
        }
        
        //第一次需要监听
        if(firstAdd && validArr.length !== 0){
            //添加监听
            this.watch(this.srcDom.model,this.__field,(m,k,ov,nv)=>{
                this.__verify(nv);
            });
        }
    }

    /**
     * 校验
     * @param value - 值
     * @returns         true 通过 false 失败
     */
    public __verify(value?:unknown):boolean{
        if(this.__validator.length===0){
            return true;
        }
        value = value || this.get(this.srcDom.model,this.__field);
        let msg,r;
        for(const v of this.__validator){
            if(v.type === 'regexp'){
                if(!v.reg.test(<string>value)){
                    msg = v.msg || UIValidator.getMsg('unknown');
                    break;
                }
            }else if(v.type === 'method'){
                r = this.invokeMethod(v.method,value,this.srcDom.model);
                if(r){
                    msg = v.msg || r;
                    break;
                }
            }else {
                if(v.params){
                    r = UIValidator.verify(v.type,value,...v.params);
                }else{
                    r = UIValidator.verify(v.type,value);
                }
                if(r){
                    msg = v.msg || r;
                    break;
                }
            }
        }
        this.model['__errorMessage'] = msg;
        return msg === undefined;
    }

    /**
     * 清除错误提示
     */
    public __clearErrorTip(){
        this.model['__errorMessage'] = undefined;
    }
    /**
     * 第一次渲染前事件
     * @privateRemarks
     */
    onBeforeFirstRender(){
        (<UIForm>ModuleFactory.get(this.srcDom.slotModuleId)).__addItem(this);
    }
}
Nodom.registModule(UIForm,'ui-form');
Nodom.registModule(UIFormItem,'ui-form-item');