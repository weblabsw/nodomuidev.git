import { Module, Nodom } from "nodom3";
import { UITool } from "./uibase";

const formatBlock = 'formatBlock'
const queryCommandState = command => document.queryCommandState(command)
const queryCommandValue = command => document.queryCommandValue(command)
const exec = (command, value = null) => document.execCommand(command, false, value)

/**
 * 参数说明
 * separator              文本默认分隔符
 * configs                功能配置, 控制富文本组件的功能  `Array<string | config>`
 * onChange               内容改变时的回调函数 `(html) => void`
 * class
 * style
 */

/**
 * UIRichEditor 富文本
 * @alpha
 */
export class UIRichEditor extends Module {
  data() {
    return {
      configArray: null,
    };
  }
  /**
   * 模板函数
   * @privateRemarks
   */
  template(props?: object): string {
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    const { configs, class: ca, ...others } = props;

    const defaultConfig = {
      bold: {
        icon: '<b>B</b>',
        title: '粗体',
        state: () => queryCommandState('bold'),
        result: () => exec('bold')
      },
      italic: {
        icon: '<i>I</i>',
        title: '斜体',
        state: () => queryCommandState('italic'),
        result: () => exec('italic')
      },
      underline: {
        icon: '<u>U</u>',
        title: '下划线',
        state: () => queryCommandState('underline'),
        result: () => exec('underline')
      },
      strikethrough: {
        icon: '<strike>S</strike>',
        title: '删除线',
        state: () => queryCommandState('strikeThrough'),
        result: () => exec('strikeThrough')
      },
      heading1: {
        icon: '<b>H<sub>1</sub></b>',
        title: '一级标题',
        result: () => exec(formatBlock, '<h1>')
      },
      heading2: {
        icon: '<b>H<sub>2</sub></b>',
        title: '二级标题',
        result: () => exec(formatBlock, '<h2>')
      },
      heading3: {
        icon: '<b>H<sub>3</sub></b>',
        title: '三级标题',
        result: () => exec(formatBlock, '<h3>')
      },
      paragraph: {
        icon: 'P',
        title: '段落',
        result: () => exec(formatBlock, '<p>')
      },
      quote: {
        icon: '“ ”',
        title: '引用',
        result: () => exec(formatBlock, '<blockquote>')
      },
      olist: {
        icon: '#',
        title: '有序列表',
        result: () => exec('insertOrderedList')
      },
      ulist: {
        icon: '<b>·</b>',
        title: '无序列表',
        result: () => exec('insertUnorderedList')
      },
      code: {
        icon: '代码',
        title: '代码块',
        result: () => exec(formatBlock, '<pre>')
      },
      line: {
        icon: '<b>—</b>',
        title: '分割线',
        result: () => exec('insertHorizontalRule')
      },
      link: {
        icon: '🔗',
        title: '插入链接',
        result: () => {
          const url = window.prompt('请输入链接的URL')
          if (url) exec('createLink', url)
        }
      },
      image: {
        icon: '📷',
        title: '插入图片',
        result: () => {
          const url = window.prompt('请输入图片的URL')
          if (url) exec('insertImage', url)
        }
      }
    }

    if (this.model['configArray'] === null) {
      this.model['configArray'] = configs ? configs.map(config => {
        if (typeof config === 'string') {
          return { ...defaultConfig[config], select: false };
        } else if (defaultConfig[config.name]) {
          return { ...defaultConfig[config.name], ...config, select: false };
        } else {
          return { ...config, select: false };
        }
      }) : Object.keys(defaultConfig).map(config => ({ ...defaultConfig[config], select: false }));;
    }

    const renderToolBar = () => {
      return this.model['configArray'].map((config, index) =>
        `<button
          type='button'
          class={{'ui-rich-editor-button${config.select ? " ui-rich-editor-button-selected" : ""}'}}
          title='${config.title}'
          id='${index}'
          e-click='__handleBarItemClick'
        >${config.icon}</button>`).join(' ');
    }

    this.setExcludeProps(['separator', 'configs', 'onChange']);
    return `<div
      class='ui-rich-editor ${ca || ""}'
      ${UITool.toAttrString(others)}
    >
      <div class='ui-rich-editor-toolbar'>
        ${renderToolBar()}
      </div>
      <div
        class='ui-rich-editor-content'
        contenteditable='true'
        e-input='__handleContentInput'
        e-keydown='__handleContentKeydown'
        e-keyup='__handleContentUp'
        e-mouseup='__handleContentUp'
      ></div>
    </div>`;
  }

  /**
   * 处理输入
   * @param model - 模型
   * @param dom - 当前节点
   * @param evObj - event object
   * @param e - html event
   */
  private __handleContentInput(model,dom,evObj,e) {
    const separator = this.props['separator'] || 'div';
    const onChange = this.props['onchange'];
    const contentDom = e.target;
    const firstChild = e.target.firstChild;
    if (firstChild && firstChild.nodeType === 3) exec(formatBlock, `<${separator}>`);
    else if (contentDom.innerHTML === '<br>') contentDom.innerHTML = '';
    onChange && onChange(contentDom.innerHTML);
  }
  /**
   * 当按下enter
   * @param model - 模型
   * @param dom - 当前节点
   * @param evObj - event object
   * @param e - html event
   */
  private __handleContentKeydown(model,dom,evObj,e) {
    const separator = this.props['separator'] || 'div';

    if (e.key === 'Enter' && queryCommandValue(formatBlock) === 'blockquote') {
      setTimeout(() => exec(formatBlock, `<${separator}>`), 0);
    }
  }
  /**
   * 点击
   * @param model - 模型
   * @param dom - 当前节点
   * @param evObj - event object
   * @param e - html event
   */
  private __handleBarItemClick(model, dom, evObj, e) {
    const config = this.model['configArray'][dom.props.id];
    config.result() && e.target.parentNode.parentNode.children[1].focus();
    if (config.state) {
      config.select = config.state();
    }
  }

  /**
   * 设置
   */
  __handleContentUp() {
    for (const config of this.model['configArray']) {
      if (config.state) {
        config.select = config.state();
      }
    }
  }
}

Nodom.registModule(UIRichEditor, 'ui-rich-editor');