import { Module,Nodom} from "nodom3";

/**
 * 配置参数
 *  vertical    是否为纵向分布，默认false（横向分布）
 */

/**
 * UILayout 布局
 *  @public
 */
export class UILayout extends Module{
    template(props?: object): string {
        this.setExcludeProps(['vertical']);
        return `
            <div class="ui-layout ${props.hasOwnProperty('vertical')?'ui-layout-vert':'ui-layout-hori'}" >
                <slot/>
            </div>
        `;
    }
}

/**
 * UILayoutTop 顶部布局
 * @public
 */
export class UILayoutTop extends Module{
    template(): string {
        return `
            <div class='ui-layout-top'>
                <slot/>
            </div>
        `;
    }
}

/**
 * UILayoutLeft 左侧布局
 * 必须设置宽度
 * @public
 */
export class UILayoutLeft extends Module{
    /**
     * 模板函数
     * @privateRemarks
     */
    template(props?: object): string {
        const sizeStr = props.hasOwnProperty('sizable')?`
            <div class='ui-layout-spliter' 
                e-mousedown='__dragStart'  
            ></div>`:"";
        return `
            <div class='ui-layout-left'>
                <div class='ui-layout-content'><slot/></div>
                ${sizeStr}
            </div>
        `;
    }
    
    /**
     * 拖动开始事件
     * @param model - 当前节点对应model
     * @param dom - virtual dom节点
     * @param evObj - NEvent对象
     * @param e - event对象
     */
    private __dragStart(model,dom,evObj,e){
        e.preventDefault();
        const el = e.currentTarget.parentElement;
        const width = el.offsetWidth;
        const initX = e.pageX;
        //添加mousemove事件
        window.addEventListener('mousemove',moveEvent);
        //添加mouseup事件
        window.addEventListener('mouseup',upEvent);
        /**
         * mousemove事件钩子
         * @param e - 鼠标事件
         */
        function moveEvent(e){
            el.style.width = (e.pageX-initX + width) + 'px';
        }
        /**
         * mouseup 事件钩子
         */
        function upEvent(){
            window.removeEventListener('mousemove',moveEvent);
            window.removeEventListener('mouseup',upEvent);
        }
    }
}

/**
 * UILayoutCenter 中心布局
 * @public
 */
export class UILayoutCenter extends Module{
    /**
     * 模板函数
     * @privateRemarks
     */
    template(): string {
        return `
            <div class='ui-layout-center'>
                <slot/>
            </div>
        `;
    }
}

/**
 * UILayoutRight 右部布局
 * @public
 */
export class UILayoutRight extends Module{
    /**
     * 模板函数
     * @privateRemarks
     */
    template(props?: object): string {
        const sizeStr = props.hasOwnProperty('sizable')?`
            <div class='ui-layout-spliter' 
                e-mousedown='__dragStart'  
            ></div>`:"";
        return `
            <div class='ui-layout-right'>
                ${sizeStr}
                <div class='ui-layout-content'><slot/></div>
            </div>
        `
    }
    /**
     * 拖动开始事件
     * @param model -     模型
     * @param dom -       dom节点
     * @param evObj -     nodom event对象
     * @param event -     html event对象 
     */
    private __dragStart(model,dom,evObj,event){
        event.preventDefault();
        const el = event.currentTarget.parentElement;
        const width = el.offsetWidth;
        const initX = event.pageX;
        //添加mousemove事件
        window.addEventListener('mousemove',moveEvent);
        //添加mouseup事件
        window.addEventListener('mouseup',upEvent);
        /**
         * mousemove事件钩子
         * @param e - 鼠标事件
         */
        function moveEvent(e){
            el.style.width = (initX-e.pageX + width) + 'px';
        }
        /**
         * mouseup 事件狗子
         */
        function upEvent(){
            window.removeEventListener('mousemove',moveEvent);
            window.removeEventListener('mouseup',upEvent);
        }
    }
}

/**
 * UILayoutBottom 底部布局
 * @public
 */
export class UILayoutBottom extends Module{
    /**
     * 模板函数
     * @privateRemarks
     */
    template(): string {
        return `
            <div class='ui-layout-bottom'>
                <slot/>
            </div>
        `;
    }
}
//注册模块
Nodom.registModule(UILayout,'ui-layout');
Nodom.registModule(UILayoutTop,'ui-layout-top');
Nodom.registModule(UILayoutLeft,'ui-layout-left');
Nodom.registModule(UILayoutCenter,'ui-layout-center');
Nodom.registModule(UILayoutRight,'ui-layout-right');
Nodom.registModule(UILayoutBottom,'ui-layout-bottom');