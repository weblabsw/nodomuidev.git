
import {Module, ModuleFactory, Nodom} from "nodom3";
import { UITool } from "./uibase";

/**
 * TabItem 配置接口类型
 * @public
 */
export type TabItem = {
    /**
     * tab标题
     */
    title:string;
    /**
     * 是否可关闭
     */
    closable?:boolean;
    /**
     * 是否激活
     */
    active?:boolean;

    /**
     * tab item 插件
     */
    tab:UITabItem;
}

/**
 * bgcolor:         默认背景色
 * color:           默认前景色
 * active-bgcolor:  激活背景色
 * active-color:    激活前景色
 * onTabChange:     tab页切换钩子函数，页面切换时调用
 */

/**
 * UITab 标签页
 * @public
 */
export class UITab extends Module{
    /**
     * 当前tab配置项
     */
    private __currentTab:TabItem;

    /**
     * 拖动参数
     */
    private __dragParam = {dx:0,x:0};

    /**
     * tab 关闭事件
     */
    private __onTabClose:string;

    /**
     * tab 切换事件
     */
    private __onTabChange:string;
    /**
     * 模板函数
     * @privateRemarks
     */
    template(props){
        this.__onTabClose = props.ontabclose;
        this.__onTabChange = props.ontabchange;
        let tabStyle = '';
        let activeStyle = '';
        if(props['active-bgcolor']){
            activeStyle += 'background-color:' + props['active-bgcolor'];
        }
        //激活前景色，如果没设置，但是设置了默认前景色，则使用默认前景色
        const ac = props['active-color'] || props.color;
        if(ac){
            if(activeStyle !== ''){
                activeStyle += ';';
            }
            activeStyle += 'color:' + ac;
        }
        let defaultStyle = '';
        if(props.bgcolor){
            defaultStyle += 'background-color:' + props.bgcolor;
        }
        if(props.color){
            if(defaultStyle !== ''){
                defaultStyle += ';';
            }
            defaultStyle += 'color:' + props.color;
        }
        if(activeStyle){
            tabStyle = `style={{active?'${activeStyle}':'${defaultStyle}'}}`;
        }else if(defaultStyle){
            tabStyle=`style={{!active?'${props.defaultstyle}':''}}`;
        }
        const headBg = props.bgcolor?"style='background-color:" + props.bgcolor + "'" :'';
        this.setExcludeProps(['activebgcolor','activecolor','bgcolor','color','ontabchange','ontabclose']);
        return `
            <div class='ui-tab'>
                <div class='ui-tab-headct' ${headBg} >
                    <div class='ui-tab-head' e-drag='__dragHead' e-mousedown='__dragStart' e-mousemove='__drag' e-mouseup='__dragEnd' e-mouseleave='__dragEnd'>
                        <for cond={{tabs}} index='idx' class={{'ui-tab-item' + (active?' ui-tab-item-active':'')}} e-click='__clickTab' ${tabStyle}>
                            {{title}}
                            <b class="ui-tab-close" x-show={{closable}} e-click='__closeTab:nopopo'/>
                        </for>
                    </div>
                </div>
                <div class='ui-tab-body'>
                    <slot />
                </div>
            </div>
        `;
    }
    /**
     * 模型
     * @privateRemarks
     */
    data() {
        return {
            tabs:[]
        };
    }

    /**
     * 添加tab
     * @param cfg - tab配置项
     */
    public __addTab(cfg:TabItem){
        if(this.model['tabs'].find(item=>item.tab === cfg.tab)){
            return;
        }
        this.model['tabs'].push(cfg);
        this.__setDefaultActive();
    }

    /**
     * 点击tab
     * @param model - 对应节点model
     */
    private __clickTab(model){
        this.__activeTab(model);
    }

    /**
     * 切换到目标tab
     * @param tab - 目标tab
     * @returns 
     */
    private __changeTo(tab){
        if(!tab || tab === this.__currentTab || tab && this.__currentTab && tab.idx && tab.idx === this.__currentTab['idx']){
            return;
        }
        if(this.__currentTab){
            this.__currentTab.active = false;
            this.__currentTab.tab.__hide();
        }
        tab.active = true;
        if(this.__onTabChange){
            UITool.invokePropMethod(this,this.__onTabChange,this.__currentTab,tab);
        }
        tab.tab.__show();
        this.__currentTab = tab;
    }

    /**
     * 获取tab
     * @param data - tab config对象 或 title 或index
     * @returns  tab
     */
    private __getTab(data:TabItem|string|number){
        switch(typeof data){
            case 'number': 
                return this.model['tabs'][data];
            case 'string':
                return this.model['tabs'].find((item)=>item.title === data);
            default:
                return data;
        }
    }

    /**
     * 激活新tab
     * @param data - tab config对象 或 title 或index
     */
    public __activeTab(data:TabItem|string|number){
        this.__changeTo(this.__getTab(data));
    }

    /**
     * 关闭页签
     * @param data - tab config对象 或 title 或index
     * @returns 
     */
    public __closeTab(data:TabItem|string|number){
        //最后一个不删除
        if(this.model['tabs'].length === 1){
            return;
        }
        const tab = this.__getTab(data);
        if(!tab){
            return;
        }
        const index = tab.idx;
        if(index >= 0 && index < this.model['tabs'].length){
            //执行tabclose事件
            if(this.__onTabClose){
                UITool.invokePropMethod(this,this.__onTabClose,tab);
            }
            //移除
            this.model['tabs'].splice(index,1);
            //被删除为当前tab，需要切换当前tab
            if(this.__currentTab && index === this.__currentTab['idx']){
                if(index === this.model['tabs'].length){ //最后一个
                    this.__changeTo(this.model['tabs'][index-1]);
                }else{ //取后面一个
                    this.__changeTo(this.model['tabs'][index]);
                }
            }
        }
    }

    /**
     * 关闭所有tab
     */
    public __closeAll(){
        this.model['tabs'] = [];
    }

    /**
     * 头部拖动开始
     * @param model - 当前节点对应model
     * @param dom - virtual dom节点
     * @param evObj - NEvent对象
     * @param e - event对象
     */
    private __dragStart(model,dom,evObj,e){
        const el = e.currentTarget;
        const pel = el.parentElement;
        //设置宽度
        let w = 0;
        for(const d of el.children){
            w += d.offsetWidth;
        }
        el.style.width = (w+1) + 'px';
        
        //不比父宽，不处理
        if(el.offsetWidth < pel.offsetWidth){
            return;
        }
        this.__dragParam.x = e.pageX;
    }

    /**
     * 拖动
     * @param model - 当前节点对应model
     * @param dom - virtual dom节点
     * @param evObj - NEvent对象
     * @param e - event对象
     * @returns 
     */
    private __drag(model,dom,evObj,e){
        if(!this.__dragParam.x){
            return;
        }
        this.__move(e);
    } 

    /**
     * 拖动停止
     * @param model - 当前节点对应model
     * @param dom - virtual dom节点
     * @param evObj - NEvent对象
     * @param e - event对象
     */
    private __dragEnd(model,dom,evObj,e){
        this.__move(e);
        delete this.__dragParam.x;
    }


    /**
     * 移动
     * @param e - 事件对象
     * @returns 
     */
    private __move(e){
        if(!this.__dragParam.x){
            return;
        }
        const dx = e.pageX - this.__dragParam.x;
        if(Math.abs(dx)<2){
            return;
        }
        this.__dragParam.dx += dx;
        
        if(this.__dragParam.dx > 0){
            this.__dragParam.dx = 0;
        }else{
            const el = e.currentTarget;
            const pel = el.parentElement;
            if(el.offsetWidth + this.__dragParam.dx < pel.offsetWidth){
                this.__dragParam.dx = pel.offsetWidth - el.offsetWidth;
            }
        }
        this.__dragParam.x = e.pageX;
        e.currentTarget.style.transform = 'translateX(' + this.__dragParam.dx + 'px)';
    }

    private __setDefaultActive(){
        const tabs = this.model['tabs'];
        let atab;
        if(tabs && tabs.length>0){
            for(let i=tabs.length-1;i>=0;i--){
                const tab = tabs[i];
                // 最后一个active tab优先
                if(tab.active){
                    if(!atab){ // 
                        atab = tab;
                    }else{  //找到active tab后，另外的active置false
                        tab.active = false;
                    }
                }
            }
            //如果没有设置active tab，默认第一个
            this.__changeTo(atab||tabs[0]);
        }
    }
}

/**
 * 配置说明
 * title    tab标题
 * closable 是否可关闭
 * active   是否处于打开状态
 */

/**
 * UTabItem 标签页个体
 * @public
 */
export class UITabItem extends Module{
    /**
     * 标题
     */
    private __title:string;

    /**
     * 是否可关闭
     */
    private __closable:boolean;

    /**
     * 是否处于激活状态
     */
    private __active:boolean;

    /**
     * 已添加标志，如果已添加，则不再添加
     */
    private __added:boolean;
    /**
     * 模板函数
     * @privateRemarks
     */
    template(props){
        this.__active = (props.active === 'true' || props.active===true);
        this.__closable = (props.closable === 'true' || props.closable===true);
        this.__title = props.title;
        this.setExcludeProps(['title','active','closable']);
        return `
            <div x-show={{show}}>
                <slot/>
            </div>
        `;
    }

    /**
     * 模型
     * @privateRemarks
     */
    data() {
        return {
            show:false
        }
    }

    /**
     * 渲染前
     */
    onBeforeFirstRender(){
        //添加到父
        const pm:UITab = <UITab>ModuleFactory.get(this.srcDom.slotModuleId);
        if(pm.constructor !== UITab){
            return;
        }
        
        //追加到ui tab插件
        pm.__addTab({
            title:this.__title,
            active:this.__active,
            closable:this.__closable,
            tab:this
        });
    }

    /**
     * 隐藏
     */
    __hide(){
        this.model['show'] = false;
    }

    /**
     * 显示
     */
    __show(){
        this.model['show'] = true;
        this.active();
    }
}

//注册模块
Nodom.registModule(UITab,'ui-tab');
Nodom.registModule(UITabItem,'ui-tab-item');