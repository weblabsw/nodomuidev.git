import { Module, Nodom } from "nodom3";

export class UICard extends Module {
    /**
     * 高度
     */
    private height: string;
    /**
     * 宽度
     */
    private width: string;
    /**
     * 阴影
     */
    private shadow: "always" | "hover" | "never" = "always";

    template(props?: object): string {
        this.width = props["width"] ? props["width"] : "";
        this.height = props["height"] ? props["height"] : "";
        let cardStyle = 'style="';
        if (this.width) {
            cardStyle += `width:${this.width};`;
        }
        if (this.height) {
            cardStyle += `height:${this.height};`;
        }
        cardStyle += '"';

        let bodyStyle = 'style="';
        if (props["body-style"]) {
            bodyStyle += props["body-style"];
        }
        bodyStyle += '"';

        if (props["shadow"]) {
            this.shadow = props["shadow"];
        }

        const clazz = `class="ui-card ${this.judgeShadow(this.shadow)}"`;

        //设置不渲染属性
        this.setExcludeProps(["width", "height", "shadow", "body-style"]);

        return `
            <div ${clazz} ${cardStyle}>
                <div class="ui-card-header" x-if={{this.slots.has("header")}}>
                    <slot name="header">{{ header }}</slot>
                </div>
                <div class="ui-card-body" ${bodyStyle}>
                    <slot />
                </div>
                <div class="ui-card-footer" x-if={{this.slots.has("footer")}}>
                    <slot name="footer">{{ footer }}</slot>
                </div>
            </div>
        `;
    }

    judgeShadow(shadow: string): string {
        if (shadow === "always") {
            return "ui-card-always";
        } else if (shadow === "hover") {
            return "ui-card-hover";
        } else {
            return "";
        }
    }
}

//注册模块
Nodom.registModule(UICard, "ui-card");
