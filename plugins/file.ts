import { Nodom, Renderer } from "nodom3";
import { BaseInput } from "./baseinput";
import { NodomUI } from "./nodomui";

/**
 * file上传插件
 * 配置项
 * field:           对应数据项名
 * value-field:        值数据项名，对应上传后返回的valueField
 * display-field:    显示数据项名，对应上传后返回的displayField
 * multiple:        是否支持多个文件，设置则表示上传多个文件
 * url-field:       上传后返回的文件url对应的字段名
 * upload-name:     上传名，与服务器接收保持一致
 * upload-url:        上传url
 * delete-url:        删除url
 * max-count:        最大上传数量，multiple设置时有效
 * file-type:        上传资源类型，如果为image，上传成功后显示缩略图，displayField为url对应项，否则显示文件名，对应数据项为displayField。
 */

/**
 * UIFile 文件
 * @public
 */
export class UIFile extends BaseInput {
    /**
     * 上传字段名
     */
    private __uploadName: string;
    /**
     * 值数据项
     */
    private __valueField: string;
    /**
     * 显示数据项
     */
    private __displayField: string;
    /**
     * url字段名
     */
    private __urlField: string;
    /**
     * 是否允许上传多个
     */
    private __multiple: boolean;
    /**
     * 上传url
     */
    private __uploadUrl: string;
    /**
     * 删除url
     */
    private __deleteUrl: string;
    /**
     * 最大文件数
     */
    private __maxCount: number;
    /**
     * 文件类型
     * 如果为image，上传成功后显示缩略图，否则显示文件名，对应数据项为__displayField
     */
    private __fileType: string;
    /**
     * 每个显示项宽度
     */
    private __width: string;
    /**
     * 每个显示项高度
     */
    private __height: string;
    /**
     * 模板函数html
     * @privateRemarks
     */
    template(props: object) {
        super.template(props);
        this.__uploadName = props['upload-name'];
        this.__valueField = props['value-field'];
        this.__displayField = props['display-field'];
        this.__multiple = props['multiple'];
        this.__uploadUrl = props['upload-url'];
        this.__deleteUrl = props['delete-url'];
        this.__maxCount = props['max-count'] ? parseInt(props['max-count']) : 0;
        this.__fileType = props['file-type'];
        this.__urlField = props['url-field'];
        this.__width = props['width'] || '100%';
        this.__height = props['height'] || '100%';
        this.setExcludeProps(['multiple', 'value-field', 'display-field', 'upload-url', 'delete-url', 'filet-ype', 'max-count', 'width', 'height', 'url-field', 'upload-name']);
        //根据不同类型显示不同结果串
        const showStr = this.__fileType === 'image' ? `<img src={{${this.__urlField}}} />` : `<div>{{${this.__displayField}}}</div>`;
        const singleStr = !this.__multiple ? 'ui-file-single' : '';
        return `
            <div class="ui-file ${singleStr}" >
                <for class='ui-file-showct' cond={{__value}} >
                    <a class="ui-file-content" target="blank" href={{${this.__urlField}}} style='width:${this.__width};height:${this.__height}'>
                        ${showStr}
                    </a>
                    <b class="ui-file-del" e-click='__delete'/>
                </for>
                <div class="ui-file-uploadct"  style='width:${this.__width};height:${this.__height}'
                    x-show={{!__value || __value.length===0 || this.__multiple && (this.__maxCount === 0 || this.__maxCount > __value.length)}}>
                    <if cond={{__uploading}}>
                        上传中...
                    </if>
                    <else>
                        <div class="ui-file-toupload">
                            <span class='ui-file-add'>+</span>
                        </div>
                        <input type="file" class="ui-file-input" ${this.__multiple ? 'multiple' : ''} e-change='__changeFile'/>
                    </else>
                </div>
            </div>
        `;
    }
    /**
     * 模型
     * @privateRemarks
     */
    data() {
        return {
            __uploading: false,
            __uploadingText: NodomUI.getText('uploadingText')
        }
    }

    /**
     * 文件修改
     * @param model - 对应模型
     * @param dom - virtual dom节点
     */
    private __changeFile(model, dom) {
        const el = <HTMLInputElement>dom.node;
        if (!el.files) {
            return;
        }
        model.__uploading = true;
        const form = new FormData();
        for (let i = 0; i < el.files.length; i++) {
            form.append(this.__uploadName, el.files[i]);
        }
        //提交请求
        Nodom.request({
            url: this.__uploadUrl,
            method: 'POST',
            params: form,
            header: {
                'Content-Type': 'multipart/form-data'
            },
            type: 'json'
        }).then((r) => {
            //上传显示
            model.__uploading = false;
            if(!model.__value){
                model.__value = [r];
            }else{
                model.__value.push(r);
            }
        });
    }

    /**
     * 删除上传文件
     * @param model - 对应模型
     */
    private __delete(model) {
        if (!this.__deleteUrl || !this.__valueField) {
            return;
        }
        const param = {};
        param[this.__valueField] = model[this.__valueField];
        Nodom.request({
            url: this.__deleteUrl,
            method: 'GET',
            params: param,
            header: {
                'Content-Type': 'multipart/form-data'
            },
            type: 'json'
        });
        //移除
        const ind = this.model['__value'].findIndex(item => item[this.__valueField] === model[this.__valueField]);
        if (ind !== -1) {
            this.model['__value'].splice(ind, 1);
            Renderer.add(this);
        }
    }
}
Nodom.registModule(UIFile, 'ui-file');
