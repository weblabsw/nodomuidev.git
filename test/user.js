import { Module, Nodom } from "/dist/nodomui.min.js";

export class MUser extends Module {
    modules = [];
    template() {
        return `
    <ui-form label-width="100px" x-model='formData'>
    <div class="ui-row">
        <ui-form-item
            label="用户名"
            field="userName"
            require="true"
            validator="maxLength:64">
            <ui-input type="text" field="userName" />
        </ui-form-item>
        <ui-form-item
            label="真实姓名"
            field="realName"
            require="true"
            validator="maxLength:64">
            <ui-input type="text" field="realName" />
        </ui-form-item>
    </div>
    <!--
    <div class="ui-row">
        <ui-form-item
            label="联系电话"
            field="tel"
            validator="maxLength:15">
            <ui-input type="text" field="tel" />
        </ui-form-item>
        <ui-form-item
            label="手机号"
            field="mobile"
            validator="maxLength:11">
            <ui-input type="text" field="mobile" />
        </ui-form-item>
    </div>-->
    <div class="ui-row">
        <ui-form-item
            label="电子邮箱"
            field="email"
            validator="maxLength:50">
            <ui-input type="text" field="email" />
        </ui-form-item>
        
        <ui-form-item label="所属组" field="amGroups">
            <ui-select
                allow-empty
                field="amGroups"
                $data={{m_1}}
                display-field="groupName"
                value-field="groupId"
                multiple>
                {{groupName}}
            </ui-select>
        </ui-form-item>
    </div>
    <!--<div class="ui-row">
        <ui-form-item label="所属机构" field="amCompany">
            <ui-select
                allow-empty
                field="amCompany.companyId"
                $data={{m_2}}
                $d={{amCompany.companyId}}
                display-field="companyName"
                value-field="companyId">
                {{companyName}}
            </ui-select>
        </ui-form-item>
        <ui-form-item
            label="备注"
            field="remarks"
            validator="maxLength:256">
            <ui-input type="text" field="remarks" />
        </ui-form-item>
    </div>-->
</ui-form>

                `;
    }

    data() {
        return { formData: {} };
    }

    // onMount() {
    //     Nodom.request({
    //         url: "/amgroup/query",
    //         type: "json",
    //         params: { fields: "groupId,groupName" }
    //     }).then((r) => {
    //         this.model["m_1"] = r.rows;
    //     });

    //     Nodom.request({
    //         url: "/amcompany/query",
    //         type: "json",
    //         params: { fields: "companyId,companyName" }
    //     }).then((r) => {
    //         this.model["m_2"] = r.rows;
    //     });
    // }
}
